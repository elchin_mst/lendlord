#!/bin/bash
echo -n "Скрипт пересоздаст базу. Продолжить? (y/n) "

read item

if [ $item != "y" ]; then
    exit 0
else
    php bin/console doctrine:schema:drop --force
    php bin/console doctrine:schema:create
    php bin/console doctrine:fixtures:load --no-interaction
    php bin/console cache:clear
fi

exit 1