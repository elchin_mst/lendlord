<?php

namespace FiasBundle\Controller;

use AppBundle\Controller\BaseRestApiController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * User controller.
 *
 * @Route("/api/fias")
 */
class AddressObjectsController extends BaseRestApiController
{
    /**
     * @Rest\Get("/check_aoguid/{id}", name="check_aoguid")
     */
    public function checkExistAOGUID($id)
    {
        $adObj = $this->getDoctrine()->getRepository(
            'FiasBundle:AddressObjects'
        )->getActual($id);

        return $this->createApiViewResponse($adObj, 'View', 200);
    }

    /**
     *
     * @Rest\Get("/get_parents/{id}", name="get_parents")
     */
    public function getParentsAoguid($id)
    {

        $adObj = $this->get('fias.query')->findAllFamilyAoguid($id);
        $resp = new JsonResponse($adObj);

        return $resp;
    }

    /**
     *
     * @Rest\Post("/check_relations", name="check_relations")
     */
    public function checkRelations(Request $request)
    {
        $response = new JsonResponse();

        /*
         {
    "region" : "f10763dc-63e3-48db-83e1-9c566fe3092b" ,
    "city":    "c1cfe4b9-f7c2-423c-abfa-6ed1c05a15c5",
    "street" : "d70feddd-1672-49f2-ae84-6e1096c9314b"
        }
        */

        try {
            $jsonData = $request->getContent();
            $jsonData = json_decode($jsonData, true);

            $arrayIds = $this->get('fias.query')->findAllFamilyAoguid($jsonData["street"]);

            foreach ($jsonData as $k => $v) {
                if (!in_array($v, $arrayIds)) {
                    $exist = false;
                }
            }

            if (!isset($exist)) {
                $exist = true;
            }

            $response->setData(array('status' => $exist));
        } catch (\Exception $e) {

            $response->setData(array('error' => $e->getMessage()));
        }

        return $response;
    }
}
