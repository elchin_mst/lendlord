<?php

namespace FiasBundle\Service;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;

//TODO: in future make autoload updating - get from site delta, load it and call import
//todo: maybe in future make parser from xml and converter dbf to xml-file

class FiasParser
{
    private $em;

    private $container;

    private $path;

    private $log;

    const MODE_READ = 0;

    function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->em = $em;
        $this->container = $container;
        $this->path = $this->container->getParameter('path_to_files');
        $this->log = $this->container->get('logger');
    }

    public function run()
    {
        //echo $this->path;
        try {
            $files = array();

            $files = scandir($this->path);
            $files = array_diff($files, array('..', '.'));

            if (!empty($files)) {
                foreach ($files as $i => $file) {

                    if (strtoupper(substr($file, -4)) == '.DBF') {
                        $files[$i] = $this->path . $file;
                    }
                }
            }

            if (empty($files)) {
                throw new \Exception('Файлы не найдены ' . $this->path);
            }

            foreach ($files as $file) {

                if (is_readable($file)) {
                    $db = dbase_open($file, self::MODE_READ);
                    if ($db) {
                        $record_numbers = dbase_numrecords($db);

                        $this->writeText('Найдено на обработку ' . $record_numbers);

                        $arrayInfo = $this->getClassNameFromFileName($file);

                        for ($i = 1; $i <= $record_numbers; $i++) {

                            $row = dbase_get_record_with_names($db, $i);

                            $entity = $this->findOrCreateEntity($arrayInfo['class'], $row[$arrayInfo['key']]);

                            foreach ($row as $k => $v) {
                                $lowK = strtolower($k);
                                if (property_exists($entity, $lowK)) {
                                    $method = 'Set' . ucfirst($lowK);
                                    $entity->$method($this->convertEncoding(trim($v)));
                                }
                            }

                            $this->em->persist($entity);
                            $this->em->flush();

                            unset($entity);
                        }

                        dbase_close($db);
                        unlink($file);
                    } else {

                        throw new \Exception('DBF  open failed: ' . $file);
                    }
                } else {
                    throw new \Exception('Файл не доступен на чтение ' . $file);
                }
            }
        } catch (\Exception $e) {
            if (PHP_SAPI == 'cli') {
                echo 'Ошибка парсера! ' . $e->getCode() . ' ' . $e->getMessage();
            } else {
                $this->log->error('Ошибка парсера! ' . $e->getCode() . ' - ' . $e->getMessage());
                throw new \Exception('Выполнение прервано');
            }
        }
    }

    private function writeText($text)
    {
        if (PHP_SAPI == 'cli') {
            echo $text;
        } else {

            $this->log->error('Парсер ФИАС: ' . $text);
        }
    }

    private function getClassNameFromFileName($fileName)
    {
        $name = str_replace(
            array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.DBF'),
            '',
            strtoupper(basename($fileName))
        );

        if (!isset(FiasMapping::$fias_mapping[$name])) {
            $tableName = null;
            throw new \Exception('Не предусмотрена обработка ' . $name);
        }
        $arr = FiasMapping::$fias_mapping[$name];

        return $arr;
    }

    private function convertEncoding($value)
    {
        return iconv('cp866', 'utf8', $value);
    }

    private function findOrCreateEntity($className, $uid)
    {
        $entity = $this->em->getRepository($className)->find($uid);
        if (is_null($entity)) {
            $entity = new $className();
        }

        return $entity;
    }
}