<?php

namespace FiasBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;

class FiasQuery
{
    private $em;

    private $container;

    private $log;

    function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->em = $em;
        $this->container = $container;
        $this->log = $this->container->get('logger');
    }

    function findAllFamilyAoguid($aoguid)
    {

        $childObj = $this->em->getRepository('FiasBundle:AddressObjects')
            ->getActual($aoguid);

        if ($childObj) {
            $arObjs = $this->em->getRepository('FiasBundle:AddressObjects')
                ->getParent($childObj->getAoid());

            return array_column($arObjs, 'AOGUID');        //todo: maybe add type of record

        }

        return array();
    }
}