<?php

namespace FiasBundle\Service;

class FiasMapping
{
    public static $fias_mapping = array(
        'ADDROBJ' => array('class' => '\FiasBundle\Entity\AddressObjects', 'key' => 'AOID'),
        'ADDROB' => array('class' => '\FiasBundle\Entity\AddressObjects', 'key' => 'AOID'),
        'HOUSE' => array('class' => '\FiasBundle\Entity\House', 'key' => 'HOUSEID'),
    );
}