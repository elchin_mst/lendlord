<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Actualstatus
 *
 * @ORM\Table(name="ActualStatus")
 * @ORM\Entity
 */
class Actualstatus
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="ACTSTATID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $actstatid;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Actualstatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get actstatid
     *
     * @return integer
     */
    public function getActstatid()
    {
        return $this->actstatid;
    }
}
