<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Addressobjecttype
 *
 * @ORM\Table(name="AddressObjectType")
 * @ORM\Entity
 */
class Addressobjecttype
{
    /**
     * @var integer
     *
     * @ORM\Column(name="LEVEL", type="integer", nullable=true)
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="SCNAME", type="string", length=10, nullable=true)
     */
    private $scname;

    /**
     * @var string
     *
     * @ORM\Column(name="SOCRNAME", type="string", length=50, nullable=true)
     */
    private $socrname;

    /**
     * @var string
     *
     * @ORM\Column(name="KOD_T_ST", type="string", length=4)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $kodTSt;

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return Addressobjecttype
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set scname
     *
     * @param string $scname
     *
     * @return Addressobjecttype
     */
    public function setScname($scname)
    {
        $this->scname = $scname;

        return $this;
    }

    /**
     * Get scname
     *
     * @return string
     */
    public function getScname()
    {
        return $this->scname;
    }

    /**
     * Set socrname
     *
     * @param string $socrname
     *
     * @return Addressobjecttype
     */
    public function setSocrname($socrname)
    {
        $this->socrname = $socrname;

        return $this;
    }

    /**
     * Get socrname
     *
     * @return string
     */
    public function getSocrname()
    {
        return $this->socrname;
    }

    /**
     * Get kodTSt
     *
     * @return string
     */
    public function getKodTSt()
    {
        return $this->kodTSt;
    }
}
