<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Deletednormativedocument
 *
 * @ORM\Table(name="DeletedNormativeDocument")
 * @ORM\Entity
 */
class Deletednormativedocument
{
    /**
     * @var string
     *
     * @ORM\Column(name="NORMDOCID", type="string", length=36, nullable=true)
     */
    private $normdocid;

    /**
     * @var string
     *
     * @ORM\Column(name="DOCNAME", type="string", length=21783, nullable=true)
     */
    private $docname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DOCDATE", type="date", nullable=true)
     */
    private $docdate;

    /**
     * @var string
     *
     * @ORM\Column(name="DOCNUM", type="string", length=20, nullable=true)
     */
    private $docnum;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCTYPE", type="integer", nullable=true)
     */
    private $doctype;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCIMGID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $docimgid;

    /**
     * Set normdocid
     *
     * @param string $normdocid
     *
     * @return Deletednormativedocument
     */
    public function setNormdocid($normdocid)
    {
        $this->normdocid = $normdocid;

        return $this;
    }

    /**
     * Get normdocid
     *
     * @return string
     */
    public function getNormdocid()
    {
        return $this->normdocid;
    }

    /**
     * Set docname
     *
     * @param string $docname
     *
     * @return Deletednormativedocument
     */
    public function setDocname($docname)
    {
        $this->docname = $docname;

        return $this;
    }

    /**
     * Get docname
     *
     * @return string
     */
    public function getDocname()
    {
        return $this->docname;
    }

    /**
     * Set docdate
     *
     * @param \DateTime $docdate
     *
     * @return Deletednormativedocument
     */
    public function setDocdate($docdate)
    {
        $this->docdate = $docdate;

        return $this;
    }

    /**
     * Get docdate
     *
     * @return \DateTime
     */
    public function getDocdate()
    {
        return $this->docdate;
    }

    /**
     * Set docnum
     *
     * @param string $docnum
     *
     * @return Deletednormativedocument
     */
    public function setDocnum($docnum)
    {
        $this->docnum = $docnum;

        return $this;
    }

    /**
     * Get docnum
     *
     * @return string
     */
    public function getDocnum()
    {
        return $this->docnum;
    }

    /**
     * Set doctype
     *
     * @param integer $doctype
     *
     * @return Deletednormativedocument
     */
    public function setDoctype($doctype)
    {
        $this->doctype = $doctype;

        return $this;
    }

    /**
     * Get doctype
     *
     * @return integer
     */
    public function getDoctype()
    {
        return $this->doctype;
    }

    /**
     * Get docimgid
     *
     * @return integer
     */
    public function getDocimgid()
    {
        return $this->docimgid;
    }
}
