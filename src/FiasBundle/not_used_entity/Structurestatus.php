<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Structurestatus
 *
 * @ORM\Table(name="StructureStatus")
 * @ORM\Entity
 */
class Structurestatus
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=20, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="SHORTNAME", type="string", length=20, nullable=true)
     */
    private $shortname;

    /**
     * @var integer
     *
     * @ORM\Column(name="STRSTATID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $strstatid;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Structurestatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortname
     *
     * @param string $shortname
     *
     * @return Structurestatus
     */
    public function setShortname($shortname)
    {
        $this->shortname = $shortname;

        return $this;
    }

    /**
     * Get shortname
     *
     * @return string
     */
    public function getShortname()
    {
        return $this->shortname;
    }

    /**
     * Get strstatid
     *
     * @return integer
     */
    public function getStrstatid()
    {
        return $this->strstatid;
    }
}
