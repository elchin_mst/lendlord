<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Currentstatus
 *
 * @ORM\Table(name="CurrentStatus")
 * @ORM\Entity
 */
class Currentstatus
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="CURENTSTID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $curentstid;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Currentstatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get curentstid
     *
     * @return integer
     */
    public function getCurentstid()
    {
        return $this->curentstid;
    }
}
