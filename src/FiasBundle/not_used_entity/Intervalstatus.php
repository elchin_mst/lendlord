<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Intervalstatus
 *
 * @ORM\Table(name="IntervalStatus")
 * @ORM\Entity
 */
class Intervalstatus
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=60, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="INTVSTATID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $intvstatid;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Intervalstatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get intvstatid
     *
     * @return integer
     */
    public function getIntvstatid()
    {
        return $this->intvstatid;
    }
}
