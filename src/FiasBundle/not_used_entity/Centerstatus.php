<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Centerstatus
 *
 * @ORM\Table(name="CenterStatus")
 * @ORM\Entity
 */
class Centerstatus
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="CENTERSTID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $centerstid;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Centerstatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get centerstid
     *
     * @return integer
     */
    public function getCenterstid()
    {
        return $this->centerstid;
    }
}
