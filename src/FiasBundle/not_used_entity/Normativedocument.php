<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Normativedocument
 *
 * @ORM\Table(name="NormativeDocument")
 * @ORM\Entity
 */
class Normativedocument
{
    /**
     * @var string
     *
     * @ORM\Column(name="DOCNAME", type="string", length=21783, nullable=true)
     */
    private $docname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DOCDATE", type="date", nullable=true)
     */
    private $docdate;

    /**
     * @var string
     *
     * @ORM\Column(name="DOCNUM", type="string", length=20, nullable=true)
     */
    private $docnum;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCTYPE", type="integer", nullable=true)
     */
    private $doctype;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCIMGID", type="integer", nullable=true)
     */
    private $docimgid;

    /**
     * @var string
     *
     * @ORM\Column(name="NORMDOCID", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $normdocid;

    /**
     * Set docname
     *
     * @param string $docname
     *
     * @return Normativedocument
     */
    public function setDocname($docname)
    {
        $this->docname = $docname;

        return $this;
    }

    /**
     * Get docname
     *
     * @return string
     */
    public function getDocname()
    {
        return $this->docname;
    }

    /**
     * Set docdate
     *
     * @param \DateTime $docdate
     *
     * @return Normativedocument
     */
    public function setDocdate($docdate)
    {
        $this->docdate = $docdate;

        return $this;
    }

    /**
     * Get docdate
     *
     * @return \DateTime
     */
    public function getDocdate()
    {
        return $this->docdate;
    }

    /**
     * Set docnum
     *
     * @param string $docnum
     *
     * @return Normativedocument
     */
    public function setDocnum($docnum)
    {
        $this->docnum = $docnum;

        return $this;
    }

    /**
     * Get docnum
     *
     * @return string
     */
    public function getDocnum()
    {
        return $this->docnum;
    }

    /**
     * Set doctype
     *
     * @param integer $doctype
     *
     * @return Normativedocument
     */
    public function setDoctype($doctype)
    {
        $this->doctype = $doctype;

        return $this;
    }

    /**
     * Get doctype
     *
     * @return integer
     */
    public function getDoctype()
    {
        return $this->doctype;
    }

    /**
     * Set docimgid
     *
     * @param integer $docimgid
     *
     * @return Normativedocument
     */
    public function setDocimgid($docimgid)
    {
        $this->docimgid = $docimgid;

        return $this;
    }

    /**
     * Get docimgid
     *
     * @return integer
     */
    public function getDocimgid()
    {
        return $this->docimgid;
    }

    /**
     * Get normdocid
     *
     * @return string
     */
    public function getNormdocid()
    {
        return $this->normdocid;
    }
}
