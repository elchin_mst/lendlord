<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Operationstatus
 *
 * @ORM\Table(name="OperationStatus")
 * @ORM\Entity
 */
class Operationstatus
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="OPERSTATID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $operstatid;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Operationstatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get operstatid
     *
     * @return integer
     */
    public function getOperstatid()
    {
        return $this->operstatid;
    }
}
