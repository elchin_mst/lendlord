<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Estatestatus
 *
 * @ORM\Table(name="EstateStatus")
 * @ORM\Entity
 */
class Estatestatus
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=20, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="SHORTNAME", type="string", length=20, nullable=true)
     */
    private $shortname;

    /**
     * @var integer
     *
     * @ORM\Column(name="ESTSTATID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $eststatid;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Estatestatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortname
     *
     * @param string $shortname
     *
     * @return Estatestatus
     */
    public function setShortname($shortname)
    {
        $this->shortname = $shortname;

        return $this;
    }

    /**
     * Get shortname
     *
     * @return string
     */
    public function getShortname()
    {
        return $this->shortname;
    }

    /**
     * Get eststatid
     *
     * @return integer
     */
    public function getEststatid()
    {
        return $this->eststatid;
    }
}
