<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * House
 *
 * @ORM\Table(name="fias_house")
 * @ORM\Entity(repositoryClass="FiasBundle\Repository\HouseRepository")
 */
class House
{
    //CADNUM  deleted

    /**
     * @var string
     *
     * @ORM\Column(name="POSTALCODE", type="string", length=6, nullable=true)
     */
    private $postalcode;

    /**
     * @var string
     *
     * @ORM\Column(name="IFNSFL", type="string", length=4, nullable=true)
     */
    private $ifnsfl;

    /**
     * @var string
     *
     * @ORM\Column(name="TERRIFNSFL", type="string", length=4, nullable=true)
     */
    private $terrifnsfl;

    /**
     * @var string
     *
     * @ORM\Column(name="IFNSUL", type="string", length=4, nullable=true)
     */
    private $ifnsul;

    /**
     * @var string
     *
     * @ORM\Column(name="TERRIFNSUL", type="string", length=4, nullable=true)
     */
    private $terrifnsul;

    /**
     * @var string
     *
     * @ORM\Column(name="OKATO", type="string", length=11, nullable=true)
     */
    private $okato;

    /**
     * @var string
     *
     * @ORM\Column(name="OKTMO", type="string", length=11, nullable=true)
     */
    private $oktmo;

    /**
     * @var string
     *
     * @ORM\Column(name="UPDATEDATE", type="string", nullable=true)
     */
    private $updatedate;

    /**
     * @var string
     *
     * @ORM\Column(name="HOUSENUM", type="string", length=20, nullable=true)
     */
    private $housenum;

    /**
     * @var integer
     *
     * @ORM\Column(name="ESTSTATUS", type="integer", nullable=true)
     */
    private $eststatus;

    /**
     * @var string
     *
     * @ORM\Column(name="BUILDNUM", type="string", length=10, nullable=true)
     */
    private $buildnum;

    /**
     * @var string
     *
     * @ORM\Column(name="STRUCNUM", type="string", length=10, nullable=true)
     */
    private $strucnum;

    /**
     * @var integer
     *
     * @ORM\Column(name="STRSTATUS", type="integer", nullable=true)
     */
    private $strstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="HOUSEGUID", type="string", length=36, nullable=true)
     */
    private $houseguid;

    /**
     * @var string
     *
     * @ORM\Column(name="AOGUID", type="string", length=36, nullable=true)
     */
    private $aoguid;

    /**
     * @var string
     *
     * @ORM\Column(name="STARTDATE", type="string", nullable=true)
     */
    private $startdate;

    /**
     * @var string
     *
     * @ORM\Column(name="ENDDATE", type="string", nullable=true)
     */
    private $enddate;

    /**
     * @var integer
     *
     * @ORM\Column(name="STATSTATUS", type="integer", nullable=true)
     */
    private $statstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="NORMDOC", type="string", length=36, nullable=true)
     */
    private $normdoc;

    /**
     * @var integer
     *
     * @ORM\Column(name="COUNTER", type="integer", nullable=true)
     */
    private $counter;

    /**
     * @var string
     *
     * @ORM\Column(name="HOUSEID", type="string", length=36)
     * @ORM\Id
     */
    private $houseid;

    /**
     * @var integer
     *
     * @ORM\Column(name="DIVTYPE", type="integer", nullable=true)
     */
    private $divtype;

    /**
     * @var string
     *
     * @ORM\Column(name="CADNUM", type="string", length=100, nullable=true)
     */
    private $cadnum;

    //Признак владения

    static $eststatus_mapping = array(
        "1" => "владение",
        "2" => "дом",
        "3" => "домовладение",
        "4" => "участок",
    );

    // Признак строения

    static $strstatus_mapping = array(
        "1" => "строение",
        "2" => "сооружение",
        "3" => "литер",
    );

    //  Тип деления:
    static $divtype_mapping = array(
        0 => "не определено",
        1 => "муниципальное",
        2 => "административное",
    );

    /**
     * Set postalcode
     *
     * @param string $postalcode
     *
     * @return House
     */
    public function setPostalcode($postalcode)
    {
        $this->postalcode = $postalcode;

        return $this;
    }

    /**
     * Get postalcode
     *
     * @return string
     */
    public function getPostalcode()
    {
        return $this->postalcode;
    }

    /**
     * Set ifnsfl
     *
     * @param string $ifnsfl
     *
     * @return House
     */
    public function setIfnsfl($ifnsfl)
    {
        $this->ifnsfl = $ifnsfl;

        return $this;
    }

    /**
     * Get ifnsfl
     *
     * @return string
     */
    public function getIfnsfl()
    {
        return $this->ifnsfl;
    }

    /**
     * Set terrifnsfl
     *
     * @param string $terrifnsfl
     *
     * @return House
     */
    public function setTerrifnsfl($terrifnsfl)
    {
        $this->terrifnsfl = $terrifnsfl;

        return $this;
    }

    /**
     * Get terrifnsfl
     *
     * @return string
     */
    public function getTerrifnsfl()
    {
        return $this->terrifnsfl;
    }

    /**
     * Set ifnsul
     *
     * @param string $ifnsul
     *
     * @return House
     */
    public function setIfnsul($ifnsul)
    {
        $this->ifnsul = $ifnsul;

        return $this;
    }

    /**
     * Get ifnsul
     *
     * @return string
     */
    public function getIfnsul()
    {
        return $this->ifnsul;
    }

    /**
     * Set terrifnsul
     *
     * @param string $terrifnsul
     *
     * @return House
     */
    public function setTerrifnsul($terrifnsul)
    {
        $this->terrifnsul = $terrifnsul;

        return $this;
    }

    /**
     * Get terrifnsul
     *
     * @return string
     */
    public function getTerrifnsul()
    {
        return $this->terrifnsul;
    }

    /**
     * Set okato
     *
     * @param string $okato
     *
     * @return House
     */
    public function setOkato($okato)
    {
        $this->okato = $okato;

        return $this;
    }

    /**
     * Get okato
     *
     * @return string
     */
    public function getOkato()
    {
        return $this->okato;
    }

    /**
     * Set oktmo
     *
     * @param string $oktmo
     *
     * @return House
     */
    public function setOktmo($oktmo)
    {
        $this->oktmo = $oktmo;

        return $this;
    }

    /**
     * Get oktmo
     *
     * @return string
     */
    public function getOktmo()
    {
        return $this->oktmo;
    }

    /**
     * Set updatedate
     *
     * @param string $updatedate
     *
     * @return House
     */
    public function setUpdatedate($updatedate)
    {
        $this->updatedate = $updatedate;

        return $this;
    }

    /**
     * Get updatedate
     *
     * @return string
     */
    public function getUpdatedate()
    {
        return $this->updatedate;
    }

    /**
     * Set housenum
     *
     * @param string $housenum
     *
     * @return House
     */
    public function setHousenum($housenum)
    {
        $this->housenum = $housenum;

        return $this;
    }

    /**
     * Get housenum
     *
     * @return string
     */
    public function getHousenum()
    {
        return $this->housenum;
    }

    /**
     * Set eststatus
     *
     * @param integer $eststatus
     *
     * @return House
     */
    public function setEststatus($eststatus)
    {
        $this->eststatus = $eststatus;

        return $this;
    }

    /**
     * Get eststatus
     *
     * @return integer
     */
    public function getEststatus()
    {
        return $this->eststatus;
    }

    /**
     * Set buildnum
     *
     * @param string $buildnum
     *
     * @return House
     */
    public function setBuildnum($buildnum)
    {
        $this->buildnum = $buildnum;

        return $this;
    }

    /**
     * Get buildnum
     *
     * @return string
     */
    public function getBuildnum()
    {
        return $this->buildnum;
    }

    /**
     * Set strucnum
     *
     * @param string $strucnum
     *
     * @return House
     */
    public function setStrucnum($strucnum)
    {
        $this->strucnum = $strucnum;

        return $this;
    }

    /**
     * Get strucnum
     *
     * @return string
     */
    public function getStrucnum()
    {
        return $this->strucnum;
    }

    /**
     * Set strstatus
     *
     * @param integer $strstatus
     *
     * @return House
     */
    public function setStrstatus($strstatus)
    {
        $this->strstatus = $strstatus;

        return $this;
    }

    /**
     * Get strstatus
     *
     * @return integer
     */
    public function getStrstatus()
    {
        return $this->strstatus;
    }

    /**
     * Set houseguid
     *
     * @param string $houseguid
     *
     * @return House
     */
    public function setHouseguid($houseguid)
    {
        $this->houseguid = $houseguid;

        return $this;
    }

    /**
     * Get houseguid
     *
     * @return string
     */
    public function getHouseguid()
    {
        return $this->houseguid;
    }

    /**
     * Set aoguid
     *
     * @param string $aoguid
     *
     * @return House
     */
    public function setAoguid($aoguid)
    {
        $this->aoguid = $aoguid;

        return $this;
    }

    /**
     * Get aoguid
     *
     * @return string
     */
    public function getAoguid()
    {
        return $this->aoguid;
    }

    /**
     * Set startdate
     *
     * @param string $startdate
     *
     * @return House
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    /**
     * Get startdate
     *
     * @return string
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Set enddate
     *
     * @param string $enddate
     *
     * @return House
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Get enddate
     *
     * @return string
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set statstatus
     *
     * @param integer $statstatus
     *
     * @return House
     */
    public function setStatstatus($statstatus)
    {
        $this->statstatus = $statstatus;

        return $this;
    }

    /**
     * Get statstatus
     *
     * @return integer
     */
    public function getStatstatus()
    {
        return $this->statstatus;
    }

    /**
     * Set normdoc
     *
     * @param string $normdoc
     *
     * @return House
     */
    public function setNormdoc($normdoc)
    {
        $this->normdoc = $normdoc;

        return $this;
    }

    /**
     * Get normdoc
     *
     * @return string
     */
    public function getNormdoc()
    {
        return $this->normdoc;
    }

    /**
     * Set counter
     *
     * @param integer $counter
     *
     * @return House
     */
    public function setCounter($counter)
    {
        $this->counter = $counter;

        return $this;
    }

    /**
     * Get counter
     *
     * @return integer
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * Get houseid
     *
     * @return string
     */
    public function getHouseid()
    {
        return $this->houseid;
    }

    public function setHouseid($houseid)
    {
        $this->houseid = $houseid;

        return $this;
    }

    /**
     * Set divtype
     *
     * @param integer $divtype
     *
     * @return House
     */
    public function setDivtype($divtype)
    {
        $this->divtype = $divtype;

        return $this;
    }

    /**
     * Get divtype
     *
     * @return integer
     */
    public function getDivtype()
    {
        return $this->divtype;
    }

    /**
     * Set cadnum
     *
     * @param string $cadnum
     *
     * @return House
     */
    public function setCadnum($cadnum)
    {
        $this->cadnum = $cadnum;

        return $this;
    }

    /**
     * Get cadnum
     *
     * @return string
     */
    public function getCadnum()
    {
        return $this->cadnum;
    }
}
