<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Normativedocumenttype
 *
 * @ORM\Table(name="NormativeDocumentType")
 * @ORM\Entity
 */
class Normativedocumenttype
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=250, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="NDTYPEID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ndtypeid;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Normativedocumenttype
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get ndtypeid
     *
     * @return integer
     */
    public function getNdtypeid()
    {
        return $this->ndtypeid;
    }
}
