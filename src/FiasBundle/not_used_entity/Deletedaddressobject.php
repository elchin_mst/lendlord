<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Deletedaddressobject
 *
 * @ORM\Table(name="DeletedAddressObject")
 * @ORM\Entity
 */
class Deletedaddressobject
{
    /**
     * @var string
     *
     * @ORM\Column(name="AOGUID", type="string", length=36, nullable=true)
     */
    private $aoguid;

    /**
     * @var string
     *
     * @ORM\Column(name="FORMALNAME", type="string", length=120, nullable=true)
     */
    private $formalname;

    /**
     * @var string
     *
     * @ORM\Column(name="REGIONCODE", type="string", length=2, nullable=true)
     */
    private $regioncode;

    /**
     * @var string
     *
     * @ORM\Column(name="AUTOCODE", type="string", length=1, nullable=true)
     */
    private $autocode;

    /**
     * @var string
     *
     * @ORM\Column(name="AREACODE", type="string", length=3, nullable=true)
     */
    private $areacode;

    /**
     * @var string
     *
     * @ORM\Column(name="CITYCODE", type="string", length=3, nullable=true)
     */
    private $citycode;

    /**
     * @var string
     *
     * @ORM\Column(name="CTARCODE", type="string", length=3, nullable=true)
     */
    private $ctarcode;

    /**
     * @var string
     *
     * @ORM\Column(name="PLACECODE", type="string", length=3, nullable=true)
     */
    private $placecode;

    /**
     * @var string
     *
     * @ORM\Column(name="STREETCODE", type="string", length=4, nullable=true)
     */
    private $streetcode;

    /**
     * @var string
     *
     * @ORM\Column(name="EXTRCODE", type="string", length=4, nullable=true)
     */
    private $extrcode;

    /**
     * @var string
     *
     * @ORM\Column(name="SEXTCODE", type="string", length=3, nullable=true)
     */
    private $sextcode;

    /**
     * @var string
     *
     * @ORM\Column(name="OFFNAME", type="string", length=120, nullable=true)
     */
    private $offname;

    /**
     * @var string
     *
     * @ORM\Column(name="POSTALCODE", type="string", length=6, nullable=true)
     */
    private $postalcode;

    /**
     * @var string
     *
     * @ORM\Column(name="IFNSFL", type="string", length=4, nullable=true)
     */
    private $ifnsfl;

    /**
     * @var string
     *
     * @ORM\Column(name="TERRIFNSFL", type="string", length=4, nullable=true)
     */
    private $terrifnsfl;

    /**
     * @var string
     *
     * @ORM\Column(name="IFNSUL", type="string", length=4, nullable=true)
     */
    private $ifnsul;

    /**
     * @var string
     *
     * @ORM\Column(name="TERRIFNSUL", type="string", length=4, nullable=true)
     */
    private $terrifnsul;

    /**
     * @var string
     *
     * @ORM\Column(name="OKATO", type="string", length=11, nullable=true)
     */
    private $okato;

    /**
     * @var string
     *
     * @ORM\Column(name="OKTMO", type="string", length=11, nullable=true)
     */
    private $oktmo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="UPDATEDATE", type="date", nullable=true)
     */
    private $updatedate;

    /**
     * @var string
     *
     * @ORM\Column(name="SHORTNAME", type="string", length=10, nullable=true)
     */
    private $shortname;

    /**
     * @var integer
     *
     * @ORM\Column(name="AOLEVEL", type="integer", nullable=true)
     */
    private $aolevel;

    /**
     * @var string
     *
     * @ORM\Column(name="PARENTGUID", type="string", length=36, nullable=true)
     */
    private $parentguid;

    /**
     * @var string
     *
     * @ORM\Column(name="PREVID", type="string", length=36, nullable=true)
     */
    private $previd;

    /**
     * @var string
     *
     * @ORM\Column(name="NEXTID", type="string", length=36, nullable=true)
     */
    private $nextid;

    /**
     * @var string
     *
     * @ORM\Column(name="CODE", type="string", length=17, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="PLAINCODE", type="string", length=15, nullable=true)
     */
    private $plaincode;

    /**
     * @var integer
     *
     * @ORM\Column(name="ACTSTATUS", type="integer", nullable=true)
     */
    private $actstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="CENTSTATUS", type="integer", nullable=true)
     */
    private $centstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="OPERSTATUS", type="integer", nullable=true)
     */
    private $operstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="CURRSTATUS", type="integer", nullable=true)
     */
    private $currstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="STARTDATE", type="date", nullable=true)
     */
    private $startdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ENDDATE", type="date", nullable=true)
     */
    private $enddate;

    /**
     * @var string
     *
     * @ORM\Column(name="NORMDOC", type="string", length=36, nullable=true)
     */
    private $normdoc;

    /**
     * @var string
     *
     * @ORM\Column(name="LIVESTATUS", type="string", nullable=true)
     */
    private $livestatus;

    /**
     * @var string
     *
     * @ORM\Column(name="AOID", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $aoid;

    /**
     * Set aoguid
     *
     * @param string $aoguid
     *
     * @return Deletedaddressobject
     */
    public function setAoguid($aoguid)
    {
        $this->aoguid = $aoguid;

        return $this;
    }

    /**
     * Get aoguid
     *
     * @return string
     */
    public function getAoguid()
    {
        return $this->aoguid;
    }

    /**
     * Set formalname
     *
     * @param string $formalname
     *
     * @return Deletedaddressobject
     */
    public function setFormalname($formalname)
    {
        $this->formalname = $formalname;

        return $this;
    }

    /**
     * Get formalname
     *
     * @return string
     */
    public function getFormalname()
    {
        return $this->formalname;
    }

    /**
     * Set regioncode
     *
     * @param string $regioncode
     *
     * @return Deletedaddressobject
     */
    public function setRegioncode($regioncode)
    {
        $this->regioncode = $regioncode;

        return $this;
    }

    /**
     * Get regioncode
     *
     * @return string
     */
    public function getRegioncode()
    {
        return $this->regioncode;
    }

    /**
     * Set autocode
     *
     * @param string $autocode
     *
     * @return Deletedaddressobject
     */
    public function setAutocode($autocode)
    {
        $this->autocode = $autocode;

        return $this;
    }

    /**
     * Get autocode
     *
     * @return string
     */
    public function getAutocode()
    {
        return $this->autocode;
    }

    /**
     * Set areacode
     *
     * @param string $areacode
     *
     * @return Deletedaddressobject
     */
    public function setAreacode($areacode)
    {
        $this->areacode = $areacode;

        return $this;
    }

    /**
     * Get areacode
     *
     * @return string
     */
    public function getAreacode()
    {
        return $this->areacode;
    }

    /**
     * Set citycode
     *
     * @param string $citycode
     *
     * @return Deletedaddressobject
     */
    public function setCitycode($citycode)
    {
        $this->citycode = $citycode;

        return $this;
    }

    /**
     * Get citycode
     *
     * @return string
     */
    public function getCitycode()
    {
        return $this->citycode;
    }

    /**
     * Set ctarcode
     *
     * @param string $ctarcode
     *
     * @return Deletedaddressobject
     */
    public function setCtarcode($ctarcode)
    {
        $this->ctarcode = $ctarcode;

        return $this;
    }

    /**
     * Get ctarcode
     *
     * @return string
     */
    public function getCtarcode()
    {
        return $this->ctarcode;
    }

    /**
     * Set placecode
     *
     * @param string $placecode
     *
     * @return Deletedaddressobject
     */
    public function setPlacecode($placecode)
    {
        $this->placecode = $placecode;

        return $this;
    }

    /**
     * Get placecode
     *
     * @return string
     */
    public function getPlacecode()
    {
        return $this->placecode;
    }

    /**
     * Set streetcode
     *
     * @param string $streetcode
     *
     * @return Deletedaddressobject
     */
    public function setStreetcode($streetcode)
    {
        $this->streetcode = $streetcode;

        return $this;
    }

    /**
     * Get streetcode
     *
     * @return string
     */
    public function getStreetcode()
    {
        return $this->streetcode;
    }

    /**
     * Set extrcode
     *
     * @param string $extrcode
     *
     * @return Deletedaddressobject
     */
    public function setExtrcode($extrcode)
    {
        $this->extrcode = $extrcode;

        return $this;
    }

    /**
     * Get extrcode
     *
     * @return string
     */
    public function getExtrcode()
    {
        return $this->extrcode;
    }

    /**
     * Set sextcode
     *
     * @param string $sextcode
     *
     * @return Deletedaddressobject
     */
    public function setSextcode($sextcode)
    {
        $this->sextcode = $sextcode;

        return $this;
    }

    /**
     * Get sextcode
     *
     * @return string
     */
    public function getSextcode()
    {
        return $this->sextcode;
    }

    /**
     * Set offname
     *
     * @param string $offname
     *
     * @return Deletedaddressobject
     */
    public function setOffname($offname)
    {
        $this->offname = $offname;

        return $this;
    }

    /**
     * Get offname
     *
     * @return string
     */
    public function getOffname()
    {
        return $this->offname;
    }

    /**
     * Set postalcode
     *
     * @param string $postalcode
     *
     * @return Deletedaddressobject
     */
    public function setPostalcode($postalcode)
    {
        $this->postalcode = $postalcode;

        return $this;
    }

    /**
     * Get postalcode
     *
     * @return string
     */
    public function getPostalcode()
    {
        return $this->postalcode;
    }

    /**
     * Set ifnsfl
     *
     * @param string $ifnsfl
     *
     * @return Deletedaddressobject
     */
    public function setIfnsfl($ifnsfl)
    {
        $this->ifnsfl = $ifnsfl;

        return $this;
    }

    /**
     * Get ifnsfl
     *
     * @return string
     */
    public function getIfnsfl()
    {
        return $this->ifnsfl;
    }

    /**
     * Set terrifnsfl
     *
     * @param string $terrifnsfl
     *
     * @return Deletedaddressobject
     */
    public function setTerrifnsfl($terrifnsfl)
    {
        $this->terrifnsfl = $terrifnsfl;

        return $this;
    }

    /**
     * Get terrifnsfl
     *
     * @return string
     */
    public function getTerrifnsfl()
    {
        return $this->terrifnsfl;
    }

    /**
     * Set ifnsul
     *
     * @param string $ifnsul
     *
     * @return Deletedaddressobject
     */
    public function setIfnsul($ifnsul)
    {
        $this->ifnsul = $ifnsul;

        return $this;
    }

    /**
     * Get ifnsul
     *
     * @return string
     */
    public function getIfnsul()
    {
        return $this->ifnsul;
    }

    /**
     * Set terrifnsul
     *
     * @param string $terrifnsul
     *
     * @return Deletedaddressobject
     */
    public function setTerrifnsul($terrifnsul)
    {
        $this->terrifnsul = $terrifnsul;

        return $this;
    }

    /**
     * Get terrifnsul
     *
     * @return string
     */
    public function getTerrifnsul()
    {
        return $this->terrifnsul;
    }

    /**
     * Set okato
     *
     * @param string $okato
     *
     * @return Deletedaddressobject
     */
    public function setOkato($okato)
    {
        $this->okato = $okato;

        return $this;
    }

    /**
     * Get okato
     *
     * @return string
     */
    public function getOkato()
    {
        return $this->okato;
    }

    /**
     * Set oktmo
     *
     * @param string $oktmo
     *
     * @return Deletedaddressobject
     */
    public function setOktmo($oktmo)
    {
        $this->oktmo = $oktmo;

        return $this;
    }

    /**
     * Get oktmo
     *
     * @return string
     */
    public function getOktmo()
    {
        return $this->oktmo;
    }

    /**
     * Set updatedate
     *
     * @param \DateTime $updatedate
     *
     * @return Deletedaddressobject
     */
    public function setUpdatedate($updatedate)
    {
        $this->updatedate = $updatedate;

        return $this;
    }

    /**
     * Get updatedate
     *
     * @return \DateTime
     */
    public function getUpdatedate()
    {
        return $this->updatedate;
    }

    /**
     * Set shortname
     *
     * @param string $shortname
     *
     * @return Deletedaddressobject
     */
    public function setShortname($shortname)
    {
        $this->shortname = $shortname;

        return $this;
    }

    /**
     * Get shortname
     *
     * @return string
     */
    public function getShortname()
    {
        return $this->shortname;
    }

    /**
     * Set aolevel
     *
     * @param integer $aolevel
     *
     * @return Deletedaddressobject
     */
    public function setAolevel($aolevel)
    {
        $this->aolevel = $aolevel;

        return $this;
    }

    /**
     * Get aolevel
     *
     * @return integer
     */
    public function getAolevel()
    {
        return $this->aolevel;
    }

    /**
     * Set parentguid
     *
     * @param string $parentguid
     *
     * @return Deletedaddressobject
     */
    public function setParentguid($parentguid)
    {
        $this->parentguid = $parentguid;

        return $this;
    }

    /**
     * Get parentguid
     *
     * @return string
     */
    public function getParentguid()
    {
        return $this->parentguid;
    }

    /**
     * Set previd
     *
     * @param string $previd
     *
     * @return Deletedaddressobject
     */
    public function setPrevid($previd)
    {
        $this->previd = $previd;

        return $this;
    }

    /**
     * Get previd
     *
     * @return string
     */
    public function getPrevid()
    {
        return $this->previd;
    }

    /**
     * Set nextid
     *
     * @param string $nextid
     *
     * @return Deletedaddressobject
     */
    public function setNextid($nextid)
    {
        $this->nextid = $nextid;

        return $this;
    }

    /**
     * Get nextid
     *
     * @return string
     */
    public function getNextid()
    {
        return $this->nextid;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Deletedaddressobject
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set plaincode
     *
     * @param string $plaincode
     *
     * @return Deletedaddressobject
     */
    public function setPlaincode($plaincode)
    {
        $this->plaincode = $plaincode;

        return $this;
    }

    /**
     * Get plaincode
     *
     * @return string
     */
    public function getPlaincode()
    {
        return $this->plaincode;
    }

    /**
     * Set actstatus
     *
     * @param integer $actstatus
     *
     * @return Deletedaddressobject
     */
    public function setActstatus($actstatus)
    {
        $this->actstatus = $actstatus;

        return $this;
    }

    /**
     * Get actstatus
     *
     * @return integer
     */
    public function getActstatus()
    {
        return $this->actstatus;
    }

    /**
     * Set centstatus
     *
     * @param integer $centstatus
     *
     * @return Deletedaddressobject
     */
    public function setCentstatus($centstatus)
    {
        $this->centstatus = $centstatus;

        return $this;
    }

    /**
     * Get centstatus
     *
     * @return integer
     */
    public function getCentstatus()
    {
        return $this->centstatus;
    }

    /**
     * Set operstatus
     *
     * @param integer $operstatus
     *
     * @return Deletedaddressobject
     */
    public function setOperstatus($operstatus)
    {
        $this->operstatus = $operstatus;

        return $this;
    }

    /**
     * Get operstatus
     *
     * @return integer
     */
    public function getOperstatus()
    {
        return $this->operstatus;
    }

    /**
     * Set currstatus
     *
     * @param integer $currstatus
     *
     * @return Deletedaddressobject
     */
    public function setCurrstatus($currstatus)
    {
        $this->currstatus = $currstatus;

        return $this;
    }

    /**
     * Get currstatus
     *
     * @return integer
     */
    public function getCurrstatus()
    {
        return $this->currstatus;
    }

    /**
     * Set startdate
     *
     * @param \DateTime $startdate
     *
     * @return Deletedaddressobject
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    /**
     * Get startdate
     *
     * @return \DateTime
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     *
     * @return Deletedaddressobject
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set normdoc
     *
     * @param string $normdoc
     *
     * @return Deletedaddressobject
     */
    public function setNormdoc($normdoc)
    {
        $this->normdoc = $normdoc;

        return $this;
    }

    /**
     * Get normdoc
     *
     * @return string
     */
    public function getNormdoc()
    {
        return $this->normdoc;
    }

    /**
     * Set livestatus
     *
     * @param string $livestatus
     *
     * @return Deletedaddressobject
     */
    public function setLivestatus($livestatus)
    {
        $this->livestatus = $livestatus;

        return $this;
    }

    /**
     * Get livestatus
     *
     * @return string
     */
    public function getLivestatus()
    {
        return $this->livestatus;
    }

    /**
     * Get aoid
     *
     * @return string
     */
    public function getAoid()
    {
        return $this->aoid;
    }
}
