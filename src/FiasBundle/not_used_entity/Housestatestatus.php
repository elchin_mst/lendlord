<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Housestatestatus
 *
 * @ORM\Table(name="HouseStateStatus")
 * @ORM\Entity
 */
class Housestatestatus
{
    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=60, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="HOUSESTID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $housestid;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Housestatestatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get housestid
     *
     * @return integer
     */
    public function getHousestid()
    {
        return $this->housestid;
    }
}
