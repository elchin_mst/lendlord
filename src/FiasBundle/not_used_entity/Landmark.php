<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Landmark
 *
 * @ORM\Table(name="Landmark")
 * @ORM\Entity
 */
class Landmark
{
    /**
     * @var string
     *
     * @ORM\Column(name="LOCATION", type="string", length=500, nullable=true)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="POSTALCODE", type="string", length=6, nullable=true)
     */
    private $postalcode;

    /**
     * @var string
     *
     * @ORM\Column(name="IFNSFL", type="string", length=4, nullable=true)
     */
    private $ifnsfl;

    /**
     * @var string
     *
     * @ORM\Column(name="TERRIFNSFL", type="string", length=4, nullable=true)
     */
    private $terrifnsfl;

    /**
     * @var string
     *
     * @ORM\Column(name="INFSUL", type="string", length=4, nullable=true)
     */
    private $infsul;

    /**
     * @var string
     *
     * @ORM\Column(name="TERRIFNSUL", type="string", length=4, nullable=true)
     */
    private $terrifnsul;

    /**
     * @var string
     *
     * @ORM\Column(name="OKATO", type="string", length=11, nullable=true)
     */
    private $okato;

    /**
     * @var string
     *
     * @ORM\Column(name="OKTMO", type="string", length=11, nullable=true)
     */
    private $oktmo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="UPDATEDATE", type="date", nullable=true)
     */
    private $updatedate;

    /**
     * @var string
     *
     * @ORM\Column(name="LANDID", type="string", length=36, nullable=true)
     */
    private $landid;

    /**
     * @var string
     *
     * @ORM\Column(name="AOGUID", type="string", length=36, nullable=true)
     */
    private $aoguid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="STARTDATE", type="date", nullable=true)
     */
    private $startdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ENDDATE", type="date", nullable=true)
     */
    private $enddate;

    /**
     * @var string
     *
     * @ORM\Column(name="NORMDOC", type="string", length=36, nullable=true)
     */
    private $normdoc;

    /**
     * @var string
     *
     * @ORM\Column(name="LANDGUID", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $landguid;

    /**
     * Set location
     *
     * @param string $location
     *
     * @return Landmark
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set postalcode
     *
     * @param string $postalcode
     *
     * @return Landmark
     */
    public function setPostalcode($postalcode)
    {
        $this->postalcode = $postalcode;

        return $this;
    }

    /**
     * Get postalcode
     *
     * @return string
     */
    public function getPostalcode()
    {
        return $this->postalcode;
    }

    /**
     * Set ifnsfl
     *
     * @param string $ifnsfl
     *
     * @return Landmark
     */
    public function setIfnsfl($ifnsfl)
    {
        $this->ifnsfl = $ifnsfl;

        return $this;
    }

    /**
     * Get ifnsfl
     *
     * @return string
     */
    public function getIfnsfl()
    {
        return $this->ifnsfl;
    }

    /**
     * Set terrifnsfl
     *
     * @param string $terrifnsfl
     *
     * @return Landmark
     */
    public function setTerrifnsfl($terrifnsfl)
    {
        $this->terrifnsfl = $terrifnsfl;

        return $this;
    }

    /**
     * Get terrifnsfl
     *
     * @return string
     */
    public function getTerrifnsfl()
    {
        return $this->terrifnsfl;
    }

    /**
     * Set infsul
     *
     * @param string $infsul
     *
     * @return Landmark
     */
    public function setInfsul($infsul)
    {
        $this->infsul = $infsul;

        return $this;
    }

    /**
     * Get infsul
     *
     * @return string
     */
    public function getInfsul()
    {
        return $this->infsul;
    }

    /**
     * Set terrifnsul
     *
     * @param string $terrifnsul
     *
     * @return Landmark
     */
    public function setTerrifnsul($terrifnsul)
    {
        $this->terrifnsul = $terrifnsul;

        return $this;
    }

    /**
     * Get terrifnsul
     *
     * @return string
     */
    public function getTerrifnsul()
    {
        return $this->terrifnsul;
    }

    /**
     * Set okato
     *
     * @param string $okato
     *
     * @return Landmark
     */
    public function setOkato($okato)
    {
        $this->okato = $okato;

        return $this;
    }

    /**
     * Get okato
     *
     * @return string
     */
    public function getOkato()
    {
        return $this->okato;
    }

    /**
     * Set oktmo
     *
     * @param string $oktmo
     *
     * @return Landmark
     */
    public function setOktmo($oktmo)
    {
        $this->oktmo = $oktmo;

        return $this;
    }

    /**
     * Get oktmo
     *
     * @return string
     */
    public function getOktmo()
    {
        return $this->oktmo;
    }

    /**
     * Set updatedate
     *
     * @param \DateTime $updatedate
     *
     * @return Landmark
     */
    public function setUpdatedate($updatedate)
    {
        $this->updatedate = $updatedate;

        return $this;
    }

    /**
     * Get updatedate
     *
     * @return \DateTime
     */
    public function getUpdatedate()
    {
        return $this->updatedate;
    }

    /**
     * Set landid
     *
     * @param string $landid
     *
     * @return Landmark
     */
    public function setLandid($landid)
    {
        $this->landid = $landid;

        return $this;
    }

    /**
     * Get landid
     *
     * @return string
     */
    public function getLandid()
    {
        return $this->landid;
    }

    /**
     * Set aoguid
     *
     * @param string $aoguid
     *
     * @return Landmark
     */
    public function setAoguid($aoguid)
    {
        $this->aoguid = $aoguid;

        return $this;
    }

    /**
     * Get aoguid
     *
     * @return string
     */
    public function getAoguid()
    {
        return $this->aoguid;
    }

    /**
     * Set startdate
     *
     * @param \DateTime $startdate
     *
     * @return Landmark
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    /**
     * Get startdate
     *
     * @return \DateTime
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     *
     * @return Landmark
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set normdoc
     *
     * @param string $normdoc
     *
     * @return Landmark
     */
    public function setNormdoc($normdoc)
    {
        $this->normdoc = $normdoc;

        return $this;
    }

    /**
     * Get normdoc
     *
     * @return string
     */
    public function getNormdoc()
    {
        return $this->normdoc;
    }

    /**
     * Get landguid
     *
     * @return string
     */
    public function getLandguid()
    {
        return $this->landguid;
    }
}
