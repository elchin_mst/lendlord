<?php

namespace FiasBundle\Repository;

/**
 * HouseRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class HouseRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param $houseguid
     *
     * @return array|null
     */
    public function getActualByEndDate($houseguid)
    {
        $query = $this
            ->createQueryBuilder('h')
            ->where('h.houseguid=:houseguid')
            ->setParameters(array('houseguid' => $houseguid))
            ->orderBy('h.enddate', 'ASC')
            ->setFirstResult(1)
            ->getQuery();
        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
