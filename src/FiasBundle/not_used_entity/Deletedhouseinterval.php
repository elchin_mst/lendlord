<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Deletedhouseinterval
 *
 * @ORM\Table(name="DeletedHouseInterval")
 * @ORM\Entity
 */
class Deletedhouseinterval
{
    /**
     * @var string
     *
     * @ORM\Column(name="POSTALCODE", type="string", length=6, nullable=true)
     */
    private $postalcode;

    /**
     * @var string
     *
     * @ORM\Column(name="IFNSFL", type="string", length=4, nullable=true)
     */
    private $ifnsfl;

    /**
     * @var string
     *
     * @ORM\Column(name="TERRIFNSFL", type="string", length=4, nullable=true)
     */
    private $terrifnsfl;

    /**
     * @var string
     *
     * @ORM\Column(name="IFNSUL", type="string", length=4, nullable=true)
     */
    private $ifnsul;

    /**
     * @var string
     *
     * @ORM\Column(name="TERRIFNSUL", type="string", length=4, nullable=true)
     */
    private $terrifnsul;

    /**
     * @var string
     *
     * @ORM\Column(name="OKATO", type="string", length=11, nullable=true)
     */
    private $okato;

    /**
     * @var string
     *
     * @ORM\Column(name="OKTMO", type="string", length=11, nullable=true)
     */
    private $oktmo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="UPDATEDATE", type="date", nullable=true)
     */
    private $updatedate;

    /**
     * @var integer
     *
     * @ORM\Column(name="INTSTART", type="integer", nullable=true)
     */
    private $intstart;

    /**
     * @var integer
     *
     * @ORM\Column(name="INTEND", type="integer", nullable=true)
     */
    private $intend;

    /**
     * @var string
     *
     * @ORM\Column(name="INTGUID", type="string", length=36, nullable=true)
     */
    private $intguid;

    /**
     * @var string
     *
     * @ORM\Column(name="AOGUID", type="string", length=36, nullable=true)
     */
    private $aoguid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="STARTDATE", type="date", nullable=true)
     */
    private $startdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ENDDATE", type="date", nullable=true)
     */
    private $enddate;

    /**
     * @var integer
     *
     * @ORM\Column(name="INTSTATUS", type="integer", nullable=true)
     */
    private $intstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="NORMDOC", type="string", length=36, nullable=true)
     */
    private $normdoc;

    /**
     * @var integer
     *
     * @ORM\Column(name="COUNTER", type="integer", nullable=true)
     */
    private $counter;

    /**
     * @var string
     *
     * @ORM\Column(name="HOUSEINTID", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $houseintid;

    /**
     * Set postalcode
     *
     * @param string $postalcode
     *
     * @return Deletedhouseinterval
     */
    public function setPostalcode($postalcode)
    {
        $this->postalcode = $postalcode;

        return $this;
    }

    /**
     * Get postalcode
     *
     * @return string
     */
    public function getPostalcode()
    {
        return $this->postalcode;
    }

    /**
     * Set ifnsfl
     *
     * @param string $ifnsfl
     *
     * @return Deletedhouseinterval
     */
    public function setIfnsfl($ifnsfl)
    {
        $this->ifnsfl = $ifnsfl;

        return $this;
    }

    /**
     * Get ifnsfl
     *
     * @return string
     */
    public function getIfnsfl()
    {
        return $this->ifnsfl;
    }

    /**
     * Set terrifnsfl
     *
     * @param string $terrifnsfl
     *
     * @return Deletedhouseinterval
     */
    public function setTerrifnsfl($terrifnsfl)
    {
        $this->terrifnsfl = $terrifnsfl;

        return $this;
    }

    /**
     * Get terrifnsfl
     *
     * @return string
     */
    public function getTerrifnsfl()
    {
        return $this->terrifnsfl;
    }

    /**
     * Set ifnsul
     *
     * @param string $ifnsul
     *
     * @return Deletedhouseinterval
     */
    public function setIfnsul($ifnsul)
    {
        $this->ifnsul = $ifnsul;

        return $this;
    }

    /**
     * Get ifnsul
     *
     * @return string
     */
    public function getIfnsul()
    {
        return $this->ifnsul;
    }

    /**
     * Set terrifnsul
     *
     * @param string $terrifnsul
     *
     * @return Deletedhouseinterval
     */
    public function setTerrifnsul($terrifnsul)
    {
        $this->terrifnsul = $terrifnsul;

        return $this;
    }

    /**
     * Get terrifnsul
     *
     * @return string
     */
    public function getTerrifnsul()
    {
        return $this->terrifnsul;
    }

    /**
     * Set okato
     *
     * @param string $okato
     *
     * @return Deletedhouseinterval
     */
    public function setOkato($okato)
    {
        $this->okato = $okato;

        return $this;
    }

    /**
     * Get okato
     *
     * @return string
     */
    public function getOkato()
    {
        return $this->okato;
    }

    /**
     * Set oktmo
     *
     * @param string $oktmo
     *
     * @return Deletedhouseinterval
     */
    public function setOktmo($oktmo)
    {
        $this->oktmo = $oktmo;

        return $this;
    }

    /**
     * Get oktmo
     *
     * @return string
     */
    public function getOktmo()
    {
        return $this->oktmo;
    }

    /**
     * Set updatedate
     *
     * @param \DateTime $updatedate
     *
     * @return Deletedhouseinterval
     */
    public function setUpdatedate($updatedate)
    {
        $this->updatedate = $updatedate;

        return $this;
    }

    /**
     * Get updatedate
     *
     * @return \DateTime
     */
    public function getUpdatedate()
    {
        return $this->updatedate;
    }

    /**
     * Set intstart
     *
     * @param integer $intstart
     *
     * @return Deletedhouseinterval
     */
    public function setIntstart($intstart)
    {
        $this->intstart = $intstart;

        return $this;
    }

    /**
     * Get intstart
     *
     * @return integer
     */
    public function getIntstart()
    {
        return $this->intstart;
    }

    /**
     * Set intend
     *
     * @param integer $intend
     *
     * @return Deletedhouseinterval
     */
    public function setIntend($intend)
    {
        $this->intend = $intend;

        return $this;
    }

    /**
     * Get intend
     *
     * @return integer
     */
    public function getIntend()
    {
        return $this->intend;
    }

    /**
     * Set intguid
     *
     * @param string $intguid
     *
     * @return Deletedhouseinterval
     */
    public function setIntguid($intguid)
    {
        $this->intguid = $intguid;

        return $this;
    }

    /**
     * Get intguid
     *
     * @return string
     */
    public function getIntguid()
    {
        return $this->intguid;
    }

    /**
     * Set aoguid
     *
     * @param string $aoguid
     *
     * @return Deletedhouseinterval
     */
    public function setAoguid($aoguid)
    {
        $this->aoguid = $aoguid;

        return $this;
    }

    /**
     * Get aoguid
     *
     * @return string
     */
    public function getAoguid()
    {
        return $this->aoguid;
    }

    /**
     * Set startdate
     *
     * @param \DateTime $startdate
     *
     * @return Deletedhouseinterval
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    /**
     * Get startdate
     *
     * @return \DateTime
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     *
     * @return Deletedhouseinterval
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set intstatus
     *
     * @param integer $intstatus
     *
     * @return Deletedhouseinterval
     */
    public function setIntstatus($intstatus)
    {
        $this->intstatus = $intstatus;

        return $this;
    }

    /**
     * Get intstatus
     *
     * @return integer
     */
    public function getIntstatus()
    {
        return $this->intstatus;
    }

    /**
     * Set normdoc
     *
     * @param string $normdoc
     *
     * @return Deletedhouseinterval
     */
    public function setNormdoc($normdoc)
    {
        $this->normdoc = $normdoc;

        return $this;
    }

    /**
     * Get normdoc
     *
     * @return string
     */
    public function getNormdoc()
    {
        return $this->normdoc;
    }

    /**
     * Set counter
     *
     * @param integer $counter
     *
     * @return Deletedhouseinterval
     */
    public function setCounter($counter)
    {
        $this->counter = $counter;

        return $this;
    }

    /**
     * Get counter
     *
     * @return integer
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * Get houseintid
     *
     * @return string
     */
    public function getHouseintid()
    {
        return $this->houseintid;
    }
}
