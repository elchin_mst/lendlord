<?php

namespace FiasBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FiasImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('fias:import')
            ->setDescription('Command for import fias files');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fias = $this->getContainer()->get('fias.import')->run();
        echo $fias;
        $output->writeln('Command result.');
    }
}
