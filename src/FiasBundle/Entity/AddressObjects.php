<?php

namespace FiasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * AddressObjects
 *
 * @ORM\Table(name="fias_address_objects")
 * @ORM\Entity(repositoryClass="FiasBundle\Repository\AddressObjectsRepository")
 */
class AddressObjects
{
    //not include - PLANCODE CADNUM  deleted

    /**
     * @var string
     *
     * @ORM\Column(name="AOGUID", type="string", length=36, nullable=true)
     * @JMS\Groups({"View"})
     */
    private $aoguid;

    /**
     * @var string
     *
     * @ORM\Column(name="FORMALNAME", type="string", length=120, nullable=true)
     * @JMS\Groups({"View"})
     */
    private $formalname;

    /**
     * @var string
     *
     * @ORM\Column(name="REGIONCODE", type="string", length=2, nullable=true)
     */
    private $regioncode;

    /**
     * @var string
     *
     * @ORM\Column(name="AUTOCODE", type="string", length=1, nullable=true)
     */
    private $autocode;

    /**
     * @var string
     *
     * @ORM\Column(name="AREACODE", type="string", length=3, nullable=true)
     */
    private $areacode;

    /**
     * @var string
     *
     * @ORM\Column(name="CITYCODE", type="string", length=3, nullable=true)
     */
    private $citycode;

    /**
     * @var string
     *
     * @ORM\Column(name="CTARCODE", type="string", length=3, nullable=true)
     */
    private $ctarcode;

    /**
     * @var string
     *
     * @ORM\Column(name="PLACECODE", type="string", length=3, nullable=true)
     */
    private $placecode;

    /**
     * @var string
     *
     * @ORM\Column(name="STREETCODE", type="string", length=4, nullable=true)
     */
    private $streetcode;

    /**
     * @var string
     *
     * @ORM\Column(name="EXTRCODE", type="string", length=4, nullable=true)
     */
    private $extrcode;

    /**
     * @var string
     *
     * @ORM\Column(name="SEXTCODE", type="string", length=3, nullable=true)
     */
    private $sextcode;

    /**
     * @var string
     *
     * @ORM\Column(name="OFFNAME", type="string", length=120, nullable=true)
     */
    private $offname; //используется в документах/при отпр.почты

    /**
     * @var string
     *
     * @ORM\Column(name="CADNUM", type="string", length=100, nullable=true)
     */
    private $cadnum;

    /**
     * @var string
     *
     * @ORM\Column(name="POSTALCODE", type="string", length=6, nullable=true)
     * @JMS\Groups({"View"})
     */
    private $postalcode;

    /**
     * @var string
     *
     * @ORM\Column(name="IFNSFL", type="string", length=4, nullable=true)
     */
    private $ifnsfl;

    /**
     * @var string
     *
     * @ORM\Column(name="TERRIFNSFL", type="string", length=4, nullable=true)
     */
    private $terrifnsfl;

    /**
     * @var string
     *
     * @ORM\Column(name="IFNSUL", type="string", length=4, nullable=true)
     */
    private $ifnsul;

    /**
     * @var string
     *
     * @ORM\Column(name="TERRIFNSUL", type="string", length=4, nullable=true)
     */
    private $terrifnsul;

    /**
     * @var string
     *
     * @ORM\Column(name="OKATO", type="string", length=11, nullable=true)
     */
    private $okato;

    /**
     * @var string
     *
     * @ORM\Column(name="OKTMO", type="string", length=11, nullable=true)
     */
    private $oktmo;

    /**
     * @var string
     *
     * @ORM\Column(name="UPDATEDATE", type="string", nullable=true)
     */
    private $updatedate;

    /**
     * @var string
     *
     * @ORM\Column(name="SHORTNAME", type="string", length=10, nullable=true)
     * @JMS\Groups({"View"})
     * */
    private $shortname;

    /**
     * @var integer
     *
     * @ORM\Column(name="AOLEVEL", type="integer", nullable=true)
     */
    private $aolevel;

    /**
     * @var string
     *
     * @ORM\Column(name="PARENTGUID", type="string", length=36, nullable=true)
     */
    private $parentguid;

    /**
     * @var string
     *
     * @ORM\Column(name="PREVID", type="string", length=36, nullable=true)
     */
    private $previd;

    /**
     * @var string
     *
     * @ORM\Column(name="NEXTID", type="string", length=36, nullable=true)
     */
    private $nextid;

    /**
     * @var string
     *
     * @ORM\Column(name="CODE", type="string", length=17, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="PLAINCODE", type="string", length=15, nullable=true)
     */
    private $plaincode;

    /**
     * @var integer
     *
     * @ORM\Column(name="ACTSTATUS", type="integer", nullable=true)
     */
    private $actstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="CENTSTATUS", type="integer", nullable=true)
     */
    private $centstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="OPERSTATUS", type="integer", nullable=true)
     */
    private $operstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="CURRSTATUS", type="integer", nullable=true)
     */
    private $currstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="STARTDATE", type="string", nullable=true)
     */
    private $startdate;

    /**
     * @var string
     *
     * @ORM\Column(name="ENDDATE", type="string", nullable=true)
     */
    private $enddate;

    /**
     * @var string
     *
     * @ORM\Column(name="NORMDOC", type="string", length=36, nullable=true)
     */
    private $normdoc;

    /**
     * @var string
     *
     * @ORM\Column(name="LIVESTATUS", type="string", nullable=true)
     */
    private $livestatus;

    /**
     * @var string
     *
     * @ORM\Column(name="AOID", type="string", length=56)
     * @ORM\Id
     */
    private $aoid;

    /**
     * @var integer
     *
     * @ORM\Column(name="DIVTYPE", type="integer", nullable=true)
     */
    private $divtype;

    // статус центра

    static $centstatus_mapping = array(
        0 => "не является центром административно-территориального образования",
        1 => "является центром района",
        2 => "является центром (столицей) региона",
        3 => "является одновременно и центром района и центром региона",
    );

    // Уровень адресного объекта

    static $aolevel_mapping = array(
        1 => "регион",
        2 => "автономный округ",
        3 => "район",
        4 => "город",
        5 => "внутригородская территория",
        6 => "населенный пункт",
        7 => "улица",
        90 => "дополнительная территория",
        91 => "улица на дополнительной территории",
    );

    //  Тип деления:
    static $divtype_mapping = array(
        0 => "не определено",
        1 => "муниципальное",
        2 => "административное",
    );

    /**
     * Set aoguid
     *
     * @param string $aoguid
     *
     * @return Addressobject
     */
    public function setAoguid($aoguid)
    {
        $this->aoguid = $aoguid;

        return $this;
    }

    /**
     * Get aoguid
     *
     * @return string
     */
    public function getAoguid()
    {
        return $this->aoguid;
    }

    /**
     * Set formalname
     *
     * @param string $formalname
     *
     * @return Addressobject
     */
    public function setFormalname($formalname)
    {
        $this->formalname = $formalname;

        return $this;
    }

    /**
     * Get formalname
     *
     * @return string
     */
    public function getFormalname()
    {
        return $this->formalname;
    }

    /**
     * Set regioncode
     *
     * @param string $regioncode
     *
     * @return Addressobject
     */
    public function setRegioncode($regioncode)
    {
        $this->regioncode = $regioncode;

        return $this;
    }

    /**
     * Get regioncode
     *
     * @return string
     */
    public function getRegioncode()
    {
        return $this->regioncode;
    }

    /**
     * Set autocode
     *
     * @param string $autocode
     *
     * @return Addressobject
     */
    public function setAutocode($autocode)
    {
        $this->autocode = $autocode;

        return $this;
    }

    /**
     * Get autocode
     *
     * @return string
     */
    public function getAutocode()
    {
        return $this->autocode;
    }

    /**
     * Set areacode
     *
     * @param string $areacode
     *
     * @return Addressobject
     */
    public function setAreacode($areacode)
    {
        $this->areacode = $areacode;

        return $this;
    }

    /**
     * Get areacode
     *
     * @return string
     */
    public function getAreacode()
    {
        return $this->areacode;
    }

    /**
     * Set citycode
     *
     * @param string $citycode
     *
     * @return Addressobject
     */
    public function setCitycode($citycode)
    {
        $this->citycode = $citycode;

        return $this;
    }

    /**
     * Get citycode
     *
     * @return string
     */
    public function getCitycode()
    {
        return $this->citycode;
    }

    /**
     * Set ctarcode
     *
     * @param string $ctarcode
     *
     * @return Addressobject
     */
    public function setCtarcode($ctarcode)
    {
        $this->ctarcode = $ctarcode;

        return $this;
    }

    /**
     * Get ctarcode
     *
     * @return string
     */
    public function getCtarcode()
    {
        return $this->ctarcode;
    }

    /**
     * Set placecode
     *
     * @param string $placecode
     *
     * @return Addressobject
     */
    public function setPlacecode($placecode)
    {
        $this->placecode = $placecode;

        return $this;
    }

    /**
     * Get placecode
     *
     * @return string
     */
    public function getPlacecode()
    {
        return $this->placecode;
    }

    /**
     * Set streetcode
     *
     * @param string $streetcode
     *
     * @return Addressobject
     */
    public function setStreetcode($streetcode)
    {
        $this->streetcode = $streetcode;

        return $this;
    }

    /**
     * Get streetcode
     *
     * @return string
     */
    public function getStreetcode()
    {
        return $this->streetcode;
    }

    /**
     * Set extrcode
     *
     * @param string $extrcode
     *
     * @return Addressobject
     */
    public function setExtrcode($extrcode)
    {
        $this->extrcode = $extrcode;

        return $this;
    }

    /**
     * Get extrcode
     *
     * @return string
     */
    public function getExtrcode()
    {
        return $this->extrcode;
    }

    /**
     * Set sextcode
     *
     * @param string $sextcode
     *
     * @return Addressobject
     */
    public function setSextcode($sextcode)
    {
        $this->sextcode = $sextcode;

        return $this;
    }

    /**
     * Get sextcode
     *
     * @return string
     */
    public function getSextcode()
    {
        return $this->sextcode;
    }

    /**
     * Set offname
     *
     * @param string $offname
     *
     * @return Addressobject
     */
    public function setOffname($offname)
    {
        $this->offname = $offname;

        return $this;
    }

    /**
     * Get offname
     *
     * @return string
     */
    public function getOffname()
    {
        return $this->offname;
    }

    /**
     * Set postalcode
     *
     * @param string $postalcode
     *
     * @return Addressobject
     */
    public function setPostalcode($postalcode)
    {
        $this->postalcode = $postalcode;

        return $this;
    }

    /**
     * Get postalcode
     *
     * @return string
     */
    public function getPostalcode()
    {
        return $this->postalcode;
    }

    /**
     * Set ifnsfl
     *
     * @param string $ifnsfl
     *
     * @return Addressobject
     */
    public function setIfnsfl($ifnsfl)
    {
        $this->ifnsfl = $ifnsfl;

        return $this;
    }

    /**
     * Get ifnsfl
     *
     * @return string
     */
    public function getIfnsfl()
    {
        return $this->ifnsfl;
    }

    /**
     * Set terrifnsfl
     *
     * @param string $terrifnsfl
     *
     * @return Addressobject
     */
    public function setTerrifnsfl($terrifnsfl)
    {
        $this->terrifnsfl = $terrifnsfl;

        return $this;
    }

    /**
     * Get terrifnsfl
     *
     * @return string
     */
    public function getTerrifnsfl()
    {
        return $this->terrifnsfl;
    }

    /**
     * Set ifnsul
     *
     * @param string $ifnsul
     *
     * @return Addressobject
     */
    public function setIfnsul($ifnsul)
    {
        $this->ifnsul = $ifnsul;

        return $this;
    }

    /**
     * Get ifnsul
     *
     * @return string
     */
    public function getIfnsul()
    {
        return $this->ifnsul;
    }

    /**
     * Set terrifnsul
     *
     * @param string $terrifnsul
     *
     * @return Addressobject
     */
    public function setTerrifnsul($terrifnsul)
    {
        $this->terrifnsul = $terrifnsul;

        return $this;
    }

    /**
     * Get terrifnsul
     *
     * @return string
     */
    public function getTerrifnsul()
    {
        return $this->terrifnsul;
    }

    /**
     * Set okato
     *
     * @param string $okato
     *
     * @return Addressobject
     */
    public function setOkato($okato)
    {
        $this->okato = $okato;

        return $this;
    }

    /**
     * Get okato
     *
     * @return string
     */
    public function getOkato()
    {
        return $this->okato;
    }

    /**
     * Set oktmo
     *
     * @param string $oktmo
     *
     * @return Addressobject
     */
    public function setOktmo($oktmo)
    {
        $this->oktmo = $oktmo;

        return $this;
    }

    /**
     * Get oktmo
     *
     * @return string
     */
    public function getOktmo()
    {
        return $this->oktmo;
    }

    /**
     * Set updatedate
     *
     * @param string $updatedate
     *
     * @return Addressobject
     */
    public function setUpdatedate($updatedate)
    {
        $this->updatedate = $updatedate;

        return $this;
    }

    /**
     * Get updatedate
     *
     * @return string
     */
    public function getUpdatedate()
    {
        return $this->updatedate;
    }

    /**
     * Set shortname
     *
     * @param string $shortname
     *
     * @return Addressobject
     */
    public function setShortname($shortname)
    {
        $this->shortname = $shortname;

        return $this;
    }

    /**
     * Get shortname
     *
     * @return string
     */
    public function getShortname()
    {
        return $this->shortname;
    }

    /**
     * Set aolevel
     *
     * @param integer $aolevel
     *
     * @return Addressobject
     */
    public function setAolevel($aolevel)
    {
        $this->aolevel = $aolevel;

        return $this;
    }

    /**
     * Get aolevel
     *
     * @return integer
     */
    public function getAolevel()
    {
        return $this->aolevel;
    }

    /**
     * Set parentguid
     *
     * @param string $parentguid
     *
     * @return Addressobject
     */
    public function setParentguid($parentguid)
    {
        $this->parentguid = $parentguid;

        return $this;
    }

    /**
     * Get parentguid
     *
     * @return string
     */
    public function getParentguid()
    {
        return $this->parentguid;
    }

    /**
     * Set previd
     *
     * @param string $previd
     *
     * @return Addressobject
     */
    public function setPrevid($previd)
    {
        $this->previd = $previd;

        return $this;
    }

    /**
     * Get previd
     *
     * @return string
     */
    public function getPrevid()
    {
        return $this->previd;
    }

    /**
     * Set nextid
     *
     * @param string $nextid
     *
     * @return Addressobject
     */
    public function setNextid($nextid)
    {
        $this->nextid = $nextid;

        return $this;
    }

    /**
     * Get nextid
     *
     * @return string
     */
    public function getNextid()
    {
        return $this->nextid;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Addressobject
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set plaincode
     *
     * @param string $plaincode
     *
     * @return Addressobject
     */
    public function setPlaincode($plaincode)
    {
        $this->plaincode = $plaincode;

        return $this;
    }

    /**
     * Get plaincode
     *
     * @return string
     */
    public function getPlaincode()
    {
        return $this->plaincode;
    }

    /**
     * Set actstatus
     *
     * @param integer $actstatus
     *
     * @return Addressobject
     */
    public function setActstatus($actstatus)
    {
        $this->actstatus = $actstatus;

        return $this;
    }

    /**
     * Get actstatus
     *
     * @return integer
     */
    public function getActstatus()
    {
        return $this->actstatus;
    }

    /**
     * Set centstatus
     *
     * @param integer $centstatus
     *
     * @return Addressobject
     */
    public function setCentstatus($centstatus)
    {
        $this->centstatus = $centstatus;

        return $this;
    }

    /**
     * Get centstatus
     *
     * @return integer
     */
    public function getCentstatus()
    {
        return $this->centstatus;
    }

    /**
     * Set operstatus
     *
     * @param integer $operstatus
     *
     * @return Addressobject
     */
    public function setOperstatus($operstatus)
    {
        $this->operstatus = $operstatus;

        return $this;
    }

    /**
     * Get operstatus
     *
     * @return integer
     */
    public function getOperstatus()
    {
        return $this->operstatus;
    }

    /**
     * Set currstatus
     *
     * @param integer $currstatus
     *
     * @return Addressobject
     */
    public function setCurrstatus($currstatus)
    {
        $this->currstatus = $currstatus;

        return $this;
    }

    /**
     * Get currstatus
     *
     * @return integer
     */
    public function getCurrstatus()
    {
        return $this->currstatus;
    }

    /**
     * Set startdate
     *
     * @param string $startdate
     *
     * @return Addressobject
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    /**
     * Get startdate
     *
     * @return string
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Set enddate
     *
     * @param string $enddate
     *
     * @return Addressobject
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Get enddate
     *
     * @return string
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set normdoc
     *
     * @param string $normdoc
     *
     * @return Addressobject
     */
    public function setNormdoc($normdoc)
    {
        $this->normdoc = $normdoc;

        return $this;
    }

    /**
     * Get normdoc
     *
     * @return string
     */
    public function getNormdoc()
    {
        return $this->normdoc;
    }

    /**
     * Set livestatus
     *
     * @param string $livestatus
     *
     * @return Addressobject
     */
    public function setLivestatus($livestatus)
    {
        $this->livestatus = $livestatus;

        return $this;
    }

    /**
     * Get livestatus
     *
     * @return string
     */
    public function getLivestatus()
    {
        return $this->livestatus;
    }

    /**
     * Get aoid
     *
     * @return string
     */
    public function getAoid()
    {
        return $this->aoid;
    }

    public function setAoid($aoid)
    {
        $this->aoid = $aoid;

        return $this;
    }

    /**
     * Set divtype
     *
     * @param integer $divtype
     *
     * @return AddressObjects
     */
    public function setDivtype($divtype)
    {
        $this->divtype = $divtype;

        return $this;
    }

    /**
     * Get divtype
     *
     * @return integer
     */
    public function getDivtype()
    {
        return $this->divtype;
    }

    /**
     * Set cadnum
     *
     * @param string $cadnum
     *
     * @return AddressObjects
     */
    public function setCadnum($cadnum)
    {
        $this->cadnum = $cadnum;

        return $this;
    }

    /**
     * Get cadnum
     *
     * @return string
     */
    public function getCadnum()
    {
        return $this->cadnum;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("aolevel")
     * @JMS\Groups({"View"})
     *
     */
    public function getLvL()
    {
        return self::$aolevel_mapping[$this->aolevel];
    }
}
