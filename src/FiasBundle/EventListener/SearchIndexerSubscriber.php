<?php
namespace FiasBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use FiasBundle\Entity\House;
use FiasBundle\Entity\AddressObjects;
use Doctrine\DBAL\DriverManager;

class SearchIndexerSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
            'postUpdate',
        );
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function index(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // perhaps you only want to act on some "Product" entitygi
        if ($entity instanceof House) {
            $entityManager = $args->getEntityManager();
            //  $item = $entityManager->getReference('FiasBundle\Entity\AddressObjects', $entity->getAoguid());
            // dump($item);
            //link with aoid =(
            //  $entity->addAddrobject($item);

            //  $conn->insert('addrobj_house', array('house_id' => $entity->getHouseid(), 'addr_aoguid' => $entity->getAoguid()));

        }

        if ($entity instanceof AddressObjects) {
            // $entityManager = $args->getEntityManager();
            //  $item = $entityManager->getReference('FiasBundle\Entity\AddressObjects', $entity->getAoguid());
            //dump($item);
            // $entity->addParentAddrObj($item);

        }
    }
}