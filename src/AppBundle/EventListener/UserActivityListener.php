<?php

namespace AppBundle\EventListener;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\HttpKernel\HttpKernel;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class UserActivityListener
 * Listener that updates the last activity of the authenticated user
 *
 * @package AppBundle\EventListener
 */
class UserActivityListener
{
    protected $tokenStorage;

    protected $router;

    protected $em;

    protected $authorizationChecker;

    protected $container;

    public function __construct(
        TokenStorage $tokenStorage,
        EntityManager $em,
        AuthorizationChecker $authirizationChecker,
        ContainerInterface $container
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authirizationChecker;
        $this->em = $em;
        $this->container = $container;
        $this->router = $this->container->get('router');
    }

    /**
     * @param FilterControllerEvent $event
     *
     * @return RedirectResponse|void
     */
    public function onCoreController(FilterControllerEvent $event)
    {
        // Check that the current request is a "MASTER_REQUEST"
        // Ignore any sub-request
        if ($event->getRequestType() !== HttpKernel::MASTER_REQUEST) {
            return;
        }

        $logger = $this->container->get('logger');

        // Check token authentication availability
        if ($this->tokenStorage->getToken()) {

            $user = $this->tokenStorage->getToken()->getUser();

            if ($user instanceof User) {

                if ($user->isActiveNow()) {
                    $user->setLastActivityAt(new \DateTime());
                    $this->em->flush($user);
                } elseif (
                    (!$user->isActiveNow()) &&
                    $this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')
                ) {
                    $this->container->get('security.token_storage')->setToken(null);
                    $this->container->get('session')->clear();
                    $this->container->get('request_stack')->getCurrentRequest()->getSession()->invalidate();
                }
            }
        }
    }
}