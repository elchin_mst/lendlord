<?php
namespace AppBundle\Serializer;

use JMS\Serializer\Context;
use JMS\Serializer\JsonDeserializationVisitor as ParentJsonDeserializationVisitor;
use JMS\Serializer\Metadata\ClassMetadata;
use JMS\Serializer\Metadata\PropertyMetadata;

// if we want get different array when deserialize and serialize values  we need use custom deserializer

class JsonDeserializationVisitor extends ParentJsonDeserializationVisitor
{
    /**
     * @param PropertyMetadata $metadata
     * @param mixed $data
     * @param Context $context
     */
    public function visitProperty(
        PropertyMetadata $metadata, $data, Context $context
    )
    {
        //This type is the information you put into the @Type annotation.
        $type = $metadata->type;

        $expand = null;

        parent::visitProperty($metadata, $data, $context);
    }
}