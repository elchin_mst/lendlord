<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * CommercialProperty controller.
 *
 * @Route("/api/commercial_property")
 */
class CommercialPropertyController extends BaseRestApiController
{
    const LIMIT = 30;

    private static $availableSearchFields = array(
        'without-validation',
    );

    private static $selectableFields = array();

    /**
     * Get entity list
     *
     * @Rest\Get("/list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function indexAction(Request $request)
    {
        if ((int)$request->get('page') && $request->get('page') > 0) {
            $currentPage = (int)$request->get('page');
        } else {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * self::LIMIT;

        $search = $this->getSearchArray(
            $request->get('search'),
            self::$availableSearchFields
        );

        $commercialProperty = $this->getDoctrine()->getRepository('AppBundle:CommercialProperty')->findBy(
            $search, array('id' => 'ASC'), self::LIMIT, $offset
        );

        return $this->createApiViewResponse($commercialProperty, 'List', 200);
    }

    /**
     * Get filtered list
     *
     * @Rest\Post("/search")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function searchAction(Request $request)
    {
        return $this->createApiSearchResponse(
            $request,
            'AppBundle\Entity\CommercialProperty',
            'List',
            self::$availableSearchFields,
            self::$selectableFields
        );
    }

    /**
     * Get entity info
     *
     * @Rest\Get("/view/{id}", requirements={"id": "\d+"})
     *
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function viewAction($id)
    {
        $commercialProperty = $this->getDoctrine()->getRepository('AppBundle:CommercialProperty')->find($id);

        return $this->createApiViewResponse($commercialProperty, 'Selected', 200);
    }

    /**
     * Add new record
     *
     * @Rest\Post("/add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function newAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\CommercialProperty');
    }

    /**
     * Edit record
     *
     * @Rest\Put("/edit")
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function editAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\CommercialProperty');
    }

    /**
     * Delete record
     *
     * @Rest\Delete("/remove/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $commercialProperty = $em->getRepository('AppBundle:CommercialProperty')->find($id);

        return $this->createApiDeleteResponse($commercialProperty);
    }
}
