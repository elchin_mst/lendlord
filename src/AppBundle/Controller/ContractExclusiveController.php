<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class ContractExclusiveController
 *
 * @Route("/api/contract_exclusive")
 */
class ContractExclusiveController extends BaseRestApiController
{
    const LIMIT = 30;

    /**
     * Get contract exclusive list
     *
     * @Rest\Get("/list")
     */
    public function indexAction(Request $request)
    {
        if ((int) $request->get('page') && $request->get('page') > 0) {
            $currentPage = (int) $request->get('page');
        } else {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * self::LIMIT;

        $contractsExclusive = $this->getDoctrine()->getRepository('AppBundle:ContractExclusive')->findBy(
            array(), array('id' => 'ASC'), self::LIMIT, $offset
        );

        return $this->createApiViewResponse($contractsExclusive, 'List', 200);
    }

    /**
     * Get contract exclusive info
     *
     * @Rest\Get("/view/{id}", name="get_contract_exclusive", requirements={"id": "\d+"})
     */
    public function viewAction($id)
    {
        $contractExclusive = $this->getDoctrine()->getRepository('AppBundle:ContractExclusive')->find($id);

        return $this->createApiViewResponse($contractExclusive, 'Selected', 200);
    }

    /**
     * Add new record
     *
     * @Rest\Post("/add")
     */
    public function newAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\ContractExclusive', true);
    }

    /**
     * Edit record
     *
     * @Rest\Put("/edit")
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function editAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\ContractExclusive', true);
    }

    /**
     * Delete record
     *
     * @Rest\Delete("/remove/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $contractExclusive = $em->getRepository('AppBundle:ContractExclusive')->find($id);

        return $this->createApiDeleteResponse($contractExclusive);
    }
}
