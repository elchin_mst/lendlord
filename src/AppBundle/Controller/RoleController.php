<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * User controller.
 *
 * @Route("/api/roles")
 */
class RoleController extends BaseRestApiController
{
    /**
     * Get all roles
     *
     * @Rest\Get("/list")
     */
    public function indexAction(Request $request)
    {
        $roles = $this->getDoctrine()->getRepository('AppBundle:Role')->findAll();

        return $this->createApiViewResponse($roles, 'List', 200);
    }

    /**
     * Get info about selected role
     *
     * @Rest\Get("/view/{id}")
     */
    public function viewAction($id)
    {
        $role = $this->getDoctrine()->getRepository('AppBundle:Role')->find($id);

        return $this->createApiViewResponse($role, 'Selected', 200);
    }

    /**
     * Add new record
     *
     * @Rest\Post("/add")
     */
    public function newAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Role');
    }

    /**
     * Edit record
     *
     * @Rest\Put("/edit")
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function editAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Role');
    }

    /**
     * Delete record
     *
     * @Rest\Delete("/remove/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $role = $em->getRepository('AppBundle:Role')->find($id);

        return $this->createApiDeleteResponse($role);
    }
}
