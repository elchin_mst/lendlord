<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Office controller.
 *
 * @Route("/api/office")
 */
class OfficeController extends BaseRestApiController
{
    const LIMIT = 30;

    /**
     * Get offices list
     *
     * @Rest\Get("/list")
     */
    public function indexAction(Request $request)
    {
        if ((int) $request->get('page') && $request->get('page') > 0) {
            $currentPage = (int) $request->get('page');
        } else {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * self::LIMIT;

        $offices = $this->getDoctrine()->getRepository('AppBundle:Office')->findBy(
            array(), array('id' => 'ASC'), self::LIMIT, $offset
        );

        return $this->createApiViewResponse($offices, 'List', 200);
    }

    /**
     * Get office info
     *
     * @Rest\Get("/view/{id}", name="get_office", requirements={"id": "\d+"})
     */
    public function viewAction($id)
    {
        $office = $this->getDoctrine()->getRepository('AppBundle:Office')->find($id);

        return $this->createApiViewResponse($office, 'Selected', 200);
    }

    /**
     * Add new record
     *
     * @Rest\Post("/add")
     */
    public function newAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Office');
    }

    /**
     * Edit record
     *
     * @Rest\Put("/edit")
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function editAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Office');
    }

    /**
     * Delete record
     *
     * @Rest\Delete("/remove/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $office = $em->getRepository('AppBundle:Office')->find($id);

        return $this->createApiDeleteResponse($office);
    }
}
