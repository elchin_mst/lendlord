<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Position controller.
 *
 * @Route("/api/position")
 */
class PositionController extends BaseRestApiController
{
    const LIMIT = 30;

    /**
     * Get offices list
     *
     * @Rest\Get("/list")
     */
    public function indexAction(Request $request)
    {
        if ((int) $request->get('page') && $request->get('page') > 0) {
            $currentPage = (int) $request->get('page');
        } else {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * self::LIMIT;

        $positions = $this->getDoctrine()->getRepository('AppBundle:Position')->findBy(
            array(), array('id' => 'ASC'), self::LIMIT, $offset
        );

        return $this->createApiViewResponse($positions, 'List', 200);
    }

    /**
     * Get position info
     *
     * @Rest\Get("/view/{id}", name="get_position", requirements={"id": "\d+"})
     */
    public function viewAction($id)
    {
        $position = $this->getDoctrine()->getRepository('AppBundle:Position')->find($id);

        return $this->createApiViewResponse($position, 'Selected', 200);
    }

    /**
     * Add new record
     *
     * @Rest\Post("/add")
     */
    public function newAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Position');
    }

    /**
     * Edit record
     *
     * @Rest\Put("/edit")
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function editAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Position');
    }

    /**
     * Delete record
     *
     * @Rest\Delete("/remove/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $position = $em->getRepository('AppBundle:Position')->find($id);

        return $this->createApiDeleteResponse($position);
    }
}
