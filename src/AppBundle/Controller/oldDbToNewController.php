<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Config\Definition\Exception\Exception;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Route("/api/oldDbToNew")
 *
 *
 */
class oldDbToNewController extends Controller
{
    /**
     * @Rest\Get("/transfer")
     *
     * @return Response
     */
    public function findNoteAction(Request $request){
        $id = $request->getContent();
        $oldArray = $this->getInfo($id);

        return new Response();
    }

    public function getInfo($id){
        $ch = curl_init();
        if (!empty($id)){
            $url = 'http://178.76.250.230:84/cgi-bin/info/base.pl?type=flats&id=' . $id . '&translate=1';
            curl_setopt($ch, CURLOPT_URL, $url);

        } else {
            return 'ERROR empty id';
        }

    }
}
