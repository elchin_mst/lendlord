<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Department controller.
 *
 * @Route("/api/department")
 */
class DepartmentController extends BaseRestApiController
{
    const LIMIT = 30;

    /**
     * Get offices list
     *
     * @Rest\Get("/list")
     */
    public function indexAction(Request $request)
    {
        if ((int) $request->get('page') && $request->get('page') > 0) {
            $currentPage = (int) $request->get('page');
        } else {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * self::LIMIT;

        $departments = $this->getDoctrine()->getRepository('AppBundle:Department')->findBy(
            array(), array('id' => 'ASC'), self::LIMIT, $offset
        );

        return $this->createApiViewResponse($departments, 'List', 200);
    }

    /**
     * Get department info
     *
     * @Rest\Get("/view/{id}", name="get_department", requirements={"id": "\d+"})
     */
    public function viewAction($id)
    {
        $department = $this->getDoctrine()->getRepository('AppBundle:Department')->find($id);

        return $this->createApiViewResponse($department, 'Selected', 200);
    }

    /**
     * Add new record
     *
     * @Rest\Post("/add")
     */
    public function newAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Department');
    }

    /**
     * Edit record
     *
     * @Rest\Put("/edit")
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function editAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Department');
    }

    /**
     * Delete record
     *
     * @Rest\Delete("/remove/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $department = $em->getRepository('AppBundle:Department')->find($id);

        return $this->createApiDeleteResponse($department);
    }
}
