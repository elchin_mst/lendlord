<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Source controller.
 *
 * @Route("/api/source")
 */
class SourceController extends BaseRestApiController
{
    const LIMIT = 30;

    /**
     * Get entity list
     *
     * @Rest\Get("/list")
     */
    public function indexAction(Request $request)
    {
        if ((int) $request->get('page') && $request->get('page') > 0) {
            $currentPage = (int) $request->get('page');
        } else {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * self::LIMIT;

        $sources = $this->getDoctrine()->getRepository('AppBundle:Source')->findBy(
            array(), array('id' => 'ASC'), self::LIMIT, $offset
        );

        return $this->createApiViewResponse($sources, 'List', 200);
    }

    /**
     * Get entity info
     *
     * @Rest\Get("/view/{id}", name="get_source", requirements={"id": "\d+"})
     */
    public function viewAction($id)
    {
        $source = $this->getDoctrine()->getRepository('AppBundle:Source')->find($id);

        return $this->createApiViewResponse($source, 'Selected', 200);
    }

    /**
     * Add new record
     *
     * @Rest\Post("/add")
     */
    public function newAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Source');
    }

    /**
     * Edit record
     *
     * @Rest\Put("/edit")
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function editAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Source');
    }

    /**
     * Delete record
     *
     * @Rest\Delete("/remove/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $source = $em->getRepository('AppBundle:Source')->find($id);

        return $this->createApiDeleteResponse($source);
    }
}
