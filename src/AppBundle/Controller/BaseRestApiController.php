<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializationContext;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Validator\Validation;

class BaseRestApiController extends Controller
{
    const LIMIT = 30;

    protected $context;

    protected $serializer;

    private $validErrorMess;

    //TODO: there is no __contsructor in controller , think about more clear way

    //TODO: one error handing, wrap in json-format with status all exceptions

    public function setSerializer()
    {
        $this->serializer = $this->get('jms_serializer');
    }

    protected function setSerializeContext($params)
    {
        $params = is_array($params) ?: array($params);
        $this->context = new SerializationContext();
        $this->context->setSerializeNull(true);
        $this->context->setGroups($params);

        return $this->context;
    }

    protected function serialize($objects, $context = "")
    {
        $data = $this->serializer->serialize(
            $objects, 'json', $this->context
        );

        return json_decode($data);
    }

    protected function deserialize($jsonData, $entityName)
    {
        return $this->serializer->deserialize($jsonData, $entityName, 'json');
    }

    /**
     * Method for all view actions, get context and objects
     *
     * @param        $objects
     * @param string $context
     * @param int $status
     *
     * @return JsonResponse
     */
    protected function createApiViewResponse($objects, $context, $status = 200)
    {
        $this->setSerializer();
        $this->setSerializeContext($context);
        $response = new JsonResponse();

        try {

            if ($objects) {
                $response->setData($this->serialize($objects));
                $response->setStatusCode($status);
            } else {
                $response->setData(array('error' => 'Not found'));
                $response->setStatusCode(404);
            }
        } catch (\Exception $e) {

            $response->setData(array('error' => $e->getMessage()));
        }

        return $response;
    }

    /**
     * Searches for objects by parameters
     *
     * @param Request $request
     * @param string $entityName
     * @param string $context,
     * @param array $availableFields
     *
     * @return JsonResponse
     */
    protected function createApiSearchResponse(
        Request $request,
        $entityName,
        $context,
        $availableFields = array(),
        $selectableFields = array()
    ) {
        $this->setSerializer();
        $this->setSerializeContext($context);
        $response = new JsonResponse();
        $jsonData = $request->getContent();

        $searchArray = array();
        $limit = 30;
        $offset = 0;

        try {
            $resArr = json_decode($jsonData, true);
        } catch (\Exception $e) {
            $response->setData(array('error' => 'json_decode is failed'));
            $response->setStatusCode(500);

            return $response;
        }

        if ($resArr) {
            if (array_key_exists('search', $resArr) && is_array($resArr['search'])) {
                $searchArray = $resArr['search'];
            }

            if (array_key_exists('limit', $resArr) && (int)$resArr['limit']) {
                $limit = (int)$resArr['limit'];
            }

            if (array_key_exists('offset', $resArr) && (int)$resArr['offset']) {
                $offset = (int)$resArr['offset'];
            }
        }

        $searchArray = $this->getSearchArray(
            $searchArray,
            $availableFields
        );

        list($isValid, $message) = $this->validationFilterArray($searchArray);

        if (!$isValid) {
            $response->setData(array('error' => $message));
            $response->setStatusCode(406);

            return $response;
        }

        $qb = $this->makeQueryBuilder($entityName, $searchArray, $selectableFields);

        $qb->setFirstResult($offset);
        $qb->setMaxResults($limit);

        $query = $qb->getQuery();

        $objects = $query->getResult();

        if ($objects) {
            $response->setData($this->serialize($objects));
            $response->setStatusCode(200);
        } else {
            $response->setData(array('error' => 'Not found'));
            $response->setStatusCode(404);
        }

        return $response;
    }

    /**
     * Method for all add/edit actions, get request and entity class name with path
     *
     * @param Request $request
     * @param string $entityName
     *
     * @return JsonResponse
     */
    protected function createApiEditResponse(Request $request, $entityName, $dataProcessing = false)
    {
        $response = new JsonResponse();
        $this->setSerializer();

        try {

            $jsonData = $request->getContent();
            $this->sendTolog($jsonData);

            $em = $this->getDoctrine()->getManager();
            $object = $this->deserialize($jsonData, $entityName);

            if ($dataProcessing) {
                $this->dataProcessing($object);
            }

            if ($this->validation($object)) {

                $em->persist($object);
                $em->flush();

                $response->setStatusCode(200);
                $response->setData(array('id' => $object->getId()));
            } else {
                $response->setStatusCode(422);
                $response->setData(
                    array(
                        'error' => 'Data not valid. ',
                        'message' => $this->validErrorMess,
                    )
                );
            }
        } catch (\Exception $e) {

            $response->setData(array('error' => $e->getMessage()));
        }

        return $response;
    }

    /**
     * Method for delete action, get current object
     *
     * @param $object
     *
     * @return Response
     */
    protected function createApiDeleteResponse($object)
    {
        $response = new JsonResponse();

        try {
            if ($object) {
                $id = $object->getId();

                $em = $this->getDoctrine()->getManager();
                $em->remove($object);
                $em->flush();

                $response->setData(
                    array(
                        'status' => 'success',
                        'id' => $id,
                    )
                );
            } else {

                $response->setStatusCode(404);
                $response->setData(array('error' => 'Unable to find object entity'));
            }
        } catch (\Exception $e) {
            $response->setStatusCode(500);
            $response->setData(array('error' => $e->getMessage()));
        }

        return $response;
    }

    /**
     * Validate entity
     *
     * @param object $object
     *
     * @return bool
     */
    protected function validation($object)
    {
        if (!is_object($object)) {
            return false;
        }

        $validator = $this->get('validator');
        $errors = $validator->validate($object);

        if (count($errors) > 0) {
            $this->validErrorMess = (string)$errors;

            return false;
        }

        return true;
    }

    /**
     * @param $data
     */
    protected function sendTolog($data)
    {
        $logger = $this->get('logger');
        $logger->info('Params -----------' . $data);
    }

    /**
     * Processing object fields when you create or update entity
     *
     * @param $object
     *
     * @return bool
     */
    private function dataProcessing($object)
    {
        if (!is_object($object)) {
            return false;
        }

        switch (true) {
            case $object instanceof \AppBundle\Entity\User:
                $factory = $this->get('security.encoder_factory');
                $encoder = $factory->getEncoder($object);
                $password = $encoder->encodePassword(
                    $object->getPassword(),
                    $object->getSalt()
                );
                $object->setPassword($password);
                break;
            case $object instanceof \AppBundle\Entity\ContractExclusive:
            case $object instanceof \AppBundle\Entity\ContractView:
            case $object instanceof \AppBundle\Entity\ContractCooperation:
                $user = $this->get('security.token_storage')->getToken()->getUser();
                if (!$object->getId()) {
                    $object->setCreatedUser($user);
                }
                $object->setUpdatedUser($user);
                break;
            default:
                return false;
        }

        return true;
    }

    /**
     * Convert string to camel case
     *
     * @param string $str
     *
     * @return string
     * @throws \Exception
     */
    protected static function toCamelCase($str)
    {
        $newStr = lcfirst($str);

        while (preg_match('/_(\w{1})/', $newStr, $res)) {
            if (array_key_exists(1, $res)) {
                $newStr = str_replace(
                    '_' . $res[1],
                    mb_strtoupper($res[1]),
                    $newStr
                );
            }
        }

        return $newStr;
    }

    /**
     * Convert to snake case
     *
     * @param string $str
     *
     * @return string
     */
    protected static function toSnakeCase($str)
    {
        $newStr = lcfirst($str);

        while (preg_match('/([A-Z])+/', $newStr, $res)) {
            if (array_key_exists(1, $res)) {
                $ch = mb_strtolower($res[1]);
                $newStr = str_replace(
                    $res[1],
                    '_' . $ch,
                    $newStr
                );
            }
        }

        return $newStr;
    }

    /**
     * Formats and validate search array
     *
     * @param array $array
     * @param array $availableFields
     *
     * @return array
     */
    protected function getSearchArray($array = array(), $availableFields = array())
    {
        if (!is_array($array) || !$array) {
            return array();
        }

        $newArray = array();

        foreach ($array as $k => $v) {
            $newKey = self::toCamelCase($k);

            $newArray[$newKey] = $v;
        }

        if ('without-validation' !== reset($availableFields)) {
            foreach ($newArray as $k => $v) {
                if (!in_array($k, $availableFields)) {
                    unset($newArray[$k]);
                }
            }
        }

        return $newArray;
    }

    /**
     * Validate filter array
     *
     * @param array $searchArray
     *
     * @return array
     */
    private function validationFilterArray($searchArray = array())
    {
        if (!$searchArray) {
            return array(true, 'Search array is empty');
        }

        $notValidMess = 'Search array not valid.';
        $availableOperators = array(
            '=',
            '!=',
            '>',
            '<',
            '>=',
            '<=',
            'd',
        );
        $mustExistsKey = array(
            'value',
            'operator'
        );

        foreach ($searchArray as $field => $params) {

            if (!is_array($params)) {
                $field = self::toSnakeCase($field);

                return array(
                    false,
                    $notValidMess . ' Key "' . $field . '" must contain an associative array',
                );
            }

            foreach ($mustExistsKey as $key) {
                if (!array_key_exists($key, $params)) {
                    $field = self::toSnakeCase($field);

                    return array(
                        false,
                        $notValidMess . ' Key "' . $key . '" not found in "' . $field . '"',
                    );
                }
            }

            $value = $params['value'];
            if (is_array($value)) {
                $validVal = $value ? true : false;
            } else {
                if (is_int($value) || is_float($value)) {
                    $validVal = true;
                } else {
                    $validVal = trim($value) ? true : false;
                }
            }
            if (!$validVal) {
                $field = self::toSnakeCase($field);

                return array(
                    false,
                    $notValidMess . ' In key "' . $field . '" "value" is empty,',
                );
            }

            $operator = (string)$params['operator'];
            if (!in_array($operator, $availableOperators)) {
                return array(
                    false,
                    $notValidMess . ' Operator "' . $operator . '" is not allowed.'
                        . ' Error in key "' . $field . '".',
                );
            }

            switch ($operator) {
                case '>':
                case '<':
                case '>=':
                case '<=':
                    if (is_array($value)) {
                        return array(
                            false,
                            $notValidMess . ' Operator "' . $operator
                                . '" do not support array as parameter.'
                                . ' Error in key "' . $field . '".',
                        );
                    }
                    break;
                case 'd':
                    if (!is_array($value) || count($value) !== 2) {
                        return array(
                            false,
                            $notValidMess . ' Operator "' . $operator
                                . '" supports only arrays consisting of two elements',
                        );
                    }
                    break;
            }

            if (is_array($value)) {
                foreach ($value as $arrEl) {
                    if (is_array($arrEl)) {
                        return array(
                            false,
                            $notValidMess . ' Array can not contain an array.'
                                . ' Error in key "' . $field . '".',
                        );
                    }
                }
            }
        }

        return array(
            true,
            'Search array is valid',
        );
    }

    /**
     * Get query string from array consist of rules and parameters
     *
     * @param array $searchArray
     * @param string $entityName
     *
     * @return QueryBuilder
     */
    private function makeQueryBuilder(
        $entityName,
        $searchArray = array(),
        $selectableFields = array()
    ) {
        $pref = 'o';
        $format = '%s.%s %s %s';
        $format2 = '%s.%s %s(:%s)';
        $format3 = '%s.%s %s';

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();

        if ($selectableFields) {
            $selectableFields = array_values($selectableFields);
            $sf = &$selectableFields;

            foreach ($sf as &$v) {
                $v = sprintf('%s.%s', $pref, $v);
            }

            $selectable = $sf;
            unset($v, $sf);
        } else {
            $selectable = $pref;
        }

        $qb->select($selectable)->from($entityName, $pref);

        unset($selectableFields, $selectable);

        if (!$searchArray) {
            return $qb;
        }

        $refactorValue = function (&$val) {
            $formatDate = function (&$x) {
                if (
                    !is_int($x) &&
                    preg_match('/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/', $x)
                ) {
                    $x = new \DateTime($x);
                    $x = $x->format('Y-m-d H:i:s');
                }
            };

            $escaping = function (&$y) {
                if (preg_match('/\s/', $y) || !preg_match('/^\d+$/', $y)) {
                    $y = '\'' . $y . '\'';
                }
            };

            if (is_array($val)) {
                $val = array_values($val);

                foreach ($val as &$v) {
                    $formatDate($v);
                }
            } else {
                $formatDate($val);
                $escaping($val);
            }
        };

        $firstTermInclude = false;

        foreach ($searchArray as $field => $params) {
            if (
                !array_key_exists('operator', $params) ||
                !array_key_exists('value', $params)
            ) {
                unset($searchArray[$field]);
                continue;
            }

            $value = $params['value'];
            $value = is_array($value) ? $value : trim($value);

            if (!$value) {
                unset($searchArray[$field]);
                continue;
            }

            $operator = trim($params['operator']);

            if (array_key_exists('include_null', $params)) {
                $includeNull = (bool)$params['include_null'];
            } else {
                $includeNull = false;
            }

            $refactorValue($value);

            $operatorWasFound = true;

            switch ($operator) {
                case '=':
                case '!=':
                    if (is_array($value)) {
                        $fieldMark = $field . uniqid();

                        if ($operator === '=') {
                            $customOper = 'IN';
                        } else {
                            $customOper = 'NOT IN';
                        }

                        if (!$firstTermInclude) {
                            $qb->where(sprintf($format2, $pref, $field, $customOper, $fieldMark));
                            $firstTermInclude = true;
                        } else {
                            $qb->andWhere(sprintf($format2, $pref, $field, $customOper, $fieldMark));
                        }

                        $qb->setParameter($fieldMark, array_values($value));
                    } else {
                        if (!$firstTermInclude) {
                            $qb->where(sprintf($format, $pref, $field, $operator, $value));
                            $firstTermInclude = true;
                        } else {
                            $qb->andWhere(sprintf($format, $pref, $field, $operator, $value));
                        }
                    }
                    break;
                case '>':
                case '<':
                case '>=':
                case '<=':
                    if (!is_array($value)) {
                        if (!$firstTermInclude) {
                            $qb->where(sprintf($format, $pref, $field, $operator, $value));
                            $firstTermInclude = true;
                        } else {
                            $qb->andWhere(sprintf($format, $pref, $field, $operator, $value));
                        }
                    }
                    break;
                case 'd':
                    if (is_array($value)) {
                        $value = array_values($value);

                        if (count($value) === 2) {
                            if (!$firstTermInclude) {
                                $qb->where(sprintf($format, $pref, $field, '>=', reset($value)));
                            } else {
                                $qb->andWhere(sprintf($format, $pref, $field, '>=', reset($value)));
                            }
                            $qb->andWhere(sprintf($format, $pref, $field, '<=', end($value)));
                        }
                    }
                    break;
                default:
                    $operatorWasFound = false;
            }

            if ($operatorWasFound) {
                if ($includeNull) {
                    $qb->orWhere(sprintf($format3, $pref, $field, 'IS NULL'));
                }
            }

            unset($searchArray[$field]);
        }

        return $qb;
    }
}
