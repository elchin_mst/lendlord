<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class ContractCooperationController
 *
 * @Route("/api/contract_cooperation")
 */
class ContractCooperationController extends BaseRestApiController
{
    const LIMIT = 30;

    /**
     * Get contract cooperation list
     *
     * @Rest\Get("/list")
     */
    public function indexAction(Request $request)
    {
        if ((int) $request->get('page') && $request->get('page') > 0) {
            $currentPage = (int) $request->get('page');
        } else {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * self::LIMIT;

        $contractsCooperation = $this->getDoctrine()->getRepository('AppBundle:ContractCooperation')->findBy(
            array(), array('id' => 'ASC'), self::LIMIT, $offset
        );

        return $this->createApiViewResponse($contractsCooperation, 'List', 200);
    }

    /**
     * Get contract exclusive info
     *
     * @Rest\Get("/view/{id}", name="get_contract_cooperation", requirements={"id": "\d+"})
     */
    public function viewAction($id)
    {
        $contractCooperation = $this->getDoctrine()->getRepository('AppBundle:ContractCooperation')->find($id);

        return $this->createApiViewResponse($contractCooperation, 'Selected', 200);
    }

    /**
     * Add new record
     *
     * @Rest\Post("/add")
     */
    public function newAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\ContractCooperation', true);
    }

    /**
     * Edit record
     *
     * @Rest\Put("/edit")
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function editAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\ContractCooperation', true);
    }

    /**
     * Delete record
     *
     * @Rest\Delete("/remove/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $contractCooperation = $em->getRepository('AppBundle:ContractCooperation')->find($id);

        return $this->createApiDeleteResponse($contractCooperation);
    }
}
