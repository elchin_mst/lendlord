<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Office departament reference controller.
 *
 * @Route("/api/office_dep_ref")
 */
class OfficeDepartmentController extends BaseRestApiController
{
    /**
     * Get office department reference list
     *
     * @Rest\Get("/list")
     */
    public function indexAction(Request $request)
    {
        if ((int) $request->get('page') && $request->get('page') > 0) {
            $currentPage = (int) $request->get('page');
        } else {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * self::LIMIT;

        $officeDepRefs = $this->getDoctrine()->getRepository('AppBundle:OfficeDepartmentReference')->findBy(
            array(), array('id' => 'ASC'), self::LIMIT, $offset
        );

        return $this->createApiViewResponse($officeDepRefs, 'List', 200);
    }

    /**
     * Get office info
     *
     * @Rest\Get("/view/{id}", name="view_office_dep_ref", requirements={"id": "\d+"})
     */
    public function viewAction($id)
    {
        $officeDepRef = $this->getDoctrine()->getRepository('AppBundle:OfficeDepartmentReference')->find($id);

        return $this->createApiViewResponse($officeDepRef, 'Selected', 200);
    }

    /**
     * Get office department reference
     *
     * @Rest\Post("/get", name="get_office_dep_ref")
     */
    public function getAction(Request $request)
    {
        $content = $request->getContent();

        $data = json_decode($content, true) ? json_decode($content, true) : array();

        unset($content);

        if (array_key_exists('id', $data)) {
            $officeDepRef = $this->getDoctrine()
                ->getRepository('AppBundle:OfficeDepartmentReference')->find($data['id']);
        } else {
            $keys = array(
                'id',
                'head',
                'office',
                'department',
            );

            $data = array_filter(
                $data,
                function($v, $k) use ($keys) {
                    return (!empty($v) && in_array($k, $keys));
                },
                ARRAY_FILTER_USE_BOTH
            );

            $officeDepRef = $this->getDoctrine()
                ->getRepository('AppBundle:OfficeDepartmentReference')->findBy($data);
        }

        return $this->createApiViewResponse($officeDepRef, 'Selected', 200);
    }

    /**
     * Add new record
     *
     * @Rest\Post("/add")
     */
    public function newAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\OfficeDepartmentReference');
    }

    /**
     * Edit record
     *
     * @Rest\Put("/edit")
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function editAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\OfficeDepartmentReference');
    }

    /**
     * Delete record
     *
     * @Rest\Delete("/remove/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $officeDepRef = $em->getRepository('AppBundle:OfficeDepartmentReference')->find($id);

        return $this->createApiDeleteResponse($officeDepRef);
    }
}
