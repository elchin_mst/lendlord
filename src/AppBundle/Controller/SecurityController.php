<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authentication\Token\RememberMeToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

// TODO: take out all actions that are not part of security
// TODO: clear garbage!

class SecurityController extends Controller
{
    /**
     * login action
     *
     * action for users, return 200 status code if user login, 404 if username not found, 401 if authorization failed
     * @Rest\Post("/api/login", name="login")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function loginAction(Request $request)
    {
        $response = new JsonResponse();

        $jsonData = $request->getContent();
        $jsonData = json_decode($jsonData, true);

        if ($this->loginDataValid($jsonData)) {

            $userEmail = $jsonData['email'];
            $password = $jsonData['password'];
            $remember = $jsonData['remember_me'];

            $user = $this->getDoctrine()
                ->getRepository('AppBundle:User')
                ->findOneBy(
                    [
                        'email' => $userEmail,
                        'isActive' => true,
                    ]
                );

            if (!$user) {
                $response->setData(array('error' => $this->getParameter('not_user')));
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
            } else {
                $factory = $this->get('security.encoder_factory');
                $encoder = $factory->getEncoder($user);
                $passwordEncode = $encoder->encodePassword($password, $user->getSalt());

                if ($user->getPassword() === $passwordEncode) {

                    $response->setStatusCode(Response::HTTP_OK);
                    $providerKey = $user->getUsername();

                    if (!$remember) {
                        $token = new UsernamePasswordToken($user, $user->getPassword(), 'public', $user->getRoles());
                        $this->container->get('security.token_storage')->setToken($token);
                    } else {

                        $key = $this->getParameter('secret');
                        $token = new RememberMeToken($user, $providerKey, $key, $user->getRoles());
                        $this->get('security.token_storage')->setToken($token);
                    }

                    //------------- now dispatch the login event

                    $event = new InteractiveLoginEvent($request, $token);
                    $this->get('event_dispatcher')->dispatch('security.interactive_login', $event);

                    $response->setData($this->get('app.getUserInfo')->getUserInfo());
                } else {
                    $response->setData(array('error' => $this->getParameter('not_user')));
                    $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
                }
            }

            return $response;
        }

        $response->setStatusCode(Response::HTTP_UNAUTHORIZED);

        return $response;
    }

    /**
     * @param Response $response
     * @Rest\Post("/api/login_check", name="login_check")
     *
     * @return Response
     */

    public function LoginCheckAction(Response $response)
    {
        return $response;
    }

    /**
     * This is the route the user can use to logout.
     *
     * But, this will never be executed. Symfony will intercept this first
     * and handle the logout automatically. See logout in app/config/security.yml
     *
     * @Rest\Get("/api/logout", name="security_logout")
     *
     * @return JsonResponse
     */
    public function logoutAction()
    {
        $this->get('security.token_storage')->setToken(null);
        $this->get('session')->clear();
        $this->get('request_stack')->getCurrentRequest()->getSession()->invalidate();

        $token = $this->get('security.token_storage')->getToken();

        $response = new JsonResponse();

        if (null === $token) {
            $response->setData(array('authorize' => false));
        } else {
            $response->setData(array('authorize' => true));
        }

        return $response;
    }

    /**
     * @Rest\Get("/api/user_info", name="user_info")
     */
    public function getUserInfoAction(Request $request)
    {
        $jsonResponse = new JsonResponse($this->get('app.getUserInfo')->getUserInfo());

        return $jsonResponse;
    }

    /**
     * @Route("/api/role_permissions", name="role_permissions")
     */
    public function getUserRolePermissionsAction(Request $request)
    {
        $roleId = $request->get('id');

        $response = new JsonResponse();
        $response->setData($this->get('app.getUserInfo')->getRolePermission($roleId));

        return $response;
    }

    /**
     * Validation login data
     *
     * @param array $data
     *
     * @return bool
     */
    private function loginDataValid($data)
    {
        $keys = [
            'email',
            'password',
            'remember_me',
        ];

        foreach ($keys as $key) {
            if (!array_key_exists($key, $data)) {
                return false;
            }
        }

        if (!$data['email']) {
            return false;
        }

        return true;
    }

    /**
     * @Route("/admin/login", name="admin_login")
     */
    public function loginAdminAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'default/login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error' => $error,
            )
        );
    }

    /**
     * @Route("/admin/login_check", name="admin_login_check")
     */
    public function loginAdminCheckAction(Response $response)
    {
        return $response;
    }

    /**
     * @Route("/admin/logout", name="security_logout_admin")
     */
    public function logoutAdminAction()
    {
        throw new \Exception('This should never be reached!');
    }
}
