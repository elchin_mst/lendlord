<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class DealController
 *
 * @Route("/api/deal")
 */
class DealController extends BaseRestApiController
{
    const LIMIT = 30;

    /**
     * Get deal list
     *
     * @Rest\Get("/list")
     */
    public function indexAction(Request $request)
    {
        if ((int) $request->get('page') && $request->get('page') > 0) {
            $currentPage = (int) $request->get('page');
        } else {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * self::LIMIT;

        $deals = $this->getDoctrine()->getRepository('AppBundle:Deal')->findBy(
            array(), array('id' => 'ASC'), self::LIMIT, $offset
        );

        return $this->createApiViewResponse($deals, 'List', 200);
    }

    /**
     * Get deal info
     *
     * @Rest\Get("/view/{id}", name="get_deal", requirements={"id": "\d+"})
     */
    public function viewAction($id)
    {
        $deal = $this->getDoctrine()->getRepository('AppBundle:Deal')->find($id);

        return $this->createApiViewResponse($deal, 'Selected', 200);
    }

    /**
     * Add new record
     *
     * @Rest\Post("/add")
     */
    public function newAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Deal');
    }

    /**
     * Edit record
     *
     * @Rest\Put("/edit")
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function editAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Deal');
    }

    /**
     * Delete record
     *
     * @Rest\Delete("/remove/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $deal = $em->getRepository('AppBundle:Deal')->find($id);

        return $this->createApiDeleteResponse($deal);
    }
}
