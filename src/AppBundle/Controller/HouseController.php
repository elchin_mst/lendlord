<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * House controller.
 *
 * @Route("/api/house")
 */
class HouseController extends BaseRestApiController
{
    const LIMIT = 30;

    private static $availableSearchFields = array(
        'without-validation',
    );

    private static $selectableFields = array();

    /**
     * Get entity list
     *
     * @Rest\Get("/list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function indexAction(Request $request)
    {
        if ((int)$request->get('page') && $request->get('page') > 0) {
            $currentPage = (int)$request->get('page');
        } else {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * self::LIMIT;

        $search = $this->getSearchArray(
            $request->get('search'),
            self::$availableSearchFields
        );

        $houses = $this->getDoctrine()->getRepository('AppBundle:House')->findBy(
            $search, array('id' => 'ASC'), self::LIMIT, $offset
        );

        return $this->createApiViewResponse($houses, 'List', 200);
    }

    /**
     * Get filtered list
     *
     * @Rest\Post("/search")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function searchAction(Request $request)
    {
        return $this->createApiSearchResponse(
            $request,
            'AppBundle\Entity\House',
            'List',
            self::$availableSearchFields,
            self::$selectableFields
        );
    }

    /**
     * Get entity info
     *
     * @Rest\Get("/view/{id}", name="get_house", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function viewAction($id)
    {
        $house = $this->getDoctrine()->getRepository('AppBundle:House')->find($id);

        return $this->createApiViewResponse($house, 'Selected', 200);
    }

    /**
     * Add new record
     *
     * @Rest\Post("/add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function newAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\House');
    }

    /**
     * Edit record
     *
     * @Rest\Put("/edit")
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function editAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\House');
    }

    /**
     * Delete record
     *
     * @Rest\Delete("/remove/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $apartment = $em->getRepository('AppBundle:House')->find($id);

        return $this->createApiDeleteResponse($apartment);
    }
}
