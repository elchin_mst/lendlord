<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializationContext;

class DefaultController extends Controller
{
    /**
     * @Route("/admin/test", name="homepage")
     */
    public function indexAction(Request $request)
    {

     $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();
        $serialize =  $this->get('jms_serializer') ;

        $context = new SerializationContext();
        $context->setSerializeNull(TRUE);
        $context->setGroups('Selected');
        $test= $serialize->serialize(
            $categories, 'json',$context
        );


        dump($test);
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
        ]);
    }
}
