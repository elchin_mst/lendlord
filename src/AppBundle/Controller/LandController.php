<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Land controller.
 *
 * @Route("/api/land")
 */
class LandController extends BaseRestApiController
{
    const LIMIT = 30;

    private static $availableSearchFields = array(
        'without-validation',
    );

    private static $selectableFields = array();

    /**
     * Get entity list
     *
     * @Rest\Get("/list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function indexAction(Request $request)
    {
        if ((int)$request->get('page') && $request->get('page') > 0) {
            $currentPage = (int)$request->get('page');
        } else {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * self::LIMIT;

        $search = $this->getSearchArray(
            $request->get('search'),
            self::$availableSearchFields
        );

        $lands = $this->getDoctrine()->getRepository('AppBundle:Land')->findBy(
            $search, array('id' => 'ASC'), self::LIMIT, $offset
        );

        return $this->createApiViewResponse($lands, 'List', 200);
    }

    /**
     * Get filtered list
     *
     * @Rest\Post("/search")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function searchAction(Request $request)
    {
        return $this->createApiSearchResponse(
            $request,
            'AppBundle\Entity\Land',
            'List',
            self::$availableSearchFields,
            self::$selectableFields
        );
    }

    /**
     * Get entity info
     *
     * @Rest\Get("/view/{id}", name="get_land", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function viewAction($id)
    {
        $land = $this->getDoctrine()->getRepository('AppBundle:Land')->find($id);

        return $this->createApiViewResponse($land, 'Selected', 200);
    }

    /**
     * Add new record
     *
     * @Rest\Post("/add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function newAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Land');
    }

    /**
     * Edit record
     *
     * @Rest\Put("/edit")
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function editAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Land');
    }

    /**
     * Delete record
     *
     * @Rest\Delete("/remove/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $land = $em->getRepository('AppBundle:Land')->find($id);

        return $this->createApiDeleteResponse($land);
    }
}
