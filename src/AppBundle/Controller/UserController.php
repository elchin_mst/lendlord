<?php

namespace AppBundle\Controller;

use DoctrineTest\InstantiatorTestAsset\ExceptionAsset;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializationContext;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * User controller.
 *
 * @Route("/api/user")
 */

// TODO: 1. maybe there is methods for 1) another way add Context - Group 2) automatic add name for retirned array("name" => value)
// TODO: 2. write tests! + it is likely that the success of processing depends on the order of variables to test this further !
// TODO: 3. password handling
// TODO: 4. maybe there is more clear api pagination solutions?

class UserController extends BaseRestApiController
{
    const LIMIT = 30;

    private static $availableSearchFields = array(
        'isFired',
        'username',
        'email',
        'isActive',
        'mobilePhone',
        'homePhone',
    );

    /**
     * We need get all worked and dismissed users
     *
     * @Rest\Get("/list")
     */
    public function indexAction(Request $request)
    {
        if ((int)$request->get('page') && $request->get('page') > 0) {
            $currentPage = (int)$request->get('page');
        } else {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * self::LIMIT;

        $search = $this->getSearchArray(
            $request->get('search'),
            self::$availableSearchFields
        );

        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findBy(
            $search, array(), self::LIMIT, $offset
        );

        return $this->createApiViewResponse($users, 'List', 200);
    }

    /**
     * Get info about user
     *
     * @Rest\Get("/view/{id}")
     */
    public function viewAction($id)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);

        return $this->createApiViewResponse($user, 'Selected', 200);
    }

    /**
     * Add new record
     *
     * @Rest\Post("/add")
     */
    public function newAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\User', true);
    }

    /**
     * Edit record
     *
     * @Rest\Put("/edit")
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function editAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\User');
    }

    /**
     * Delete record
     *
     * @Rest\Delete("/remove/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);

        return $this->createApiDeleteResponse($user);
    }
}
