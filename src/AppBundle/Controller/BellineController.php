<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Client;
use AppBundle\Entity\Deal;
use AppBundle\Entity\IpBeeline;
use AppBundle\Entity\Note;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;


/**
 * @Route("/api/beeline")
 *
 *
 */
class BellineController extends Controller
{
    /**
     * @Rest\Get("/event")
     *
     * @return Response
     */
    public function indexAction()
    {
        $link = 'http://landlord.gamma.pve.61s.ru//api/beeline/recbee';
        $number = '9614275140';

        $serverResponse = $this->SubEvents($link, $number, 3600);

        return new Response();
    }

    /**
     * @param $link
     * @param $number
     * @param $duration
     *
     * @return mixed
     */
    public function SubEvents($link, $number, $duration)
    {
        $url = 'https://cloudpbx.beeline.ru/apis/portal/subscription';
        $header1 = 'X-MPBX-API-AUTH-TOKEN: c41b8118-948e-422a-9f63-8a429abbf6c5';
        $header2 = 'Content-Type: application/json';
        $date = json_encode(array
        (
            "pattern" => $number,
            "expires" => $duration,
            "subscriptionType" => 'BASIC_CALL',
            "url" => $link,
        ), true
        );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                $header1,
                $header2,
            )
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $date);
        $status = json_decode(curl_exec($ch), true);

        curl_close($ch);
        return $status;


    }

    /**
     * @param $url
     * @param $login
     * @param $passwd
     */
    public function loginLandlord($url, $login, $passwd)
    {
        $ch = curl_init();
        $data = json_encode(array(
            "email" => $login,
            "password" => $passwd,
            "remember_me" => '1',
        ));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        file_put_contents('/home/el/log/log3', curl_exec($ch));
        curl_close($ch);

    }

    /**
     * @Rest\Post("/recbee")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function recAction(Request $request)
    {
        $xmlRequest = $request->getContent();
        $idCall = $this->getIdCall($xmlRequest);
        $beelinePhone = '9614275140';
        $beelinePhone = $this->nuberFormat($beelinePhone);
        try {
            preg_match('/<xsi:eventData.*type="xsi:(.+)".*xsi:eventData>/U', $xmlRequest, $typeStr);
            try {
                if (!empty($typeStr) && $typeStr[1] == 'CallReleasedEvent') {
                    $callDuration = $this->duration($xmlRequest);
                } elseif (!empty($typeStr) && $typeStr[1] == 'CallOriginatedEvent') {
                    $direction = 'out';
                } elseif (!empty($typeStr) && $typeStr[1] == 'CallReceivedEvent') {
                    $direction = 'in';
                } elseif (!empty($typeStr) && $typeStr[1] == '') {

                }
            } catch (Exception $e) {
                var_dump($e->getMessage());
            }
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }

        if (empty($direction)) {
            if (!empty($callDuration)) {
                preg_match('/<xsi:address countryCode="7">tel:+.*(\d+)<\/xsi:address>/U', $xmlRequest, $phone);
                $field = $this->getDoctrine()->getRepository('AppBundle:IpBeeline')->find($idCall);
                $directionCall = $field->getDirection();
                $callData = array(
                        'phone' => $this->nuberFormat($phone[1]),
                        'direction' => $directionCall,
                        'call_duration' => $callDuration,
                    );
                $contactPhone = $this->nuberFormat(substr($phone[1], 1));
                //beelinePhone тестовая заглушка
                $contactAndUser = $this->findContactAndUser($beelinePhone, $contactPhone);


                if ($contactAndUser !== false){
                    $this->addNote($contactAndUser, $callData);
                    file_put_contents('/home/el/log/log', json_encode($callData));
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($field);
                    $em->flush();
                }
            }
        } else {
            $field = $this->getDoctrine()->getRepository('AppBundle:IpBeeline')->find($idCall);
            $field->setDirection($direction);
            $em = $this->getDoctrine()->getManager();
            $em->persist($field);
            $em->flush();
        }

        return new Response();
    }

    /**
     * @param $xmlRequest
     *
     * @return float|int
     */
    public function duration($xmlRequest)
    {
        preg_match('/<xsi:answerTime>(\d+)<\/xsi:answerTime>/U', $xmlRequest, $startTime);
        preg_match('/<xsi:releaseTime>(\d+)<\/xsi:releaseTime>/U', $xmlRequest, $endTime);
        if (empty($startTime)) {
            $callDuration = 'пропущенный вызов';
        } else {
            $callDuration = ($endTime[1] - $startTime[1]) / 1000;
        }//file_put_contents('/home/el/log/log2', $callDuration . "\n" . $phone[1]);

        return $callDuration;
    }

    /**
     * @param $xmlRequest
     *
     * @return int
     */
    public function getIdCall($xmlRequest)
    {
        $object = new IpBeeline();
        preg_match('/<xsi:call><xsi:callId>callhalf-.*(\S+)<\/xsi:callId>/U', $xmlRequest, $idcall);
        if (!empty($idcall[1])) {
            $id = $idcall[1];

            if (!empty($id)) {
                $field = $this->getDoctrine()->getRepository('AppBundle:IpBeeline')->findOneBy(array('callId' => $id));
                if (!empty($field)) {
                    return $field->getId();
                } else {
                    $object->setCallId($id);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($object);
                    $em->flush();
                    return $object->getId();
                }
            }
        }
    }

    /**
     * @param $number
     *
     * @return string
     */
    public function nuberFormat($number){
        $newNumber = '+7 ' . substr($number, 0, 3) . ' ' .
            substr($number, 3, 3) . '-' . substr($number, 6, 2) . '-' .
            substr($number, 8, 2);
        return $newNumber;
    }

    public function findContactAndUser($userPhone, $contactPhone){

        $searchClient = array('mobilePhone' => $contactPhone);
        $searchUser = array('mobilePhone' => $userPhone);
        $clients = $this->getDoctrine()->getRepository('AppBundle:Client')->findBy(
            $searchClient, array()
        );
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findBy(
            $searchUser, array()
        );
        if (!empty($clients) && !empty($users)){
            return $cluser = array(
                'client' => reset($clients),
                'users' => reset($users),
            );
        } elseif (empty($clients)) {
            $client = new Client();
            $client->setFirstName('New');
            $client->setLastName('Call');
            $client->setMobilePhone($contactPhone);
            $client->setManager(reset($users));
            $em = $this->getDoctrine()->getManager();
            $em->persist($client);



            $deal = new Deal();
            $deal->setName('Новый входящий звонок');
            $deal->setManager(reset($users));
            $deal->setStatus(1);

            $em = $this->getDoctrine()->getManager();
            $em->persist($deal);
            $em->flush();

            $deal->addClient($client);
            $em = $this->getDoctrine()->getManager();
            $em->persist($deal);
            $em->flush();

            return $cluser = array(
                'client' => $client,
                'users' => reset($users),
            );
        } else {
            return false;
        }
    }

    public function addNote(array $contAndUsers, array $callData) {
        $note = new Note();
        $note->setText(implode(' ', $callData));
        $note->setType(1);
        $note->setClient($contAndUsers['client']);
        $em = $this->getDoctrine()->getManager();
        $em->persist($note);
        $em->flush();
    }

}