<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Unit controller.
 *
 * @Route("/api/unit")
 */
class UnitController extends BaseRestApiController
{
    const LIMIT = 30;

    /**
     * Get offices list
     *
     * @Rest\Get("/list")
     */
    public function indexAction(Request $request)
    {
        if ((int) $request->get('page') && $request->get('page') > 0) {
            $currentPage = (int) $request->get('page');
        } else {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * self::LIMIT;

        $units = $this->getDoctrine()->getRepository('AppBundle:Unit')->findBy(
            array(), array('id' => 'ASC'), self::LIMIT, $offset
        );

        return $this->createApiViewResponse($units, 'List', 200);
    }

    /**
     * Get unit info
     *
     * @Rest\Get("/view/{id}", name="get_unit", requirements={"id": "\d+"})
     */
    public function viewAction($id)
    {
        $unit = $this->getDoctrine()->getRepository('AppBundle:Unit')->find($id);

        return $this->createApiViewResponse($unit, 'Selected', 200);
    }

    /**
     * Add new record
     *
     * @Rest\Post("/add")
     */
    public function newAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Unit');
    }

    /**
     * Edit record
     *
     * @Rest\Put("/edit")
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function editAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Unit');
    }

    /**
     * Delete record
     *
     * @Rest\Delete("/remove/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $unit = $em->getRepository('AppBundle:Unit')->find($id);

        return $this->createApiDeleteResponse($unit);
    }
}
