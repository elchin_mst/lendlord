<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Permissions controller.
 *
 * @Route("/api/permissions")
 */
class PermissionsController extends BaseRestApiController
{
    const LIMIT = 30;

    /**
     * Get permissions list
     *
     * @Rest\Get("/list")
     */
    public function indexAction(Request $request)
    {
        if ((int) $request->get('page') && $request->get('page') > 0) {
            $currentPage = (int) $request->get('page');
        } else {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * self::LIMIT;

        $permissions = $this->getDoctrine()->getRepository('AppBundle:Permissions')->findBy(
            array(), array('id' => 'ASC'), self::LIMIT, $offset
        );

        return $this->createApiViewResponse($permissions, 'List', 200);
    }

    /**
     * Get permission info
     *
     * @Rest\Get("/view/{id}", name="get_permission", requirements={"id": "\d+"})
     */
    public function viewAction($id)
    {
        $permission = $this->getDoctrine()->getRepository('AppBundle:Permissions')->find($id);

        return $this->createApiViewResponse($permission, 'Selected', 200);
    }

    /**
     * Add new record
     *
     * @Rest\Post("/add")
     */
    public function newAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Permissions');
    }

    /**
     * Edit record
     *
     * @Rest\Put("/edit")
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function editAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Permissions');
    }

    /**
     * Delete record
     *
     * @Rest\Delete("/remove/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $permission = $em->getRepository('AppBundle:Permissions')->find($id);

        return $this->createApiDeleteResponse($permission);
    }
}
