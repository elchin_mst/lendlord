<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class ClientController
 *
 * @Route("/api/client")
 */
class ClientController extends BaseRestApiController
{
    const LIMIT = 30;

    private static $availableSearchFields = array(
        'mobilePhone',
        'workPhone',
        'email',
    );

    /**
     * Get client list
     *
     * @Rest\Get("/list")
     */
    public function indexAction(Request $request)
    {
        if ((int) $request->get('page') && $request->get('page') > 0) {
            $currentPage = (int) $request->get('page');
        } else {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * self::LIMIT;

        $search = $this->getSearchArray(
            $request->get('search'),
            self::$availableSearchFields
        );

        $clients = $this->getDoctrine()->getRepository('AppBundle:Client')->findBy(
            $search, array('id' => 'ASC'), self::LIMIT, $offset
        );

        return $this->createApiViewResponse($clients, 'List', 200);
    }

    /**
     * Get client info
     *
     * @Rest\Get("/view/{id}", name="get_client", requirements={"id": "\d+"})
     */
    public function viewAction($id)
    {
        $client = $this->getDoctrine()->getRepository('AppBundle:Client')->find($id);

        return $this->createApiViewResponse($client, 'Selected', 200);
    }

    /**
     * Add new record
     *
     * @Rest\Post("/add")
     */
    public function newAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Client');
    }

    /**
     * Edit record
     *
     * @Rest\Put("/edit")
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function editAction(Request $request)
    {
        return $this->createApiEditResponse($request, 'AppBundle\Entity\Client');
    }

    /**
     * Delete record
     *
     * @Rest\Delete("/remove/{id}", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('AppBundle:Client')->find($id);

        return $this->createApiDeleteResponse($client);
    }
}
