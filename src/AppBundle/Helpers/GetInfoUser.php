<?php
namespace AppBundle\Helpers;

use AppBundle\Entity\Role;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\SerializationContext;

/**
 * Class GetInfoUser
 *
 *  returns array of information about the currently logged on user
 *
 * @package AppBundle\Helpers
 */
//TODO: clear in future

class GetInfoUser
{
    private $container;
    private $em;
    private $router;
    private $request;
    private $logger;
    private $serializer;

    /**
     * GetInfoUser constructor.
     *
     * @param ContainerInterface $container
     * @param RequestStack $requestStack
     * @param EntityManager $em
     */
    public function __construct(ContainerInterface $container, RequestStack $requestStack, EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;
        $this->serializer = $this->container->get('jms_serializer');
        $this->request = $requestStack;
        $this->router = $container->get('router');
        $this->token = $this->container->get('security.token_storage')
            ->getToken();
        $this->logger = $this->container->get('logger');
    }

    public function getUserInfo()
    {
        if (null !== ($token = $this->token)) {
            // TODO: create service for generate context
            $context = new SerializationContext();
            $context->setSerializeNull(true);
            $context->setGroups(array('Login'));

            return
                json_decode(
                    $this->serializer->serialize(
                        $this->token->getUser(), 'json',
                        $context
                    )
                );

        } else {
            return array(
                'user' => array(
                    'authorize' => false,
                ),
            );
        }
    }

    public function getRolePermission($roleId)
    {
        return
            $this->getPermissionsCurrentRole($roleId);
    }

    /**
     * Get roles array
     *
     * @return array
     */
    private function getRolesArray()
    {
        $roles = array();
        $this->logger->info('count' . count($this->token->getUser()->getRolesCollection()));

        foreach ($this->token->getUser()->getRolesCollection() as $role) {
            $this->logger->info($role);
            $roles[] = $role->getId();
        }

        return $roles;
    }

    /**
     * Get permissions array
     *
     * @return array
     */
    private function getPermissions()
    {

        $permissions = array();

        foreach ($this->token->getUser()->getRolesCollection() as $role) {

            $permissions[$role->getRole()] = $this->getPermissionsCurrentRole($role->getId());
        }

        return $permissions;
    }

    // TODO: refactor with jms bundle later

    /**
     * @param int $roleId
     *
     * @return array
     */
    private function getPermissionsCurrentRole($roleId)
    {

        $permissions = [];
        $permissionsFromBD = $this->em->getRepository(
            'AppBundle:Permissions'
        )->getPermissionForRole($roleId);

        if (!empty($permissionsFromBD)) {
            foreach ($permissionsFromBD as $permission) {
                $permissions[$permission['moduleName']][] = $permission['action'];
            }
        } else {
            $permissions = [];
        }

        return $permissions;
    }
}
