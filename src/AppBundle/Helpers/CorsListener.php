<?php
namespace AppBundle\Helpers;

use AppBundle\Controller\TokenAuthenticatedController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

/**
 * Class CorsListener
 * add headers for all request (need for frontend working)
 * @package AppBundle\Helpers
 */
// TODO: close for a specific domain on production

class CorsListener
{
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $responseHeaders = $event->getResponse()->headers;

        //   $responseHeaders->set('Access-Control-Allow-Headers', 'origin, content-type, accept');
        //   $responseHeaders->set('Access-Control-Allow-Origin', '*');
        // $responseHeaders->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
    }
}