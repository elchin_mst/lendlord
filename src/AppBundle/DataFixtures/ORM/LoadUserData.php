<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'username' => 'admin',
                'email' => 'admin@mail.ru',
                'password' => 'P6rQqwTfl0r6v1nzV4YsHap32ePGS02b9I2b3FOlIAtYgkihoPHqP57ulRwb253gZbERR8nx9RHvehMoG4bnQg==',
                'isActive' => true,
                'LastPasswordChangeTime' => new \DateTime(),
                'role' => array('role1', 'role2', 'role4'),
                'adress' => 'Коломенский 10',
                'name' => 'Екатерина',
                'midleName' => 'Ивановна',
                'lastName' => 'Белых',
                'birthday' => new \DateTime('2 weeks ago'),
                'internalPhone' => '+7 915 744 11 11',
                'mobilePhone' => '+7 961 427-51-40',
                'isWorked' => true,
                'department' => 'department1',
                'position' => 'position1',
                'unit' => 'unit1',
                'category' => 'category1',
                'offices' => array('office1'),
            ],
            2 => [
                'username' => 'irina_abramova',
                'email' => 'abramova@landlord.ru',
                'password' => 'P6rQqwTfl0r6v1nzV4YsHap32ePGS02b9I2b3FOlIAtYgkihoPHqP57ulRwb253gZbERR8nx9RHvehMoG4bnQg==',
                'isActive' => true,
                'isWorked' => false,
                'LastPasswordChangeTime' => new \DateTime('2 weeks ago'),
                'role' => array(),
                'adress' => 'Ворошиловский 25',
                'name' => 'Ирина',
                'midleName' => 'Сергеевна',
                'lastName' => 'Абрамова',
                'birthday' => new \DateTime('10 weeks ago'),
                'internalPhone' => '+7 918 589-21-02',
                'mobilePhone' => '+7 918 589-21-02',
                'department' => null,
                'position' => 'position2',
                'unit' => null,
                'category' => 'category2',
                'offices' => array('office1'),
                'payment_terms' => 'до выполнения плана 1н%, при перевыполнении плана 2й%',
                'indicator_of_efficiency' => 'выполнение плана',
                'in_position_data' => null,
                'is_head_office' => true,
            ],
            3 => [
                'username' => 'albina_palhovskay',
                'email' => 'palhovskay@landlord.ru',
                'password' => 'P6rQqwTfl0r6v1nzV4YsHap32ePGS02b9I2b3FOlIAtYgkihoPHqP57ulRwb253gZbERR8nx9RHvehMoG4bnQg==',
                'isActive' => true,
                'isWorked' => false,
                'LastPasswordChangeTime' => new \DateTime('2 weeks ago'),
                'role' => array(),
                'adress' => 'Волкова 11',
                'name' => 'Альбина',
                'midleName' => 'Мухатбиновна',
                'lastName' => 'Палховская',
                'birthday' => new \DateTime('10 weeks ago'),
                'internalPhone' => '+7 918 589-21-02',
                'mobilePhone' => '+7 918 589-21-02',
                'department' => 'department1',
                'position' => 'position5',
                'unit' => null,
                'category' => 'category2',
                'offices' => array('office1'),
                'payment_terms' => '%',
                'indicator_of_efficiency' => null,
                'in_position_data' => null,
            ],
            4 => [
                'username' => 'nikolay_sushkin',
                'email' => 'sushkin@landlord.ru',
                'password' => 'P6rQqwTfl0r6v1nzV4YsHap32ePGS02b9I2b3FOlIAtYgkihoPHqP57ulRwb253gZbERR8nx9RHvehMoG4bnQg==',
                'isActive' => true,
                'isWorked' => false,
                'LastPasswordChangeTime' => new \DateTime('2 weeks ago'),
                'role' => array(),
                'adress' => 'Волкова 15',
                'name' => 'Николай',
                'midleName' => 'Валерьевич',
                'lastName' => 'Сушкин',
                'birthday' => new \DateTime('4 weeks ago'),
                'internalPhone' => '+7 925 744 11 13',
                'mobilePhone' => '+7 950 366-66-66',
                'department' => 'department1',
                'position' => 'position6',
                'unit' => 'unit1',
                'category' => 'category1',
                'offices' => array('office1'),
                'payment_terms' => '1-%/2й-%/3й-%',
                'indicator_of_efficiency' => '1',
                'in_position_data' => new \DateTime('2016-09-17'),
            ],
            5 => [
                'username' => 'tatiana_shihaleeva',
                'email' => 'shihaleeva@landlord.ru',
                'password' => 'P6rQqwTfl0r6v1nzV4YsHap32ePGS02b9I2b3FOlIAtYgkihoPHqP57ulRwb253gZbERR8nx9RHvehMoG4bnQg==',
                'isActive' => true,
                'isWorked' => false,
                'LastPasswordChangeTime' => new \DateTime('2 weeks ago'),
                'role' => array(),
                'adress' => 'Волкова 16',
                'name' => 'Татьяна',
                'midleName' => 'Евгеньевна',
                'lastName' => 'Шихалеева',
                'birthday' => new \DateTime('4 weeks ago'),
                'internalPhone' => '+7 950 522-22-07',
                'mobilePhone' => '+7 950 522-22-07',
                'department' => 'department1',
                'position' => 'position12',
                'unit' => 'unit1',
                'category' => null,
                'offices' => array('office1'),
                'payment_terms' => '1-%/2й-%/3й-%',
                'indicator_of_efficiency' => '1.2',
                'in_position_data' => new \DateTime('2016-10-01'),
            ],
            6 => [
                'username' => 'larisa_titova',
                'email' => 'titova@landlord.ru',
                'password' => 'P6rQqwTfl0r6v1nzV4YsHap32ePGS02b9I2b3FOlIAtYgkihoPHqP57ulRwb253gZbERR8nx9RHvehMoG4bnQg==',
                'isActive' => true,
                'isWorked' => false,
                'LastPasswordChangeTime' => new \DateTime('2 weeks ago'),
                'role' => array(),
                'adress' => 'Волкова 16',
                'name' => 'Лариса',
                'midleName' => 'Анатольевна',
                'lastName' => 'Титова',
                'birthday' => new \DateTime('4 weeks ago'),
                'internalPhone' => '+7 950 522-22-07',
                'mobilePhone' => '+7 950 522-22-07',
                'department' => 'department1',
                'position' => 'position13',
                'unit' => 'unit5',
                'category' => null,
                'offices' => array('office1'),
                'payment_terms' => '1-%/2й-%',
                'indicator_of_efficiency' => '0.5',
                'in_position_data' => new \DateTime('2016-10-01'),
            ],
            7 => [
                'username' => 'konstantin_shaposhnikov',
                'email' => 'shaposhnikov@landlord.ru',
                'password' => 'P6rQqwTfl0r6v1nzV4YsHap32ePGS02b9I2b3FOlIAtYgkihoPHqP57ulRwb253gZbERR8nx9RHvehMoG4bnQg==',
                'isActive' => true,
                'isWorked' => false,
                'LastPasswordChangeTime' => new \DateTime('2 weeks ago'),
                'role' => array(),
                'adress' => 'Волкова 16',
                'name' => 'Константин',
                'midleName' => 'Сергеевич',
                'lastName' => 'Шапошников',
                'birthday' => new \DateTime('4 weeks ago'),
                'internalPhone' => '+7 950 522-22-07',
                'mobilePhone' => '+7 950 522-22-07',
                'department' => 'department1',
                'position' => 'position13',
                'unit' => 'unit1',
                'category' => null,
                'offices' => array('office1'),
            ],
            8 => [
                'username' => 'nina_vodolagina',
                'email' => 'vodolagina@landlord.ru',
                'password' => 'P6rQqwTfl0r6v1nzV4YsHap32ePGS02b9I2b3FOlIAtYgkihoPHqP57ulRwb253gZbERR8nx9RHvehMoG4bnQg==',
                'isActive' => true,
                'isWorked' => false,
                'LastPasswordChangeTime' => new \DateTime('2 weeks ago'),
                'role' => array(),
                'adress' => 'Волкова 16',
                'name' => 'Нина',
                'midleName' => 'Александровна',
                'lastName' => 'Водолагина',
                'birthday' => new \DateTime('4 weeks ago'),
                'internalPhone' => '+7 950 522-22-07',
                'mobilePhone' => '+7 950 522-22-07',
                'department' => 'department1',
                'position' => 'position10',
                'unit' => 'unit1',
                'category' => null,
                'offices' => array('office1'),
            ],
            9 => [
                'username' => 'alexey_nikolaev',
                'email' => 'nikolaev@landlord.ru',
                'password' => 'P6rQqwTfl0r6v1nzV4YsHap32ePGS02b9I2b3FOlIAtYgkihoPHqP57ulRwb253gZbERR8nx9RHvehMoG4bnQg==',
                'isActive' => true,
                'isWorked' => false,
                'LastPasswordChangeTime' => new \DateTime('2 weeks ago'),
                'role' => array(),
                'adress' => 'Мечникова 3',
                'name' => 'Алексей',
                'midleName' => 'Валентинович',
                'lastName' => 'Николаев',
                'birthday' => new \DateTime('4 weeks ago'),
                'internalPhone' => '+7 950 522-22-07',
                'mobilePhone' => '+7 950 522-22-07',
                'department' => 'department1',
                'position' => 'position8',
                'unit' => 'unit2',
                'category' => null,
                'offices' => array('office1'),
            ],
            10 => [
                'username' => 'ludmila_zukova',
                'email' => 'zukova@landlord.ru',
                'password' => 'P6rQqwTfl0r6v1nzV4YsHap32ePGS02b9I2b3FOlIAtYgkihoPHqP57ulRwb253gZbERR8nx9RHvehMoG4bnQg==',
                'isActive' => true,
                'isWorked' => false,
                'LastPasswordChangeTime' => new \DateTime('2 weeks ago'),
                'role' => array(),
                'adress' => 'Волкова 16',
                'name' => 'Людмила',
                'midleName' => 'Александровна',
                'lastName' => 'Жукова',
                'birthday' => new \DateTime('4 weeks ago'),
                'internalPhone' => '+7 950 522-22-07',
                'mobilePhone' => '+7 950 522-22-07',
                'department' => 'department1',
                'position' => 'position9',
                'unit' => 'unit1',
                'category' => null,
                'offices' => array('office1'),
                'is_mentor' => true,
            ],
            11 => [
                'username' => 'irina_raskidaylova',
                'email' => 'raskidaylova@landlord.ru',
                'password' => 'P6rQqwTfl0r6v1nzV4YsHap32ePGS02b9I2b3FOlIAtYgkihoPHqP57ulRwb253gZbERR8nx9RHvehMoG4bnQg==',
                'isActive' => true,
                'isWorked' => false,
                'LastPasswordChangeTime' => new \DateTime('2 weeks ago'),
                'role' => array(),
                'adress' => 'Ашхабадский 16',
                'name' => 'Ирина',
                'midleName' => 'Николаевна',
                'lastName' => 'Раскидайлова',
                'birthday' => new \DateTime('4 weeks ago'),
                'internalPhone' => '+7 950 522-22-07',
                'mobilePhone' => '+7 950 522-22-07',
                'department' => 'department1',
                'position' => 'position7',
                'unit' => 'unit1',
                'category' => null,
                'offices' => array('office1'),
                'mentor' => 'usr10',
            ],

        ];

        foreach ($data as $key => $value) {
            $user = new User();
            $user->setUsername($value['username']);
            $user->setFirstName($value['name']);
            $user->setMiddleName($value['midleName']);
            $user->setLastName($value['lastName']);
            $user->setAddress($value['adress']);
            $user->setEmail($value['email']);
            $user->setBirthday($value['birthday']);
            $user->setIsFired($value['isWorked']);
            $user->setMobilePhone($value['mobilePhone']);
            $user->setInternalPhone($value['internalPhone']);
            $user->setPassword($value['password']);
            $user->setIsActive($value['isActive']);
            $user->setLastPasswordChangeTime($value['LastPasswordChangeTime']);

            foreach ($value['role'] as $role) {
                $user->addRole($this->getReference($role));
            }

            foreach ($value['offices'] as $office) {
                $user->addOffice($this->getReference($office));
            }

            if ($value['department']) {
                $user->setDepartment($this->getReference($value['department']));
            }
            if ($value['position']) {
                $user->setPosition($this->getReference($value['position']));
            }
            if ($value['category']) {
                $user->setCategory($this->getReference($value['category']));
            }
            if ($value['unit']) {
                $user->setUnit($this->getReference($value['unit']));
            }
            if (array_key_exists('mentor', $value)) {
                $user->setMentor($this->getReference($value['mentor']));
            }
            if (array_key_exists('is_head_office', $value) && $value['is_head_office']) {
                $user->setIsHeadOffice($value['is_head_office']);
            }
            if (array_key_exists('is_mentor', $value) && $value['is_mentor']) {
                $user->setIsMentor($value['is_mentor']);
            }
            if (array_key_exists('payment_terms', $value) && $value['payment_terms']) {
                $user->setPaymentTerms($value['payment_terms']);
            }
            if (array_key_exists('indicator_of_efficiency', $value) && $value['indicator_of_efficiency']) {
                $user->setIndicatorOfEfficiency($value['indicator_of_efficiency']);
            }
            if (array_key_exists('in_position_data', $value) && $value['in_position_data']) {
                $user->setInPositionDate($value['in_position_data']);
            }

            $manager->persist($user);
            $manager->flush();

            $this->addReference('usr' . $key, $user);
        }
    }

    public function getOrder()
    {
        return 8;
    }
}