<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Deal;

class LoadDealsData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'name' => '#919247',
                'manager' => 'usr1',
                'status' => 1,
                'clients' => array('client1', 'client2'),
            ],
            2 => [
                'name' => '#916543',
                'manager' => 'usr2',
                'status' => 2,
                'clients' => array('client2'),
            ],
            3 => [
                'name' => '#974824',
                'manager' => 'usr3',
                'status' => 3,
                'clients' => array('client3'),
            ],
            4 => [
                'name' => '#135183',
                'manager' => 'usr4',
                'status' => 1,
                'clients' => array('client1', 'client4'),
            ],
            5 => [
                'name' => '#497298',
                'manager' => 'usr5',
                'status' => 2,
                'clients' => array('client4'),
            ],
        ];

        foreach ($data as $key => $value) {
            $deal = new Deal();
            $deal->setName($value['name']);
            $deal->setManager($this->getReference($value['manager']));
            $deal->setStatus($value['status']);

            foreach ($value['clients'] as $k => $v) {
                $deal->addClient($this->getReference($v));
            }

            $manager->persist($deal);
            $manager->flush();

            $this->addReference('deal' . $key, $deal);
        }
    }

    public function getOrder()
    {
        return 13;
    }
}