<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Permissions;

class LoadPermissionsData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'moduleName' => 'Section1',
                'action' => 'all',
                'role' => array('role1'),
            ],
            2 => [
                'moduleName' => 'Section2',
                'action' => 'view',
                'role' => array('role2'),
            ],
            3 => [
                'moduleName' => 'Section3',
                'action' => 'delete',
                'role' => array('role3'),
            ],
        ];

        foreach ($data as $key => $value) {
            $permissions = new Permissions();
            $permissions->setModuleName($value['moduleName']);

            $permissions->setAction($value['action']);

            foreach ($value['role'] as $role) {
                $permissions->addRole($this->getReference($role));
            }

            $manager->persist($permissions);
            $manager->flush();
            $this->addReference('permissions' . $key, $permissions);
        }
    }

    public function getOrder()
    {
        return 3;
    }
}