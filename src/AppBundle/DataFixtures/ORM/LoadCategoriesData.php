<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Category;

class LoadCategoriesData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'name' => 'Продажа квартир',
                'position' => '1',
                'plan' => 3.5,
                'percent' => 14.8
            ],
            2 => [
                'name' => 'Продажа квартир',
                'position' => '2',
                'plan' => 4.5,
                'percent' => 11.8
            ],
            3 => [
                'name' => 'Продажа квартир',
                'position' => '3',
                'plan' => 5.6,
                'percent' => 11.8
            ],
            4 => [
                'name' => 'Аренда квартир',
                'position' => '1',
                'plan' => 5.6,
                'percent' => 10.8
            ],
            5 => [
                'name' => 'Аренда квартир',
                'position' => '2',
                'plan' => 5.6,
                'percent' => 10.8
            ],
            5 => [
                'name' => 'Аренда квартир',
                'position' => '3',
                'plan' => 5.6,
                'percent' => 10.8
            ],
            6 => [
                'name' => 'Аренда квартир',
                'position' => '4',
                'plan' => 5.6,
                'percent' => 9.8
            ],
            7 => [
                'name' => 'Аренда квартир',
                'position' => '5',
                'plan' => 5.6,
                'percent' => 5.8
            ],
            8 => [
                'name' => 'Продажа домов',
                'position' => '1',
                'plan' => 5.6,
                'percent' => 5.8
            ],
        ];

        foreach ($data as $key => $value) {
            $category = new Category();
            $category->setName($value['name']);
            $category->setPosition(
                $this->getReference('position' . $value['position'])
            );
            $category->setPlan($value['plan']);
            $category->setPercent($value['percent']);

            $manager->persist($category);
            $manager->flush();

            $this->addReference('category' . $key, $category);
        }
    }

    public function getOrder()
    {
        return 4;
    }
}