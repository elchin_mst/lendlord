<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Unit;

class LoadUnitUserReferenceData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $n = 6;

        for ($i = 1; $i <= $n; $i++) {
            $unit = $this->getReference('unit' . $i);

            $unit->setHead($this->getReference('usr' . $i));

            $manager->persist($unit);
            $manager->flush();
        }
    }

    public function getOrder()
    {
        return 10;
    }
}