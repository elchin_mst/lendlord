<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\House;

class LoadHousesData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objectArr = array (
            'city_fias_id' => array(
                'value' => array(
                    'c1cfe4b9-f7c2-423c-abfa-6ed1c05a15c5',
                    '3809afb6-fdfd-4115-9e55-236abf108c81',
                    '9bebf626-3ee7-4e1b-9e91-569c9d402152',
                ),
                'type' => 'string',
                'nullable' => false,
                'method' => 'setCityFiasId',
            ),
            'street_fias_id' => array(
                'value' => array(
                    'c1cfe4b9-f7c2-423c-abfa-6ed1c05a15c5' => array(
                        'f1dfd951-706d-403c-9cc8-0970947f9f10',
                        'd129b9dd-2323-49b6-a9f6-b34a4f84d2b0',
                        'f9e2c4f6-497f-45cf-a901-049f7cbb311d',
                        'c81e0fed-a2e3-40ee-80e7-cbb30d494abf',
                    ),
                    '3809afb6-fdfd-4115-9e55-236abf108c81' => array(
                        'e37376f1-ff6e-4fc5-8aee-869743049c85',
                    ),
                    '9bebf626-3ee7-4e1b-9e91-569c9d402152' => array(
                        'ff860f2e-d60a-4dac-9635-2aee15a884ac',
                    ),
                ),
                'type' => 'string',
                'nullable' => false,
                'method' => 'setStreetFiasId',
            ),
            'house_fias_id' => array(
                'value' => array(
                    'f1dfd951-706d-403c-9cc8-0970947f9f10' => array(
                        'bb98bcd7-cf79-4eaa-8fbe-9931eb798089',
                        '02bfafe9-ec95-4c44-b761-7f4c0f11e1f2',
                    ),
                    'd129b9dd-2323-49b6-a9f6-b34a4f84d2b0' => array(
                        '7eb7bcba-1067-41be-8048-839450e78c25',
                        '6364ddf6-2157-45f4-9538-9a4a224a7c5c',
                    ),
                    'f9e2c4f6-497f-45cf-a901-049f7cbb311d' => array(
                        '47353d55-19f1-469e-a406-713279e0e9a7',
                        '78cb93f1-8bd2-4028-bbdb-b0be6271f5a6',
                    ),
                    'c81e0fed-a2e3-40ee-80e7-cbb30d494abf' => array(
                        'd67a45c9-956d-49ab-aca8-707e78a6d43f',
                        'ef66471a-b19c-4060-aaf6-36f5eeecb9eb',
                    ),
                    'e37376f1-ff6e-4fc5-8aee-869743049c85' => array(
                        'fdbdb4f2-7c0c-48ee-b2f6-d2fc4e3c2556',
                    ),
                    'ff860f2e-d60a-4dac-9635-2aee15a884ac' => array(
                        '26dd1bb0-8580-42f8-bcca-a6648d4520fa',
                    ),
                ),
                'type' => 'string',
                'nullable' => false,
                'method' => 'setHouseFiasId',
            ),
            'district' => array(
                'value' => array(1, 8),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setDistrict',
            ),
            'source' => array(
                'value' => array(1, 6),
                'type' => 'obj',
                'nullable' => false,
                'prefix' => 'source',
                'method' => 'setSource',
            ),
            'agent' => array(
                'value' => array(1, 10),
                'type' => 'obj',
                'nullable' => false,
                'prefix' => 'usr',
                'method' => 'setAgent',
            ),
            'contacts' => array(
                'value' => array(1, 4),
                'type' => 'obj',
                'nullable' => false,
                'prefix' => 'client',
                'method' => 'addContact',
            ),

            'house_number' => array(
                'value' => 'uniqid',
                'type' => 'string',
                'nullable' => true,
                'prefix' => 'Номер дома',
                'method' => 'setHouseNumber',
            ),
            'block' => array(
                'value' => 'uniqid',
                'type' => 'string',
                'nullable' => true,
                'prefix' => 'Корпус',
                'method' => 'setBlock',
            ),
            'exclusive' => array(
                'value' => array(0, 4),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setExclusive',
            ),
            'location' => array(
                'value' => array(0, 7),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setLocation',
            ),
            'moderation_status' => array(
                'value' => array(0, 3),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setModerationStatus',
            ),
            'comment' => array(
                'value' => 'uniqid',
                'type' => 'string',
                'nullable' => true,
                'prefix' => 'Комментарий',
                'method' => 'setComment',
            ),
            'text_on_the_site' => array(
                'value' => 'uniqid',
                'type' => 'string',
                'nullable' => true,
                'prefix' => 'Текст на сайте',
                'method' => 'setTextOnTheSite',
            ),
            'open_sale' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setOpenSale',
            ),
            'net_sale' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setNetSale',
            ),
            'from_firm' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setFromFirm',
            ),
            'building_year' => array(
                'value' => array(1970, (int)date('Y')),
                'type' => 'int',
                'nullable' => true,
                'method' => 'setBuildingYear',
            ),
            'floors' => array(
                'value' => array(0, 3),
                'type' => 'int',
                'nullable' => true,
                'method' => 'setFloors',
            ),
            'rooms' => array(
                'value' => array(2, 8),
                'type' => 'int',
                'nullable' => true,
                'method' => 'setRooms',
            ),
            'owners_num' => array(
                'value' => array(1, 3),
                'type' => 'int',
                'nullable' => true,
                'method' => 'setOwnersNum',
            ),
            'underage_owners_num' => array(
                'value' => array(1, 3),
                'type' => 'int',
                'nullable' => true,
                'method' => 'setUnderageOwnersNum',
            ),
            'elite' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setElite',
            ),
            'garage' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setGarage',
            ),
            'has_yard' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setHasYard',
            ),
            'surveying' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setSurveying',
            ),
            'exit_to_pond' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setExitToPond',
            ),
            'distance_to_stations' => array(
                'value' => 'uniqid',
                'type' => 'string',
                'nullable' => true,
                'prefix' => 'Расстояние до остановок',
                'method' => 'setDistanceToStations',
            ),
            'ground_area' => array(
                'value' => array(1, 20),
                'type' => 'float',
                'nullable' => true,
                'method' => 'setGroundArea',
            ),
            'ceiling_height' => array(
                'value' => array(2, 5),
                'type' => 'float',
                'nullable' => true,
                'method' => 'setCeilingHeight',
            ),
            'roadway_width' => array(
                'value' => array(2, 5),
                'type' => 'float',
                'nullable' => true,
                'method' => 'setRoadwayWidth',
            ),
            'house_location' => array(
                'value' => array(0, 50),
                'type' => 'float',
                'nullable' => true,
                'method' => 'setHouseLocation',
                'priority' => 0,
            ),
            'price' => array(
                'value' => array(800000, 10000000),
                'type' => 'float',
                'nullable' => false,
                'method' => 'setPrice',
            ),
            'special_price' => array(
                'value' => array(800000, 10000000),
                'type' => 'float',
                'nullable' => false,
                'method' => 'setSpecialPrice',
            ),
            'completed_at' => array(
                'value' => array(1970, (int)date('Y')),
                'type' => 'datetime',
                'nullable' => true,
                'method' => 'setCompletedAt',
            ),
            'type' => array(
                'value' => array(1, 4),
                'type' => 'int',
                'nullable' => true,
                'method' => 'setType',
            ),
            'material' => array(
                'value' => array(1, 4),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setMaterial',
            ),
            'condition' => array(
                'value' => array(1, 6),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setCondition',
            ),
            'commodities' => array(
                'value' => array(1, 3),
                'type' => 'int',
                'nullable' => true,
                'method' => 'setCommodities',
            ),
            'roof_type' => array(
                'value' => array(1, 7),
                'type' => 'int',
                'nullable' => true,
                'method' => 'setRoofType',
            ),
            'floor_type' => array(
                'value' => array(1, 12),
                'type' => 'int',
                'nullable' => true,
                'method' => 'setFloorType',
            ),
            'bathroom' => array(
                'value' => array(1, 4),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setBathroom',
            ),
            'hot_water' => array(
                'value' => array(1, 7),
                'type' => 'int',
                'nullable' => true,
                'method' => 'setHotWater',
            ),
            'heating' => array(
                'value' => array(1, 6),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setHeating',
            ),
            'yard' => array(
                'value' => array(1, 4),
                'type' => 'int',
                'nullable' => true,
                'method' => 'setYard',
            ),
            'right_type_object' => array(
                'value' => array(1, 2),
                'type' => 'int',
                'nullable' => true,
                'method' => 'setRightTypeObject',
            ),
            'right_type_land'  => array(
                'value' => array(1, 3),
                'type' => 'int',
                'nullable' => true,
                'method' => 'setRightTypeLand',
            ),
            'entrace_type'  => array(
                'value' => array(1, 5),
                'type' => 'int',
                'nullable' => true,
                'method' => 'setEntranceType',
            ),
            'land_location' => array(
                'value' => array(1, 2),
                'type' => 'int',
                'nullable' => true,
                'method' => 'setLandLocation',
            ),
            'relief' => array(
                'value' => array(1, 3),
                'type' => 'int',
                'nullable' => true,
                'method' => 'setRelief',
            ),
            'has_cafes' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setHasCafes',
            ),
            'has_cinemas' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setHasCinemas',
            ),
            'has_hospitals' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setHasHospitals',
            ),
            'has_educational' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setHasEducational',
            ),
            'wooden_windows' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setWoodenWindows',
            ),
            'metal_plast_windows' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setMetalPlastWindows',
            ),
            'wooden_windows_eur' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setWoodenWindowsEur',
            ),
            'home_security_alarm' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setHomeSecurityAlarm',
            ),
            'land_security_alarm' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setLandSecurityAlarm',
            ),
            'home_fire_alarm' => array(
                'value' => array(0, 2),
                'type' => 'int',
                'nullable' => false,
                'method' => 'setHomeFireAlarm',
            ),
        );

        $n = 200;
        $i = 0;

        while ($i < $n) {
            $house = new House();

            $cityFiasKey = array_rand($objectArr['city_fias_id']['value']);
            $cityFiasId = $objectArr['city_fias_id']['value'][$cityFiasKey];
            $streetFiasKey = array_rand(
                $objectArr['street_fias_id']['value'][$cityFiasId]
            );
            $streetFiasId = $objectArr['street_fias_id']['value'][$cityFiasId][$streetFiasKey];
            $houseFiasKey = array_rand(
                $objectArr['house_fias_id']['value'][$streetFiasId]
            );
            $houseFiasId = $objectArr['house_fias_id']['value'][$streetFiasId][$houseFiasKey];

            $house->setCityFiasId($cityFiasId);
            $house->setStreetFiasId($streetFiasId);
            $house->setHouseFiasId($houseFiasId);

            unset(
                $cityFiasId,
                $streetFiasId,
                $houseFiasId,
                $cityFiasKey,
                $streetFiasKey,
                $houseFiasKey
            );

            $totalSq = mt_rand(17, 250);
            $livingSq = $totalSq * 0.01 * 80;
            $kitchenSq = $totalSq - $livingSq;

            $house->setTotalSq($totalSq);
            $house->setLivingSq($livingSq);
            $house->setKitchenSq($kitchenSq);

            unset($totalSq, $livingSq, $kitchenSq);

            $skipArr = array(
                'city_fias_id',
                'street_fias_id',
                'house_fias_id',
            );

            foreach ($objectArr as $field => $data) {
                if (in_array($field, $skipArr)) {
                    continue;
                }

                if (array_key_exists('nullable', $data) && $data['nullable']) {
                    $num = mt_rand(0, 100);
                    if ($num <= 20) {
                        continue;
                    }
                }

                $value = $data['value'];
                $type = $data['type'];
                $method = $data['method'];
                $valueForSet = null;

                if (array_key_exists('nullable', $data) && $data['nullable']) {
                    $num = mt_rand(0, 100);
                    if ($num <= 20) {
                        continue;
                    }
                }

                if (array_key_exists('priority', $data)) {
                    if (mt_rand(0, 1)) {
                        $priorityValue = $data['priority'];
                    }
                }

                switch ($type) {
                    case 'int':
                        if (is_array($value) && $value && reset($value) < end($value)) {
                            if (isset($priorityValue)) {
                                $valueForSet = $priorityValue;
                            } else {
                                $valueForSet = mt_rand(reset($value), end($value));
                            }
                        } else {
                            throw new \Exception('Not valid value in key "' . $field . '"');
                        }
                        break;
                    case 'float':
                        if (is_array($value) && $value && reset($value) < end($value)) {
                            if (isset($priorityValue)) {
                                $valueForSet = $priorityValue;
                            } else {
                                $valueForSet = mt_rand(reset($value), end($value));
                                $fCh = lcg_value();
                                if ($valueForSet === end($value)) {
                                    $valueForSet -= $fCh;
                                } else {
                                    $valueForSet += $fCh;
                                }
                                $valueForSet = (float)$valueForSet;
                            }
                        } else {
                            throw new \Exception('Not valid value in key "' . $field . '"');
                        }
                        break;
                    case 'string':
                        if (!is_string($value)) {
                            throw new \Exception('Not support value for "string"');
                        }
                        if (isset($priorityValue)) {
                            $valueForSet = $priorityValue;
                        } else {
                            if (array_key_exists('prefix', $data) && $data['prefix']) {
                                $valueForSet = $data['prefix'] . ' ' . uniqid();
                            } else {
                                throw new \Exception('Prefix not set for field ' . $field);
                            }
                        }
                        break;
                    case 'obj':
                        if (!is_array($value)) {
                            throw new \Exception('Not support value for "obj"');
                        }
                        if (isset($priorityValue)) {
                            $valueForSet = $this->getReference($priorityValue);
                        } else {
                            if (array_key_exists('prefix', $data) && $data['prefix']) {
                                $rand = mt_rand(reset($value), end($value));
                                $valueForSet = $this->getReference($data['prefix'] . $rand);
                            } else {
                                throw new \Exception('Prefix not set for field ' . $field);
                            }
                        }
                        break;
                    case 'datetime':
                        if (!is_array($value)) {
                            throw new \Exception('Not support value for "datetime"');
                        }
                        if (isset($priorityValue)) {
                            $valueForSet = $priorityValue;
                        } else {
                            $valueForSet = mt_rand(reset($value), end($value));
                        }
                        if (is_int($valueForSet)) {
                            $valueForSet = new \DateTime(
                                sprintf('%s-01-01', $valueForSet)
                            );
                        } elseif (is_string($valueForSet)) {
                            $valueForSet = new \DateTime($valueForSet);
                        }
                        break;
                    default:
                        throw new \Exception('Could not determine the data type');
                }

                $house->$method($valueForSet);

                unset($priorityValue);
            }

            $manager->persist($house);
            $manager->flush();

            $i++;

            $this->addReference('house' . $i, $house);
        }
    }

    public function getOrder()
    {
        return 18;
    }
}