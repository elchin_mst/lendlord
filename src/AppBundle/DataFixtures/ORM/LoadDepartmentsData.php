<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Department;

class LoadDepartmentsData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'name' => 'Отдел подаж',
            ],
            2 => [
                'name' => 'Отдел маркетинга',
            ],
            3 => [
                'name' => 'Бухгалтерия',
            ],
            4 => [
                'name' => 'Финансовый отдел',
            ],
        ];

        foreach ($data as $key => $value) {
            $department = new Department();
            $department->setName($value['name']);


            $manager->persist($department);
            $manager->flush();

            $this->addReference('department' . $key, $department);
        }
    }

    public function getOrder()
    {
        return 6;
    }
}