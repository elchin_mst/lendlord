<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\OfficeDepartmentReference;

class LoadOfficeDepartmentData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'head' => 'usr1',
                'office' => 'office1',
                'department' => 'department1',
                'plan' => 5.6,
            ],
            2 => [
                'head' => 'usr2',
                'office' => 'office2',
                'department' => 'department2',
                'plan' => 5.9,
            ],
            3 => [
                'head' => 'usr3',
                'office' => 'office3',
                'department' => 'department3',
                'plan' => 4.0,
            ],
            4 => [
                'head' => 'usr4',
                'office' => 'office2',
                'department' => 'department1',
                'plan' => 7.8,
            ],
        ];

        foreach ($data as $key => $value) {
            $offDepRef = new OfficeDepartmentReference();
            $offDepRef->setHead($this->getReference($value['head']));
            $offDepRef->setOffice($this->getReference($value['office']));
            $offDepRef->setDepartment($this->getReference($value['department']));
            $offDepRef->setPlan($value['plan']);

            $manager->persist($offDepRef);
            $manager->flush();

            $this->addReference('offDepRef' . $key, $offDepRef);
        }
    }

    public function getOrder()
    {
        return 9;
    }
}