<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Office;

class LoadOfficesData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'name' => 'Центральный',
                'address' => 'ул. Соколова 80',
                'phone' => '+7 863 291 0910',
                'email' => 'centre@landlord.ru',
                'plan' => 5.6,

            ],
            2 => [
                'name' => 'Нахичеванский',
                'address' => 'ул. Советская 56',
                'phone' => '+7 988 255 5240',
                'email' => 'sovet@landlord.ru',
                'plan' => 5.9,
            ],
            3 => [
                'name' => 'Офис в ЗЖМ',
                'address' => 'ул. Зорге 72',
                'phone' => '+7 863 290 1901',
                'email' => 'west@landlord.ru',
                'plan' => 8.6,
            ],
            4 => [
                'name' => 'Офис в СЖМ',
                'address' => 'пр. Космонавтов 15',
                'phone' => '+7 863 292 6426',
                'email' => 'north@landlord.ru',
                'plan' => 7.8,
            ],
            5 => [
                'name' => 'Офис на Варфоломеева',
                'address' => 'ул. Варфоломеева 227',
                'phone' => '+7 863 299 0990',
                'email' => 'var@landlord.ru',
                'plan' => 7.3,
            ],
            6 => [
                'name' => 'Офис на Военведе',
                'address' => 'ул. Таганрогская 133/1',
                'phone' => '+7 863 290 8908',
                'email' => 'voenved@landlord.ru',
                'plan' => 9.8,
            ],
            7 => [
                'name' => 'Офис на Чкаловском',
                'address' => 'ул. Вятская 41',
                'phone' => '+7 863 250 7507',
                'email' => 'chkal@landlord.ru',
                'plan' => 6.8,
            ],
        ];

        foreach ($data as $key => $value) {
            $office = new Office();
            $office->setName($value['name']);
            $office->setAddress($value['address']);
            $office->setPhone($value['phone']);
            $office->setEmail($value['email']);
            $office->setPlan($value['plan']);

            $manager->persist($office);
            $manager->flush();

            $this->addReference('office' . $key, $office);
        }
    }

    public function getOrder()
    {
        return 5;
    }
}