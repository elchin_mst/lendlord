<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractView;

class LoadContractViewData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'number' => 'НДП/3223-01',
                'agent' => 'usr1',
                'client' => 'client1',
                'deal' => 'deal1',
                'apartment_sale' => array('apartment_sale1'),
                'apartment_rent' => array(),
                'house' => array(),
                'land' => array(),
                'commercial_property' => array(),
            ],
            2 => [
                'number' => 'НДП/3223-02',
                'agent' => 'usr2',
                'client' => 'client1',
                'deal' => 'deal2',
                'apartment_sale' => array('apartment_sale2', 'apartment_sale3'),
                'apartment_rent' => array(),
                'house' => array(),
                'land' => array(),
                'commercial_property' => array(),
            ],
            3 => [
                'number' => 'НДП/3223-03',
                'agent' => 'usr3',
                'client' => 'client4',
                'deal' => 'deal3',
                'apartment_sale' => array(),
                'apartment_rent' => array(),
                'house' => array('house1'),
                'land' => array(),
                'commercial_property' => array(),
            ],
            4 => [
                'number' => 'НДП/3223-04',
                'agent' => 'usr3',
                'client' => 'client3',
                'deal' => 'deal4',
                'apartment_sale' => array(),
                'apartment_rent' => array(),
                'house' => array(),
                'land' => array('land1'),
                'commercial_property' => array(),
            ],
        ];

        foreach ($data as $key => $value) {
            $contractView = new ContractView();

            $contractView->setNumber($value['number']);
            $contractView->setAgent($this->getReference($value['agent']));
            $contractView->setClient($this->getReference($value['client']));
            $contractView->setCreatedUser($this->getReference($value['agent']));
            $contractView->setUpdatedUser($this->getReference($value['agent']));
            $contractView->setDeal($this->getReference($value['deal']));

            foreach ($value['apartment_sale'] as $k => $v) {
                $contractView->addApartmentsSale(
                    $this->getReference($v)
                );
            }

            foreach ($value['apartment_rent'] as $k => $v) {
                $contractView->addApartmentsRent(
                    $this->getReference($v)
                );
            }

            foreach ($value['house'] as $k => $v) {
                $contractView->addHouse(
                    $this->getReference($v)
                );
            }

            foreach ($value['land'] as $k => $v) {
                $contractView->addLand(
                    $this->getReference($v)
                );
            }

            foreach ($value['commercial_property'] as $k => $v) {
                $contractView->addCommercialProperty(
                    $this->getReference($v)
                );
            }

            $manager->persist($contractView);
            $manager->flush();

            $this->addReference('contract_view' . $key, $contractView);
        }
    }

    public function getOrder()
    {
        return 22;
    }
}