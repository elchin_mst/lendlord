<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ApartmentRent;

class LoadApartmentsRentData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'city_fias_id' => 'c1cfe4b9-f7c2-423c-abfa-6ed1c05a15c5',
                'district' => 1,
                'street_fias_id' => 'd129b9dd-2323-49b6-a9f6-b34a4f84d2b0',
                'house_fias_id' => 'af1a70eb-eb7b-4e59-aaa1-2722f10c62a5',
                'source' => 'source2',
                'type' => 1,
                'floor' => 2,
                'floors' => 5,
                'apartment' => '143A',
                'price' => 20000,
                'condition' => 1,
                'agent' => 'usr1',
                'contacts' => array('client1', 'client3'),
                'prepay' => 3000,
                'furniture' => 1,
                'housing_stock' => 1,
                'bathroom' => 1,
                'loggia' => 2,
                'balcony' => 2,
            ],
            2 => [
                'city_fias_id' => '28bafcb3-92b2-445b-9443-a341be73fdb9',
                'district' => 2,
                'street_fias_id' => 'a460e76e-b3e0-4c9e-bfe6-92d686f7e576',
                'house_fias_id' => '32c58a3a-90d8-4661-93bf-753b3f741180',
                'source' => 'source2',
                'type' => 3,
                'floor' => 3,
                'floors' => 9,
                'apartment' => '143A',
                'price' => 15000,
                'condition' => 1,
                'agent' => 'usr2',
                'contacts' => array('client3', 'client4'),
                'prepay' => 2500,
                'furniture' => 3,
                'housing_stock' => 1,
                'bathroom' => 1,
                'loggia' => 1,
                'balcony' => 2,
            ],
            3 => [
                'city_fias_id' => 'bce1a4f2-7576-4427-8bd8-8d8b4e35ad11',
                'district' => 2,
                'street_fias_id' => 'dcb53ddc-0fe0-4ed2-999f-a2d36e0786ce',
                'house_fias_id' => 'a9bfd5a4-7d6b-4db0-8a4c-d00124f93200',
                'source' => 'source3',
                'type' => 1,
                'floor' => 3,
                'floors' => 9,
                'apartment' => '143A',
                'price' => 25000,
                'condition' => 1,
                'agent' => 'usr3',
                'contacts' => array('client1', 'client4'),
                'prepay' => 3200,
                'furniture' => 2,
                'housing_stock' => 1,
                'bathroom' => 1,
                'loggia' => 2,
                'balcony' => 2,
            ],
            4 => [
                'city_fias_id' => 'bce1a4f2-7576-4427-8bd8-8d8b4e35ad11',
                'district' => 4,
                'street_fias_id' => 'dcb53ddc-0fe0-4ed2-999f-a2d36e0786ce',
                'house_fias_id' => 'a9bfd5a4-7d6b-4db0-8a4c-d00124f93200',
                'source' => 'source4',
                'type' => 1,
                'floor' => 3,
                'floors' => 16,
                'apartment' => '178В',
                'price' => 12000,
                'condition' => 1,
                'agent' => 'usr3',
                'contacts' => array('client1', 'client4'),
                'prepay' => 5000,
                'furniture' => 1,
                'housing_stock' => 1,
                'bathroom' => 2,
                'loggia' => 2,
                'balcony' => 2,
            ],
        ];

        foreach ($data as $key => $value) {
            $apartment = new ApartmentRent();
            $apartment->setCityFiasId($value['city_fias_id']);
            $apartment->setDistrict($value['district']);
            $apartment->setStreetFiasId($value['street_fias_id']);
            $apartment->setHouseFiasId($value['house_fias_id']);
            $apartment->setSource($this->getReference($value['source']));
            $apartment->setType($value['type']);
            $apartment->setFloor($value['floor']);
            $apartment->setFloors($value['floors']);
            $apartment->setApartment($value['apartment']);
            $apartment->setPrice($value['price']);
            $apartment->setCondition($value['condition']);

            $apartment->setAgent(
                $this->getReference($value['agent'])
            );

            foreach ($value['contacts'] as $contact) {
                $apartment->addContact($this->getReference($contact));
            }

            $apartment->setHousingStock($value['housing_stock']);
            $apartment->setBathroom($value['bathroom']);
            $apartment->setLoggia($value['loggia']);
            $apartment->setBalcony($value['balcony']);
            $apartment->setPrepay($value['prepay']);
            $apartment->setFurniture($value['furniture']);

            $dateTime = new \DateTime();

            $apartment->setCreatedAt($dateTime);
            $apartment->setUpdatedAt($dateTime);

            $manager->persist($apartment);
            $manager->flush();

            $this->addReference('apartment_rent' . $key, $apartment);
        }
    }

    public function getOrder()
    {
        return 17;
    }
}