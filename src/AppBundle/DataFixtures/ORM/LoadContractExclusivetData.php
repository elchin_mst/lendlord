<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractExclusive;

class LoadContractExclusiveData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $day = 86400;

        $data = [
            1 => [
                'number' => 'НДЭ/3223-01',
                'agent' => 'usr1',
                'client' => 'client1',
                'until_date' => new \DateTime(date('Y-m-d H:i:s', time() + ($day * 20))),
                'apartment_sale' => array('apartment_sale1'),
                'apartment_rent' => array(),
                'house' => array(),
                'land' => array(),
                'commercial_property' => array('commercial_property1'),
            ],
            2 => [
                'number' => 'НДЭ/3223-02',
                'agent' => 'usr2',
                'client' => 'client1',
                'until_date' => new \DateTime(date('Y-m-d H:i:s', time() + ($day * 30))),
                'apartment_sale' => array(),
                'apartment_rent' => array('apartment_rent2', 'apartment_rent3'),
                'house' => array(),
                'land' => array(),
                'commercial_property' => array(),
            ],
            3 => [
                'number' => 'НДЭ/3223-03',
                'agent' => 'usr3',
                'client' => 'client4',
                'until_date' => new \DateTime(date('Y-m-d H:i:s', time() + ($day * 10))),
                'apartment_sale' => array(),
                'apartment_rent' => array(),
                'house' => array('house1'),
                'land' => array(),
                'commercial_property' => array(),
            ],
            4 => [
                'number' => 'НДЭ/3223-04',
                'agent' => 'usr3',
                'client' => 'client3',
                'until_date' => new \DateTime(date('Y-m-d H:i:s', time() + ($day * 10))),
                'apartment_sale' => array('apartment_sale2'),
                'apartment_rent' => array(),
                'house' => array(),
                'land' => array('land2'),
                'commercial_property' => array(),
            ],
        ];

        foreach ($data as $key => $value) {
            $contractExclusive = new ContractExclusive();

            $contractExclusive->setNumber($value['number']);
            $contractExclusive->setAgent($this->getReference($value['agent']));
            $contractExclusive->setClient($this->getReference($value['client']));
            $contractExclusive->setCreatedUser($this->getReference($value['agent']));
            $contractExclusive->setUpdatedUser($this->getReference($value['agent']));
            $contractExclusive->setSinceDate(new \DateTime());
            $contractExclusive->setUntilDate($value['until_date']);

            foreach ($value['apartment_sale'] as $k => $v) {
                $contractExclusive->addApartmentsSale(
                    $this->getReference($v)
                );
            }

            foreach ($value['apartment_rent'] as $k => $v) {
                $contractExclusive->addApartmentsRent(
                    $this->getReference($v)
                );
            }

            foreach ($value['house'] as $k => $v) {
                $contractExclusive->addHouse(
                    $this->getReference($v)
                );
            }

            foreach ($value['land'] as $k => $v) {
                $contractExclusive->addLand(
                    $this->getReference($v)
                );
            }

            foreach ($value['commercial_property'] as $k => $v) {
                $contractExclusive->addCommercialProperty(
                    $this->getReference($v)
                );
            }

            $manager->persist($contractExclusive);
            $manager->flush();

            $this->addReference('contract_exclusive' . $key, $contractExclusive);
        }
    }

    public function getOrder()
    {
        return 21;
    }
}