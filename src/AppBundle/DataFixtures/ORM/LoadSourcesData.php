<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Source;

class LoadSourcesData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'name' => 'Ва-банк',
                'type' => 1,
            ],
            2 => [
                'name' => 'Avito.ru',
                'type' => 2,
            ],
            3 => [
                'name' => 'Life Realty',
                'type' => 2,
            ],
            4 => [
                'name' => '161.ru',
                'type' => 2,
            ],
            5 => [
                'name' => 'Щиты',
                'type' => 3,
            ],
            6 => [
                'name' => 'Вывески',
                'type' => 3,
            ],
        ];

        foreach ($data as $key => $value) {
            $source = new Source();
            $source->setName($value['name']);
            $source->setType($value['type']);

            $manager->persist($source);
            $manager->flush();

            $this->addReference('source' . $key, $source);
        }
    }

    public function getOrder()
    {
        return 15;
    }
}