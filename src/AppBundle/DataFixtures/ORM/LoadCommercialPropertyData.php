<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\CommercialProperty;

class LoadCommercialPropertyData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'city_fias_id' => 'c1cfe4b9-f7c2-423c-abfa-6ed1c05a15c5',
                'district' => 1,
                'street_fias_id' => 'd129b9dd-2323-49b6-a9f6-b34a4f84d2b0',
                'house_fias_id' => 'af1a70eb-eb7b-4e59-aaa1-2722f10c62a5',
                'source' => 'source2',
                'agent' => 'usr3',
                'contacts' => array('client1', 'client4'),
                'condition' => 2,
                'heating' => 2,
            ],
            2 => [
                'city_fias_id' => '28bafcb3-92b2-445b-9443-a341be73fdb9',
                'district' => 2,
                'street_fias_id' => 'a460e76e-b3e0-4c9e-bfe6-92d686f7e576',
                'house_fias_id' => '32c58a3a-90d8-4661-93bf-753b3f741180',
                'source' => 'source2',
                'agent' => 'usr4',
                'contacts' => array('client2', 'client4'),
                'condition' => 1,
                'heating' => 1,
            ],
            3 => [
                'city_fias_id' => 'bce1a4f2-7576-4427-8bd8-8d8b4e35ad11',
                'district' => 3,
                'street_fias_id' => 'dcb53ddc-0fe0-4ed2-999f-a2d36e0786ce',
                'house_fias_id' => 'a9bfd5a4-7d6b-4db0-8a4c-d00124f93200',
                'source' => 'source3',
                'agent' => 'usr1',
                'contacts' => array('client3', 'client4'),
                'condition' => 3,
                'heating' => 2,
            ],
            4 => [
                'city_fias_id' => 'bce1a4f2-7576-4427-8bd8-8d8b4e35ad11',
                'district' => 4,
                'street_fias_id' => 'dcb53ddc-0fe0-4ed2-999f-a2d36e0786ce',
                'house_fias_id' => 'a9bfd5a4-7d6b-4db0-8a4c-d00124f93200',
                'source' => 'source4',
                'agent' => 'usr1',
                'contacts' => array('client3', 'client4'),
                'condition' => 2,
                'heating' => 1,
            ],
        ];

        foreach ($data as $key => $value) {
            $commercialProperty = new CommercialProperty();
            $commercialProperty->setCityFiasId($value['city_fias_id']);
            $commercialProperty->setDistrict($value['district']);
            $commercialProperty->setStreetFiasId($value['street_fias_id']);
            $commercialProperty->setHouseFiasId($value['house_fias_id']);
            $commercialProperty->setSource($this->getReference($value['source']));

            $commercialProperty->setAgent(
                $this->getReference($value['agent'])
            );

            foreach ($value['contacts'] as $contact) {
                $commercialProperty->addContact($this->getReference($contact));
            }

            $commercialProperty->setCondition($value['condition']);
            $commercialProperty->setHeating($value['heating']);

            $dateTime = new \DateTime();

            $commercialProperty->setCreatedAt($dateTime);
            $commercialProperty->setUpdatedAt($dateTime);

            $manager->persist($commercialProperty);
            $manager->flush();

            $this->addReference('commercial_property' . $key, $commercialProperty);
        }
    }

    public function getOrder()
    {
        return 20;
    }
}