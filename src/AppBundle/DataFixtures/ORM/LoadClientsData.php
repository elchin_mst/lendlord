<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Client;

class LoadClientsData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'first_name' => 'Иван',
                'last_name' => 'Волынский',
                'mobile_phone' => '+7 989 636-47-23',
                'work_phone' => '+7 906 957-42-55',
                'email' => 'volynsky_i@mail.ru',
                'manager' => 'usr1',
                'allow_sms' => true,

            ],
            2 => [
                'first_name' => 'Олег',
                'last_name' => 'Кутайсов',
                'mobile_phone' => '+7 904 068-35-20',
                'work_phone' => '+7 906 475-83-13',
                'email' => 'obolensky_ya@yandex.ru',
                'manager' => 'usr2',
                'allow_sms' => false,
            ],
            3 => [
                'first_name' => 'Ярослав',
                'last_name' => 'Оболенский',
                'mobile_phone' => '+7 906 902-41-54',
                'work_phone' => '+7 904 025-28-01',
                'email' => 'obolensky_ya@yandex.ru',
                'manager' => 'usr3',
                'allow_sms' => true,
            ],
            4 => [
                'first_name' => 'Анатолий',
                'last_name' => 'Ягужинский',
                'mobile_phone' => '+7 904 603-22-42',
                'work_phone' => '+7 906 176-59-08',
                'email' => 'yaguzhinsky_a@mail.ru',
                'manager' => 'usr4',
                'allow_sms' => true,
            ],
        ];

        foreach ($data as $key => $value) {
            $client = new Client();
            $client->setFirstName($value['first_name']);
            $client->setLastName($value['last_name']);
            $client->setMobilePhone($value['mobile_phone']);
            $client->setWorkPhone($value['work_phone']);
            $client->setEmail($value['email']);
            $client->setManager($this->getReference($value['manager']));
            $client->setAlowSms($value['allow_sms']);

            $manager->persist($client);
            $manager->flush();

            $this->addReference('client' . $key, $client);
        }
    }

    public function getOrder()
    {
        return 12;
    }
}