<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Role;

class LoadRolesData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'name' => 'admin',
                'role' => 'ROLE_ADMIN',
            ],
            2 => [
                'name' => 'user',
                'role' => 'ROLE_USER',
            ],
            3 => [
                'name' => 'manager',
                'role' => 'ROLE_MANAGER',
            ],
            4 => [
                'name' => 'manager',
                'role' => 'ROLE_SELLER',
            ],
        ];

        foreach ($data as $key => $value) {
            $role = new Role();
            $role->setRole($value['role']);
            $role->setPriority($key);

            $manager->persist($role);
            $manager->flush();

            $this->addReference('role' . $key, $role);
        }
    }

    public function getOrder()
    {
        return 1;
    }
}