<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Position;

class LoadPositionsData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'name' => 'Директор',
            ],
            2 => [
                'name' => 'Управляющий',
            ],
            3 => [
                'name' => 'Руководитель отдела',
            ],
            4 => [
                'name' => 'Руководитель направления',
            ],
            5 => [
                'name' => 'Менеджер',
            ],
            6 => [
                'name' => 'Агент',
            ],
            7 => [
                'name' => 'Агент стажер',
            ],
            8 => [
                'name' => 'Брокер',
            ],
            9 => [
                'name' => 'Бронзовый брокер',
            ],
            10 => [
                'name' => 'Серебряный брокер',
            ],
            11 => [
                'name' => 'Стажёр',
            ],
            12 => [
                'name' => 'Специалист',
            ],
            13 => [
                'name' => 'Специалист в/к',
            ],
        ];

        foreach ($data as $key => $value) {
            $position = new Position();
            $position->setName($value['name']);

            $manager->persist($position);
            $manager->flush();

            $this->addReference('position' . $key, $position);
        }
    }

    public function getOrder()
    {
        return 2;
    }
}