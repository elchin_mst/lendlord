<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Unit;

class LoadUnitsData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'name' => 'Квартиры',
                'offices' => array('office1', 'office2'),
            ],
            2 => [
                'name' => 'Дома',
                'offices' => array('office1', 'office2'),
            ],
            3 => [
                'name' => 'Участки',
                'offices' => array('office1', 'office2'),
            ],
            4 => [
                'name' => 'Коммерческая недвижимость',
                'offices' => array('office1', 'office2'),
            ],
            5 => [
                'name' => 'Аренда',
                'offices' => array('office1', 'office2'),
            ],
            6 => [
                'name' => 'Бухгалтерия',
                'offices' => array('office1', 'office2'),
            ],
        ];

        foreach ($data as $key => $value) {
            $unit = new Unit();
            $unit->setName($value['name']);

            foreach ($value['offices'] as $office) {
                $unit->addOffice($this->getReference($office));
            }

            $manager->persist($unit);
            $manager->flush();

            $this->addReference('unit' . $key, $unit);
        }
    }

    public function getOrder()
    {
        return 7;
    }
}