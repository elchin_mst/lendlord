<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractCooperation;

class LoadContractCooperationData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'number' => 'НЗС/3223-01',
                'agent' => 'usr1',
                'agent2' => 'usr3',
                'deal' => 'deal1',
                'apartment_sale' => array('apartment_sale1'),
                'apartment_rent' => array(),
                'house' => array(),
                'land' => array(),
                'commercial_property' => array(),
            ],
            2 => [
                'number' => 'НЗС/3223-02',
                'agent' => 'usr2',
                'agent2' => 'usr4',
                'deal' => 'deal2',
                'apartment_sale' => array('apartment_sale2', 'apartment_sale3'),
                'apartment_rent' => array(),
                'house' => array(),
                'land' => array(),
                'commercial_property' => array(),
            ],
            3 => [
                'number' => 'НЗС/3223-03',
                'agent' => 'usr3',
                'agent2' => 'usr1',
                'deal' => 'deal3',
                'apartment_sale' => array(),
                'apartment_rent' => array(),
                'house' => array('house1'),
                'land' => array(),
                'commercial_property' => array(),
            ],
            4 => [
                'number' => 'НЗС/3223-04',
                'agent' => 'usr3',
                'agent2' => 'usr2',
                'deal' => 'deal4',
                'apartment_sale' => array(),
                'apartment_rent' => array(),
                'house' => array(),
                'land' => array('land1'),
                'commercial_property' => array(),
            ],
        ];

        foreach ($data as $key => $value) {
            $contractCooperation = new ContractCooperation();

            $contractCooperation->setNumber($value['number']);
            $contractCooperation->setAgent($this->getReference($value['agent']));
            $contractCooperation->setAgent2($this->getReference($value['agent2']));
            $contractCooperation->setCreatedUser($this->getReference($value['agent']));
            $contractCooperation->setUpdatedUser($this->getReference($value['agent']));
            $contractCooperation->setDeal($this->getReference($value['deal']));
            $contractCooperation->setHandlingDate(new \DateTime());

            foreach ($value['apartment_sale'] as $k => $v) {
                $contractCooperation->addApartmentsSale(
                    $this->getReference($v)
                );
            }

            foreach ($value['apartment_rent'] as $k => $v) {
                $contractCooperation->addApartmentsRent(
                    $this->getReference($v)
                );
            }

            foreach ($value['house'] as $k => $v) {
                $contractCooperation->addHouse(
                    $this->getReference($v)
                );
            }

            foreach ($value['land'] as $k => $v) {
                $contractCooperation->addLand(
                    $this->getReference($v)
                );
            }

            foreach ($value['commercial_property'] as $k => $v) {
                $contractCooperation->addCommercialProperty(
                    $this->getReference($v)
                );
            }

            $manager->persist($contractCooperation);
            $manager->flush();

            $this->addReference('contract_cooperation' . $key, $contractCooperation);
        }
    }

    public function getOrder()
    {
        return 23;
    }
}