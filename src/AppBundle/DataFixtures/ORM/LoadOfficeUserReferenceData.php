<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Unit;

class LoadOfficeUserReferenceData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $n = 4;

        for ($i = 1; $i <= $n; $i++) {
            $office = $this->getReference('office' . $i);

            $office->setHead($this->getReference('usr' . $i));

            $manager->persist($office);
            $manager->flush();
        }
    }

    public function getOrder()
    {
        return 11;
    }
}