<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Note;

class LoadNotesData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            1 => [
                'text' => 'Text. Note text 1',
                'type' => 1,
                'client' => 'client1',
                'deal' => null,
            ],
            2 => [
                'text' => 'Text. Note text 2',
                'type' => 2,
                'client' => null,
                'deal' => 'deal1',
            ],
            3 => [
                'text' => 'Text. Note text 3',
                'type' => 4,
                'client' => null,
                'deal' => 'deal2',
            ],
            4 => [
                'text' => 'Text. Note text 4',
                'type' => 3,
                'client' => 'client1',
                'deal' => null,
            ],
            5 => [
                'text' => 'Text. Note text 5',
                'type' => 2,
                'client' => null,
                'deal' => 'deal3',
            ],
            6 => [
                'text' => 'Text. Note text 6',
                'type' => 1,
                'client' => 'client1',
                'deal' => null,
            ],
        ];

        foreach ($data as $key => $value) {
            $note = new Note();
            $note->setText($value['text']);
            $note->setType($value['type']);

            if ($value['client']) {
                $note->setClient($this->getReference($value['client']));
            }

            if ($value['deal']) {
                $note->setDeal($this->getReference($value['deal']));
            }

            $manager->persist($note);
            $manager->flush();

            $this->addReference('note' . $key, $note);
        }
    }

    public function getOrder()
    {
        return 14;
    }
}