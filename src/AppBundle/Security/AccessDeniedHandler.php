<?php
namespace AppBundle\Security;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

//check is authorize user in api
//TODO: clear in future if tokenStorage not needed

class AccessDeniedHandler implements AuthenticationEntryPointInterface
{
    public function __construct(TokenStorage $tokenStorage, AuthorizationChecker $authirizationChecker)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authirizationChecker;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $response = new JsonResponse();

        if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_ANONYMOUSLY')) {
            $response->setData(array('authorize' => 'false'));
        }

        return $response;
    }
}
