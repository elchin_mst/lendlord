<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contract
 *
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class Contract
{
    /**
     * @var string
     *
     * @ORM\Column(
     *     name="number",
     *     type="string",
     *     nullable=false,
     *     unique=true
     * )
     *
     * @JMS\Type("string")
     * @JMS\Groups({"List", "Selected"})
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    protected $number;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(
     *     name="agent_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     *
     * @JMS\Type("AppBundle\Entity\User")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    protected $agent;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(
     *     name="created_user_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     *
     * @JMS\Type("AppBundle\Entity\User")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    protected $createdUser;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(
     *     name="updated_user_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     *
     * @JMS\Type("AppBundle\Entity\User")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    protected $updatedUser;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("DateTime")
     * @Assert\DateTime()
     */
    protected $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("DateTime")
     * @Assert\DateTime()
     */
    protected $updatedAt;

    /**
     * Set created_at and updated_at
     *
     * @ORM\PrePersist
     */
    public function setDateTime()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set updated_at
     *
     * @ORM\PreUpdate
     */
    public function updateDateTime()
    {
        $this->updatedAt = new \DateTime();
    }
}

