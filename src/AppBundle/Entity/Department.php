<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Department - Отдел (словарь)
 *
 * @ORM\Table(name="departments")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DepartmentRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Department
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true, nullable=false)
     * @JMS\Groups({"List", "Selected"})
     */
    private $name;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="OfficeDepartmentReference", mappedBy="department")
     */
    private $officeDapartmentRefs;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="department")
     */
    private $users;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Department
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->officeDapartmentRefs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Add officeDapartmentRef
     *
     * @param \AppBundle\Entity\OfficeDepartmentReference $officeDapartmentRef
     *
     * @return Department
     */
    public function addOfficeDapartmentRef(\AppBundle\Entity\OfficeDepartmentReference $officeDapartmentRef)
    {
        $this->officeDapartmentRefs[] = $officeDapartmentRef;

        return $this;
    }

    /**
     * Remove officeDapartmentRef
     *
     * @param \AppBundle\Entity\OfficeDepartmentReference $officeDapartmentRef
     */
    public function removeOfficeDapartmentRef(\AppBundle\Entity\OfficeDepartmentReference $officeDapartmentRef)
    {
        $this->officeDapartmentRefs->removeElement($officeDapartmentRef);
    }

    /**
     * Get officeDapartmentRefs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOfficeDapartmentRefs()
    {
        return $this->officeDapartmentRefs->toArray();
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Department
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;
        $user->setDepartment($this);

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users->toArray();
    }

    /**
     * @ORM\PreRemove
     */
    public function deleteAllLinkedEntities()
    {
        // for OneToMany not working cascade set_null, to clean itself
        $users = $this->getUsers();

        foreach ($users as $user) {
            $user->setDepartment(null);
        }
    }
}
