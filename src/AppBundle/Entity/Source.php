<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Sources
 *
 * @ORM\Table(name="sources")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SourcesRepository")
 */
class Source
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false, unique=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 1)
     */
    private $type;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ApartmentSale", mappedBy="source")
     */
    private $apartmentsSale;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ApartmentRent", mappedBy="source")
     */
    private $apartmentsRent;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="House", mappedBy="source")
     */
    private $houses;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Land", mappedBy="source")
     */
    private $lands;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CommercialProperty", mappedBy="source")
     */
    private $commercialProperty;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Source
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Source
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->apartmentsSale = new \Doctrine\Common\Collections\ArrayCollection();
        $this->apartmentsRent = new \Doctrine\Common\Collections\ArrayCollection();
        $this->houses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->lands = new \Doctrine\Common\Collections\ArrayCollection();
        $this->commercialProperty = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add apartmentsSale
     *
     * @param \AppBundle\Entity\ApartmentSale $apartmentsSale
     *
     * @return Source
     */
    public function addApartmentsSale(\AppBundle\Entity\ApartmentSale $apartmentsSale)
    {
        $this->apartmentsSale[] = $apartmentsSale;

        return $this;
    }

    /**
     * Remove apartmentsSale
     *
     * @param \AppBundle\Entity\ApartmentSale $apartmentsSale
     */
    public function removeApartmentsSale(\AppBundle\Entity\ApartmentSale $apartmentsSale)
    {
        $this->apartmentsSale->removeElement($apartmentsSale);
    }

    /**
     * Get apartmentsSale
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApartmentsSale()
    {
        return $this->apartmentsSale;
    }

    /**
     * Add apartmentsRent
     *
     * @param \AppBundle\Entity\ApartmentRent $apartmentsRent
     *
     * @return Source
     */
    public function addApartmentsRent(\AppBundle\Entity\ApartmentRent $apartmentsRent)
    {
        $this->apartmentsRent[] = $apartmentsRent;

        return $this;
    }

    /**
     * Remove apartmentsRent
     *
     * @param \AppBundle\Entity\ApartmentRent $apartmentsRent
     */
    public function removeApartmentsRent(\AppBundle\Entity\ApartmentRent $apartmentsRent)
    {
        $this->apartmentsRent->removeElement($apartmentsRent);
    }

    /**
     * Get apartmentsRent
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApartmentsRent()
    {
        return $this->apartmentsRent;
    }

    /**
     * Add house
     *
     * @param \AppBundle\Entity\House $house
     *
     * @return Source
     */
    public function addHouse(\AppBundle\Entity\House $house)
    {
        $this->houses[] = $house;

        return $this;
    }

    /**
     * Remove house
     *
     * @param \AppBundle\Entity\House $house
     */
    public function removeHouse(\AppBundle\Entity\House $house)
    {
        $this->houses->removeElement($house);
    }

    /**
     * Get houses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHouses()
    {
        return $this->houses;
    }

    /**
     * Add land
     *
     * @param \AppBundle\Entity\Land $land
     *
     * @return Source
     */
    public function addLand(\AppBundle\Entity\Land $land)
    {
        $this->lands[] = $land;

        return $this;
    }

    /**
     * Remove land
     *
     * @param \AppBundle\Entity\Land $land
     */
    public function removeLand(\AppBundle\Entity\Land $land)
    {
        $this->lands->removeElement($land);
    }

    /**
     * Get lands
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLands()
    {
        return $this->lands;
    }

    /**
     * Add commercialProperty
     *
     * @param \AppBundle\Entity\CommercialProperty $commercialProperty
     *
     * @return Source
     */
    public function addCommercialProperty(\AppBundle\Entity\CommercialProperty $commercialProperty)
    {
        $this->commercialProperty[] = $commercialProperty;

        return $this;
    }

    /**
     * Remove commercialProperty
     *
     * @param \AppBundle\Entity\CommercialProperty $commercialProperty
     */
    public function removeCommercialProperty(\AppBundle\Entity\CommercialProperty $commercialProperty)
    {
        $this->commercialProperty->removeElement($commercialProperty);
    }

    /**
     * Get commercialProperty
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommercialProperty()
    {
        return $this->commercialProperty;
    }
}
