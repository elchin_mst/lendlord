<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OfficeDepartmentReference
 *
 * @ORM\Table(name="office_department_reference")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OfficeDepartmentReferenceRepository")
 */
class OfficeDepartmentReference
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * One Department has One head.
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\NotBlank()
     */
    private $head;

    /**
     * @var Department
     * @ORM\ManyToOne(targetEntity="Department", inversedBy="officeDepartmentRefs")
     * @ORM\JoinColumn(name="departmentId", referencedColumnName="id", nullable=false, onDelete="SET NULL")
     * @Assert\NotBlank()
     */
    private $department;

    //TODO: think about working with linked-table

    /**
     * @var Office
     *
     * @ORM\ManyToOne(targetEntity="Office", inversedBy="officeDepartmentRefs")
     * @ORM\JoinColumn(name="officeId", referencedColumnName="id", nullable=false, onDelete="SET NULL")
     * @Assert\NotBlank()
     */
    private $office;

    /**
     * @var decimal
     *
     * @ORM\Column(name="plan", type="decimal", nullable=true, precision=2, scale=1)
     * @JMS\Groups({"List", "Selected"})
     */
    private $plan;

    /**
     * @return string
     */
    public function __toString()
    {
        if (($this->office !== null)
            && ($this->department->getName() !== null)
        ) {
            return (string)$this->office->getName() . "-"
                . $this->department->getName();
        } else {
            return (string) $this->id;
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set head
     *
     * @param \AppBundle\Entity\User $head
     *
     * @return OfficeDepartmentReference
     */
    public function setHead(\AppBundle\Entity\User $head = null)
    {
        $this->head = $head;

        return $this;
    }

    /**
     * Get head
     *
     * @return \AppBundle\Entity\User
     */
    public function getHead()
    {
        return $this->head;
    }

    /**
     * Set department
     *
     * @param \AppBundle\Entity\Department $department
     *
     * @return OfficeDepartmentReference
     */
    public function setDepartment(\AppBundle\Entity\Department $department)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \AppBundle\Entity\Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set office
     *
     * @param \AppBundle\Entity\Office $office
     *
     * @return OfficeDepartmentReference
     */
    public function setOffice(\AppBundle\Entity\Office $office)
    {
        $this->office = $office;

        return $this;
    }

    /**
     * Get office
     *
     * @return \AppBundle\Entity\Office
     */
    public function getOffice()
    {
        return $this->office;
    }

    /**
     * Set plan
     *
     * @param string $plan
     *
     * @return Department
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return string
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("department")
     * @JMS\Groups({"List", "Selected"})
     * @return array|null
     */
    public function getDepartmentId()
    {
        if ($this->department) {
            return array('id' => $this->department->getId());
        }

        return null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("office")
     * @JMS\Groups({"List", "Selected"})
     * @return array|null
     */
    public function getOfficeId()
    {
        if ($this->office) {
            return array('id' => $this->office->getId());
        }

        return null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("head")
     * @JMS\Groups({"List", "Selected"})
     * @return array|null
     */
    public function getHeadId()
    {
        if ($this->head) {
            return array('id' => $this->head->getId());
        }

        return null;
    }
}
