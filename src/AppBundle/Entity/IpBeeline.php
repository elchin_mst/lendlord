<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * IpBeeline
 *
 * @ORM\Table(name="ip_beeline")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IpBeelineRepository")
 */
class IpBeeline
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="call_id", type="string", unique=true, nullable=true)
     */
    private $callId;

    /**
     * @var string
     *
     * @ORM\Column(name="direction", type="string", nullable=true)
     */
    private $direction;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set callId
     *
     * @param $callId
     *
     * @return IpBeeline
     */
    public function setCallId($callId)
    {
        $this->callId = $callId;

        return $this;
    }

    /**
     * Get CallId
     *
     * @return string
     */
    public function getCallId()
    {
        return $this->callId;
    }

    /**
     * Set direction
     *
     * @param string $direction
     *
     * @return IpBeeline
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * Get direction
     *
     * @return string
     */
    public function getDirection()
    {
        return $this->direction;
    }
}
