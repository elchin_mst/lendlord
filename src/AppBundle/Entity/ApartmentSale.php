<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ApartmentSale
 *
 * @ORM\Table(name="apartments_sale")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApartmentSaleRepository")
 */
class ApartmentSale extends Apartment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * @var Source
     *
     * @ORM\ManyToOne(targetEntity="Source", inversedBy="apartmentsSale")
     * @ORM\JoinColumn(
     *     name="source_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     *
     * @JMS\Type("AppBundle\Entity\Source")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    protected $source;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     *
     * @JMS\Type("AppBundle\Entity\User")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $agent;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Client", inversedBy="apartmentsSale")
     * @ORM\JoinTable(
     *     name="apartment_sale_client",
     *     joinColumns={
     *          @ORM\JoinColumn(
     *              name="apart_sale__id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="client_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     }
     * )
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\Client>")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $contacts;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ContractExclusive", mappedBy="apartmentsSale")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ContractExclusive>")
     */
    private $contractsExclusive;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ContractView", mappedBy="apartmentsSale")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ContractView>")
     */
    private $contractsView;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ContractCooperation", mappedBy="apartmentsSale")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ContractCooperation>")
     */
    private $contractsCooperation;

    /**
     * @var int
     *
     * @ORM\Column(name="building_year", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $buildingYear;

    /**
     * @var int
     *
     * @ORM\Column(name="purchase_year", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $purchaseYear;

    /**
     * @var int
     *
     * @ORM\Column(name="appart_on_floor", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $appartOnFloor;

    /**
     * @var int
     *
     * @ORM\Column(name="owners_num", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $ownersNum;

    /**
     * @var int
     *
     * @ORM\Column(name="underage_owners_num", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $underageOwnersNum;

    /**
     * @var int
     *
     * @ORM\Column(name="parking_num", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $parkingNum;

    /**
     * @var int
     *
     * @ORM\Column(name="unfinished", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $unfinished = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="urgent_sell", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $urgentSell = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="mortgage", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $mortgage = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="passenger_lift", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $passengerLift = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="service_lift", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $serviceLift = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="for_rent", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $forRent;

    /**
     * @var int
     *
     * @ORM\Column(name="guardrail", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $guardrail = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="access_control", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $accessControl = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="secure_parking", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $secureParking = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_dedic_parking", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasDedicParking = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="lift_parking_connect", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $liftParkingConnect = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="concierge", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $concierge = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="backup_power_system", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $backupPowerSystem = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="video_surv_system", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $videoSurvSystem = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="house_territory", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $houseTerritory = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="distance_to_stations", type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $distanceToStations;

    /**
     * @var boolean
     *
     * @ORM\Column(name="completed", type="boolean", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $completed;

    /**
     * @var float
     *
     * @ORM\Column(name="ceiling_height", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $ceilingHeight;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="completed_at", type="datetime", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\DateTime()
     */
    private $completedAt;

    /**
     * @var int
     *
     * @ORM\Column(name="balcony", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 1)
     */
    private $balcony;

    /**
     * @var int
     *
     * @ORM\Column(name="material", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 1)
     */
    private $material;

    /**
     * @var int
     *
     * @ORM\Column(name="housing_stock", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 1)
     */
    private $housingStock;

    /**
     * @var int
     *
     * @ORM\Column(name="floor_type", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $floorType;

    /**
     * @var int
     *
     * @ORM\Column(name="bathroom", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 1)
     */
    private $bathroom;

    /**
     * @var int
     *
     * @ORM\Column(name="hotWater", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $hotWater;

    /**
     * @var int
     *
     * @ORM\Column(name="heating", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 1)
     */
    private $heating;

    /**
     * @var int
     *
     * @ORM\Column(name="loggia", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 1)
     */
    private $loggia;

    /**
     * @var int
     *
     * @ORM\Column(name="related", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $related;

    /**
     * @var int
     *
     * @ORM\Column(name="placement", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $placement;

    /**
     * @var int
     *
     * @ORM\Column(name="redevelopment", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $redevelopment = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="windows_to_street", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $windowsToStreet = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="windows_to_yard", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $windowsToYard = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_cafes", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasCafes = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_cinemas", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasCinemas = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_hospitals", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasHospitals = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_educational", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasEducational = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldapsale_privatization_contract", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldapsalePrivatizationContract = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldapsale_contract_of_sale", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldapsaleContractOfSale = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldapsale_contract_of_gift", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldapsaleContractOfGift = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldapsale_barter_contract", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldapsaleBarterContract = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldapsale_court_dec_amicable_agr", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldapsaleCourtDecAmicableAgr = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldapsale_certificate_of_inheritance", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldapsaleCertificateOfInheritance = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldapsale_share_contract", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldapsaleShareContract = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldapsale_decree_local_administration", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldapsaleDecreeLocalAdministration = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldapsale_rent_contract", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldapsaleRentContract = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldapsale_certificate_of_payment", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldapsaleCertificateOfPayment = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldapsale_investment_contract", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldapsaleInvestmentContract = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldapsale_agreement_sharing_property", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldapsaleAgreementSharingProperty = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldapsale_agreemen_alloc_owner_interest", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldapsaleAgreemenAllocOwnerInterest = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldapsale_agreement_compensation", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldapsaleAgreementCompensation = 0;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contacts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set buildingYear
     *
     * @param integer $buildingYear
     *
     * @return ApartmentSale
     */
    public function setBuildingYear($buildingYear)
    {
        $this->buildingYear = $buildingYear;

        return $this;
    }

    /**
     * Get buildingYear
     *
     * @return integer
     */
    public function getBuildingYear()
    {
        return $this->buildingYear;
    }

    /**
     * Set purchaseYear
     *
     * @param integer $purchaseYear
     *
     * @return ApartmentSale
     */
    public function setPurchaseYear($purchaseYear)
    {
        $this->purchaseYear = $purchaseYear;

        return $this;
    }

    /**
     * Get purchaseYear
     *
     * @return integer
     */
    public function getPurchaseYear()
    {
        return $this->purchaseYear;
    }

    /**
     * Set appartOnFloor
     *
     * @param integer $appartOnFloor
     *
     * @return ApartmentSale
     */
    public function setAppartOnFloor($appartOnFloor)
    {
        $this->appartOnFloor = $appartOnFloor;

        return $this;
    }

    /**
     * Get appartOnFloor
     *
     * @return integer
     */
    public function getAppartOnFloor()
    {
        return $this->appartOnFloor;
    }

    /**
     * Set ownersNum
     *
     * @param integer $ownersNum
     *
     * @return ApartmentSale
     */
    public function setOwnersNum($ownersNum)
    {
        $this->ownersNum = $ownersNum;

        return $this;
    }

    /**
     * Get ownersNum
     *
     * @return integer
     */
    public function getOwnersNum()
    {
        return $this->ownersNum;
    }

    /**
     * Set underageOwnersNum
     *
     * @param integer $underageOwnersNum
     *
     * @return ApartmentSale
     */
    public function setUnderageOwnersNum($underageOwnersNum)
    {
        $this->underageOwnersNum = $underageOwnersNum;

        return $this;
    }

    /**
     * Get underageOwnersNum
     *
     * @return integer
     */
    public function getUnderageOwnersNum()
    {
        return $this->underageOwnersNum;
    }

    /**
     * Set parkingNum
     *
     * @param integer $parkingNum
     *
     * @return ApartmentSale
     */
    public function setParkingNum($parkingNum)
    {
        $this->parkingNum = $parkingNum;

        return $this;
    }

    /**
     * Get parkingNum
     *
     * @return integer
     */
    public function getParkingNum()
    {
        return $this->parkingNum;
    }

    /**
     * Set unfinished
     *
     * @param integer $unfinished
     *
     * @return ApartmentSale
     */
    public function setUnfinished($unfinished)
    {
        $this->unfinished = $unfinished;

        return $this;
    }

    /**
     * Get unfinished
     *
     * @return integer
     */
    public function getUnfinished()
    {
        return $this->unfinished;
    }

    /**
     * Set urgentSell
     *
     * @param integer $urgentSell
     *
     * @return ApartmentSale
     */
    public function setUrgentSell($urgentSell)
    {
        $this->urgentSell = $urgentSell;

        return $this;
    }

    /**
     * Get urgentSell
     *
     * @return integer
     */
    public function getUrgentSell()
    {
        return $this->urgentSell;
    }

    /**
     * Set mortgage
     *
     * @param integer $mortgage
     *
     * @return ApartmentSale
     */
    public function setMortgage($mortgage)
    {
        $this->mortgage = $mortgage;

        return $this;
    }

    /**
     * Get mortgage
     *
     * @return integer
     */
    public function getMortgage()
    {
        return $this->mortgage;
    }

    /**
     * Set passengerLift
     *
     * @param integer $passengerLift
     *
     * @return ApartmentSale
     */
    public function setPassengerLift($passengerLift)
    {
        $this->passengerLift = $passengerLift;

        return $this;
    }

    /**
     * Get passengerLift
     *
     * @return integer
     */
    public function getPassengerLift()
    {
        return $this->passengerLift;
    }

    /**
     * Set serviceLift
     *
     * @param integer $serviceLift
     *
     * @return ApartmentSale
     */
    public function setServiceLift($serviceLift)
    {
        $this->serviceLift = $serviceLift;

        return $this;
    }

    /**
     * Get serviceLift
     *
     * @return integer
     */
    public function getServiceLift()
    {
        return $this->serviceLift;
    }

    /**
     * Set forRent
     *
     * @param integer $forRent
     *
     * @return ApartmentSale
     */
    public function setForRent($forRent)
    {
        $this->forRent = $forRent;

        return $this;
    }

    /**
     * Get forRent
     *
     * @return integer
     */
    public function getForRent()
    {
        return $this->forRent;
    }

    /**
     * Set guardrail
     *
     * @param integer $guardrail
     *
     * @return ApartmentSale
     */
    public function setGuardrail($guardrail)
    {
        $this->guardrail = $guardrail;

        return $this;
    }

    /**
     * Get guardrail
     *
     * @return integer
     */
    public function getGuardrail()
    {
        return $this->guardrail;
    }

    /**
     * Set accessControl
     *
     * @param integer $accessControl
     *
     * @return ApartmentSale
     */
    public function setAccessControl($accessControl)
    {
        $this->accessControl = $accessControl;

        return $this;
    }

    /**
     * Get accessControl
     *
     * @return integer
     */
    public function getAccessControl()
    {
        return $this->accessControl;
    }

    /**
     * Set secureParking
     *
     * @param integer $secureParking
     *
     * @return ApartmentSale
     */
    public function setSecureParking($secureParking)
    {
        $this->secureParking = $secureParking;

        return $this;
    }

    /**
     * Get secureParking
     *
     * @return integer
     */
    public function getSecureParking()
    {
        return $this->secureParking;
    }

    /**
     * Set hasDedicParking
     *
     * @param integer $hasDedicParking
     *
     * @return ApartmentSale
     */
    public function setHasDedicParking($hasDedicParking)
    {
        $this->hasDedicParking = $hasDedicParking;

        return $this;
    }

    /**
     * Get hasDedicParking
     *
     * @return integer
     */
    public function getHasDedicParking()
    {
        return $this->hasDedicParking;
    }

    /**
     * Set liftParkingConnect
     *
     * @param integer $liftParkingConnect
     *
     * @return ApartmentSale
     */
    public function setLiftParkingConnect($liftParkingConnect)
    {
        $this->liftParkingConnect = $liftParkingConnect;

        return $this;
    }

    /**
     * Get liftParkingConnect
     *
     * @return integer
     */
    public function getLiftParkingConnect()
    {
        return $this->liftParkingConnect;
    }

    /**
     * Set concierge
     *
     * @param integer $concierge
     *
     * @return ApartmentSale
     */
    public function setConcierge($concierge)
    {
        $this->concierge = $concierge;

        return $this;
    }

    /**
     * Get concierge
     *
     * @return integer
     */
    public function getConcierge()
    {
        return $this->concierge;
    }

    /**
     * Set backupPowerSystem
     *
     * @param integer $backupPowerSystem
     *
     * @return ApartmentSale
     */
    public function setBackupPowerSystem($backupPowerSystem)
    {
        $this->backupPowerSystem = $backupPowerSystem;

        return $this;
    }

    /**
     * Get backupPowerSystem
     *
     * @return integer
     */
    public function getBackupPowerSystem()
    {
        return $this->backupPowerSystem;
    }

    /**
     * Set videoSurvSystem
     *
     * @param integer $videoSurvSystem
     *
     * @return ApartmentSale
     */
    public function setVideoSurvSystem($videoSurvSystem)
    {
        $this->videoSurvSystem = $videoSurvSystem;

        return $this;
    }

    /**
     * Get videoSurvSystem
     *
     * @return integer
     */
    public function getVideoSurvSystem()
    {
        return $this->videoSurvSystem;
    }

    /**
     * Set houseTerritory
     *
     * @param integer $houseTerritory
     *
     * @return ApartmentSale
     */
    public function setHouseTerritory($houseTerritory)
    {
        $this->houseTerritory = $houseTerritory;

        return $this;
    }

    /**
     * Get houseTerritory
     *
     * @return integer
     */
    public function getHouseTerritory()
    {
        return $this->houseTerritory;
    }

    /**
     * Set distanceToStations
     *
     * @param string $distanceToStations
     *
     * @return ApartmentSale
     */
    public function setDistanceToStations($distanceToStations)
    {
        $this->distanceToStations = $distanceToStations;

        return $this;
    }

    /**
     * Get distanceToStations
     *
     * @return string
     */
    public function getDistanceToStations()
    {
        return $this->distanceToStations;
    }

    /**
     * Set completed
     *
     * @param boolean $completed
     *
     * @return ApartmentSale
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;

        return $this;
    }

    /**
     * Get completed
     *
     * @return boolean
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * Set ceilingHeight
     *
     * @param float $ceilingHeight
     *
     * @return ApartmentSale
     */
    public function setCeilingHeight($ceilingHeight)
    {
        $this->ceilingHeight = $ceilingHeight;

        return $this;
    }

    /**
     * Get ceilingHeight
     *
     * @return float
     */
    public function getCeilingHeight()
    {
        return $this->ceilingHeight;
    }

    /**
     * Set completedAt
     *
     * @param \DateTime $completedAt
     *
     * @return ApartmentSale
     */
    public function setCompletedAt($completedAt)
    {
        $this->completedAt = $completedAt;

        return $this;
    }

    /**
     * Get completedAt
     *
     * @return \DateTime
     */
    public function getCompletedAt()
    {
        return $this->completedAt;
    }

    /**
     * Set balcony
     *
     * @param integer $balcony
     *
     * @return ApartmentSale
     */
    public function setBalcony($balcony)
    {
        $this->balcony = $balcony;

        return $this;
    }

    /**
     * Get balcony
     *
     * @return integer
     */
    public function getBalcony()
    {
        return $this->balcony;
    }

    /**
     * Set material
     *
     * @param integer $material
     *
     * @return ApartmentSale
     */
    public function setMaterial($material)
    {
        $this->material = $material;

        return $this;
    }

    /**
     * Get material
     *
     * @return integer
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * Set housingStock
     *
     * @param integer $housingStock
     *
     * @return ApartmentSale
     */
    public function setHousingStock($housingStock)
    {
        $this->housingStock = $housingStock;

        return $this;
    }

    /**
     * Get housingStock
     *
     * @return integer
     */
    public function getHousingStock()
    {
        return $this->housingStock;
    }

    /**
     * Set floorType
     *
     * @param integer $floorType
     *
     * @return ApartmentSale
     */
    public function setFloorType($floorType)
    {
        $this->floorType = $floorType;

        return $this;
    }

    /**
     * Get floorType
     *
     * @return integer
     */
    public function getFloorType()
    {
        return $this->floorType;
    }

    /**
     * Set bathroom
     *
     * @param integer $bathroom
     *
     * @return ApartmentSale
     */
    public function setBathroom($bathroom)
    {
        $this->bathroom = $bathroom;

        return $this;
    }

    /**
     * Get bathroom
     *
     * @return integer
     */
    public function getBathroom()
    {
        return $this->bathroom;
    }

    /**
     * Set hotWater
     *
     * @param integer $hotWater
     *
     * @return ApartmentSale
     */
    public function setHotWater($hotWater)
    {
        $this->hotWater = $hotWater;

        return $this;
    }

    /**
     * Get hotWater
     *
     * @return integer
     */
    public function getHotWater()
    {
        return $this->hotWater;
    }

    /**
     * Set heating
     *
     * @param integer $heating
     *
     * @return ApartmentSale
     */
    public function setHeating($heating)
    {
        $this->heating = $heating;

        return $this;
    }

    /**
     * Get heating
     *
     * @return integer
     */
    public function getHeating()
    {
        return $this->heating;
    }

    /**
     * Set loggia
     *
     * @param integer $loggia
     *
     * @return ApartmentSale
     */
    public function setLoggia($loggia)
    {
        $this->loggia = $loggia;

        return $this;
    }

    /**
     * Get loggia
     *
     * @return integer
     */
    public function getLoggia()
    {
        return $this->loggia;
    }

    /**
     * Set related
     *
     * @param integer $related
     *
     * @return ApartmentSale
     */
    public function setRelated($related)
    {
        $this->related = $related;

        return $this;
    }

    /**
     * Get related
     *
     * @return integer
     */
    public function getRelated()
    {
        return $this->related;
    }

    /**
     * Set placement
     *
     * @param integer $placement
     *
     * @return ApartmentSale
     */
    public function setPlacement($placement)
    {
        $this->placement = $placement;

        return $this;
    }

    /**
     * Get placement
     *
     * @return integer
     */
    public function getPlacement()
    {
        return $this->placement;
    }

    /**
     * Set redevelopment
     *
     * @param integer $redevelopment
     *
     * @return ApartmentSale
     */
    public function setRedevelopment($redevelopment)
    {
        $this->redevelopment = $redevelopment;

        return $this;
    }

    /**
     * Get redevelopment
     *
     * @return integer
     */
    public function getRedevelopment()
    {
        return $this->redevelopment;
    }

    /**
     * Set windowsToStreet
     *
     * @param integer $windowsToStreet
     *
     * @return ApartmentSale
     */
    public function setWindowsToStreet($windowsToStreet)
    {
        $this->windowsToStreet = $windowsToStreet;

        return $this;
    }

    /**
     * Get windowsToStreet
     *
     * @return integer
     */
    public function getWindowsToStreet()
    {
        return $this->windowsToStreet;
    }

    /**
     * Set windowsToYard
     *
     * @param integer $windowsToYard
     *
     * @return ApartmentSale
     */
    public function setWindowsToYard($windowsToYard)
    {
        $this->windowsToYard = $windowsToYard;

        return $this;
    }

    /**
     * Get windowsToYard
     *
     * @return integer
     */
    public function getWindowsToYard()
    {
        return $this->windowsToYard;
    }

    /**
     * Set hasCafes
     *
     * @param integer $hasCafes
     *
     * @return ApartmentSale
     */
    public function setHasCafes($hasCafes)
    {
        $this->hasCafes = $hasCafes;

        return $this;
    }

    /**
     * Get hasCafes
     *
     * @return integer
     */
    public function getHasCafes()
    {
        return $this->hasCafes;
    }

    /**
     * Set hasCinemas
     *
     * @param integer $hasCinemas
     *
     * @return ApartmentSale
     */
    public function setHasCinemas($hasCinemas)
    {
        $this->hasCinemas = $hasCinemas;

        return $this;
    }

    /**
     * Get hasCinemas
     *
     * @return integer
     */
    public function getHasCinemas()
    {
        return $this->hasCinemas;
    }

    /**
     * Set hasHospitals
     *
     * @param integer $hasHospitals
     *
     * @return ApartmentSale
     */
    public function setHasHospitals($hasHospitals)
    {
        $this->hasHospitals = $hasHospitals;

        return $this;
    }

    /**
     * Get hasHospitals
     *
     * @return integer
     */
    public function getHasHospitals()
    {
        return $this->hasHospitals;
    }

    /**
     * Set hasEducational
     *
     * @param integer $hasEducational
     *
     * @return ApartmentSale
     */
    public function setHasEducational($hasEducational)
    {
        $this->hasEducational = $hasEducational;

        return $this;
    }

    /**
     * Get hasEducational
     *
     * @return integer
     */
    public function getHasEducational()
    {
        return $this->hasEducational;
    }

    /**
     * Set floor
     *
     * @param integer $floor
     *
     * @return ApartmentSale
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get floor
     *
     * @return integer
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set floors
     *
     * @param integer $floors
     *
     * @return ApartmentSale
     */
    public function setFloors($floors)
    {
        $this->floors = $floors;

        return $this;
    }

    /**
     * Get floors
     *
     * @return integer
     */
    public function getFloors()
    {
        return $this->floors;
    }

    /**
     * Set elite
     *
     * @param integer $elite
     *
     * @return ApartmentSale
     */
    public function setElite($elite)
    {
        $this->elite = $elite;

        return $this;
    }

    /**
     * Get elite
     *
     * @return integer
     */
    public function getElite()
    {
        return $this->elite;
    }

    /**
     * Set terms
     *
     * @param string $terms
     *
     * @return ApartmentSale
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;

        return $this;
    }

    /**
     * Get terms
     *
     * @return string
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * Set apartment
     *
     * @param string $apartment
     *
     * @return ApartmentSale
     */
    public function setApartment($apartment)
    {
        $this->apartment = $apartment;

        return $this;
    }

    /**
     * Get apartment
     *
     * @return string
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * Set totalSq
     *
     * @param float $totalSq
     *
     * @return ApartmentSale
     */
    public function setTotalSq($totalSq)
    {
        $this->totalSq = $totalSq;

        return $this;
    }

    /**
     * Get totalSq
     *
     * @return float
     */
    public function getTotalSq()
    {
        return $this->totalSq;
    }

    /**
     * Set livingSq
     *
     * @param float $livingSq
     *
     * @return ApartmentSale
     */
    public function setLivingSq($livingSq)
    {
        $this->livingSq = $livingSq;

        return $this;
    }

    /**
     * Get livingSq
     *
     * @return float
     */
    public function getLivingSq()
    {
        return $this->livingSq;
    }

    /**
     * Set kitchenSq
     *
     * @param float $kitchenSq
     *
     * @return ApartmentSale
     */
    public function setKitchenSq($kitchenSq)
    {
        $this->kitchenSq = $kitchenSq;

        return $this;
    }

    /**
     * Get kitchenSq
     *
     * @return float
     */
    public function getKitchenSq()
    {
        return $this->kitchenSq;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return ApartmentSale
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set specialPrice
     *
     * @param string $specialPrice
     *
     * @return ApartmentSale
     */
    public function setSpecialPrice($specialPrice)
    {
        $this->specialPrice = $specialPrice;

        return $this;
    }

    /**
     * Get specialPrice
     *
     * @return string
     */
    public function getSpecialPrice()
    {
        return $this->specialPrice;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return ApartmentSale
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set condition
     *
     * @param integer $condition
     *
     * @return ApartmentSale
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;

        return $this;
    }

    /**
     * Get condition
     *
     * @return integer
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * Set commodities
     *
     * @param integer $commodities
     *
     * @return ApartmentSale
     */
    public function setCommodities($commodities)
    {
        $this->commodities = $commodities;

        return $this;
    }

    /**
     * Get commodities
     *
     * @return integer
     */
    public function getCommodities()
    {
        return $this->commodities;
    }

    /**
     * Set woodenWindows
     *
     * @param integer $woodenWindows
     *
     * @return ApartmentSale
     */
    public function setWoodenWindows($woodenWindows)
    {
        $this->woodenWindows = $woodenWindows;

        return $this;
    }

    /**
     * Get woodenWindows
     *
     * @return integer
     */
    public function getWoodenWindows()
    {
        return $this->woodenWindows;
    }

    /**
     * Set metalPlastWindows
     *
     * @param integer $metalPlastWindows
     *
     * @return ApartmentSale
     */
    public function setMetalPlastWindows($metalPlastWindows)
    {
        $this->metalPlastWindows = $metalPlastWindows;

        return $this;
    }

    /**
     * Get metalPlastWindows
     *
     * @return integer
     */
    public function getMetalPlastWindows()
    {
        return $this->metalPlastWindows;
    }

    /**
     * Set woodenWindowsEur
     *
     * @param integer $woodenWindowsEur
     *
     * @return ApartmentSale
     */
    public function setWoodenWindowsEur($woodenWindowsEur)
    {
        $this->woodenWindowsEur = $woodenWindowsEur;

        return $this;
    }

    /**
     * Get woodenWindowsEur
     *
     * @return integer
     */
    public function getWoodenWindowsEur()
    {
        return $this->woodenWindowsEur;
    }

    /**
     * Set cityFiasId
     *
     * @param string $cityFiasId
     *
     * @return ApartmentSale
     */
    public function setCityFiasId($cityFiasId)
    {
        $this->cityFiasId = $cityFiasId;

        return $this;
    }

    /**
     * Get cityFiasId
     *
     * @return string
     */
    public function getCityFiasId()
    {
        return $this->cityFiasId;
    }

    /**
     * Set district
     *
     * @param int $district
     *
     * @return ApartmentSale
     */
    public function setDistrict($district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return int
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set streetFiasId
     *
     * @param string $streetFiasId
     *
     * @return ApartmentSale
     */
    public function setStreetFiasId($streetFiasId)
    {
        $this->streetFiasId = $streetFiasId;

        return $this;
    }

    /**
     * Get streetFiasId
     *
     * @return string
     */
    public function getStreetFiasId()
    {
        return $this->streetFiasId;
    }

    /**
     * Set houseFiasId
     *
     * @param string $houseFiasId
     *
     * @return ApartmentSale
     */
    public function setHouseFiasId($houseFiasId)
    {
        $this->houseFiasId = $houseFiasId;

        return $this;
    }

    /**
     * Get houseFiasId
     *
     * @return string
     */
    public function getHouseFiasId()
    {
        return $this->houseFiasId;
    }

    /**
     * Set block
     *
     * @param string $block
     *
     * @return ApartmentSale
     */
    public function setBlock($block)
    {
        $this->block = $block;

        return $this;
    }

    /**
     * Get block
     *
     * @return string
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * Set exclusive
     *
     * @param integer $exclusive
     *
     * @return ApartmentSale
     */
    public function setExclusive($exclusive)
    {
        $this->exclusive = $exclusive;

        return $this;
    }

    /**
     * Get exclusive
     *
     * @return integer
     */
    public function getExclusive()
    {
        return $this->exclusive;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return ApartmentSale
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return ApartmentSale
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set location
     *
     * @param integer $location
     *
     * @return ApartmentSale
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return integer
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set moderationStatus
     *
     * @param integer $moderationStatus
     *
     * @return ApartmentSale
     */
    public function setModerationStatus($moderationStatus)
    {
        $this->moderationStatus = $moderationStatus;

        return $this;
    }

    /**
     * Get moderationStatus
     *
     * @return integer
     */
    public function getModerationStatus()
    {
        return $this->moderationStatus;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ApartmentSale
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set textOnTheSite
     *
     * @param string $textOnTheSite
     *
     * @return ApartmentSale
     */
    public function setTextOnTheSite($textOnTheSite)
    {
        $this->textOnTheSite = $textOnTheSite;

        return $this;
    }

    /**
     * Get textOnTheSite
     *
     * @return string
     */
    public function getTextOnTheSite()
    {
        return $this->textOnTheSite;
    }

    /**
     * Set openSale
     *
     * @param integer $openSale
     *
     * @return ApartmentSale
     */
    public function setOpenSale($openSale)
    {
        $this->openSale = $openSale;

        return $this;
    }

    /**
     * Get openSale
     *
     * @return integer
     */
    public function getOpenSale()
    {
        return $this->openSale;
    }

    /**
     * Set netSale
     *
     * @param integer $netSale
     *
     * @return ApartmentSale
     */
    public function setNetSale($netSale)
    {
        $this->netSale = $netSale;

        return $this;
    }

    /**
     * Get netSale
     *
     * @return integer
     */
    public function getNetSale()
    {
        return $this->netSale;
    }

    /**
     * Set moderationAt
     *
     * @param \DateTime $moderationAt
     *
     * @return ApartmentSale
     */
    public function setModerationAt($moderationAt)
    {
        $this->moderationAt = $moderationAt;

        return $this;
    }

    /**
     * Get moderationAt
     *
     * @return \DateTime
     */
    public function getModerationAt()
    {
        return $this->moderationAt;
    }

    /**
     * Set lastСalled
     *
     * @param \DateTime $lastСalled
     *
     * @return ApartmentSale
     */
    public function setLastСalled($lastСalled)
    {
        $this->lastСalled = $lastСalled;

        return $this;
    }

    /**
     * Get lastСalled
     *
     * @return \DateTime
     */
    public function getLastСalled()
    {
        return $this->lastСalled;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ApartmentSale
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ApartmentSale
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set fromFirm
     *
     * @param integer $fromFirm
     *
     * @return ApartmentSale
     */
    public function setFromFirm($fromFirm)
    {
        $this->fromFirm = $fromFirm;

        return $this;
    }

    /**
     * Get fromFirm
     *
     * @return integer
     */
    public function getFromFirm()
    {
        return $this->fromFirm;
    }

    /**
     * Set source
     *
     * @param \AppBundle\Entity\Source $source
     *
     * @return ApartmentSale
     */
    public function setSource(\AppBundle\Entity\Source $source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return \AppBundle\Entity\Source
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set agent
     *
     * @param \AppBundle\Entity\User $agent
     *
     * @return ApartmentSale
     */
    public function setAgent(\AppBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \AppBundle\Entity\User
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Add contact
     *
     * @param \AppBundle\Entity\Client $contact
     *
     * @return ApartmentSale
     */
    public function addContact(\AppBundle\Entity\Client $contact)
    {
        $this->contacts[] = $contact;

        return $this;
    }

    /**
     * Remove contact
     *
     * @param \AppBundle\Entity\Client $contact
     */
    public function removeContact(\AppBundle\Entity\Client $contact)
    {
        $this->contacts->removeElement($contact);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Set ldapsalePrivatizationContract
     *
     * @param integer $ldapsalePrivatizationContract
     *
     * @return ApartmentSale
     */
    public function setLdapsalePrivatizationContract($ldapsalePrivatizationContract)
    {
        $this->ldapsalePrivatizationContract = $ldapsalePrivatizationContract;

        return $this;
    }

    /**
     * Get ldapsalePrivatizationContract
     *
     * @return integer
     */
    public function getLdapsalePrivatizationContract()
    {
        return $this->ldapsalePrivatizationContract;
    }

    /**
     * Set ldapsaleContractOfSale
     *
     * @param integer $ldapsaleContractOfSale
     *
     * @return ApartmentSale
     */
    public function setLdapsaleContractOfSale($ldapsaleContractOfSale)
    {
        $this->ldapsaleContractOfSale = $ldapsaleContractOfSale;

        return $this;
    }

    /**
     * Get ldapsaleContractOfSale
     *
     * @return integer
     */
    public function getLdapsaleContractOfSale()
    {
        return $this->ldapsaleContractOfSale;
    }

    /**
     * Set ldapsaleContractOfGift
     *
     * @param integer $ldapsaleContractOfGift
     *
     * @return ApartmentSale
     */
    public function setLdapsaleContractOfGift($ldapsaleContractOfGift)
    {
        $this->ldapsaleContractOfGift = $ldapsaleContractOfGift;

        return $this;
    }

    /**
     * Get ldapsaleContractOfGift
     *
     * @return integer
     */
    public function getLdapsaleContractOfGift()
    {
        return $this->ldapsaleContractOfGift;
    }

    /**
     * Set ldapsaleBarterContract
     *
     * @param integer $ldapsaleBarterContract
     *
     * @return ApartmentSale
     */
    public function setLdapsaleBarterContract($ldapsaleBarterContract)
    {
        $this->ldapsaleBarterContract = $ldapsaleBarterContract;

        return $this;
    }

    /**
     * Get ldapsaleBarterContract
     *
     * @return integer
     */
    public function getLdapsaleBarterContract()
    {
        return $this->ldapsaleBarterContract;
    }

    /**
     * Set ldapsaleCourtDecAmicableAgr
     *
     * @param integer $ldapsaleCourtDecAmicableAgr
     *
     * @return ApartmentSale
     */
    public function setLdapsaleCourtDecAmicableAgr($ldapsaleCourtDecAmicableAgr)
    {
        $this->ldapsaleCourtDecAmicableAgr = $ldapsaleCourtDecAmicableAgr;

        return $this;
    }

    /**
     * Get ldapsaleCourtDecAmicableAgr
     *
     * @return integer
     */
    public function getLdapsaleCourtDecAmicableAgr()
    {
        return $this->ldapsaleCourtDecAmicableAgr;
    }

    /**
     * Set ldapsaleCertificateOfInheritance
     *
     * @param integer $ldapsaleCertificateOfInheritance
     *
     * @return ApartmentSale
     */
    public function setLdapsaleCertificateOfInheritance($ldapsaleCertificateOfInheritance)
    {
        $this->ldapsaleCertificateOfInheritance = $ldapsaleCertificateOfInheritance;

        return $this;
    }

    /**
     * Get ldapsaleCertificateOfInheritance
     *
     * @return integer
     */
    public function getLdapsaleCertificateOfInheritance()
    {
        return $this->ldapsaleCertificateOfInheritance;
    }

    /**
     * Set ldapsaleShareContract
     *
     * @param integer $ldapsaleShareContract
     *
     * @return ApartmentSale
     */
    public function setLdapsaleShareContract($ldapsaleShareContract)
    {
        $this->ldapsaleShareContract = $ldapsaleShareContract;

        return $this;
    }

    /**
     * Get ldapsaleShareContract
     *
     * @return integer
     */
    public function getLdapsaleShareContract()
    {
        return $this->ldapsaleShareContract;
    }

    /**
     * Set ldapsaleDecreeLocalAdministration
     *
     * @param integer $ldapsaleDecreeLocalAdministration
     *
     * @return ApartmentSale
     */
    public function setLdapsaleDecreeLocalAdministration($ldapsaleDecreeLocalAdministration)
    {
        $this->ldapsaleDecreeLocalAdministration = $ldapsaleDecreeLocalAdministration;

        return $this;
    }

    /**
     * Get ldapsaleDecreeLocalAdministration
     *
     * @return integer
     */
    public function getLdapsaleDecreeLocalAdministration()
    {
        return $this->ldapsaleDecreeLocalAdministration;
    }

    /**
     * Set ldapsaleRentContract
     *
     * @param integer $ldapsaleRentContract
     *
     * @return ApartmentSale
     */
    public function setLdapsaleRentContract($ldapsaleRentContract)
    {
        $this->ldapsaleRentContract = $ldapsaleRentContract;

        return $this;
    }

    /**
     * Get ldapsaleRentContract
     *
     * @return integer
     */
    public function getLdapsaleRentContract()
    {
        return $this->ldapsaleRentContract;
    }

    /**
     * Set ldapsaleCertificateOfPayment
     *
     * @param integer $ldapsaleCertificateOfPayment
     *
     * @return ApartmentSale
     */
    public function setLdapsaleCertificateOfPayment($ldapsaleCertificateOfPayment)
    {
        $this->ldapsaleCertificateOfPayment = $ldapsaleCertificateOfPayment;

        return $this;
    }

    /**
     * Get ldapsaleCertificateOfPayment
     *
     * @return integer
     */
    public function getLdapsaleCertificateOfPayment()
    {
        return $this->ldapsaleCertificateOfPayment;
    }

    /**
     * Set ldapsaleInvestmentContract
     *
     * @param integer $ldapsaleInvestmentContract
     *
     * @return ApartmentSale
     */
    public function setLdapsaleInvestmentContract($ldapsaleInvestmentContract)
    {
        $this->ldapsaleInvestmentContract = $ldapsaleInvestmentContract;

        return $this;
    }

    /**
     * Get ldapsaleInvestmentContract
     *
     * @return integer
     */
    public function getLdapsaleInvestmentContract()
    {
        return $this->ldapsaleInvestmentContract;
    }

    /**
     * Set ldapsaleAgreementSharingProperty
     *
     * @param integer $ldapsaleAgreementSharingProperty
     *
     * @return ApartmentSale
     */
    public function setLdapsaleAgreementSharingProperty($ldapsaleAgreementSharingProperty)
    {
        $this->ldapsaleAgreementSharingProperty = $ldapsaleAgreementSharingProperty;

        return $this;
    }

    /**
     * Get ldapsaleAgreementSharingProperty
     *
     * @return integer
     */
    public function getLdapsaleAgreementSharingProperty()
    {
        return $this->ldapsaleAgreementSharingProperty;
    }

    /**
     * Set ldapsaleAgreemenAllocOwnerInterest
     *
     * @param integer $ldapsaleAgreemenAllocOwnerInterest
     *
     * @return ApartmentSale
     */
    public function setLdapsaleAgreemenAllocOwnerInterest($ldapsaleAgreemenAllocOwnerInterest)
    {
        $this->ldapsaleAgreemenAllocOwnerInterest = $ldapsaleAgreemenAllocOwnerInterest;

        return $this;
    }

    /**
     * Get ldapsaleAgreemenAllocOwnerInterest
     *
     * @return integer
     */
    public function getLdapsaleAgreemenAllocOwnerInterest()
    {
        return $this->ldapsaleAgreemenAllocOwnerInterest;
    }

    /**
     * Set ldapsaleAgreementCompensation
     *
     * @param integer $ldapsaleAgreementCompensation
     *
     * @return ApartmentSale
     */
    public function setLdapsaleAgreementCompensation($ldapsaleAgreementCompensation)
    {
        $this->ldapsaleAgreementCompensation = $ldapsaleAgreementCompensation;

        return $this;
    }

    /**
     * Get ldapsaleAgreementCompensation
     *
     * @return integer
     */
    public function getLdapsaleAgreementCompensation()
    {
        return $this->ldapsaleAgreementCompensation;
    }

    /**
     * Add contractsExclusive
     *
     * @param \AppBundle\Entity\ContractExclusive $contractsExclusive
     *
     * @return ApartmentSale
     */
    public function addContractsExclusive(\AppBundle\Entity\ContractExclusive $contractsExclusive)
    {
        $this->contractsExclusive[] = $contractsExclusive;

        return $this;
    }

    /**
     * Remove contractsExclusive
     *
     * @param \AppBundle\Entity\ContractExclusive $contractsExclusive
     */
    public function removeContractsExclusive(\AppBundle\Entity\ContractExclusive $contractsExclusive)
    {
        $this->contractsExclusive->removeElement($contractsExclusive);
    }

    /**
     * Get contractsExclusive
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContractsExclusive()
    {
        return $this->contractsExclusive;
    }

    /**
     * Add contractsView
     *
     * @param \AppBundle\Entity\ContractView $contractsView
     *
     * @return ApartmentSale
     */
    public function addContractsView(\AppBundle\Entity\ContractView $contractsView)
    {
        $this->contractsView[] = $contractsView;

        return $this;
    }

    /**
     * Remove contractsView
     *
     * @param \AppBundle\Entity\ContractView $contractsView
     */
    public function removeContractsView(\AppBundle\Entity\ContractView $contractsView)
    {
        $this->contractsView->removeElement($contractsView);
    }

    /**
     * Get contractsView
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContractsView()
    {
        return $this->contractsView;
    }

    /**
     * Add contractsCooperation
     *
     * @param \AppBundle\Entity\ContractCooperation $contractsCooperation
     *
     * @return ApartmentSale
     */
    public function addContractsCooperation(\AppBundle\Entity\ContractCooperation $contractsCooperation)
    {
        $this->contractsCooperation[] = $contractsCooperation;

        return $this;
    }

    /**
     * Remove contractsCooperation
     *
     * @param \AppBundle\Entity\ContractCooperation $contractsCooperation
     */
    public function removeContractsCooperation(\AppBundle\Entity\ContractCooperation $contractsCooperation)
    {
        $this->contractsCooperation->removeElement($contractsCooperation);
    }

    /**
     * Get contractsCooperation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContractsCooperation()
    {
        return $this->contractsCooperation;
    }

    /**
     * Return source id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("source")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getSourceId()
    {
        if ($this->source) {
            return array('id' => $this->source->getId());
        }

        return null;
    }

    /**
     * Return agent id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("agent")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getAgentId()
    {
        if ($this->agent) {
            return array('id' => $this->agent->getId());
        }

        return null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("contacts")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getContactsId()
    {
        $arr = array();

        if ($this->contacts) {
            foreach ($this->contacts as $contact) {
                $arr[] = array('id' => $contact->getId());
            }
        }

        return $arr;
    }

    /**
     * Get contract exclusive Ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("contracts_exclusive")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getContractsExclusiveId()
    {
        $arr = array();

        if ($this->contractsExclusive) {
            foreach ($this->contractsExclusive as $contractExclusive) {
                $arr[] = array('id' => $contractExclusive->getId());
            }
        }

        return $arr;
    }

    /**
     * Get contract exclusive Ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("contracts_view")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getContractsViewId()
    {
        $arr = array();

        if ($this->contractsView) {
            foreach ($this->contractsView as $contractView) {
                $arr[] = array('id' => $contractView->getId());
            }
        }

        return $arr;
    }

    /**
     * Get contract exclusive Ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("contracts_cooperation")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getContractsCooperationId()
    {
        $arr = array();

        if ($this->contractsCooperation) {
            foreach ($this->contractsCooperation as $contractCooperation) {
                $arr[] = array('id' => $contractCooperation->getId());
            }
        }

        return $arr;
    }
}
