<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Ldap\Adapter\ExtLdap\Collection;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\Accessor;
use Symfony\Component\Validator\Constraints as Assert;

//TODO: add user-photo

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Groups({"Login", "List", "Selected"})
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     * @JMS\Groups({"Login", "List", "Selected"})
     */
    private $lastName;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     * @JMS\Groups({"Login",  "List", "Selected"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $middleName = "";

    /**
     * @ORM\Column(type="datetime")
     *
     * @Assert\DateTime()
     * @JMS\Groups({"List", "Selected"})
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $internalPhone;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $mobilePhone;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $homePhone;

    /**
     * @ORM\Column(type="string")
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_terms", type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $paymentTerms;

    /**
     * @var float
     *
     * @ORM\Column(name="indicator_of_efficiency", type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $indicatorOfEfficiency;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $isUnitManager = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $isMentor = false;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="students")
     * @ORM\JoinColumn(name="mentor_id", referencedColumnName="id")
     *
     * @JMS\Type("AppBundle\Entity\User")
     */
    private $mentor;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="mentor")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\User>")
     */
    private $students;

    /**
     * @var int
     *
     * @ORM\Column(name="headhunter_id", type="integer", nullable=true)
     */
    private $headhunterId;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $isHeadOffice = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $isHeadDepartment = false;

    /**
     * @ORM\Column(type="string", unique=true)
     *
     * @Assert\NotBlank()
     *
     * @JMS\Groups({"Login", "List", "Selected"})
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     * @ORM\Column(name="Email", type="string", length=255)
     *
     * @JMS\Groups({"Login", "Selected", "List"})
     * @Assert\Email(checkMX = true)
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min=6)
     */
    private $password;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isActive = true;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     *
     * @JMS\Groups({"List"})
     */
    private $isFired = true;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     * @ORM\JoinTable(
     *     name="user_role",
     *     joinColumns={
     *          @ORM\JoinColumn(
     *              name="user_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL",
     *              nullable=false
     *          )
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="role_id",
     *              referencedColumnName="id",
     *              onDelete="SET NULL",
     *              unique=false
     *          )
     *     }
     * )
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\Role<Expand>>")
     * @JMS\Groups({"Test"})
     * @JMS\Accessor(setter="addRoles")
     * @Assert\NotBlank()
     */
    private $roles;

    /**
     * @ORM\ManyToOne( targetEntity="Unit", inversedBy="users")
     * @ORM\JoinColumn( name="unit_id", referencedColumnName="id", onDelete="SET NULL" )
     *
     * @Assert\NotBlank()
     */
    private $unit;

    /**
     * @ORM\ManyToMany(targetEntity="Office", inversedBy="users")
     * @ORM\JoinTable(
     *     name="user_office",
     *     joinColumns={
     *          @ORM\JoinColumn(
     *              name="user_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="office_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     }
     * )
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\Office>")
     * @Assert\NotBlank()
     */
    private $offices;

    /**
     * @ORM\OneToMany(targetEntity="Office", mappedBy="head")
     */
    private $officeManages;

    /**
     * @ORM\ManyToOne(targetEntity="Department", inversedBy="users")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id", onDelete="SET NULL" )
     *
     * @Assert\NotBlank()
     */
    private $department;

    /**
     * @ORM\ManyToOne(targetEntity="Position", inversedBy="users")
     * @ORM\JoinColumn(name="position_id", referencedColumnName="id", onDelete="SET NULL" ))
     *
     * @Assert\NotBlank()
     * @JMS\Groups({"List"})
     */
    protected $position;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="users")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="SET NULL")
     *
     * @JMS\Groups({"List"})
     */
    protected $category;

    /**
     * Variable for password hashing
     * @var string
     */
    private $salt = 'test';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="in_position_time", type="datetime", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\DateTime()
     */
    private $inPositionDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastLoginTime;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $LastPasswordChangeTime;

    /**
     * Date/Time of the last activity
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastActivityAt;

    /**
     * @ORM\OneToOne(targetEntity="unit", mappedBy="head")
     */
    private $unitManages;

    /**
     * @ORM\OneToOne(targetEntity="OfficeDepartmentReference", mappedBy="head")
     */
    private $officeDepartmentManages;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Deal", mappedBy="manager")
     */
    private $deals;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Client", mappedBy="manager")
     */
    private $clients;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="House", mappedBy="agent")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\House>")
     */
    private $houses;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ApartmentSale", mappedBy="agent")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ApartmentSale>")
     */
    private $apartmentsSale;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ApartmentRent", mappedBy="agent")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ApartmentRent>")
     */
    private $apartmentsRent;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CommercialProperty", mappedBy="agent")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\CommercialProperty>")
     */
    private $commercialProp;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Land", mappedBy="agent")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\Land>")
     */
    private $lands;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->offices = new ArrayCollection();
        $this->students = new ArrayCollection();
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @inheritDoc
     */
    public function equals(UserInterface $user)
    {
        return $this->username === $user->getUsername();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add role
     *
     * @param \AppBundle\Entity\Role $role
     *
     * @return User
     */
    public function addRole(\AppBundle\Entity\Role $role)
    {
        $this->roles[] = $role;

        return $this;
    }

    /**
     * @param $roles
     *
     * @return $this
     *
     */
    public function addRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Remove role
     *
     * @param \AppBundle\Entity\Role $role
     */
    public function removeRole(\AppBundle\Entity\Role $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * removeAllUserRole
     */
    public function removeAllUserRole()
    {
        $this->roles->clear();
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        $roleNames = [];

        foreach ($this->roles as $role) {
            $roleNames[] = (string)$role->getRole();
        }

        return $roleNames;
    }

    /**
     * @return array
     */
    public function getRoleNames()
    {
        $roleNames = [];

        foreach ($this->roles as $role) {
            $roleNames[] = (string)$role->getRole();
        }

        return $roleNames;
    }

    /**
     * Returns the true ArrayCollection of Roles.
     *
     * @return array|ArrayCollection
     */
    public function getRolesCollection()
    {
        return $this->roles;
    }

    /**
     * Directly set the ArrayCollection of Roles. Type hinted as Collection which is the parent of (Array|Persistent)Collection.
     *
     * @param  $collection
     */
    public function setRolesCollection($collection)
    {
        $this->roles = $collection;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        try {
            return (string)$this->username; // If it is possible, return a string value from object.
        } catch (\Exception $e) {
            return get_class($this) . '@' . spl_object_hash(
                    $this
                ); // If it is not possible, return a preset string to identify instance of object, e.g.
        }
    }

    /**
     * Returns the salt that was originally used to encode the password.
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @return bool
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(
            array(
                $this->id,
                $this->username,
                $this->password,
                // see section on salt below
                // $this->salt,
            )
        );
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            )
            = unserialize($serialized);
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->getIsActive();
    }

    /**
     * Set lastLoginTime
     *
     * @param \DateTime $lastLoginTime
     *
     * @return User
     */
    public function setLastLoginTime($lastLoginTime)
    {
        $this->lastLoginTime = $lastLoginTime;

        return $this;
    }

    /**
     * Get lastLoginTime
     *
     * @return \DateTime
     */
    public function getLastLoginTime()
    {
        return $this->lastLoginTime;
    }

    /**
     * Set lastActivityAt
     *
     * @param \DateTime $lastActivityAt
     *
     * @return User
     */
    public function setLastActivityAt($lastActivityAt)
    {
        $this->lastActivityAt = $lastActivityAt;

        return $this;
    }

    /**
     * Get lastActivityAt
     *
     * @return \DateTime
     */
    public function getLastActivityAt()
    {
        return $this->lastActivityAt;
    }

    /**
     * @return int
     */
    public function getActiveTimeDiffInMinutes()
    {
        // Delay during which the user will be considered as still active

        $delay = new \DateTime();

        $interval = $this->getLastActivityAt()->diff($delay);

        return $interval->i;
    }

    /**
     * Set isFired
     *
     * @param boolean $isFired
     *
     * @return User
     */
    public function setIsFired($isFired)
    {
        $this->isFired = $isFired;

        return $this;
    }

    /**
     * Get isFired
     *
     * @return boolean
     */
    public function getIsFired()
    {
        return $this->isFired;
    }

    /**
     * Set lastPasswordChangeTime
     *
     * @param \DateTime $lastPasswordChangeTime
     *
     * @return User
     */
    public function setLastPasswordChangeTime($lastPasswordChangeTime)
    {
        $this->LastPasswordChangeTime = $lastPasswordChangeTime;

        return $this;
    }

    /**
     * Get lastPasswordChangeTime
     *
     * @return \DateTime
     */
    public function getLastPasswordChangeTime()
    {
        return $this->LastPasswordChangeTime;
    }

    /**
     * @return int
     */
    public function getLastPasswordChangeInDays()
    {
        // we need change pass every 7 day
        $delay = new \DateTime();
        $interval = $this->getLastPasswordChangeTime()->diff($delay);

        return $interval->d;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     *
     * @return User
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set internalPhone
     *
     * @param string $internalPhone
     *
     * @return User
     */
    public function setInternalPhone($internalPhone)
    {
        $this->internalPhone = $internalPhone;

        return $this;
    }

    /**
     * Get internalPhone
     *
     * @return string
     */
    public function getInternalPhone()
    {
        return $this->internalPhone;
    }

    /**
     * Set mobilePhone
     *
     * @param string $mobilePhone
     *
     * @return User
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;

        return $this;
    }

    /**
     * Get mobilePhone
     *
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * Set homePhone
     *
     * @param string $homePhone
     *
     * @return User
     */
    public function setHomePhone($homePhone)
    {
        $this->homePhone = $homePhone;

        return $this;
    }

    /**
     * Get homePhone
     *
     * @return string
     */
    public function getHomePhone()
    {
        return $this->homePhone;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set isUnitManager
     *
     * @param boolean $isUnitManager
     *
     * @return User
     */
    public function setIsUnitManager($isUnitManager)
    {
        $this->isUnitManager = $isUnitManager;

        return $this;
    }

    /**
     * Get isUnitManager
     *
     * @return boolean
     */
    public function getIsUnitManager()
    {
        return $this->isUnitManager;
    }

    /**
     * Set isMentor
     *
     * @param boolean $isMentor
     *
     * @return User
     */
    public function setIsMentor($isMentor)
    {
        $this->isMentor = $isMentor;

        return $this;
    }

    /**
     * Get isMentor
     *
     * @return boolean
     */
    public function getiIMentor()
    {
        return $this->isMentor;
    }

    /**
     * Set isHeadOffice
     *
     * @param boolean $isHeadOffice
     *
     * @return User
     */
    public function setIsHeadOffice($isHeadOffice)
    {
        $this->isHeadOffice = $isHeadOffice;

        return $this;
    }

    /**
     * Get isHeadOffice
     *
     * @return boolean
     */
    public function getIsHeadOffice()
    {
        return $this->isHeadOffice;
    }

    /**
     * Set isHeadDepartment
     *
     * @param boolean $isHeadDepartment
     *
     * @return User
     */
    public function setIsHeadDepartment($isHeadDepartment)
    {
        $this->isHeadDepartment = $isHeadDepartment;

        return $this;
    }

    /**
     * Get isHeadDepartment
     *
     * @return boolean
     */
    public function getIsHeadDepartment()
    {
        return $this->isHeadDepartment;
    }

    /**
     * Add office
     *
     * @param \AppBundle\Entity\Office $office
     *
     * @return User
     */
    public function addOffice(\AppBundle\Entity\Office $office)
    {
        $this->offices[] = $office;

        return $this;
    }

    /**
     * Remove office
     *
     * @param \AppBundle\Entity\Office $office
     */
    public function removeOffice(\AppBundle\Entity\Office $office)
    {
        $this->offices->removeElement($office);
    }

    /**
     * Get offices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffices()
    {
        return $this->offices->toArray();
    }

    /**
     * Set department
     *
     * @param \AppBundle\Entity\Department $department
     *
     * @return User
     */
    public function setDepartment(
        \AppBundle\Entity\Department $department = null
    ) {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \AppBundle\Entity\Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return User
     */
    public function setCategory(\AppBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Get isMentor
     *
     * @return boolean
     */
    public function getIsMentor()
    {
        return $this->isMentor;
    }

    /**
     * Set mentor
     *
     * @param \AppBundle\Entity\User $mentor
     *
     * @return User
     */
    public function setMentor(\AppBundle\Entity\User $mentor = null)
    {
        $this->mentor = $mentor;

        return $this;
    }

    /**
     * Get mentor
     *
     * @return User
     */
    public function getMentor()
    {
        return $this->mentor;
    }

    /**
     * @param \AppBundle\Entity\User|null $student
     *
     * @return User
     */
    public function setStudents(\AppBundle\Entity\User $student = null)
    {
        $this->students[] = $student;

        return $this;
    }

    /**
     * @return array
     */
    public function getStudents()
    {
        return $this->students;
    }

    /**
     * Set headhunterId
     *
     * @param integer $headhunterId
     *
     * @return User
     */
    public function setHeadhunterId($headhunterId)
    {
        $this->headhunterId = $headhunterId;

        return $this;
    }

    /**
     * Get headhunterId
     *
     * @return integer
     */
    public function getHeadhunterId()
    {
        return $this->headhunterId;
    }

    /**
     * Set position
     *
     * @param \AppBundle\Entity\Position $position
     *
     * @return User
     */
    public function setPosition(\AppBundle\Entity\Position $position = null)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return \AppBundle\Entity\Position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set unit
     *
     * @param \AppBundle\Entity\Unit $unit
     *
     * @return User
     */
    public function setUnit(\AppBundle\Entity\Unit $unit = null)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return \AppBundle\Entity\Unit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set unitManages
     *
     * @param \AppBundle\Entity\Unit $unitManages
     *
     * @return User
     */
    public function setUnitManages(\AppBundle\Entity\Unit $unitManages = null)
    {
        $this->unitManages = $unitManages;
        $unitManages->setHead($this);

        return $this;
    }

    /**
     * Get unitManages
     *
     * @return \AppBundle\Entity\Unit
     */
    public function getUnitManages()
    {
        return $this->unitManages;
    }

    /**
     * Set officeManages
     *
     * @param \AppBundle\Entity\Unit $unitManages
     *
     * @return User
     */
    public function setOfficeManages(\AppBundle\Entity\Office $officeManages = null)
    {
        $this->officeManages = $officeManages;
        $officeManages->setHead($this);

        return $this;
    }

    /**
     * Get officeManages
     *
     * @return \AppBundle\Entity\Office
     */
    public function getOfficeManages()
    {
        return $this->officeManages;
    }

    /**
     * Set officeDepartmentManages
     *
     * @param \AppBundle\Entity\OfficeDepartmentReference $officeDepartmentManages
     *
     * @return User
     */
    public function setOfficeDepartmentManages(\AppBundle\Entity\OfficeDepartmentReference $officeDepartmentManages = null)
    {
        $this->officeDepartmentManages = $officeDepartmentManages;

        return $this;
    }

    /**
     * Get officeDepartmentManages
     *
     * @return \AppBundle\Entity\OfficeDepartmentReference
     */
    public function getOfficeDepartmentManages()
    {
        return $this->officeDepartmentManages;
    }

    /**
     * Add student
     *
     * @param \AppBundle\Entity\User $student
     *
     * @return User
     */
    public function addStudent(\AppBundle\Entity\User $student)
    {
        $this->students[] = $student;

        return $this;
    }

    /**
     * Remove student
     *
     * @param \AppBundle\Entity\User $student
     */
    public function removeStudent(\AppBundle\Entity\User $student)
    {
        $this->students->removeElement($student);
    }

    /**
     * Add officeManage
     *
     * @param \AppBundle\Entity\Office $officeManage
     *
     * @return User
     */
    public function addOfficeManage(\AppBundle\Entity\Office $officeManage)
    {
        $this->officeManages[] = $officeManage;

        return $this;
    }

    /**
     * Remove officeManage
     *
     * @param \AppBundle\Entity\Office $officeManage
     */
    public function removeOfficeManage(\AppBundle\Entity\Office $officeManage)
    {
        $this->officeManages->removeElement($officeManage);
    }

    /**
     * Add deal
     *
     * @param \AppBundle\Entity\Deal $deal
     *
     * @return User
     */
    public function addDeal(\AppBundle\Entity\Deal $deal)
    {
        $this->deals[] = $deal;

        return $this;
    }

    /**
     * Remove deal
     *
     * @param \AppBundle\Entity\Deal $deal
     */
    public function removeDeal(\AppBundle\Entity\Deal $deal)
    {
        $this->deals->removeElement($deal);
    }

    /**
     * Get deals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDeals()
    {
        return $this->deals;
    }

    /**
     * Add client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return User
     */
    public function addClient(\AppBundle\Entity\Client $client)
    {
        $this->clients[] = $client;

        return $this;
    }

    /**
     * Remove client
     *
     * @param \AppBundle\Entity\Client $client
     */
    public function removeClient(\AppBundle\Entity\Deal $client)
    {
        $this->clients->removeElement($client);
    }

    /**
     * Get clients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * Add house
     *
     * @param \AppBundle\Entity\House $house
     *
     * @return User
     */
    public function addHouse(\AppBundle\Entity\House $house)
    {
        $this->houses[] = $house;

        return $this;
    }

    /**
     * Remove house
     *
     * @param \AppBundle\Entity\House $house
     */
    public function removeHouse(\AppBundle\Entity\House $house)
    {
        $this->houses->removeElement($house);
    }

    /**
     * Get houses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHouses()
    {
        return $this->houses;
    }

    /**
     * Add apartmentsSale
     *
     * @param \AppBundle\Entity\ApartmentSale $apartmentsSale
     *
     * @return User
     */
    public function addApartmentsSale(\AppBundle\Entity\ApartmentSale $apartmentsSale)
    {
        $this->apartmentsSale[] = $apartmentsSale;

        return $this;
    }

    /**
     * Remove apartmentsSale
     *
     * @param \AppBundle\Entity\ApartmentSale $apartmentsSale
     */
    public function removeApartmentsSale(\AppBundle\Entity\ApartmentSale $apartmentsSale)
    {
        $this->apartmentsSale->removeElement($apartmentsSale);
    }

    /**
     * Get apartmentsSale
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApartmentsSale()
    {
        return $this->apartmentsSale;
    }

    /**
     * Add apartmentsRent
     *
     * @param \AppBundle\Entity\ApartmentRent $apartmentsRent
     *
     * @return User
     */
    public function addApartmentsRent(\AppBundle\Entity\ApartmentRent $apartmentsRent)
    {
        $this->apartmentsRent[] = $apartmentsRent;

        return $this;
    }

    /**
     * Remove apartmentsRent
     *
     * @param \AppBundle\Entity\ApartmentRent $apartmentsRent
     */
    public function removeApartmentsRent(\AppBundle\Entity\ApartmentRent $apartmentsRent)
    {
        $this->apartmentsRent->removeElement($apartmentsRent);
    }

    /**
     * Get apartmentsRent
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApartmentsRent()
    {
        return $this->apartmentsRent;
    }

    /**
     * Add commercialProp
     *
     * @param \AppBundle\Entity\CommercialProperty $commercialProp
     *
     * @return User
     */
    public function addCommercialProp(\AppBundle\Entity\CommercialProperty $commercialProp)
    {
        $this->commercialProp[] = $commercialProp;

        return $this;
    }

    /**
     * Remove commercialProp
     *
     * @param \AppBundle\Entity\CommercialProperty $commercialProp
     */
    public function removeCommercialProp(\AppBundle\Entity\CommercialProperty $commercialProp)
    {
        $this->commercialProp->removeElement($commercialProp);
    }

    /**
     * Get commercialProp
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommercialProp()
    {
        return $this->commercialProp;
    }

    /**
     * Add land
     *
     * @param \AppBundle\Entity\Land $land
     *
     * @return User
     */
    public function addLand(\AppBundle\Entity\Land $land)
    {
        $this->lands[] = $land;

        return $this;
    }

    /**
     * Remove land
     *
     * @param \AppBundle\Entity\Land $land
     */
    public function removeLand(\AppBundle\Entity\Land $land)
    {
        $this->lands->removeElement($land);
    }

    /**
     * Get lands
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLands()
    {
        return $this->lands;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("change_password")
     * @JMS\Groups({"Login"})
     *
     * @return Bool Whether the user is active or not
     */
    public function isPasswordOld()
    {
        if (null !== $this->getLastPasswordChangeInDays()) {
            return ($this->getLastPasswordChangeInDays() >= 7) ? true : false;
        } else {
            return true;
        }
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("authorize")
     * @JMS\Groups({"Login"})
     *
     * @return Bool Whether the user is active or not
     */
    public function isActiveNow()
    {
        if (null !== $this->getLastActivityAt()) {

            return ($this->getActiveTimeDiffInMinutes() >= 20) ? false : true;
        } else {
            return false;
        }
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("roles")
     * @JMS\Groups({"Login", "Selected", "List"})
     *
     * @return array
     */
    public function getRolesId()
    {
        $arr = array();

        if ($this->roles) {
            foreach ($this->roles as $role) {
                $arr[] = array('id' => $role->getId());
            }
        }

        return $arr;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("offices")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getOfficesId()
    {
        $arr = array();

        if ($this->offices) {
            foreach ($this->offices as $of) {
                $arr[] = array('id' => $of->getId());
            }
        }

        return $arr;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("department")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getDepartmentId()
    {
        if ($this->department) {
            return array('id' => $this->department->getId());
        }

        return null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("category")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getCategoryId()
    {
        if ($this->category) {
            return array('id' => $this->category->getId());
        }

        return null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("unit")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getUnitId()
    {
        if ($this->unit) {
            return array('id' => $this->unit->getId());
        }

        return null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("position")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getPositionId()
    {
        if ($this->position) {
            return array('id' => $this->position->getId());
        }

        return null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("mentor")
     * @JMS\Groups({"Selected", "List"})
     *
     * @return array|null
     */
    public function getMentorId()
    {
        if ($this->mentor) {
            return array('id' => $this->mentor->getId());
        }

        return null;
    }

    /**
     * @ORM\PreRemove
     */
    public function deleteAllLinkedEntities()
    {
        // for OneToOne not working cascade set_null, to clean itself
        $unit = $this->getUnitManages();

        if ($unit !== null) {
            $unit->setHead(null);
        }

        $officeDepartment = $this->getOfficeDepartmentManages();

        if ($officeDepartment !== null) {
            $officeDepartment->setHead(null);
        }
    }

    /**
     * Set paymentTerms
     *
     * @param string $paymentTerms
     *
     * @return User
     */
    public function setPaymentTerms($paymentTerms)
    {
        $this->paymentTerms = $paymentTerms;

        return $this;
    }

    /**
     * Get paymentTerms
     *
     * @return string
     */
    public function getPaymentTerms()
    {
        return $this->paymentTerms;
    }

    /**
     * Set inPositionDate
     *
     * @param \DateTime $inPositionDate
     *
     * @return User
     */
    public function setInPositionDate($inPositionDate)
    {
        $this->inPositionDate = $inPositionDate;

        return $this;
    }

    /**
     * Get inPositionDate
     *
     * @return \DateTime
     */
    public function getInPositionDate()
    {
        return $this->inPositionDate;
    }

    /**
     * Set indicatorOfEfficiency
     *
     * @param string $indicatorOfEfficiency
     *
     * @return User
     */
    public function setIndicatorOfEfficiency($indicatorOfEfficiency)
    {
        $this->indicatorOfEfficiency = $indicatorOfEfficiency;

        return $this;
    }

    /**
     * Get indicatorOfEfficiency
     *
     * @return string
     */
    public function getIndicatorOfEfficiency()
    {
        return $this->indicatorOfEfficiency;
    }
}
