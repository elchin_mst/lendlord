<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Land
 *
 * @ORM\Table(name="lands")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LandRepository")
 *
 */
class Land extends Object
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * @var Source
     *
     * @ORM\ManyToOne(targetEntity="Source", inversedBy="lands")
     * @ORM\JoinColumn(
     *     name="source_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     *
     * @JMS\Type("AppBundle\Entity\Source")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    protected $source;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     *
     * @JMS\Type("AppBundle\Entity\User")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $agent;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Client", inversedBy="apartmentsSale")
     * @ORM\JoinTable(
     *     name="land_client",
     *     joinColumns={
     *          @ORM\JoinColumn(
     *              name="land__id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="client_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     }
     * )
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\Client>")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $contacts;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ContractExclusive", mappedBy="lands")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ContractExclusive>")
     */
    private $contractsExclusive;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ContractView", mappedBy="lands")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ContractView>")
     */
    private $contractsView;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ContractCooperation", mappedBy="lands")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ContractCooperation>")
     */
    private $contractsCooperation;

    /**
     * @var int
     *
     * @ORM\Column(name="is_garden", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $isGarden = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="elite", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0)
     */
    private $elite = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_adjoining_territory", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0)
     */
    private $hasAdjoiningTerritory = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="surveying", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0)
     */
    private $surveying = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="exit_to_pond", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0)
     */
    private $exitToPond = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="terms", type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $terms;

    /**
     * @var string
     *
     * @ORM\Column(name="distance_to_stations", type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $distanceToStations;

    /**
     * @var float
     *
     * @ORM\Column(name="ground_area", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $groundArea;

    /**
     * @var float
     *
     * @ORM\Column(name="measurement_x", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $measurementX;

    /**
     * @var float
     *
     * @ORM\Column(name="measurement_y", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $measurementY;

    /**
     * @var float
     *
     * @ORM\Column(name="roadway_width", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $roadwayWidth;

    /**
     * @var float
     *
     * @ORM\Column(name="land_location", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $landLocation;

    /**
     * @var float
     *
     * @ORM\Column(name="facade_width", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $facadeWidth;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 1)
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="special_price", type="decimal", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $specialPrice;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="completed_at", type="datetime", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\DateTime()
     */
    private $completedAt;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="entrace_type", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $entranceType;

    /**
     * @var integer
     *
     * @ORM\Column(name="land_position", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $landPosition;

    /**
     * @var integer
     *
     * @ORM\Column(name="relief", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $relief;

    /**
     * @var integer
     *
     * @ORM\Column(name="right_type_land", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $rightTypeLand;

    /**
     * @var int
     *
     * @ORM\Column(name="has_cafes", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasCafes = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_cinemas", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasCinemas = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_hospitals", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasHospitals = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_educational", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasEducational = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="communic_water", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $communicWater = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="communic_gas", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $communicGas = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="communic_electricity", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $communicElectricity = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="communic_phone", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $communicPhone = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="communic_canalisation", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $communicCanalisation = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_for_electricity", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $paymentForElectricity = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_for_gas", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $paymentForGas = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_for_water", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $paymentForWater = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_for_canalization", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $paymentForCanalization = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldland_contract_of_sale", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldlandContractOfSale = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldland_contract_of_gift", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldlandContractOfGift = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldland_barter_contract", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldlandBarterContract = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldland_court_dec_amicable_agr", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldlandCourtDecAmicableAgr = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldland_certificate_of_inheritance", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldlandCertificateOfInheritance = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldland_agreement_sharing_property", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldlandAgreementSharingProperty = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldland_agreemen_alloc_owner_interest", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldlandAgreemenAllocOwnerInterest = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldland_decree_local_administration", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldlandDecreeLocalAdministration = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldland_protocols_auction", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldlandProtocolsAuction = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ldland_agreement_compensation", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $ldlandAgreementCompensation = 0;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contacts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set isGarden
     *
     * @param integer $isGarden
     *
     * @return Land
     */
    public function setIsGarden($isGarden)
    {
        $this->isGarden = $isGarden;

        return $this;
    }

    /**
     * Get isGarden
     *
     * @return integer
     */
    public function getIsGarden()
    {
        return $this->isGarden;
    }

    /**
     * Set elite
     *
     * @param integer $elite
     *
     * @return Land
     */
    public function setElite($elite)
    {
        $this->elite = $elite;

        return $this;
    }

    /**
     * Get elite
     *
     * @return integer
     */
    public function getElite()
    {
        return $this->elite;
    }

    /**
     * Set hasAdjoiningTerritory
     *
     * @param integer $hasAdjoiningTerritory
     *
     * @return Land
     */
    public function setHasAdjoiningTerritory($hasAdjoiningTerritory)
    {
        $this->hasAdjoiningTerritory = $hasAdjoiningTerritory;

        return $this;
    }

    /**
     * Get hasAdjoiningTerritory
     *
     * @return integer
     */
    public function getHasAdjoiningTerritory()
    {
        return $this->hasAdjoiningTerritory;
    }

    /**
     * Set surveying
     *
     * @param integer $surveying
     *
     * @return Land
     */
    public function setSurveying($surveying)
    {
        $this->surveying = $surveying;

        return $this;
    }

    /**
     * Get surveying
     *
     * @return integer
     */
    public function getSurveying()
    {
        return $this->surveying;
    }

    /**
     * Set exitToPond
     *
     * @param integer $exitToPond
     *
     * @return Land
     */
    public function setExitToPond($exitToPond)
    {
        $this->exitToPond = $exitToPond;

        return $this;
    }

    /**
     * Get exitToPond
     *
     * @return integer
     */
    public function getExitToPond()
    {
        return $this->exitToPond;
    }

    /**
     * Set terms
     *
     * @param string $terms
     *
     * @return Land
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;

        return $this;
    }

    /**
     * Get terms
     *
     * @return string
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * Set distanceToStations
     *
     * @param string $distanceToStations
     *
     * @return Land
     */
    public function setDistanceToStations($distanceToStations)
    {
        $this->distanceToStations = $distanceToStations;

        return $this;
    }

    /**
     * Get distanceToStations
     *
     * @return string
     */
    public function getDistanceToStations()
    {
        return $this->distanceToStations;
    }

    /**
     * Set groundArea
     *
     * @param float $groundArea
     *
     * @return Land
     */
    public function setGroundArea($groundArea)
    {
        $this->groundArea = $groundArea;

        return $this;
    }

    /**
     * Get groundArea
     *
     * @return float
     */
    public function getGroundArea()
    {
        return $this->groundArea;
    }

    /**
     * Set measurementX
     *
     * @param float $measurementX
     *
     * @return Land
     */
    public function setMeasurementX($measurementX)
    {
        $this->measurementX = $measurementX;

        return $this;
    }

    /**
     * Get measurementX
     *
     * @return float
     */
    public function getMeasurementX()
    {
        return $this->measurementX;
    }

    /**
     * Set measurementY
     *
     * @param float $measurementY
     *
     * @return Land
     */
    public function setMeasurementY($measurementY)
    {
        $this->measurementY = $measurementY;

        return $this;
    }

    /**
     * Get measurementY
     *
     * @return float
     */
    public function getMeasurementY()
    {
        return $this->measurementY;
    }

    /**
     * Set roadwayWidth
     *
     * @param float $roadwayWidth
     *
     * @return Land
     */
    public function setRoadwayWidth($roadwayWidth)
    {
        $this->roadwayWidth = $roadwayWidth;

        return $this;
    }

    /**
     * Get roadwayWidth
     *
     * @return float
     */
    public function getRoadwayWidth()
    {
        return $this->roadwayWidth;
    }

    /**
     * Set landLocation
     *
     * @param float $landLocation
     *
     * @return Land
     */
    public function setLandLocation($landLocation)
    {
        $this->landLocation = $landLocation;

        return $this;
    }

    /**
     * Get landLocation
     *
     * @return float
     */
    public function getLandLocation()
    {
        return $this->landLocation;
    }

    /**
     * Set facadeWidth
     *
     * @param float $facadeWidth
     *
     * @return Land
     */
    public function setFacadeWidth($facadeWidth)
    {
        $this->facadeWidth = $facadeWidth;

        return $this;
    }

    /**
     * Get facadeWidth
     *
     * @return float
     */
    public function getFacadeWidth()
    {
        return $this->facadeWidth;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Land
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set specialPrice
     *
     * @param string $specialPrice
     *
     * @return Land
     */
    public function setSpecialPrice($specialPrice)
    {
        $this->specialPrice = $specialPrice;

        return $this;
    }

    /**
     * Get specialPrice
     *
     * @return string
     */
    public function getSpecialPrice()
    {
        return $this->specialPrice;
    }

    /**
     * Set completedAt
     *
     * @param \DateTime $completedAt
     *
     * @return Land
     */
    public function setCompletedAt($completedAt)
    {
        $this->completedAt = $completedAt;

        return $this;
    }

    /**
     * Get completedAt
     *
     * @return \DateTime
     */
    public function getCompletedAt()
    {
        return $this->completedAt;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Land
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set entranceType
     *
     * @param integer $entranceType
     *
     * @return Land
     */
    public function setEntranceType($entranceType)
    {
        $this->entranceType = $entranceType;

        return $this;
    }

    /**
     * Get entranceType
     *
     * @return integer
     */
    public function getEntranceType()
    {
        return $this->entranceType;
    }

    /**
     * Set landPosition
     *
     * @param integer $landPosition
     *
     * @return Land
     */
    public function setLandPosition($landPosition)
    {
        $this->landPosition = $landPosition;

        return $this;
    }

    /**
     * Get landPosition
     *
     * @return integer
     */
    public function getLandPosition()
    {
        return $this->landPosition;
    }

    /**
     * Set relief
     *
     * @param integer $relief
     *
     * @return Land
     */
    public function setRelief($relief)
    {
        $this->relief = $relief;

        return $this;
    }

    /**
     * Get relief
     *
     * @return integer
     */
    public function getRelief()
    {
        return $this->relief;
    }

    /**
     * Set rightTypeLand
     *
     * @param integer $rightTypeLand
     *
     * @return Land
     */
    public function setRightTypeLand($rightTypeLand)
    {
        $this->rightTypeLand = $rightTypeLand;

        return $this;
    }

    /**
     * Get rightTypeLand
     *
     * @return integer
     */
    public function getRightTypeLand()
    {
        return $this->rightTypeLand;
    }

    /**
     * Set hasCafes
     *
     * @param integer $hasCafes
     *
     * @return Land
     */
    public function setHasCafes($hasCafes)
    {
        $this->hasCafes = $hasCafes;

        return $this;
    }

    /**
     * Get hasCafes
     *
     * @return integer
     */
    public function getHasCafes()
    {
        return $this->hasCafes;
    }

    /**
     * Set hasCinemas
     *
     * @param integer $hasCinemas
     *
     * @return Land
     */
    public function setHasCinemas($hasCinemas)
    {
        $this->hasCinemas = $hasCinemas;

        return $this;
    }

    /**
     * Get hasCinemas
     *
     * @return integer
     */
    public function getHasCinemas()
    {
        return $this->hasCinemas;
    }

    /**
     * Set hasHospitals
     *
     * @param integer $hasHospitals
     *
     * @return Land
     */
    public function setHasHospitals($hasHospitals)
    {
        $this->hasHospitals = $hasHospitals;

        return $this;
    }

    /**
     * Get hasHospitals
     *
     * @return integer
     */
    public function getHasHospitals()
    {
        return $this->hasHospitals;
    }

    /**
     * Set hasEducational
     *
     * @param integer $hasEducational
     *
     * @return Land
     */
    public function setHasEducational($hasEducational)
    {
        $this->hasEducational = $hasEducational;

        return $this;
    }

    /**
     * Get hasEducational
     *
     * @return integer
     */
    public function getHasEducational()
    {
        return $this->hasEducational;
    }

    /**
     * Set communicWater
     *
     * @param integer $communicWater
     *
     * @return Land
     */
    public function setCommunicWater($communicWater)
    {
        $this->communicWater = $communicWater;

        return $this;
    }

    /**
     * Get communicWater
     *
     * @return integer
     */
    public function getCommunicWater()
    {
        return $this->communicWater;
    }

    /**
     * Set communicGas
     *
     * @param integer $communicGas
     *
     * @return Land
     */
    public function setCommunicGas($communicGas)
    {
        $this->communicGas = $communicGas;

        return $this;
    }

    /**
     * Get communicGas
     *
     * @return integer
     */
    public function getCommunicGas()
    {
        return $this->communicGas;
    }

    /**
     * Set communicElectricity
     *
     * @param integer $communicElectricity
     *
     * @return Land
     */
    public function setCommunicElectricity($communicElectricity)
    {
        $this->communicElectricity = $communicElectricity;

        return $this;
    }

    /**
     * Get communicElectricity
     *
     * @return integer
     */
    public function getCommunicElectricity()
    {
        return $this->communicElectricity;
    }

    /**
     * Set communicPhone
     *
     * @param integer $communicPhone
     *
     * @return Land
     */
    public function setCommunicPhone($communicPhone)
    {
        $this->communicPhone = $communicPhone;

        return $this;
    }

    /**
     * Get communicPhone
     *
     * @return integer
     */
    public function getCommunicPhone()
    {
        return $this->communicPhone;
    }

    /**
     * Set communicCanalisation
     *
     * @param integer $communicCanalisation
     *
     * @return Land
     */
    public function setCommunicCanalisation($communicCanalisation)
    {
        $this->communicCanalisation = $communicCanalisation;

        return $this;
    }

    /**
     * Get communicCanalisation
     *
     * @return integer
     */
    public function getCommunicCanalisation()
    {
        return $this->communicCanalisation;
    }

    /**
     * Set paymentForElectricity
     *
     * @param integer $paymentForElectricity
     *
     * @return Land
     */
    public function setPaymentForElectricity($paymentForElectricity)
    {
        $this->paymentForElectricity = $paymentForElectricity;

        return $this;
    }

    /**
     * Get paymentForElectricity
     *
     * @return integer
     */
    public function getPaymentForElectricity()
    {
        return $this->paymentForElectricity;
    }

    /**
     * Set paymentForGas
     *
     * @param integer $paymentForGas
     *
     * @return Land
     */
    public function setPaymentForGas($paymentForGas)
    {
        $this->paymentForGas = $paymentForGas;

        return $this;
    }

    /**
     * Get paymentForGas
     *
     * @return integer
     */
    public function getPaymentForGas()
    {
        return $this->paymentForGas;
    }

    /**
     * Set paymentForWater
     *
     * @param integer $paymentForWater
     *
     * @return Land
     */
    public function setPaymentForWater($paymentForWater)
    {
        $this->paymentForWater = $paymentForWater;

        return $this;
    }

    /**
     * Get paymentForWater
     *
     * @return integer
     */
    public function getPaymentForWater()
    {
        return $this->paymentForWater;
    }

    /**
     * Set paymentForCanalization
     *
     * @param integer $paymentForCanalization
     *
     * @return Land
     */
    public function setPaymentForCanalization($paymentForCanalization)
    {
        $this->paymentForCanalization = $paymentForCanalization;

        return $this;
    }

    /**
     * Get paymentForCanalization
     *
     * @return integer
     */
    public function getPaymentForCanalization()
    {
        return $this->paymentForCanalization;
    }

    /**
     * Set cityFiasId
     *
     * @param string $cityFiasId
     *
     * @return Land
     */
    public function setCityFiasId($cityFiasId)
    {
        $this->cityFiasId = $cityFiasId;

        return $this;
    }

    /**
     * Get cityFiasId
     *
     * @return string
     */
    public function getCityFiasId()
    {
        return $this->cityFiasId;
    }

    /**
     * Set district
     *
     * @param integer $district
     *
     * @return Land
     */
    public function setDistrict($district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return integer
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set streetFiasId
     *
     * @param string $streetFiasId
     *
     * @return Land
     */
    public function setStreetFiasId($streetFiasId)
    {
        $this->streetFiasId = $streetFiasId;

        return $this;
    }

    /**
     * Get streetFiasId
     *
     * @return string
     */
    public function getStreetFiasId()
    {
        return $this->streetFiasId;
    }

    /**
     * Set houseFiasId
     *
     * @param string $houseFiasId
     *
     * @return Land
     */
    public function setHouseFiasId($houseFiasId)
    {
        $this->houseFiasId = $houseFiasId;

        return $this;
    }

    /**
     * Get houseFiasId
     *
     * @return string
     */
    public function getHouseFiasId()
    {
        return $this->houseFiasId;
    }

    /**
     * Set block
     *
     * @param string $block
     *
     * @return Land
     */
    public function setBlock($block)
    {
        $this->block = $block;

        return $this;
    }

    /**
     * Get block
     *
     * @return string
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * Set exclusive
     *
     * @param integer $exclusive
     *
     * @return Land
     */
    public function setExclusive($exclusive)
    {
        $this->exclusive = $exclusive;

        return $this;
    }

    /**
     * Get exclusive
     *
     * @return integer
     */
    public function getExclusive()
    {
        return $this->exclusive;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return Land
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return Land
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set location
     *
     * @param integer $location
     *
     * @return Land
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return integer
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set moderationStatus
     *
     * @param integer $moderationStatus
     *
     * @return Land
     */
    public function setModerationStatus($moderationStatus)
    {
        $this->moderationStatus = $moderationStatus;

        return $this;
    }

    /**
     * Get moderationStatus
     *
     * @return integer
     */
    public function getModerationStatus()
    {
        return $this->moderationStatus;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Land
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set textOnTheSite
     *
     * @param string $textOnTheSite
     *
     * @return Land
     */
    public function setTextOnTheSite($textOnTheSite)
    {
        $this->textOnTheSite = $textOnTheSite;

        return $this;
    }

    /**
     * Get textOnTheSite
     *
     * @return string
     */
    public function getTextOnTheSite()
    {
        return $this->textOnTheSite;
    }

    /**
     * Set openSale
     *
     * @param integer $openSale
     *
     * @return Land
     */
    public function setOpenSale($openSale)
    {
        $this->openSale = $openSale;

        return $this;
    }

    /**
     * Get openSale
     *
     * @return integer
     */
    public function getOpenSale()
    {
        return $this->openSale;
    }

    /**
     * Set netSale
     *
     * @param integer $netSale
     *
     * @return Land
     */
    public function setNetSale($netSale)
    {
        $this->netSale = $netSale;

        return $this;
    }

    /**
     * Get netSale
     *
     * @return integer
     */
    public function getNetSale()
    {
        return $this->netSale;
    }

    /**
     * Set moderationAt
     *
     * @param \DateTime $moderationAt
     *
     * @return Land
     */
    public function setModerationAt($moderationAt)
    {
        $this->moderationAt = $moderationAt;

        return $this;
    }

    /**
     * Get moderationAt
     *
     * @return \DateTime
     */
    public function getModerationAt()
    {
        return $this->moderationAt;
    }

    /**
     * Set lastСalled
     *
     * @param \DateTime $lastСalled
     *
     * @return Land
     */
    public function setLastСalled($lastСalled)
    {
        $this->lastСalled = $lastСalled;

        return $this;
    }

    /**
     * Get lastСalled
     *
     * @return \DateTime
     */
    public function getLastСalled()
    {
        return $this->lastСalled;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Land
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Land
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set fromFirm
     *
     * @param integer $fromFirm
     *
     * @return Land
     */
    public function setFromFirm($fromFirm)
    {
        $this->fromFirm = $fromFirm;

        return $this;
    }

    /**
     * Get fromFirm
     *
     * @return integer
     */
    public function getFromFirm()
    {
        return $this->fromFirm;
    }

    /**
     * Set source
     *
     * @param \AppBundle\Entity\Source $source
     *
     * @return Land
     */
    public function setSource(\AppBundle\Entity\Source $source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return \AppBundle\Entity\Source
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set agent
     *
     * @param \AppBundle\Entity\User $agent
     *
     * @return Land
     */
    public function setAgent(\AppBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \AppBundle\Entity\User
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Add contact
     *
     * @param \AppBundle\Entity\Client $contact
     *
     * @return Land
     */
    public function addContact(\AppBundle\Entity\Client $contact)
    {
        $this->contacts[] = $contact;

        return $this;
    }

    /**
     * Remove contact
     *
     * @param \AppBundle\Entity\Client $contact
     */
    public function removeContact(\AppBundle\Entity\Client $contact)
    {
        $this->contacts->removeElement($contact);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Add contractsExclusive
     *
     * @param \AppBundle\Entity\ContractExclusive $contractsExclusive
     *
     * @return Land
     */
    public function addContractsExclusive(\AppBundle\Entity\ContractExclusive $contractsExclusive)
    {
        $this->contractsExclusive[] = $contractsExclusive;

        return $this;
    }

    /**
     * Remove contractsExclusive
     *
     * @param \AppBundle\Entity\ContractExclusive $contractsExclusive
     */
    public function removeContractsExclusive(\AppBundle\Entity\ContractExclusive $contractsExclusive)
    {
        $this->contractsExclusive->removeElement($contractsExclusive);
    }

    /**
     * Get contractsExclusive
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContractsExclusive()
    {
        return $this->contractsExclusive;
    }

    /**
     * Add contractsView
     *
     * @param \AppBundle\Entity\ContractView $contractsView
     *
     * @return Land
     */
    public function addContractsView(\AppBundle\Entity\ContractView $contractsView)
    {
        $this->contractsView[] = $contractsView;

        return $this;
    }

    /**
     * Remove contractsView
     *
     * @param \AppBundle\Entity\ContractView $contractsView
     */
    public function removeContractsView(\AppBundle\Entity\ContractView $contractsView)
    {
        $this->contractsView->removeElement($contractsView);
    }

    /**
     * Get contractsView
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContractsView()
    {
        return $this->contractsView;
    }

    /**
     * Add contractsCooperation
     *
     * @param \AppBundle\Entity\ContractCooperation $contractsCooperation
     *
     * @return Land
     */
    public function addContractsCooperation(\AppBundle\Entity\ContractCooperation $contractsCooperation)
    {
        $this->contractsCooperation[] = $contractsCooperation;

        return $this;
    }

    /**
     * Remove contractsCooperation
     *
     * @param \AppBundle\Entity\ContractCooperation $contractsCooperation
     */
    public function removeContractsCooperation(\AppBundle\Entity\ContractCooperation $contractsCooperation)
    {
        $this->contractsCooperation->removeElement($contractsCooperation);
    }

    /**
     * Get contractsCooperation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContractsCooperation()
    {
        return $this->contractsCooperation;
    }

    /**
     * Return source id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("source")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getSourceId()
    {
        if ($this->source) {
            return array('id' => $this->source->getId());
        }

        return null;
    }

    /**
     * Return agent id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("agent")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getAgentId()
    {
        if ($this->agent) {
            return array('id' => $this->agent->getId());
        }

        return null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("contacts")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getContactsId()
    {
        $arr = array();

        if ($this->contacts) {
            foreach ($this->contacts as $contact) {
                $arr[] = array('id' => $contact->getId());
            }
        }

        return $arr;
    }
}
