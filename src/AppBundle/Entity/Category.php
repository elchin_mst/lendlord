<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=false, nullable=false)
     *
     * @Assert\NotBlank()
     * @JMS\Groups({"List", "Selected"})
     */
    private $name;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="User", mappedBy="category")
     */
    protected $users;

    /**
     * @var decimal
     *
     * @ORM\Column(name="plan", type="decimal", precision=2, scale=1)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $plan;

    /**
     * @var decimal
     *
     * @ORM\Column(name="percent", type="decimal", precision=2, scale=1)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $percent;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Position", inversedBy="categories")
     * @ORM\JoinColumn(name="position_id", referencedColumnName="id", onDelete="SET NULL")
     *
     * @JMS\Type("AppBundle\Entity\Position")
     * @JMS\Accessor(setter="setPosition")
     **/
    protected $position;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Category
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;
        $user->setCategory($this);

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
        $user->setCategory(null);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users->toArray();
    }

    /**
     * Set plan
     *
     * @param string $plan
     *
     * @return Category
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return string
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set percent
     *
     * @param string $percent
     *
     * @return Category
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return string
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set position
     *
     * @param \AppBundle\Entity\Position $position
     *
     * @return Category
     */
    public function setPosition(\AppBundle\Entity\Position $position = null)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return \AppBundle\Entity\Position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @ORM\PreRemove
     */
    public function deleteAllLinkedEntities()
    {
        // for OneToMany not working cascade set_null, to clean itself
        $users = $this->getUsers();

        foreach ($users as $user) {
            $user->setCategory(null);
        }
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("position")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getPositionId()
    {
        if ($this->position) {
            return array('id' => $this->position->getId());
        }

        return null;
    }
}
