<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Client
 *
 * @ORM\Table(name="clients")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\NotBlank()
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\NotBlank()
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="patronymic", type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $patronymic;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_phone", type="string", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\NotBlank()
     */
    private $mobilePhone;

    /**
     * @var string
     *
     * @ORM\Column(name="work_phone", type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $workPhone;

    /**
     * @var bool
     *
     * @ORM\Column(name="allow_sms", type="boolean", nullable=false)
     *
     * @JMS\Groups({"Selected"})
     */
    private $alowSms = false;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="clients")
     * @ORM\JoinColumn(name="manager_id", referencedColumnName="id", nullable=false)
     *
     * @JMS\Type("AppBundle\Entity\User")
     * @Assert\NotBlank()
     */
    private $manager;

    /**
     * @var Client
     *
     * @ORM\OneToMany(targetEntity="Note", mappedBy="client")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\Note>")
     */
    private $notes;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Deal", inversedBy="clients")
     *
     * @ORM\JoinTable(
     *     name="client_deal",
     *     joinColumns={@ORM\JoinColumn(
     *         name="client_id",
     *         referencedColumnName="id"
     *     )},
     *     inverseJoinColumns={@ORM\JoinColumn(
     *         name="deal_id",
     *         referencedColumnName="id"
     *     )}
     * )
     */
    private $deals;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="House", mappedBy="contacts")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\House>")
     */
    private $houses;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ApartmentSale", mappedBy="contacts")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ApartmentSale>")
     */
    private $apartmentsSale;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ApartmentRent", mappedBy="contacts")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ApartmentRent>")
     */
    private $apartmentsRent;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->deals = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Client
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Client
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set patronymic
     *
     * @param string $patronymic
     *
     * @return Client
     */
    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    /**
     * Get patronymic
     *
     * @return string
     */
    public function getPatronymic()
    {
        return $this->patronymic;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set mobilePhone
     *
     * @param string $mobilePhone
     *
     * @return Client
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;

        return $this;
    }

    /**
     * Get mobilePhone
     *
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * Set workPhone
     *
     * @param string $workPhone
     *
     * @return Client
     */
    public function setWorkPhone($workPhone)
    {
        $this->workPhone = $workPhone;

        return $this;
    }

    /**
     * Get workPhone
     *
     * @return string
     */
    public function getWorkPhone()
    {
        return $this->workPhone;
    }

    /**
     * Set alowSms
     *
     * @param boolean $alowSms
     *
     * @return Client
     */
    public function setAlowSms($alowSms)
    {
        $this->alowSms = $alowSms;

        return $this;
    }

    /**
     * Get alowSms
     *
     * @return boolean
     */
    public function getAlowSms()
    {
        return $this->alowSms;
    }

    /**
     * Set manager
     *
     * @param \AppBundle\Entity\User $manager
     *
     * @return Client
     */
    public function setManager(\AppBundle\Entity\User $manager)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * Get manager
     *
     * @return \AppBundle\Entity\User
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Add note
     *
     * @param \AppBundle\Entity\Note $note
     *
     * @return Client
     */
    public function addNote(\AppBundle\Entity\Note $note)
    {
        $this->notes[] = $note;

        return $this;
    }

    /**
     * Remove note
     *
     * @param \AppBundle\Entity\Note $note
     */
    public function removeNote(\AppBundle\Entity\Note $note)
    {
        $this->notes->removeElement($note);
    }

    /**
     * Get notes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Add deal
     *
     * @param \AppBundle\Entity\Deal $deal
     *
     * @return Client
     */
    public function addDeal(\AppBundle\Entity\Deal $deal)
    {
        $this->deals[] = $deal;

        return $this;
    }

    /**
     * Remove deal
     *
     * @param \AppBundle\Entity\Deal $deal
     */
    public function removeDeal(\AppBundle\Entity\Deal $deal)
    {
        $this->deals->removeElement($deal);
    }

    /**
     * Get deals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDeals()
    {
        return $this->deals;
    }

    /**
     * Get manager id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("manager")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getManagerId()
    {
        if ($this->manager) {
            return array(
                'id' => $this->manager->getId(),
            );
        }

        return null;
    }

    /**
     * Get notes id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("notes")
     * @JMS\Groups({"Selected"})
     *
     * @return array
     */
    public function getNoteIds()
    {
        $arr = array();

        if ($this->notes) {
            foreach ($this->notes as $note) {
                $arr[] = array('id' => $note->getId());
            }
        }

        return $arr;
    }

    /**
     * Get notes id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("deals")
     * @JMS\Groups({"Selected"})
     *
     * @return array
     */
    public function getDealIds()
    {
        $arr = array();

        if ($this->deals) {
            foreach ($this->deals as $deal) {
                $arr[] = array('id' => $deal->getId());
            }
        }

        return $arr;
    }

    /**
     * Add house
     *
     * @param \AppBundle\Entity\House $house
     *
     * @return Client
     */
    public function addHouse(\AppBundle\Entity\House $house)
    {
        $this->houses[] = $house;

        return $this;
    }

    /**
     * Remove house
     *
     * @param \AppBundle\Entity\House $house
     */
    public function removeHouse(\AppBundle\Entity\House $house)
    {
        $this->houses->removeElement($house);
    }

    /**
     * Get houses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHouses()
    {
        return $this->houses;
    }

    /**
     * Add apartmentsSale
     *
     * @param \AppBundle\Entity\ApartmentSale $apartmentsSale
     *
     * @return Client
     */
    public function addApartmentsSale(\AppBundle\Entity\ApartmentSale $apartmentsSale)
    {
        $this->apartmentsSale[] = $apartmentsSale;

        return $this;
    }

    /**
     * Remove apartmentsSale
     *
     * @param \AppBundle\Entity\ApartmentSale $apartmentsSale
     */
    public function removeApartmentsSale(\AppBundle\Entity\ApartmentSale $apartmentsSale)
    {
        $this->apartmentsSale->removeElement($apartmentsSale);
    }

    /**
     * Get apartmentsSale
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApartmentsSale()
    {
        return $this->apartmentsSale;
    }

    /**
     * Add apartmentsRent
     *
     * @param \AppBundle\Entity\ApartmentRent $apartmentsRent
     *
     * @return Client
     */
    public function addApartmentsRent(\AppBundle\Entity\ApartmentRent $apartmentsRent)
    {
        $this->apartmentsRent[] = $apartmentsRent;

        return $this;
    }

    /**
     * Remove apartmentsRent
     *
     * @param \AppBundle\Entity\ApartmentRent $apartmentsRent
     */
    public function removeApartmentsRent(\AppBundle\Entity\ApartmentRent $apartmentsRent)
    {
        $this->apartmentsRent->removeElement($apartmentsRent);
    }

    /**
     * Get apartmentsRent
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApartmentsRent()
    {
        return $this->apartmentsRent;
    }
}
