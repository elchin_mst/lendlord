<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Apartment
 *
 * @ORM\MappedSuperclass
 */
class Apartment extends Object
{
    /**
     * @var int
     *
     * @ORM\Column(name="floor", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("integer")
     * @Assert\Range(min = 1)
     */
    protected $floor;

    /**
     * @var int
     *
     * @ORM\Column(name="floors", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("integer")
     * @Assert\Range(min = 1)
     */
    protected $floors;

    /**
     * @var int
     *
     * @ORM\Column(name="elite", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("integer")
     * @Assert\Range(min = 0)
     */
    protected $elite = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="terms", type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("string")
     */
    protected $terms;

    /**
     * @var string
     *
     * @ORM\Column(name="apartment", type="string", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("string")
     * @Assert\NotBlank()
     */
    protected $apartment;

    /**
     * @var float
     *
     * @ORM\Column(name="total_sq", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("float")
     */
    protected $totalSq;

    /**
     * @var float
     *
     * @ORM\Column(name="living_sq", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("float")
     */
    protected $livingSq;

    /**
     * @var float
     *
     * @ORM\Column(name="kitchen_sq", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("float")
     */
    protected $kitchenSq;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("float")
     * @Assert\Range(min = 1)
     */
    protected $price;

    /**
     * @var float
     *
     * @ORM\Column(name="special_price", type="decimal", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("float")
     */
    protected $specialPrice;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("integer")
     * @Assert\Range(min = 1)
     */
    protected $type;

    /**
     * @var int
     *
     * @ORM\Column(name="condition", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("integer")
     * @Assert\Range(min = 1)
     */
    protected $condition;

    /**
     * @var int
     *
     * @ORM\Column(name="commodities", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("integer")
     */
    protected $commodities;

    /**
     * @var int
     *
     * @ORM\Column(name="wooden_windows", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("integer")
     * @Assert\Range(min = 0, max = 2)
     */
    protected $woodenWindows = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="metal_plast_windows", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("integer")
     * @Assert\Range(min = 0, max = 2)
     */
    protected $metalPlastWindows = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="wooden_windows_eur", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("integer")
     * @Assert\Range(min = 0, max = 2)
     */
    protected $woodenWindowsEur = 0;
}
