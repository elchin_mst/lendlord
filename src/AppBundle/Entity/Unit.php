<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * unit - подразделение
 *
 * @ORM\Table(name="units")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UnitRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Unit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true, nullable=false)
     * @JMS\Groups({"List", "Selected"})
     */
    private $name;

    /**
     * One Unit has One head.
     *
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", unique=true,  nullable=true, onDelete="SET NULL")
     */
    private $head;

    /**
     * @ORM\ManyToMany(targetEntity="Office", inversedBy="units")
     * @ORM\JoinTable(
     *     name="office_unit",
     *     joinColumns={
     *         @ORM\JoinColumn(
     *             name="office_id",
     *             referencedColumnName="id",
     *             unique=false,
     *             onDelete="SET NULL"
     *         )
     *     },
     *     inverseJoinColumns={
     *         @ORM\JoinColumn(
     *             name="unit_id",
     *             referencedColumnName="id",
     *             unique=false,
     *             onDelete="SET NULL"
     *         )
     *     }
     * )
     */
    private $offices;

    /**
     * @ORM\OneToMany(
     *     targetEntity="User", mappedBy="unit",
     * )
     * @ORM\OrderBy({"id"="ASC"})
     */
    private $users;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Unit
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->offices = new \Doctrine\Common\Collections\ArrayCollection();
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Set head
     *
     * @param \AppBundle\Entity\User $head
     *
     * @return Unit
     */
    public function setHead(\AppBundle\Entity\User $head = null)
    {
        $this->head = $head;

        return $this;
    }

    /**
     * Get head
     *
     * @return \AppBundle\Entity\User
     */
    public function getHead()
    {
        return $this->head;
    }

    /**
     * Add office
     *
     * @param \AppBundle\Entity\Office $office
     *
     * @return Unit
     */
    public function addOffice(\AppBundle\Entity\Office $office)
    {
        $this->offices[] = $office;

        return $this;
    }

    /**
     * Remove office
     *
     * @param \AppBundle\Entity\Office $office
     */
    public function removeOffice(\AppBundle\Entity\Office $office)
    {
        $this->offices->removeElement($office);
    }

    /**
     * Get offices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffices()
    {
        return $this->offices->toArray();
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Unit
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;

        $user->setUnit($this);

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
        $user->setUnit(null);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users->toArray();
    }

    /**
     * @ORM\PreRemove
     */
    public function deleteAllLinkedEntities()
    {
        // TODO: for OneToMany not working cascade set_null, to clean itself
        $users = $this->getUsers();
        foreach ($users as $user) {
            $user->setUnit(null);
        }
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("head")
     * @JMS\Groups({"Selected"})
     *
     * @return array|null
     */
    public function getSelectedHead()
    {
        if ($this->head) {
            $arr = [
                'id' => $this->head->getId(),
                'first_name' => $this->head->getFirstName(),
                'last_name' => $this->head->getLastName(),
                'internal_phone' => $this->head->getInternalPhone(),
                'mobile_phone' => $this->head->getMobilePhone(),
                'email' => $this->head->getEmail(),
            ];

            return $arr;
        }

        return null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("offices")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getOfficesId()
    {
        $arr = array();

        if ($this->offices) {
            foreach ($this->offices as $of) {
                $arr[] = array('id' => $of->getId());
            }
        }

        return $arr;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("head")
     * @JMS\Groups({"List"})
     *
     * @return array|null
     */
    public function getListHead()
    {
        if ($this->head) {
            $arr = [
                'id' => $this->head->getId(),
                'first_name' => $this->head->getFirstName(),
                'last_name' => $this->head->getLastName(),
            ];

            return $arr;
        }

        return null;
    }
}
