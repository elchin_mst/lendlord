<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Object
 *
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class Object
{
    /**
     * @var string
     *
     * @ORM\Column(name="city_fias_id", type="string", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("string")
     * @Assert\NotBlank()
     */
    protected $cityFiasId;

    /**
     * @var string
     *
     * @ORM\Column(name="district", type="integer", nullable=false)
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("integer")
     * @Assert\NotBlank()
     */
    protected $district;

    /**
     * @var string
     *
     * @ORM\Column(name="street_fias_id", type="string", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("string")
     * @Assert\NotBlank()
     */
    protected $streetFiasId;

    /**
     * @var string
     *
     * @ORM\Column(name="house_fias_id", type="string", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("string")
     * @Assert\NotBlank()
     */
    protected $houseFiasId;

    /**
     * @var string
     *
     * @ORM\Column(name="house_number", type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("string")
     */
    protected $houseNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="block", type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("string")
     */
    protected $block;

    /**
     * @var integer
     *
     * @ORM\Column(name="exclusive", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("integer")
     * @Assert\Range(min = 0)
     */
    protected $exclusive = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("float")
     */
    protected $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", nullable=true)
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("float")
     */
    protected $latitude;

    /**
     * @var int
     *
     * @ORM\Column(name="location", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("integer")
     */
    protected $location;

    /**
     * @var integer
     *
     * @ORM\Column(name="moderation_status", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("integer")
     * @Assert\Range(min = 0)
     */
    protected $moderationStatus = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", nullable=true)
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("string")
     */
    protected $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="text_on_the_site", type="string", nullable=true)
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("string")
     */
    protected $textOnTheSite;

    /**
     * @var integer
     *
     * @ORM\Column(name="open_sale", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("integer")
     * @Assert\Range(min = 0, max = 2)
     */
    protected $openSale = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="net_sale", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("integer")
     * @Assert\Range(min = 0, max = 2)
     */
    protected $netSale = 0;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="moderation_at", type="datetime", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("DateTime")
     * @Assert\DateTime()
     */
    protected $moderationAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="last_сalled", type="datetime", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("DateTime")
     * @Assert\DateTime()
     */
    protected $lastСalled;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("DateTime")
     * @Assert\DateTime()
     */
    protected $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("DateTime")
     * @Assert\DateTime()
     */
    protected $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="from_firm", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("integer")
     * @Assert\Range(min = 0, max = 2)
     */
    protected $fromFirm = 0;

    /**
     * Set created_at and updated_at
     *
     * @ORM\PrePersist
     */
    public function setDateTime()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set updated_at
     *
     * @ORM\PreUpdate
     */
    public function updateDateTime()
    {
        $this->updatedAt = new \DateTime();
    }
}
