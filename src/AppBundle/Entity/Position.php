<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Position - должность
 *
 * @ORM\Table(name="positions")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PositionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Position
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @JMS\SerializedName("id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @JMS\Groups({"List", "Selected"})
     */
    private $name;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="User", mappedBy="position")
     */
    protected $users;

    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="position")
     * @JMS\Type("ArrayCollection<AppBundle\Entity\Category>")
     */
    private $categories;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Post
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;
        $user->setPosition($this);

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
        $user->setPosition(null);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users->toArray();
    }

    /**
     * Add category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return Position
     */
    public function addCategory(\AppBundle\Entity\Category $category)
    {
        $this->categories[] = $category;
        $category->setPosition($this);

        return $this;
    }

    /**
     * Remove category
     *
     * @param \AppBundle\Entity\Category $category
     */
    public function removeCategory(\AppBundle\Entity\Category $category)
    {
        $this->categories->removeElement($category);
        $category->setPosition(null);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories->toArray();
    }

    /**
     * @ORM\PreRemove
     */
    public function deleteAllLinkedEntities()
    {
        // for OneToMany not working cascade set_null, to clean itself

        $users = $this->getUsers();

        foreach ($users as $user) {
            $user->setPosition(null);
        }

        $categories = $this->getCategories();

        foreach ($categories as $category) {
            $category->setPosition(null);
        }
    }
}
