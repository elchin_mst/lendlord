<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ContractCooperation
 *
 * @ORM\Table(name="contract_cooperation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContractCooperationRepository")
 */
class ContractCooperation extends Contract
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(
     *     name="agent2_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     *
     * @JMS\Type("AppBundle\Entity\User")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $agent2;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="Deal")
     * @ORM\JoinColumn(
     *     name="deal_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     *
     * @JMS\Type("AppBundle\Entity\Deal")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $deal;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ApartmentSale", inversedBy="contractsCooperation")
     * @ORM\JoinTable(
     *     name="contract_cooperation_apartment_sale",
     *     joinColumns={
     *          @ORM\JoinColumn(
     *              name="contract_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="object_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     }
     * )
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ApartmentSale>")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $apartmentsSale;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ApartmentRent", inversedBy="contractsCooperation")
     * @ORM\JoinTable(
     *     name="contract_cooperation_apartment_rent",
     *     joinColumns={
     *          @ORM\JoinColumn(
     *              name="contract_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="object_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     }
     * )
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ApartmentRent>")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $apartmentsRent;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="House", inversedBy="contractsCooperation")
     * @ORM\JoinTable(
     *     name="contract_cooperation_house",
     *     joinColumns={
     *          @ORM\JoinColumn(
     *              name="contract_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="object_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     }
     * )
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\House>")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $houses;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Land", inversedBy="contractsCooperation")
     * @ORM\JoinTable(
     *     name="contract_commercial_land",
     *     joinColumns={
     *          @ORM\JoinColumn(
     *              name="contract_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="object_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     }
     * )
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\Land>")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $lands;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="CommercialProperty", inversedBy="contractsCooperation")
     * @ORM\JoinTable(
     *     name="contract_cooperation_commercial_prop",
     *     joinColumns={
     *          @ORM\JoinColumn(
     *              name="contract_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="object_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     }
     * )
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\Land>")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $commercialProperty;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="handling_date", type="datetime", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @JMS\Type("DateTime")
     * @Assert\DateTime()
     */
    protected $handlingDate;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0)
     */
    private $status = 1;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->apartmentsSale = new \Doctrine\Common\Collections\ArrayCollection();
        $this->apartmentsRent = new \Doctrine\Common\Collections\ArrayCollection();
        $this->houses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->lands = new \Doctrine\Common\Collections\ArrayCollection();
        $this->commercialProperty = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set handlingDate
     *
     * @param \DateTime $handlingDate
     *
     * @return ContractCooperation
     */
    public function setHandlingDate($handlingDate)
    {
        $this->handlingDate = $handlingDate;

        return $this;
    }

    /**
     * Get handlingDate
     *
     * @return \DateTime
     */
    public function getHandlingDate()
    {
        return $this->handlingDate;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return ContractCooperation
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return ContractCooperation
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ContractCooperation
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ContractCooperation
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set agent2
     *
     * @param \AppBundle\Entity\User $agent2
     *
     * @return ContractCooperation
     */
    public function setAgent2(\AppBundle\Entity\User $agent2)
    {
        $this->agent2 = $agent2;

        return $this;
    }

    /**
     * Get agent2
     *
     * @return \AppBundle\Entity\User
     */
    public function getAgent2()
    {
        return $this->agent2;
    }

    /**
     * Set deal
     *
     * @param \AppBundle\Entity\Deal $deal
     *
     * @return ContractView
     */
    public function setDeal(\AppBundle\Entity\Deal $deal)
    {
        $this->deal = $deal;

        return $this;
    }

    /**
     * Get deal
     *
     * @return \AppBundle\Entity\Deal
     */
    public function getDeal()
    {
        return $this->deal;
    }

    /**
     * Add apartmentsSale
     *
     * @param \AppBundle\Entity\ApartmentSale $apartmentsSale
     *
     * @return ContractCooperation
     */
    public function addApartmentsSale(\AppBundle\Entity\ApartmentSale $apartmentsSale)
    {
        $this->apartmentsSale[] = $apartmentsSale;

        return $this;
    }

    /**
     * Remove apartmentsSale
     *
     * @param \AppBundle\Entity\ApartmentSale $apartmentsSale
     */
    public function removeApartmentsSale(\AppBundle\Entity\ApartmentSale $apartmentsSale)
    {
        $this->apartmentsSale->removeElement($apartmentsSale);
    }

    /**
     * Get apartmentsSale
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApartmentsSale()
    {
        return $this->apartmentsSale;
    }

    /**
     * Add apartmentsRent
     *
     * @param \AppBundle\Entity\ApartmentRent $apartmentsRent
     *
     * @return ContractCooperation
     */
    public function addApartmentsRent(\AppBundle\Entity\ApartmentRent $apartmentsRent)
    {
        $this->apartmentsRent[] = $apartmentsRent;

        return $this;
    }

    /**
     * Remove apartmentsRent
     *
     * @param \AppBundle\Entity\ApartmentRent $apartmentsRent
     */
    public function removeApartmentsRent(\AppBundle\Entity\ApartmentRent $apartmentsRent)
    {
        $this->apartmentsRent->removeElement($apartmentsRent);
    }

    /**
     * Get apartmentsRent
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApartmentsRent()
    {
        return $this->apartmentsRent;
    }

    /**
     * Add house
     *
     * @param \AppBundle\Entity\House $house
     *
     * @return ContractCooperation
     */
    public function addHouse(\AppBundle\Entity\House $house)
    {
        $this->houses[] = $house;

        return $this;
    }

    /**
     * Remove house
     *
     * @param \AppBundle\Entity\House $house
     */
    public function removeHouse(\AppBundle\Entity\House $house)
    {
        $this->houses->removeElement($house);
    }

    /**
     * Get houses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHouses()
    {
        return $this->houses;
    }

    /**
     * Add land
     *
     * @param \AppBundle\Entity\Land $land
     *
     * @return ContractCooperation
     */
    public function addLand(\AppBundle\Entity\Land $land)
    {
        $this->lands[] = $land;

        return $this;
    }

    /**
     * Remove land
     *
     * @param \AppBundle\Entity\Land $land
     */
    public function removeLand(\AppBundle\Entity\Land $land)
    {
        $this->lands->removeElement($land);
    }

    /**
     * Get lands
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLands()
    {
        return $this->lands;
    }

    /**
     * Add commercialProperty
     *
     * @param \AppBundle\Entity\CommercialProperty $commercialProperty
     *
     * @return ContractCooperation
     */
    public function addCommercialProperty(\AppBundle\Entity\CommercialProperty $commercialProperty)
    {
        $this->commercialProperty[] = $commercialProperty;

        return $this;
    }

    /**
     * Remove commercialProperty
     *
     * @param \AppBundle\Entity\CommercialProperty $commercialProperty
     */
    public function removeCommercialProperty(\AppBundle\Entity\CommercialProperty $commercialProperty)
    {
        $this->commercialProperty->removeElement($commercialProperty);
    }

    /**
     * Get commercialProperty
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommercialProperty()
    {
        return $this->commercialProperty;
    }

    /**
     * Set agent
     *
     * @param \AppBundle\Entity\User $agent
     *
     * @return ContractCooperation
     */
    public function setAgent(\AppBundle\Entity\User $agent)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \AppBundle\Entity\User
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set createdUser
     *
     * @param \AppBundle\Entity\User $createdUser
     *
     * @return ContractCooperation
     */
    public function setCreatedUser(\AppBundle\Entity\User $createdUser)
    {
        $this->createdUser = $createdUser;

        return $this;
    }

    /**
     * Get createdUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedUser()
    {
        return $this->createdUser;
    }

    /**
     * Set updatedUser
     *
     * @param \AppBundle\Entity\User $updatedUser
     *
     * @return ContractCooperation
     */
    public function setUpdatedUser(\AppBundle\Entity\User $updatedUser)
    {
        $this->updatedUser = $updatedUser;

        return $this;
    }

    /**
     * Get updatedUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedUser()
    {
        return $this->updatedUser;
    }

    /**
     * Get agent id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("agent")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getAgentId()
    {
        if ($this->agent) {
            return array(
                'id' => $this->agent->getId(),
            );
        }

        return null;
    }

    /**
     * Get created user id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("created_user")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getCreatedUserId()
    {
        if ($this->createdUser) {
            return array(
                'id' => $this->createdUser->getId(),
            );
        }

        return null;
    }

    /**
     * Get updated user id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("updated_user")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getUpdatedUserId()
    {
        if ($this->updatedUser) {
            return array(
                'id' => $this->updatedUser->getId(),
            );
        }

        return null;
    }

    /**
     * Get client id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("agent2")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getAgent2Id()
    {
        if ($this->agent2) {
            return array(
                'id' => $this->agent2->getId(),
            );
        }

        return null;
    }

    /**
     * Get deal id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("deal")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getDealId()
    {
        if ($this->deal) {
            return array(
                'id' => $this->deal->getId(),
            );
        }

        return null;
    }

    /**
     * Get apartment sale ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("apartments_sale")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getApartmentSaleIds()
    {
        $arr = array();

        if ($this->apartmentsSale) {
            foreach ($this->apartmentsSale as $apartmentSale) {
                $arr[] = array('id' => $apartmentSale->getId());
            }
        }

        return $arr;
    }

    /**
     * Get apartment rent ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("apartments_rent")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getApartmentRentIds()
    {
        $arr = array();

        if ($this->apartmentsRent) {
            foreach ($this->apartmentsRent as $apartmentRent) {
                $arr[] = array('id' => $apartmentRent->getId());
            }
        }

        return $arr;
    }

    /**
     * Get house ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("houses")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getHouseIds()
    {
        $arr = array();

        if ($this->houses) {
            foreach ($this->houses as $house) {
                $arr[] = array('id' => $house->getId());
            }
        }

        return $arr;
    }

    /**
     * Get land ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("lands")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getLandIds()
    {
        $arr = array();

        if ($this->lands) {
            foreach ($this->lands as $land) {
                $arr[] = array('id' => $land->getId());
            }
        }

        return $arr;
    }

    /**
     * Get commercial property ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("commercial_property")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getCommercialPropertyIds()
    {
        $arr = array();

        if ($this->commercialProperty) {
            foreach ($this->commercialProperty as $commercialProperty) {
                $arr[] = array('id' => $commercialProperty->getId());
            }
        }

        return $arr;
    }
}
