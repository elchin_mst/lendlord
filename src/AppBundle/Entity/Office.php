<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Office
 *
 * @ORM\Table(name="offices")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OfficeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
//TODO: $officeDapartmentRefs
class Office
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @JMS\Groups({"List", "Selected"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, unique=true)
     * @JMS\Groups({"List", "Selected"})
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=15, unique=true)
     * @JMS\Groups({"List", "Selected"})
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @JMS\Groups({"List", "Selected"})
     */
    private $email;

    /**
     * @var decimal
     *
     * @ORM\Column(name="plan", type="decimal", precision=2, scale=1, nullable=true)
     * @JMS\Groups("Selected")
     */
    private $plan;

    /**
     * One Office has One head.
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="officeManages")
     * @ORM\JoinColumn(name="head_id", referencedColumnName="id")
     * @JMS\Type("AppBundle\Entity\User")
     */
    private $head;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="offices")
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity="Unit", mappedBy="offices")
     */
    private $units;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="OfficeDepartmentReference", mappedBy="office", cascade={"persist", "remove"})
     */
    private $officeDapartmentRefs;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Office
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->units = new \Doctrine\Common\Collections\ArrayCollection();
        $this->officeDapartmentRefs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Office
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;
        $user->addOffice($this);

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
        $user->removeOffice($this);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users->toArray();
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Office
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Office
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Office
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set plan
     *
     * @param string $plan
     *
     * @return Office
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return string
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set head
     *
     * @param \AppBundle\Entity\User $head
     *
     * @return Office
     */
    public function setHead(\AppBundle\Entity\User $head = null)
    {
        $this->head = $head;

        return $this;
    }

    /**
     * Get head
     *
     * @return \AppBundle\Entity\User
     */
    public function getHead()
    {
        return $this->head;
    }

    /**
     * Add unit
     *
     * @param \AppBundle\Entity\Unit $unit
     *
     * @return Office
     */
    public function addUnit(\AppBundle\Entity\Unit $unit)
    {
        $this->units[] = $unit;
        $unit->addOffice($this);

        return $this;
    }

    /**
     * Remove unit
     *
     * @param \AppBundle\Entity\Unit $unit
     */
    public function removeUnit(\AppBundle\Entity\Unit $unit)
    {
        $this->units->removeElement($unit);
        $unit->removeOffice($this);
    }

    /**
     * Get units
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUnits()
    {
        return $this->units->toArray();
    }

    /**
     * Add officeDapartmentRef
     *
     * @param \AppBundle\Entity\OfficeDepartmentReference $officeDapartmentRef
     *
     * @return Office
     */
    public function addOfficeDapartmentRef(\AppBundle\Entity\OfficeDepartmentReference $officeDapartmentRef)
    {
        $this->officeDapartmentRefs[] = $officeDapartmentRef;

        return $this;
    }

    /**
     * Remove officeDapartmentRef
     *
     * @param \AppBundle\Entity\OfficeDepartmentReference $officeDapartmentRef
     */
    public function removeOfficeDapartmentRef(\AppBundle\Entity\OfficeDepartmentReference $officeDapartmentRef)
    {
        $this->officeDapartmentRefs->removeElement($officeDapartmentRef);
    }

    /**
     * Get officeDapartmentRefs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOfficeDapartmentRefs()
    {
        return $this->officeDapartmentRefs->toArray();
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("head")
     * @JMS\Groups({"List", "Selected"})
     * @return array|null
     */
    public function getPositionId()
    {
        if ($this->head) {
            return array(
                'id' => $this->head->getId(),
                'first_name' => $this->head->getFirstName(),
                'last_name' => $this->head->getLastName(),
            );
        }

        return null;
    }
}
