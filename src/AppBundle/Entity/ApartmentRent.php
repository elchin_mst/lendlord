<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ApartmentRent
 *
 * @ORM\Table(name="apartments_rent")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApartmentRentRepository")
 */
class ApartmentRent extends Apartment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * @var Source
     *
     * @ORM\ManyToOne(targetEntity="Source", inversedBy="apartmentsRent")
     * @ORM\JoinColumn(
     *     name="source_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     *
     * @JMS\Type("AppBundle\Entity\Source")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    protected $source;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     *
     * @JMS\Type("AppBundle\Entity\User")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $agent;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Client", inversedBy="apartmentsRent")
     * @ORM\JoinTable(
     *     name="apartment_rent_client",
     *     joinColumns={
     *          @ORM\JoinColumn(
     *              name="apart_rent__id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="client_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     }
     * )
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\Client>")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $contacts;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ContractExclusive", mappedBy="apartmentsRent")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ContractExclusive>")
     */
    private $contractsExclusive;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ContractView", mappedBy="apartmentsRent")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ContractView>")
     */
    private $contractsView;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ContractCooperation", mappedBy="apartmentsRent")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ContractCooperation>")
     */
    private $contractsCooperation;

    /**
     * @var int
     *
     * @ORM\Column(name="rooms", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $rooms;

    /**
     * @var int
     *
     * @ORM\Column(name="has_phone", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasPhone = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_parking", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasParking = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="concierge", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $concierge = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="passenger_lift", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $passengerLift = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="service_lift", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $serviceLift = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="pledge", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $pledge = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="prepay", type="decimal", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $prepay;

    /**
     * @var int
     *
     * @ORM\Column(name="balcony", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 1)
     */
    private $balcony;

    /**
     * @var int
     *
     * @ORM\Column(name="bathroom", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 1)
     */
    private $bathroom;

    /**
     * @var int
     *
     * @ORM\Column(name="loggia", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 1)
     */
    private $loggia;

    /**
     * @var int
     *
     * @ORM\Column(name="related", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $related;

    /**
     * @var int
     *
     * @ORM\Column(name="housing_stock", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 1)
     */
    private $housingStock;

    /**
     * @var int
     *
     * @ORM\Column(name="furniture", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 1)
     */
    private $furniture;

    /**
     * @var int
     *
     * @ORM\Column(name="yard", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $yard;

    /**
     * @var int
     *
     * @ORM\Column(name="entrance", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $entrance;

    /**
     * @var int
     *
     * @ORM\Column(name="price_include_phone", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $priceIncludePhone = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="price_include_electricity", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $priceIncludeElectricity = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="price_include_internet", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $priceIncludeInternet = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="price_include_tv", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $priceIncludeTV = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="price_include_water", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $priceIncludeWater = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="price_include_gas", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $priceIncludeGas = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="rent_type_all", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $rentTypeAll = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="rent_type_for_office", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $rentTypeForOffice = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="rent_type_day", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $rentTypeDay = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_fridge", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasFridge = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_washing_machine", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasWashingMachine = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_microwave", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasMicrowave = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_dish_washer", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasDishWasher = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="small_household_appliances", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $smallHouseholdAppliances = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_tv", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasTv = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_split_system ", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasSplitSystem = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_boiler ", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasBoiler = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_vacuum_cleaner ", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasVacuumCleaner = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="pref_young_children", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $prefYoungChildren = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="pref_animals", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $prefAnimals = 0;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contacts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set rooms
     *
     * @param integer $rooms
     *
     * @return ApartmentRent
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;

        return $this;
    }

    /**
     * Get rooms
     *
     * @return integer
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Set hasPhone
     *
     * @param integer $hasPhone
     *
     * @return ApartmentRent
     */
    public function setHasPhone($hasPhone)
    {
        $this->hasPhone = $hasPhone;

        return $this;
    }

    /**
     * Get hasPhone
     *
     * @return integer
     */
    public function getHasPhone()
    {
        return $this->hasPhone;
    }

    /**
     * Set hasParking
     *
     * @param integer $hasParking
     *
     * @return ApartmentRent
     */
    public function setHasParking($hasParking)
    {
        $this->hasParking = $hasParking;

        return $this;
    }

    /**
     * Get hasParking
     *
     * @return integer
     */
    public function getHasParking()
    {
        return $this->hasParking;
    }

    /**
     * Set prepay
     *
     * @param string $prepay
     *
     * @return ApartmentRent
     */
    public function setPrepay($prepay)
    {
        $this->prepay = $prepay;

        return $this;
    }

    /**
     * Get prepay
     *
     * @return string
     */
    public function getPrepay()
    {
        return $this->prepay;
    }

    /**
     * Set furniture
     *
     * @param integer $furniture
     *
     * @return ApartmentRent
     */
    public function setFurniture($furniture)
    {
        $this->furniture = $furniture;

        return $this;
    }

    /**
     * Get furniture
     *
     * @return integer
     */
    public function getFurniture()
    {
        return $this->furniture;
    }

    /**
     * Set yard
     *
     * @param integer $yard
     *
     * @return ApartmentRent
     */
    public function setYard($yard)
    {
        $this->yard = $yard;

        return $this;
    }

    /**
     * Get yard
     *
     * @return integer
     */
    public function getYard()
    {
        return $this->yard;
    }

    /**
     * Set entrance
     *
     * @param integer $entrance
     *
     * @return ApartmentRent
     */
    public function setEntrance($entrance)
    {
        $this->entrance = $entrance;

        return $this;
    }

    /**
     * Get entrance
     *
     * @return integer
     */
    public function getEntrance()
    {
        return $this->entrance;
    }

    /**
     * Set priceIncludePhone
     *
     * @param integer $priceIncludePhone
     *
     * @return ApartmentRent
     */
    public function setPriceIncludePhone($priceIncludePhone)
    {
        $this->priceIncludePhone = $priceIncludePhone;

        return $this;
    }

    /**
     * Get priceIncludePhone
     *
     * @return integer
     */
    public function getPriceIncludePhone()
    {
        return $this->priceIncludePhone;
    }

    /**
     * Set priceIncludeElectricity
     *
     * @param integer $priceIncludeElectricity
     *
     * @return ApartmentRent
     */
    public function setPriceIncludeElectricity($priceIncludeElectricity)
    {
        $this->priceIncludeElectricity = $priceIncludeElectricity;

        return $this;
    }

    /**
     * Get priceIncludeElectricity
     *
     * @return integer
     */
    public function getPriceIncludeElectricity()
    {
        return $this->priceIncludeElectricity;
    }

    /**
     * Set priceIncludeInternet
     *
     * @param integer $priceIncludeInternet
     *
     * @return ApartmentRent
     */
    public function setPriceIncludeInternet($priceIncludeInternet)
    {
        $this->priceIncludeInternet = $priceIncludeInternet;

        return $this;
    }

    /**
     * Get priceIncludeInternet
     *
     * @return integer
     */
    public function getPriceIncludeInternet()
    {
        return $this->priceIncludeInternet;
    }

    /**
     * Set priceIncludeTV
     *
     * @param integer $priceIncludeTV
     *
     * @return ApartmentRent
     */
    public function setPriceIncludeTV($priceIncludeTV)
    {
        $this->priceIncludeTV = $priceIncludeTV;

        return $this;
    }

    /**
     * Get priceIncludeTV
     *
     * @return integer
     */
    public function getPriceIncludeTV()
    {
        return $this->priceIncludeTV;
    }

    /**
     * Set priceIncludeWater
     *
     * @param integer $priceIncludeWater
     *
     * @return ApartmentRent
     */
    public function setPriceIncludeWater($priceIncludeWater)
    {
        $this->priceIncludeWater = $priceIncludeWater;

        return $this;
    }

    /**
     * Get priceIncludeWater
     *
     * @return integer
     */
    public function getPriceIncludeWater()
    {
        return $this->priceIncludeWater;
    }

    /**
     * Set priceIncludeGas
     *
     * @param integer $priceIncludeGas
     *
     * @return ApartmentRent
     */
    public function setPriceIncludeGas($priceIncludeGas)
    {
        $this->priceIncludeGas = $priceIncludeGas;

        return $this;
    }

    /**
     * Get priceIncludeGas
     *
     * @return integer
     */
    public function getPriceIncludeGas()
    {
        return $this->priceIncludeGas;
    }

    /**
     * Set rentTypeAll
     *
     * @param integer $rentTypeAll
     *
     * @return ApartmentRent
     */
    public function setRentTypeAll($rentTypeAll)
    {
        $this->rentTypeAll = $rentTypeAll;

        return $this;
    }

    /**
     * Get rentTypeAll
     *
     * @return integer
     */
    public function getRentTypeAll()
    {
        return $this->rentTypeAll;
    }

    /**
     * Set rentTypeForOffice
     *
     * @param integer $rentTypeForOffice
     *
     * @return ApartmentRent
     */
    public function setRentTypeForOffice($rentTypeForOffice)
    {
        $this->rentTypeForOffice = $rentTypeForOffice;

        return $this;
    }

    /**
     * Get rentTypeForOffice
     *
     * @return integer
     */
    public function getRentTypeForOffice()
    {
        return $this->rentTypeForOffice;
    }

    /**
     * Set rentTypeDay
     *
     * @param integer $rentTypeDay
     *
     * @return ApartmentRent
     */
    public function setRentTypeDay($rentTypeDay)
    {
        $this->rentTypeDay = $rentTypeDay;

        return $this;
    }

    /**
     * Get rentTypeDay
     *
     * @return integer
     */
    public function getRentTypeDay()
    {
        return $this->rentTypeDay;
    }

    /**
     * Set hasFridge
     *
     * @param integer $hasFridge
     *
     * @return ApartmentRent
     */
    public function setHasFridge($hasFridge)
    {
        $this->hasFridge = $hasFridge;

        return $this;
    }

    /**
     * Get hasFridge
     *
     * @return integer
     */
    public function getHasFridge()
    {
        return $this->hasFridge;
    }

    /**
     * Set hasWashingMachine
     *
     * @param integer $hasWashingMachine
     *
     * @return ApartmentRent
     */
    public function setHasWashingMachine($hasWashingMachine)
    {
        $this->hasWashingMachine = $hasWashingMachine;

        return $this;
    }

    /**
     * Get hasWashingMachine
     *
     * @return integer
     */
    public function getHasWashingMachine()
    {
        return $this->hasWashingMachine;
    }

    /**
     * Set hasMicrowave
     *
     * @param integer $hasMicrowave
     *
     * @return ApartmentRent
     */
    public function setHasMicrowave($hasMicrowave)
    {
        $this->hasMicrowave = $hasMicrowave;

        return $this;
    }

    /**
     * Get hasMicrowave
     *
     * @return integer
     */
    public function getHasMicrowave()
    {
        return $this->hasMicrowave;
    }

    /**
     * Set hasDishWasher
     *
     * @param integer $hasDishWasher
     *
     * @return ApartmentRent
     */
    public function setHasDishWasher($hasDishWasher)
    {
        $this->hasDishWasher = $hasDishWasher;

        return $this;
    }

    /**
     * Get hasDishWasher
     *
     * @return integer
     */
    public function getHasDishWasher()
    {
        return $this->hasDishWasher;
    }

    /**
     * Set smallHouseholdAppliances
     *
     * @param integer $smallHouseholdAppliances
     *
     * @return ApartmentRent
     */
    public function setSmallHouseholdAppliances($smallHouseholdAppliances)
    {
        $this->smallHouseholdAppliances = $smallHouseholdAppliances;

        return $this;
    }

    /**
     * Get smallHouseholdAppliances
     *
     * @return integer
     */
    public function getSmallHouseholdAppliances()
    {
        return $this->smallHouseholdAppliances;
    }

    /**
     * Set hasTv
     *
     * @param integer $hasTv
     *
     * @return ApartmentRent
     */
    public function setHasTv($hasTv)
    {
        $this->hasTv = $hasTv;

        return $this;
    }

    /**
     * Get hasTv
     *
     * @return integer
     */
    public function getHasTv()
    {
        return $this->hasTv;
    }

    /**
     * Set hasSplitSystem
     *
     * @param integer $hasSplitSystem
     *
     * @return ApartmentRent
     */
    public function setHasSplitSystem($hasSplitSystem)
    {
        $this->hasSplitSystem = $hasSplitSystem;

        return $this;
    }

    /**
     * Get hasSplitSystem
     *
     * @return integer
     */
    public function getHasSplitSystem()
    {
        return $this->hasSplitSystem;
    }

    /**
     * Set hasBoiler
     *
     * @param integer $hasBoiler
     *
     * @return ApartmentRent
     */
    public function setHasBoiler($hasBoiler)
    {
        $this->hasBoiler = $hasBoiler;

        return $this;
    }

    /**
     * Get hasBoiler
     *
     * @return integer
     */
    public function getHasBoiler()
    {
        return $this->hasBoiler;
    }

    /**
     * Set hasVacuumCleaner
     *
     * @param integer $hasVacuumCleaner
     *
     * @return ApartmentRent
     */
    public function setHasVacuumCleaner($hasVacuumCleaner)
    {
        $this->hasVacuumCleaner = $hasVacuumCleaner;

        return $this;
    }

    /**
     * Get hasVacuumCleaner
     *
     * @return integer
     */
    public function getHasVacuumCleaner()
    {
        return $this->hasVacuumCleaner;
    }

    /**
     * Set prefYoungChildren
     *
     * @param integer $prefYoungChildren
     *
     * @return ApartmentRent
     */
    public function setPrefYoungChildren($prefYoungChildren)
    {
        $this->prefYoungChildren = $prefYoungChildren;

        return $this;
    }

    /**
     * Get prefYoungChildren
     *
     * @return integer
     */
    public function getPrefYoungChildren()
    {
        return $this->prefYoungChildren;
    }

    /**
     * Set prefAnimals
     *
     * @param integer $prefAnimals
     *
     * @return ApartmentRent
     */
    public function setPrefAnimals($prefAnimals)
    {
        $this->prefAnimals = $prefAnimals;

        return $this;
    }

    /**
     * Get prefAnimals
     *
     * @return integer
     */
    public function getPrefAnimals()
    {
        return $this->prefAnimals;
    }

    /**
     * Set floor
     *
     * @param integer $floor
     *
     * @return ApartmentRent
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get floor
     *
     * @return integer
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set floors
     *
     * @param integer $floors
     *
     * @return ApartmentRent
     */
    public function setFloors($floors)
    {
        $this->floors = $floors;

        return $this;
    }

    /**
     * Get floors
     *
     * @return integer
     */
    public function getFloors()
    {
        return $this->floors;
    }

    /**
     * Set elite
     *
     * @param integer $elite
     *
     * @return ApartmentRent
     */
    public function setElite($elite)
    {
        $this->elite = $elite;

        return $this;
    }

    /**
     * Get elite
     *
     * @return integer
     */
    public function getElite()
    {
        return $this->elite;
    }

    /**
     * Set terms
     *
     * @param string $terms
     *
     * @return ApartmentRent
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;

        return $this;
    }

    /**
     * Get terms
     *
     * @return string
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * Set apartment
     *
     * @param string $apartment
     *
     * @return ApartmentRent
     */
    public function setApartment($apartment)
    {
        $this->apartment = $apartment;

        return $this;
    }

    /**
     * Get apartment
     *
     * @return string
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * Set totalSq
     *
     * @param float $totalSq
     *
     * @return ApartmentRent
     */
    public function setTotalSq($totalSq)
    {
        $this->totalSq = $totalSq;

        return $this;
    }

    /**
     * Get totalSq
     *
     * @return float
     */
    public function getTotalSq()
    {
        return $this->totalSq;
    }

    /**
     * Set livingSq
     *
     * @param float $livingSq
     *
     * @return ApartmentRent
     */
    public function setLivingSq($livingSq)
    {
        $this->livingSq = $livingSq;

        return $this;
    }

    /**
     * Get livingSq
     *
     * @return float
     */
    public function getLivingSq()
    {
        return $this->livingSq;
    }

    /**
     * Set kitchenSq
     *
     * @param float $kitchenSq
     *
     * @return ApartmentRent
     */
    public function setKitchenSq($kitchenSq)
    {
        $this->kitchenSq = $kitchenSq;

        return $this;
    }

    /**
     * Get kitchenSq
     *
     * @return float
     */
    public function getKitchenSq()
    {
        return $this->kitchenSq;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return ApartmentRent
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set specialPrice
     *
     * @param string $specialPrice
     *
     * @return ApartmentRent
     */
    public function setSpecialPrice($specialPrice)
    {
        $this->specialPrice = $specialPrice;

        return $this;
    }

    /**
     * Get specialPrice
     *
     * @return string
     */
    public function getSpecialPrice()
    {
        return $this->specialPrice;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return ApartmentRent
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set condition
     *
     * @param integer $condition
     *
     * @return ApartmentRent
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;

        return $this;
    }

    /**
     * Get condition
     *
     * @return integer
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * Set commodities
     *
     * @param integer $commodities
     *
     * @return ApartmentRent
     */
    public function setCommodities($commodities)
    {
        $this->commodities = $commodities;

        return $this;
    }

    /**
     * Get commodities
     *
     * @return integer
     */
    public function getCommodities()
    {
        return $this->commodities;
    }

    /**
     * Set woodenWindows
     *
     * @param integer $woodenWindows
     *
     * @return ApartmentRent
     */
    public function setWoodenWindows($woodenWindows)
    {
        $this->woodenWindows = $woodenWindows;

        return $this;
    }

    /**
     * Get woodenWindows
     *
     * @return integer
     */
    public function getWoodenWindows()
    {
        return $this->woodenWindows;
    }

    /**
     * Set metalPlastWindows
     *
     * @param integer $metalPlastWindows
     *
     * @return ApartmentRent
     */
    public function setMetalPlastWindows($metalPlastWindows)
    {
        $this->metalPlastWindows = $metalPlastWindows;

        return $this;
    }

    /**
     * Get metalPlastWindows
     *
     * @return integer
     */
    public function getMetalPlastWindows()
    {
        return $this->metalPlastWindows;
    }

    /**
     * Set woodenWindowsEur
     *
     * @param integer $woodenWindowsEur
     *
     * @return ApartmentRent
     */
    public function setWoodenWindowsEur($woodenWindowsEur)
    {
        $this->woodenWindowsEur = $woodenWindowsEur;

        return $this;
    }

    /**
     * Get woodenWindowsEur
     *
     * @return integer
     */
    public function getWoodenWindowsEur()
    {
        return $this->woodenWindowsEur;
    }

    /**
     * Set cityFiasId
     *
     * @param string $cityFiasId
     *
     * @return ApartmentRent
     */
    public function setCityFiasId($cityFiasId)
    {
        $this->cityFiasId = $cityFiasId;

        return $this;
    }

    /**
     * Get cityFiasId
     *
     * @return string
     */
    public function getCityFiasId()
    {
        return $this->cityFiasId;
    }

    /**
     * Set district
     *
     * @param int $district
     *
     * @return ApartmentRent
     */
    public function setDistrict($district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return int
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set streetFiasId
     *
     * @param string $streetFiasId
     *
     * @return ApartmentRent
     */
    public function setStreetFiasId($streetFiasId)
    {
        $this->streetFiasId = $streetFiasId;

        return $this;
    }

    /**
     * Get streetFiasId
     *
     * @return string
     */
    public function getStreetFiasId()
    {
        return $this->streetFiasId;
    }

    /**
     * Set houseFiasId
     *
     * @param string $houseFiasId
     *
     * @return ApartmentRent
     */
    public function setHouseFiasId($houseFiasId)
    {
        $this->houseFiasId = $houseFiasId;

        return $this;
    }

    /**
     * Get houseFiasId
     *
     * @return string
     */
    public function getHouseFiasId()
    {
        return $this->houseFiasId;
    }

    /**
     * Set block
     *
     * @param string $block
     *
     * @return ApartmentRent
     */
    public function setBlock($block)
    {
        $this->block = $block;

        return $this;
    }

    /**
     * Get block
     *
     * @return string
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * Set exclusive
     *
     * @param integer $exclusive
     *
     * @return ApartmentRent
     */
    public function setExclusive($exclusive)
    {
        $this->exclusive = $exclusive;

        return $this;
    }

    /**
     * Get exclusive
     *
     * @return integer
     */
    public function getExclusive()
    {
        return $this->exclusive;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return ApartmentRent
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return ApartmentRent
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set location
     *
     * @param integer $location
     *
     * @return ApartmentRent
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return integer
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set moderationStatus
     *
     * @param integer $moderationStatus
     *
     * @return ApartmentRent
     */
    public function setModerationStatus($moderationStatus)
    {
        $this->moderationStatus = $moderationStatus;

        return $this;
    }

    /**
     * Get moderationStatus
     *
     * @return integer
     */
    public function getModerationStatus()
    {
        return $this->moderationStatus;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ApartmentRent
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set textOnTheSite
     *
     * @param string $textOnTheSite
     *
     * @return ApartmentRent
     */
    public function setTextOnTheSite($textOnTheSite)
    {
        $this->textOnTheSite = $textOnTheSite;

        return $this;
    }

    /**
     * Get textOnTheSite
     *
     * @return string
     */
    public function getTextOnTheSite()
    {
        return $this->textOnTheSite;
    }

    /**
     * Set openSale
     *
     * @param integer $openSale
     *
     * @return ApartmentRent
     */
    public function setOpenSale($openSale)
    {
        $this->openSale = $openSale;

        return $this;
    }

    /**
     * Get openSale
     *
     * @return integer
     */
    public function getOpenSale()
    {
        return $this->openSale;
    }

    /**
     * Set netSale
     *
     * @param integer $netSale
     *
     * @return ApartmentRent
     */
    public function setNetSale($netSale)
    {
        $this->netSale = $netSale;

        return $this;
    }

    /**
     * Get netSale
     *
     * @return integer
     */
    public function getNetSale()
    {
        return $this->netSale;
    }

    /**
     * Set moderationAt
     *
     * @param \DateTime $moderationAt
     *
     * @return ApartmentRent
     */
    public function setModerationAt($moderationAt)
    {
        $this->moderationAt = $moderationAt;

        return $this;
    }

    /**
     * Get moderationAt
     *
     * @return \DateTime
     */
    public function getModerationAt()
    {
        return $this->moderationAt;
    }

    /**
     * Set lastСalled
     *
     * @param \DateTime $lastСalled
     *
     * @return ApartmentRent
     */
    public function setLastСalled($lastСalled)
    {
        $this->lastСalled = $lastСalled;

        return $this;
    }

    /**
     * Get lastСalled
     *
     * @return \DateTime
     */
    public function getLastСalled()
    {
        return $this->lastСalled;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ApartmentRent
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ApartmentRent
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set fromFirm
     *
     * @param integer $fromFirm
     *
     * @return ApartmentRent
     */
    public function setFromFirm($fromFirm)
    {
        $this->fromFirm = $fromFirm;

        return $this;
    }

    /**
     * Get fromFirm
     *
     * @return integer
     */
    public function getFromFirm()
    {
        return $this->fromFirm;
    }

    /**
     * Set source
     *
     * @param \AppBundle\Entity\Source $source
     *
     * @return ApartmentRent
     */
    public function setSource(\AppBundle\Entity\Source $source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return \AppBundle\Entity\Source
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set agent
     *
     * @param \AppBundle\Entity\User $agent
     *
     * @return ApartmentRent
     */
    public function setAgent(\AppBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \AppBundle\Entity\User
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Add contact
     *
     * @param \AppBundle\Entity\Client $contact
     *
     * @return ApartmentRent
     */
    public function addContact(\AppBundle\Entity\Client $contact)
    {
        $this->contacts[] = $contact;

        return $this;
    }

    /**
     * Remove contact
     *
     * @param \AppBundle\Entity\Client $contact
     */
    public function removeContact(\AppBundle\Entity\Client $contact)
    {
        $this->contacts->removeElement($contact);
    }

    /**
     * Set concierge
     *
     * @param integer $concierge
     *
     * @return ApartmentRent
     */
    public function setConcierge($concierge)
    {
        $this->concierge = $concierge;

        return $this;
    }

    /**
     * Get concierge
     *
     * @return integer
     */
    public function getConcierge()
    {
        return $this->concierge;
    }

    /**
     * Set passengerLift
     *
     * @param integer $passengerLift
     *
     * @return ApartmentRent
     */
    public function setPassengerLift($passengerLift)
    {
        $this->passengerLift = $passengerLift;

        return $this;
    }

    /**
     * Get passengerLift
     *
     * @return integer
     */
    public function getPassengerLift()
    {
        return $this->passengerLift;
    }

    /**
     * Set serviceLift
     *
     * @param integer $serviceLift
     *
     * @return ApartmentRent
     */
    public function setServiceLift($serviceLift)
    {
        $this->serviceLift = $serviceLift;

        return $this;
    }

    /**
     * Get serviceLift
     *
     * @return integer
     */
    public function getServiceLift()
    {
        return $this->serviceLift;
    }

    /**
     * Set pledge
     *
     * @param integer $pledge
     *
     * @return ApartmentRent
     */
    public function setPledge($pledge)
    {
        $this->pledge = $pledge;

        return $this;
    }

    /**
     * Get pledge
     *
     * @return integer
     */
    public function getPledge()
    {
        return $this->pledge;
    }

    /**
     * Set balcony
     *
     * @param integer $balcony
     *
     * @return ApartmentRent
     */
    public function setBalcony($balcony)
    {
        $this->balcony = $balcony;

        return $this;
    }

    /**
     * Get balcony
     *
     * @return integer
     */
    public function getBalcony()
    {
        return $this->balcony;
    }

    /**
     * Set bathroom
     *
     * @param integer $bathroom
     *
     * @return ApartmentRent
     */
    public function setBathroom($bathroom)
    {
        $this->bathroom = $bathroom;

        return $this;
    }

    /**
     * Get bathroom
     *
     * @return integer
     */
    public function getBathroom()
    {
        return $this->bathroom;
    }

    /**
     * Set loggia
     *
     * @param integer $loggia
     *
     * @return ApartmentRent
     */
    public function setLoggia($loggia)
    {
        $this->loggia = $loggia;

        return $this;
    }

    /**
     * Get loggia
     *
     * @return integer
     */
    public function getLoggia()
    {
        return $this->loggia;
    }

    /**
     * Set related
     *
     * @param integer $related
     *
     * @return ApartmentRent
     */
    public function setRelated($related)
    {
        $this->related = $related;

        return $this;
    }

    /**
     * Get related
     *
     * @return integer
     */
    public function getRelated()
    {
        return $this->related;
    }

    /**
     * Set housingStock
     *
     * @param integer $housingStock
     *
     * @return ApartmentRent
     */
    public function setHousingStock($housingStock)
    {
        $this->housingStock = $housingStock;

        return $this;
    }

    /**
     * Get housingStock
     *
     * @return integer
     */
    public function getHousingStock()
    {
        return $this->housingStock;
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Add contractsExclusive
     *
     * @param \AppBundle\Entity\ContractExclusive $contractsExclusive
     *
     * @return ApartmentRent
     */
    public function addContractsExclusive(\AppBundle\Entity\ContractExclusive $contractsExclusive)
    {
        $this->contractsExclusive[] = $contractsExclusive;

        return $this;
    }

    /**
     * Remove contractsExclusive
     *
     * @param \AppBundle\Entity\ContractExclusive $contractsExclusive
     */
    public function removeContractsExclusive(\AppBundle\Entity\ContractExclusive $contractsExclusive)
    {
        $this->contractsExclusive->removeElement($contractsExclusive);
    }

    /**
     * Get contractsExclusive
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContractsExclusive()
    {
        return $this->contractsExclusive;
    }

    /**
     * Add contractsView
     *
     * @param \AppBundle\Entity\ContractView $contractsView
     *
     * @return ApartmentRent
     */
    public function addContractsView(\AppBundle\Entity\ContractView $contractsView)
    {
        $this->contractsView[] = $contractsView;

        return $this;
    }

    /**
     * Remove contractsView
     *
     * @param \AppBundle\Entity\ContractView $contractsView
     */
    public function removeContractsView(\AppBundle\Entity\ContractView $contractsView)
    {
        $this->contractsView->removeElement($contractsView);
    }

    /**
     * Get contractsView
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContractsView()
    {
        return $this->contractsView;
    }

    /**
     * Add contractsCooperation
     *
     * @param \AppBundle\Entity\ContractCooperation $contractsCooperation
     *
     * @return ApartmentRent
     */
    public function addContractsCooperation(\AppBundle\Entity\ContractCooperation $contractsCooperation)
    {
        $this->contractsCooperation[] = $contractsCooperation;

        return $this;
    }

    /**
     * Remove contractsCooperation
     *
     * @param \AppBundle\Entity\ContractCooperation $contractsCooperation
     */
    public function removeContractsCooperation(\AppBundle\Entity\ContractCooperation $contractsCooperation)
    {
        $this->contractsCooperation->removeElement($contractsCooperation);
    }

    /**
     * Get contractsCooperation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContractsCooperation()
    {
        return $this->contractsCooperation;
    }

    /**
     * Return source id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("source")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getSourceId()
    {
        if ($this->source) {
            return array('id' => $this->source->getId());
        }

        return null;
    }

    /**
     * Return agent id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("agent")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getAgentId()
    {
        if ($this->agent) {
            return array('id' => $this->agent->getId());
        }

        return null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("contacts")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getContactsId()
    {
        $arr = array();

        if ($this->contacts) {
            foreach ($this->contacts as $contact) {
                $arr[] = array('id' => $contact->getId());
            }
        }

        return $arr;
    }

    /**
     * Get contract exclusive Ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("contracts_exclusive")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getContractsExclusiveId()
    {
        $arr = array();

        if ($this->contractsExclusive) {
            foreach ($this->contractsExclusive as $contractExclusive) {
                $arr[] = array('id' => $contractExclusive->getId());
            }
        }

        return $arr;
    }

    /**
     * Get contract exclusive Ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("contracts_view")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getContractsViewId()
    {
        $arr = array();

        if ($this->contractsView) {
            foreach ($this->contractsView as $contractView) {
                $arr[] = array('id' => $contractView->getId());
            }
        }

        return $arr;
    }

    /**
     * Get contract exclusive Ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("contracts_cooperation")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getContractsCooperationId()
    {
        $arr = array();

        if ($this->contractsCooperation) {
            foreach ($this->contractsCooperation as $contractCooperation) {
                $arr[] = array('id' => $contractCooperation->getId());
            }
        }

        return $arr;
    }
}
