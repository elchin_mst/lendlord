<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Permissions
 *
 * @ORM\Table(name="permissions")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PermissionsRepository")
 */
class Permissions
{
    //TODO: think about this entity, maybe concat $moduleName and $action in one field

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="permissions")
     * @ORM\JoinTable(name="permissions_role", joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id", unique=false, onDelete="SET NULL")}, inverseJoinColumns={@ORM\JoinColumn(name="permissions_id", referencedColumnName="id", unique=false, onDelete="SET NULL")})
     */
    private $roles;

    /**
     * @ORM\Column(name="module_name", type="string", length=255, unique=true, nullable=false )
     * @JMS\Groups({"List", "Selected"})
     */
    private $moduleName;

    /**
     * @ORM\Column(name="action", type="string", length=55, unique=false, nullable=false )
     * @JMS\Groups({"List", "Selected"})
     */
    private $action;

    const ACTION = 'all';

    protected static $actionMapping = [
        'delete' => 'delete',
        'view' => 'view',
        'edit' => 'edit',
        'all' => 'all',
    ];

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    public function __toString()
    {
        return "" . $this->getModuleName() . "_" . $this->getAction() . "";
    }

    /**
     * Add role
     *
     * @param \AppBundle\Entity\Role $role
     *
     * @return Permissions
     */
    public function addRole(\AppBundle\Entity\Role $role)
    {
        $this->roles[] = $role;

        return $this;
    }

    /**
     * Remove role
     *
     * @param \AppBundle\Entity\Role $role
     */
    public function removeRole(\AppBundle\Entity\Role $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set moduleName
     *
     * @param string $moduleName
     *
     * @return Permissions
     */
    public function setModuleName($moduleName)
    {
        $this->moduleName = $moduleName;

        return $this;
    }

    /**
     * Get moduleName
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return Permissions
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("roles")
     * @JMS\Groups({"List", "Selected"})
     * @return array
     */
    public function getOfficesId()
    {
        $arr = array();

        if ($this->roles) {
            foreach ($this->roles as $r) {
                $arr[] = array('id' => $r->getId());
            }
        }

        return $arr;
    }
}
