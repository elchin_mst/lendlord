<?php

namespace AppBundle\Entity;

use  Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;

/**
 * Role
 *
 * @ORM\Table(name="roles")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RoleRepository")
 */
class Role implements RoleInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * @ORM\Column(name="role", type="string", length=20, unique=true)
     * @JMS\Groups({"List", "Selected"})
     */
    private $role;

    /**
     * @var int
     * @ORM\Column(name="priority", type="integer")
     * @JMS\Groups({"List", "Selected"})
     */
    private $priority;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="roles")
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity="Permissions", mappedBy="roles")
     */
    private $permissions;


    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->permissions = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->role;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return (string)$this->role;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Role
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Role
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;
        $user->addRole($this);

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
        $user->removeRole($this);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users->toArray();
    }

    /**
     * Add permission
     *
     * @param \AppBundle\Entity\Permissions $permission
     *
     * @return Role
     */
    public function addPermission(\AppBundle\Entity\Permissions $permission)
    {
        $this->permissions[] = $permission;
        $permission->addRole($this);

        return $this;
    }

    /**
     * Remove permission
     *
     * @param \AppBundle\Entity\Permissions $permission
     */
    public function removePermission(\AppBundle\Entity\Permissions $permission)
    {
        $this->permissions->removeElement($permission);
        $permission->removeRole($this);
    }

    /**
     * Get permissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPermissions()
    {
        return $this->permissions->toArray();
    }

    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return Role
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }
}
