<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ContractView
 *
 * @ORM\Table(name="contract_view")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContractViewRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ContractView extends Contract
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumn(
     *     name="client_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     *
     * @JMS\Type("AppBundle\Entity\Client")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $client;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="Deal")
     * @ORM\JoinColumn(
     *     name="deal_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     *
     * @JMS\Type("AppBundle\Entity\Deal")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $deal;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ApartmentSale", inversedBy="contractsView")
     * @ORM\JoinTable(
     *     name="contract_view_apartment_sale",
     *     joinColumns={
     *          @ORM\JoinColumn(
     *              name="contract_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="object_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     }
     * )
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ApartmentSale>")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $apartmentsSale;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ApartmentRent", inversedBy="contractsView")
     * @ORM\JoinTable(
     *     name="contract_view_apartment_rent",
     *     joinColumns={
     *          @ORM\JoinColumn(
     *              name="contract_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="object_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     }
     * )
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ApartmentRent>")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $apartmentsRent;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="House", inversedBy="contractsView")
     * @ORM\JoinTable(
     *     name="contract_view_house",
     *     joinColumns={
     *          @ORM\JoinColumn(
     *              name="contract_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="object_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     }
     * )
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\House>")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $houses;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Land", inversedBy="contractsView")
     * @ORM\JoinTable(
     *     name="contract_view_land",
     *     joinColumns={
     *          @ORM\JoinColumn(
     *              name="contract_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="object_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     }
     * )
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\Land>")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $lands;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="CommercialProperty", inversedBy="contractsView")
     * @ORM\JoinTable(
     *     name="contract_view_commercial_prop",
     *     joinColumns={
     *          @ORM\JoinColumn(
     *              name="contract_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="object_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     }
     * )
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\Land>")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $commercialProperty;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    protected $status;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->apartmentsSale = new \Doctrine\Common\Collections\ArrayCollection();
        $this->apartmentsRent = new \Doctrine\Common\Collections\ArrayCollection();
        $this->houses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->lands = new \Doctrine\Common\Collections\ArrayCollection();
        $this->commercialProperty = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return ContractView
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ContractView
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ContractView
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return ContractView
     */
    public function setClient(\AppBundle\Entity\Client $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set deal
     *
     * @param \AppBundle\Entity\Deal $deal
     *
     * @return ContractView
     */
    public function setDeal(\AppBundle\Entity\Deal $deal)
    {
        $this->deal = $deal;

        return $this;
    }

    /**
     * Get deal
     *
     * @return \AppBundle\Entity\Deal
     */
    public function getDeal()
    {
        return $this->deal;
    }

    /**
     * Add apartmentsSale
     *
     * @param \AppBundle\Entity\ApartmentSale $apartmentsSale
     *
     * @return ContractView
     */
    public function addApartmentsSale(\AppBundle\Entity\ApartmentSale $apartmentsSale)
    {
        $this->apartmentsSale[] = $apartmentsSale;

        return $this;
    }

    /**
     * Remove apartmentsSale
     *
     * @param \AppBundle\Entity\ApartmentSale $apartmentsSale
     */
    public function removeApartmentsSale(\AppBundle\Entity\ApartmentSale $apartmentsSale)
    {
        $this->apartmentsSale->removeElement($apartmentsSale);
    }

    /**
     * Get apartmentsSale
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApartmentsSale()
    {
        return $this->apartmentsSale;
    }

    /**
     * Add apartmentsRent
     *
     * @param \AppBundle\Entity\ApartmentRent $apartmentsRent
     *
     * @return ContractView
     */
    public function addApartmentsRent(\AppBundle\Entity\ApartmentRent $apartmentsRent)
    {
        $this->apartmentsRent[] = $apartmentsRent;

        return $this;
    }

    /**
     * Remove apartmentsRent
     *
     * @param \AppBundle\Entity\ApartmentRent $apartmentsRent
     */
    public function removeApartmentsRent(\AppBundle\Entity\ApartmentRent $apartmentsRent)
    {
        $this->apartmentsRent->removeElement($apartmentsRent);
    }

    /**
     * Get apartmentsRent
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApartmentsRent()
    {
        return $this->apartmentsRent;
    }

    /**
     * Add house
     *
     * @param \AppBundle\Entity\House $house
     *
     * @return ContractView
     */
    public function addHouse(\AppBundle\Entity\House $house)
    {
        $this->houses[] = $house;

        return $this;
    }

    /**
     * Remove house
     *
     * @param \AppBundle\Entity\House $house
     */
    public function removeHouse(\AppBundle\Entity\House $house)
    {
        $this->houses->removeElement($house);
    }

    /**
     * Get houses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHouses()
    {
        return $this->houses;
    }

    /**
     * Add land
     *
     * @param \AppBundle\Entity\Land $land
     *
     * @return ContractView
     */
    public function addLand(\AppBundle\Entity\Land $land)
    {
        $this->lands[] = $land;

        return $this;
    }

    /**
     * Remove land
     *
     * @param \AppBundle\Entity\Land $land
     */
    public function removeLand(\AppBundle\Entity\Land $land)
    {
        $this->lands->removeElement($land);
    }

    /**
     * Get lands
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLands()
    {
        return $this->lands;
    }

    /**
     * Add commercialProperty
     *
     * @param \AppBundle\Entity\CommercialProperty $commercialProperty
     *
     * @return ContractView
     */
    public function addCommercialProperty(\AppBundle\Entity\CommercialProperty $commercialProperty)
    {
        $this->commercialProperty[] = $commercialProperty;

        return $this;
    }

    /**
     * Remove commercialProperty
     *
     * @param \AppBundle\Entity\CommercialProperty $commercialProperty
     */
    public function removeCommercialProperty(\AppBundle\Entity\CommercialProperty $commercialProperty)
    {
        $this->commercialProperty->removeElement($commercialProperty);
    }

    /**
     * Get commercialProperty
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommercialProperty()
    {
        return $this->commercialProperty;
    }

    /**
     * Set agent
     *
     * @param \AppBundle\Entity\User $agent
     *
     * @return ContractView
     */
    public function setAgent(\AppBundle\Entity\User $agent)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \AppBundle\Entity\User
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set createdUser
     *
     * @param \AppBundle\Entity\User $createdUser
     *
     * @return ContractView
     */
    public function setCreatedUser(\AppBundle\Entity\User $createdUser)
    {
        $this->createdUser = $createdUser;

        return $this;
    }

    /**
     * Get createdUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedUser()
    {
        return $this->createdUser;
    }

    /**
     * Set updatedUser
     *
     * @param \AppBundle\Entity\User $updatedUser
     *
     * @return ContractView
     */
    public function setUpdatedUser(\AppBundle\Entity\User $updatedUser)
    {
        $this->updatedUser = $updatedUser;

        return $this;
    }

    /**
     * Get updatedUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedUser()
    {
        return $this->updatedUser;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return ContractView
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get agent id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("agent")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getAgentId()
    {
        if ($this->agent) {
            return array(
                'id' => $this->agent->getId(),
            );
        }

        return null;
    }

    /**
     * Get created user id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("created_user")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getCreatedUserId()
    {
        if ($this->createdUser) {
            return array(
                'id' => $this->createdUser->getId(),
            );
        }

        return null;
    }

    /**
     * Get updated user id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("updated_user")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getUpdatedUserId()
    {
        if ($this->updatedUser) {
            return array(
                'id' => $this->updatedUser->getId(),
            );
        }

        return null;
    }

    /**
     * Get client id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("client")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getClientId()
    {
        if ($this->client) {
            return array(
                'id' => $this->client->getId(),
            );
        }

        return null;
    }

    /**
     * Get deal id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("deal")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getDealId()
    {
        if ($this->deal) {
            return array(
                'id' => $this->deal->getId(),
            );
        }

        return null;
    }

    /**
     * Get apartment sale ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("apartments_sale")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getApartmentSaleIds()
    {
        $arr = array();

        if ($this->apartmentsSale) {
            foreach ($this->apartmentsSale as $apartmentSale) {
                $arr[] = array('id' => $apartmentSale->getId());
            }
        }

        return $arr;
    }

    /**
     * Get apartment rent ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("apartments_rent")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getApartmentRentIds()
    {
        $arr = array();

        if ($this->apartmentsRent) {
            foreach ($this->apartmentsRent as $apartmentRent) {
                $arr[] = array('id' => $apartmentRent->getId());
            }
        }

        return $arr;
    }

    /**
     * Get house ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("houses")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getHouseIds()
    {
        $arr = array();

        if ($this->houses) {
            foreach ($this->houses as $house) {
                $arr[] = array('id' => $house->getId());
            }
        }

        return $arr;
    }

    /**
     * Get land ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("lands")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getLandIds()
    {
        $arr = array();

        if ($this->lands) {
            foreach ($this->lands as $land) {
                $arr[] = array('id' => $land->getId());
            }
        }

        return $arr;
    }

    /**
     * Get commercial property ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("commercial_property")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getCommercialPropertyIds()
    {
        $arr = array();

        if ($this->commercialProperty) {
            foreach ($this->commercialProperty as $commercialProperty) {
                $arr[] = array('id' => $commercialProperty->getId());
            }
        }

        return $arr;
    }
}
