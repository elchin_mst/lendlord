<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Deal
 *
 * @ORM\Table(name="deals")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DealRepository")
 */
class Deal
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="deals")
     * @ORM\JoinColumn(name="manager_id", referencedColumnName="id", nullable=false)
     *
     * @JMS\Type("AppBundle\Entity\User")
     * @Assert\NotBlank()
     */
    private $manager;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\NotBlank()
     */
    private $status;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Note", mappedBy="deal")
     */
    private $notes;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Client", mappedBy="deals")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\Client>")
     */
    private $clients;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clients = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Deal
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set manager
     *
     * @param \AppBundle\Entity\User $manager
     *
     * @return Deal
     */
    public function setManager(\AppBundle\Entity\User $manager)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * Get manager
     *
     * @return \AppBundle\Entity\User
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Set status
     *
     * @param int
     *
     * @return Deal
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add note
     *
     * @param \AppBundle\Entity\Note $note
     *
     * @return Deal
     */
    public function addNote(\AppBundle\Entity\Note $note)
    {
        $this->notes[] = $note;

        return $this;
    }

    /**
     * Remove note
     *
     * @param \AppBundle\Entity\Note $note
     */
    public function removeNote(\AppBundle\Entity\Note $note)
    {
        $this->notes->removeElement($note);
    }

    /**
     * Get notes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Add client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return Deal
     */
    public function addClient(\AppBundle\Entity\Client $client)
    {
        $this->clients[] = $client;
        $client->addDeal($this);

        return $this;
    }

    /**
     * Remove client
     *
     * @param \AppBundle\Entity\Client $client
     */
    public function removeClient(\AppBundle\Entity\Client $client)
    {
        $this->clients->removeElement($client);
    }

    /**
     * Get clients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * Get manager id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("manager")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getManagerId()
    {
        if ($this->manager) {
            return array(
                'id' => $this->manager->getId(),
            );
        }

        return null;
    }

    /**
     * Get notes id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("notes")
     * @JMS\Groups({"Selected"})
     *
     * @return array
     */
    public function getNoteIds()
    {
        $arr = array();

        if ($this->notes) {
            foreach ($this->notes as $note) {
                $arr[] = array('id' => $note->getId());
            }
        }

        return $arr;
    }

    /**
     * Get clients id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("clients")
     * @JMS\Groups({"Selected"})
     *
     * @return array
     */
    public function getClientIds()
    {
        $arr = array();

        if ($this->clients) {
            foreach ($this->clients as $client) {
                $arr[] = array('id' => $client->getId());
            }
        }

        return $arr;
    }
}
