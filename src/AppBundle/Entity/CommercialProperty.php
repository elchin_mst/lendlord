<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CommercialProperty
 *
 * @ORM\Table(name="commercial_property")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommercialPropertyRepository")
 */
class CommercialProperty extends Object
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $id;

    /**
     * @var Source
     *
     * @ORM\ManyToOne(targetEntity="Source", inversedBy="commercialProperty")
     * @ORM\JoinColumn(
     *     name="source_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     *
     * @JMS\Type("AppBundle\Entity\Source")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $source;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     *
     * @JMS\Type("AppBundle\Entity\User")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $agent;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Client", inversedBy="apartmentsSale")
     * @ORM\JoinTable(
     *     name="commercial_prop_client",
     *     joinColumns={
     *          @ORM\JoinColumn(
     *              name="commerc_prop__id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="client_id",
     *              referencedColumnName="id",
     *              unique=false,
     *              onDelete="SET NULL"
     *          )
     *     }
     * )
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\Client>")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $contacts;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ContractExclusive", mappedBy="commercialProperty")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ContractExclusive>")
     */
    private $contractsExclusive;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ContractView", mappedBy="commercialProperty")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ContractView>")
     */
    private $contractsView;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ContractCooperation", mappedBy="commercialProperty")
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\ContractCooperation>")
     */
    private $contractsCooperation;

    /**
     * @var int
     *
     * @ORM\Column(name="phone_numbers", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $phoneNumbers;

    /**
     * @var int
     *
     * @ORM\Column(name="gates_num", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $gatesNum;

    /**
     * @var int
     *
     * @ORM\Column(name="free_pairs", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $freePairs;

    /**
     * @var int
     *
     * @ORM\Column(name="building_year", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $buildingYear;

    /**
     * @var int
     *
     * @ORM\Column(name="unfinished", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $unfinished = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="urgent_sell", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $urgentSell = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="mortgage", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $mortgage = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="dedicated_line", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $dedicatedLine = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ready_business", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $readyBusiness = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_escalator", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasEscalator = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_ramp", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasRamp = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_dedic_parking", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasDedicParking = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_refrigeration", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasRefrigeration = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="payback_period", type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $paybackPeriod;

    /**
     * @var string
     *
     * @ORM\Column(name="distance_to_stations", type="string", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $distanceToStations;

    /**
     * @var float
     *
     * @ORM\Column(name="total_sq", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $totalSq;

    /**
     * @var float
     *
     * @ORM\Column(name="hall_sq", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $hallSq;

    /**
     * @var float
     *
     * @ORM\Column(name="land_sq", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $landSq;

    /**
     * @var float
     *
     * @ORM\Column(name="rent_sq", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $rentSq;

    /**
     * @var float
     *
     * @ORM\Column(name="basement_sq", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $basementSq;

    /**
     * @var float
     *
     * @ORM\Column(name="socle_sq", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $socleSq;

    /**
     * @var float
     *
     * @ORM\Column(name="first_floor_sq", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $firstFloorSq;

    /**
     * @var float
     *
     * @ORM\Column(name="remaining_sq", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $remainingSq;

    /**
     * @var float
     *
     * @ORM\Column(name="lift_tonnage", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $liftTonnage;

    /**
     * @var float
     *
     * @ORM\Column(name="ceiling_height", type="float", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $ceilingHeight;

    /**
     * @var float
     *
     * @ORM\Column(name="selling_price", type="decimal", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $sellingPrice;

    /**
     * @var float
     *
     * @ORM\Column(name="price_sq_meter", type="decimal", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $priceSqMeter;

    /**
     * @var float
     *
     * @ORM\Column(name="rent_price", type="decimal", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $rentPrice;

    /**
     * @var float
     *
     * @ORM\Column(name="rent_price_sq_meter", type="decimal", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $rentPriceSqMeter;

    /**
     * @var float
     *
     * @ORM\Column(name="monthly_income", type="decimal", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $monthlyIncome;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="completed_at", type="datetime", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\DateTime()
     */
    private $completedAt;

    /**
     * @var int
     *
     * @ORM\Column(name="condition", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 1)
     */
    private $condition;

    /**
     * @var int
     *
     * @ORM\Column(name="phone_station", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $phoneStation;

    /**
     * @var int
     *
     * @ORM\Column(name="ownership_type", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $ownershipType;

    /**
     * @var int
     *
     * @ORM\Column(name="heating", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 1)
     */
    private $heating;

    /**
     * @var int
     *
     * @ORM\Column(name="security", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $security;

    /**
     * @var int
     *
     * @ORM\Column(name="level", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $level;

    /**
     * @var int
     *
     * @ORM\Column(name="floor_type", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $floorType;

    /**
     * @var int
     *
     * @ORM\Column(name="right_type_object", type="integer", nullable=true)
     *
     * @JMS\Groups({"List", "Selected"})
     */
    private $rightTypeObject;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_auto_service", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $profileAutoService = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_recreation_center", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $profileRecreationCenter = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_garage", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $profileGarage = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_hotel", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $profileHotel = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_cafe", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $profileCafe = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_store", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $profileStore = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_office", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $profileOffice = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_barbershop", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $profile_barbershop = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_industrial_base", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $profileIndustrialBase = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_production", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $profile_production = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_sauna", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $profileSauna = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_stock", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $profileStock = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_canteen", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $profileCanteen = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_site", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $profileSite = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_workshop", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $profileWorkshop = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_catering", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $profileCatering = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="price_include_communal", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $priceIncludeCommunal = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="price_include_nds", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $priceIncludeNDS = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="price_include_repairs", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $price_include_repairs = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="price_include_phone", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $priceIncludePhone = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="price_include_electricity", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $priceIncludeElectricity = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_autotransfer", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasAutotransfer = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_water_supply", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasWaterSupply = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_gas_supply", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasGasSupply = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_railway", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasRailway = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_canalisation", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasCanalisation = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_phone", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasPhone = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_220v", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $has220v = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_380v", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $has380v = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_optical_fiber", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasOpticalFiber = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_lift_truck", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasLiftTruck = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_tower_crane", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasTowerCrane = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_gantry_crane", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasGantryCrane = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_crane_beam", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasCraneBeam = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_lyada", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasLyada = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_overhead_crane", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasOverheadCrane = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_elevator", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasElevator = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_telpher", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasTelpher = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="passenger_lift", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $passengerLift = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="service_lift", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $serviceLift = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_cafes", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasCafes = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_cinemas", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasCinemas = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_hospitals", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasHospitals = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_educational", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $hasEducational = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="wooden_windows", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $woodenWindows = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="metal_plast_windows", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $metalPlastWindows = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="wooden_windows_eur", type="integer", nullable=false)
     *
     * @JMS\Groups({"List", "Selected"})
     * @Assert\Range(min = 0, max = 2)
     */
    private $woodenWindowsEur = 0;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contacts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set phoneNumbers
     *
     * @param integer $phoneNumbers
     *
     * @return CommercialProperty
     */
    public function setPhoneNumbers($phoneNumbers)
    {
        $this->phoneNumbers = $phoneNumbers;

        return $this;
    }

    /**
     * Get phoneNumbers
     *
     * @return integer
     */
    public function getPhoneNumbers()
    {
        return $this->phoneNumbers;
    }

    /**
     * Set gatesNum
     *
     * @param integer $gatesNum
     *
     * @return CommercialProperty
     */
    public function setGatesNum($gatesNum)
    {
        $this->gatesNum = $gatesNum;

        return $this;
    }

    /**
     * Get gatesNum
     *
     * @return integer
     */
    public function getGatesNum()
    {
        return $this->gatesNum;
    }

    /**
     * Set freePairs
     *
     * @param integer $freePairs
     *
     * @return CommercialProperty
     */
    public function setFreePairs($freePairs)
    {
        $this->freePairs = $freePairs;

        return $this;
    }

    /**
     * Get freePairs
     *
     * @return integer
     */
    public function getFreePairs()
    {
        return $this->freePairs;
    }

    /**
     * Set buildingYear
     *
     * @param integer $buildingYear
     *
     * @return CommercialProperty
     */
    public function setBuildingYear($buildingYear)
    {
        $this->buildingYear = $buildingYear;

        return $this;
    }

    /**
     * Get buildingYear
     *
     * @return integer
     */
    public function getBuildingYear()
    {
        return $this->buildingYear;
    }

    /**
     * Set unfinished
     *
     * @param integer $unfinished
     *
     * @return CommercialProperty
     */
    public function setUnfinished($unfinished)
    {
        $this->unfinished = $unfinished;

        return $this;
    }

    /**
     * Get unfinished
     *
     * @return integer
     */
    public function getUnfinished()
    {
        return $this->unfinished;
    }

    /**
     * Set urgentSell
     *
     * @param integer $urgentSell
     *
     * @return CommercialProperty
     */
    public function setUrgentSell($urgentSell)
    {
        $this->urgentSell = $urgentSell;

        return $this;
    }

    /**
     * Get urgentSell
     *
     * @return integer
     */
    public function getUrgentSell()
    {
        return $this->urgentSell;
    }

    /**
     * Set mortgage
     *
     * @param integer $mortgage
     *
     * @return CommercialProperty
     */
    public function setMortgage($mortgage)
    {
        $this->mortgage = $mortgage;

        return $this;
    }

    /**
     * Get mortgage
     *
     * @return integer
     */
    public function getMortgage()
    {
        return $this->mortgage;
    }

    /**
     * Set dedicatedLine
     *
     * @param integer $dedicatedLine
     *
     * @return CommercialProperty
     */
    public function setDedicatedLine($dedicatedLine)
    {
        $this->dedicatedLine = $dedicatedLine;

        return $this;
    }

    /**
     * Get dedicatedLine
     *
     * @return integer
     */
    public function getDedicatedLine()
    {
        return $this->dedicatedLine;
    }

    /**
     * Set readyBusiness
     *
     * @param integer $readyBusiness
     *
     * @return CommercialProperty
     */
    public function setReadyBusiness($readyBusiness)
    {
        $this->readyBusiness = $readyBusiness;

        return $this;
    }

    /**
     * Get readyBusiness
     *
     * @return integer
     */
    public function getReadyBusiness()
    {
        return $this->readyBusiness;
    }

    /**
     * Set hasEscalator
     *
     * @param integer $hasEscalator
     *
     * @return CommercialProperty
     */
    public function setHasEscalator($hasEscalator)
    {
        $this->hasEscalator = $hasEscalator;

        return $this;
    }

    /**
     * Get hasEscalator
     *
     * @return integer
     */
    public function getHasEscalator()
    {
        return $this->hasEscalator;
    }

    /**
     * Set hasRamp
     *
     * @param integer $hasRamp
     *
     * @return CommercialProperty
     */
    public function setHasRamp($hasRamp)
    {
        $this->hasRamp = $hasRamp;

        return $this;
    }

    /**
     * Get hasRamp
     *
     * @return integer
     */
    public function getHasRamp()
    {
        return $this->hasRamp;
    }

    /**
     * Set hasDedicParking
     *
     * @param integer $hasDedicParking
     *
     * @return CommercialProperty
     */
    public function setHasDedicParking($hasDedicParking)
    {
        $this->hasDedicParking = $hasDedicParking;

        return $this;
    }

    /**
     * Get hasDedicParking
     *
     * @return integer
     */
    public function getHasDedicParking()
    {
        return $this->hasDedicParking;
    }

    /**
     * Set hasRefrigeration
     *
     * @param integer $hasRefrigeration
     *
     * @return CommercialProperty
     */
    public function setHasRefrigeration($hasRefrigeration)
    {
        $this->hasRefrigeration = $hasRefrigeration;

        return $this;
    }

    /**
     * Get hasRefrigeration
     *
     * @return integer
     */
    public function getHasRefrigeration()
    {
        return $this->hasRefrigeration;
    }

    /**
     * Set paybackPeriod
     *
     * @param string $paybackPeriod
     *
     * @return CommercialProperty
     */
    public function setPaybackPeriod($paybackPeriod)
    {
        $this->paybackPeriod = $paybackPeriod;

        return $this;
    }

    /**
     * Get paybackPeriod
     *
     * @return string
     */
    public function getPaybackPeriod()
    {
        return $this->paybackPeriod;
    }

    /**
     * Set distanceToStations
     *
     * @param string $distanceToStations
     *
     * @return CommercialProperty
     */
    public function setDistanceToStations($distanceToStations)
    {
        $this->distanceToStations = $distanceToStations;

        return $this;
    }

    /**
     * Get distanceToStations
     *
     * @return string
     */
    public function getDistanceToStations()
    {
        return $this->distanceToStations;
    }

    /**
     * Set totalSq
     *
     * @param float $totalSq
     *
     * @return CommercialProperty
     */
    public function setTotalSq($totalSq)
    {
        $this->totalSq = $totalSq;

        return $this;
    }

    /**
     * Get totalSq
     *
     * @return float
     */
    public function getTotalSq()
    {
        return $this->totalSq;
    }

    /**
     * Set hallSq
     *
     * @param float $hallSq
     *
     * @return CommercialProperty
     */
    public function setHallSq($hallSq)
    {
        $this->hallSq = $hallSq;

        return $this;
    }

    /**
     * Get hallSq
     *
     * @return float
     */
    public function getHallSq()
    {
        return $this->hallSq;
    }

    /**
     * Set landSq
     *
     * @param float $landSq
     *
     * @return CommercialProperty
     */
    public function setLandSq($landSq)
    {
        $this->landSq = $landSq;

        return $this;
    }

    /**
     * Get landSq
     *
     * @return float
     */
    public function getLandSq()
    {
        return $this->landSq;
    }

    /**
     * Set rentSq
     *
     * @param float $rentSq
     *
     * @return CommercialProperty
     */
    public function setRentSq($rentSq)
    {
        $this->rentSq = $rentSq;

        return $this;
    }

    /**
     * Get rentSq
     *
     * @return float
     */
    public function getRentSq()
    {
        return $this->rentSq;
    }

    /**
     * Set basementSq
     *
     * @param float $basementSq
     *
     * @return CommercialProperty
     */
    public function setBasementSq($basementSq)
    {
        $this->basementSq = $basementSq;

        return $this;
    }

    /**
     * Get basementSq
     *
     * @return float
     */
    public function getBasementSq()
    {
        return $this->basementSq;
    }

    /**
     * Set socleSq
     *
     * @param float $socleSq
     *
     * @return CommercialProperty
     */
    public function setSocleSq($socleSq)
    {
        $this->socleSq = $socleSq;

        return $this;
    }

    /**
     * Get socleSq
     *
     * @return float
     */
    public function getSocleSq()
    {
        return $this->socleSq;
    }

    /**
     * Set firstFloorSq
     *
     * @param float $firstFloorSq
     *
     * @return CommercialProperty
     */
    public function setFirstFloorSq($firstFloorSq)
    {
        $this->firstFloorSq = $firstFloorSq;

        return $this;
    }

    /**
     * Get firstFloorSq
     *
     * @return float
     */
    public function getFirstFloorSq()
    {
        return $this->firstFloorSq;
    }

    /**
     * Set remainingSq
     *
     * @param float $remainingSq
     *
     * @return CommercialProperty
     */
    public function setRemainingSq($remainingSq)
    {
        $this->remainingSq = $remainingSq;

        return $this;
    }

    /**
     * Get remainingSq
     *
     * @return float
     */
    public function getRemainingSq()
    {
        return $this->remainingSq;
    }

    /**
     * Set liftTonnage
     *
     * @param float $liftTonnage
     *
     * @return CommercialProperty
     */
    public function setLiftTonnage($liftTonnage)
    {
        $this->liftTonnage = $liftTonnage;

        return $this;
    }

    /**
     * Get liftTonnage
     *
     * @return float
     */
    public function getLiftTonnage()
    {
        return $this->liftTonnage;
    }

    /**
     * Set ceilingHeight
     *
     * @param float $ceilingHeight
     *
     * @return CommercialProperty
     */
    public function setCeilingHeight($ceilingHeight)
    {
        $this->ceilingHeight = $ceilingHeight;

        return $this;
    }

    /**
     * Get ceilingHeight
     *
     * @return float
     */
    public function getCeilingHeight()
    {
        return $this->ceilingHeight;
    }

    /**
     * Set sellingPrice
     *
     * @param string $sellingPrice
     *
     * @return CommercialProperty
     */
    public function setSellingPrice($sellingPrice)
    {
        $this->sellingPrice = $sellingPrice;

        return $this;
    }

    /**
     * Get sellingPrice
     *
     * @return string
     */
    public function getSellingPrice()
    {
        return $this->sellingPrice;
    }

    /**
     * Set priceSqMeter
     *
     * @param string $priceSqMeter
     *
     * @return CommercialProperty
     */
    public function setPriceSqMeter($priceSqMeter)
    {
        $this->priceSqMeter = $priceSqMeter;

        return $this;
    }

    /**
     * Get priceSqMeter
     *
     * @return string
     */
    public function getPriceSqMeter()
    {
        return $this->priceSqMeter;
    }

    /**
     * Set rentPrice
     *
     * @param string $rentPrice
     *
     * @return CommercialProperty
     */
    public function setRentPrice($rentPrice)
    {
        $this->rentPrice = $rentPrice;

        return $this;
    }

    /**
     * Get rentPrice
     *
     * @return string
     */
    public function getRentPrice()
    {
        return $this->rentPrice;
    }

    /**
     * Set rentPriceSqMeter
     *
     * @param string $rentPriceSqMeter
     *
     * @return CommercialProperty
     */
    public function setRentPriceSqMeter($rentPriceSqMeter)
    {
        $this->rentPriceSqMeter = $rentPriceSqMeter;

        return $this;
    }

    /**
     * Get rentPriceSqMeter
     *
     * @return string
     */
    public function getRentPriceSqMeter()
    {
        return $this->rentPriceSqMeter;
    }

    /**
     * Set monthlyIncome
     *
     * @param string $monthlyIncome
     *
     * @return CommercialProperty
     */
    public function setMonthlyIncome($monthlyIncome)
    {
        $this->monthlyIncome = $monthlyIncome;

        return $this;
    }

    /**
     * Get monthlyIncome
     *
     * @return string
     */
    public function getMonthlyIncome()
    {
        return $this->monthlyIncome;
    }

    /**
     * Set completedAt
     *
     * @param \DateTime $completedAt
     *
     * @return CommercialProperty
     */
    public function setCompletedAt($completedAt)
    {
        $this->completedAt = $completedAt;

        return $this;
    }

    /**
     * Get completedAt
     *
     * @return \DateTime
     */
    public function getCompletedAt()
    {
        return $this->completedAt;
    }

    /**
     * Set condition
     *
     * @param integer $condition
     *
     * @return CommercialProperty
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;

        return $this;
    }

    /**
     * Get condition
     *
     * @return integer
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * Set phoneStation
     *
     * @param integer $phoneStation
     *
     * @return CommercialProperty
     */
    public function setPhoneStation($phoneStation)
    {
        $this->phoneStation = $phoneStation;

        return $this;
    }

    /**
     * Get phoneStation
     *
     * @return integer
     */
    public function getPhoneStation()
    {
        return $this->phoneStation;
    }

    /**
     * Set ownershipType
     *
     * @param integer $ownershipType
     *
     * @return CommercialProperty
     */
    public function setOwnershipType($ownershipType)
    {
        $this->ownershipType = $ownershipType;

        return $this;
    }

    /**
     * Get ownershipType
     *
     * @return integer
     */
    public function getOwnershipType()
    {
        return $this->ownershipType;
    }

    /**
     * Set heating
     *
     * @param integer $heating
     *
     * @return CommercialProperty
     */
    public function setHeating($heating)
    {
        $this->heating = $heating;

        return $this;
    }

    /**
     * Get heating
     *
     * @return integer
     */
    public function getHeating()
    {
        return $this->heating;
    }

    /**
     * Set security
     *
     * @param integer $security
     *
     * @return CommercialProperty
     */
    public function setSecurity($security)
    {
        $this->security = $security;

        return $this;
    }

    /**
     * Get security
     *
     * @return integer
     */
    public function getSecurity()
    {
        return $this->security;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return CommercialProperty
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set floorType
     *
     * @param integer $floorType
     *
     * @return CommercialProperty
     */
    public function setFloorType($floorType)
    {
        $this->floorType = $floorType;

        return $this;
    }

    /**
     * Get floorType
     *
     * @return integer
     */
    public function getFloorType()
    {
        return $this->floorType;
    }

    /**
     * Set rightTypeObject
     *
     * @param integer $rightTypeObject
     *
     * @return CommercialProperty
     */
    public function setRightTypeObject($rightTypeObject)
    {
        $this->rightTypeObject = $rightTypeObject;

        return $this;
    }

    /**
     * Get rightTypeObject
     *
     * @return integer
     */
    public function getRightTypeObject()
    {
        return $this->rightTypeObject;
    }

    /**
     * Set rofileAutoService
     *
     * @param integer $rofileAutoService
     *
     * @return CommercialProperty
     */
    public function setRofileAutoService($rofileAutoService)
    {
        $this->rofileAutoService = $rofileAutoService;

        return $this;
    }

    /**
     * Get rofileAutoService
     *
     * @return integer
     */
    public function getRofileAutoService()
    {
        return $this->rofileAutoService;
    }

    /**
     * Set profileRecreationCenter
     *
     * @param integer $profileRecreationCenter
     *
     * @return CommercialProperty
     */
    public function setProfileRecreationCenter($profileRecreationCenter)
    {
        $this->profileRecreationCenter = $profileRecreationCenter;

        return $this;
    }

    /**
     * Get profileRecreationCenter
     *
     * @return integer
     */
    public function getProfileRecreationCenter()
    {
        return $this->profileRecreationCenter;
    }

    /**
     * Set profileGarage
     *
     * @param integer $profileGarage
     *
     * @return CommercialProperty
     */
    public function setProfileGarage($profileGarage)
    {
        $this->profileGarage = $profileGarage;

        return $this;
    }

    /**
     * Get profileGarage
     *
     * @return integer
     */
    public function getProfileGarage()
    {
        return $this->profileGarage;
    }

    /**
     * Set profileHotel
     *
     * @param integer $profileHotel
     *
     * @return CommercialProperty
     */
    public function setProfileHotel($profileHotel)
    {
        $this->profileHotel = $profileHotel;

        return $this;
    }

    /**
     * Get profileHotel
     *
     * @return integer
     */
    public function getProfileHotel()
    {
        return $this->profileHotel;
    }

    /**
     * Set profileCafe
     *
     * @param integer $profileCafe
     *
     * @return CommercialProperty
     */
    public function setProfileCafe($profileCafe)
    {
        $this->profileCafe = $profileCafe;

        return $this;
    }

    /**
     * Get profileCafe
     *
     * @return integer
     */
    public function getProfileCafe()
    {
        return $this->profileCafe;
    }

    /**
     * Set profileStore
     *
     * @param integer $profileStore
     *
     * @return CommercialProperty
     */
    public function setProfileStore($profileStore)
    {
        $this->profileStore = $profileStore;

        return $this;
    }

    /**
     * Get profileStore
     *
     * @return integer
     */
    public function getProfileStore()
    {
        return $this->profileStore;
    }

    /**
     * Set profileOffice
     *
     * @param integer $profileOffice
     *
     * @return CommercialProperty
     */
    public function setProfileOffice($profileOffice)
    {
        $this->profileOffice = $profileOffice;

        return $this;
    }

    /**
     * Get profileOffice
     *
     * @return integer
     */
    public function getProfileOffice()
    {
        return $this->profileOffice;
    }

    /**
     * Set profileBarbershop
     *
     * @param integer $profileBarbershop
     *
     * @return CommercialProperty
     */
    public function setProfileBarbershop($profileBarbershop)
    {
        $this->profile_barbershop = $profileBarbershop;

        return $this;
    }

    /**
     * Get profileBarbershop
     *
     * @return integer
     */
    public function getProfileBarbershop()
    {
        return $this->profile_barbershop;
    }

    /**
     * Set profileIndustrialBase
     *
     * @param integer $profileIndustrialBase
     *
     * @return CommercialProperty
     */
    public function setProfileIndustrialBase($profileIndustrialBase)
    {
        $this->profileIndustrialBase = $profileIndustrialBase;

        return $this;
    }

    /**
     * Get profileIndustrialBase
     *
     * @return integer
     */
    public function getProfileIndustrialBase()
    {
        return $this->profileIndustrialBase;
    }

    /**
     * Set profileProduction
     *
     * @param integer $profileProduction
     *
     * @return CommercialProperty
     */
    public function setProfileProduction($profileProduction)
    {
        $this->profile_production = $profileProduction;

        return $this;
    }

    /**
     * Get profileProduction
     *
     * @return integer
     */
    public function getProfileProduction()
    {
        return $this->profile_production;
    }

    /**
     * Set profileSauna
     *
     * @param integer $profileSauna
     *
     * @return CommercialProperty
     */
    public function setProfileSauna($profileSauna)
    {
        $this->profileSauna = $profileSauna;

        return $this;
    }

    /**
     * Get profileSauna
     *
     * @return integer
     */
    public function getProfileSauna()
    {
        return $this->profileSauna;
    }

    /**
     * Set profileStock
     *
     * @param integer $profileStock
     *
     * @return CommercialProperty
     */
    public function setProfileStock($profileStock)
    {
        $this->profileStock = $profileStock;

        return $this;
    }

    /**
     * Get profileStock
     *
     * @return integer
     */
    public function getProfileStock()
    {
        return $this->profileStock;
    }

    /**
     * Set profileCanteen
     *
     * @param integer $profileCanteen
     *
     * @return CommercialProperty
     */
    public function setProfileCanteen($profileCanteen)
    {
        $this->profileCanteen = $profileCanteen;

        return $this;
    }

    /**
     * Get profileCanteen
     *
     * @return integer
     */
    public function getProfileCanteen()
    {
        return $this->profileCanteen;
    }

    /**
     * Set profileSite
     *
     * @param integer $profileSite
     *
     * @return CommercialProperty
     */
    public function setProfileSite($profileSite)
    {
        $this->profileSite = $profileSite;

        return $this;
    }

    /**
     * Get profileSite
     *
     * @return integer
     */
    public function getProfileSite()
    {
        return $this->profileSite;
    }

    /**
     * Set profileWorkshop
     *
     * @param integer $profileWorkshop
     *
     * @return CommercialProperty
     */
    public function setProfileWorkshop($profileWorkshop)
    {
        $this->profileWorkshop = $profileWorkshop;

        return $this;
    }

    /**
     * Get profileWorkshop
     *
     * @return integer
     */
    public function getProfileWorkshop()
    {
        return $this->profileWorkshop;
    }

    /**
     * Set profileCatering
     *
     * @param integer $profileCatering
     *
     * @return CommercialProperty
     */
    public function setProfileCatering($profileCatering)
    {
        $this->profileCatering = $profileCatering;

        return $this;
    }

    /**
     * Get profileCatering
     *
     * @return integer
     */
    public function getProfileCatering()
    {
        return $this->profileCatering;
    }

    /**
     * Set priceIncludeCommunal
     *
     * @param integer $priceIncludeCommunal
     *
     * @return CommercialProperty
     */
    public function setPriceIncludeCommunal($priceIncludeCommunal)
    {
        $this->priceIncludeCommunal = $priceIncludeCommunal;

        return $this;
    }

    /**
     * Get priceIncludeCommunal
     *
     * @return integer
     */
    public function getPriceIncludeCommunal()
    {
        return $this->priceIncludeCommunal;
    }

    /**
     * Set priceIncludeNDS
     *
     * @param integer $priceIncludeNDS
     *
     * @return CommercialProperty
     */
    public function setPriceIncludeNDS($priceIncludeNDS)
    {
        $this->priceIncludeNDS = $priceIncludeNDS;

        return $this;
    }

    /**
     * Get priceIncludeNDS
     *
     * @return integer
     */
    public function getPriceIncludeNDS()
    {
        return $this->priceIncludeNDS;
    }

    /**
     * Set priceIncludeRepairs
     *
     * @param integer $priceIncludeRepairs
     *
     * @return CommercialProperty
     */
    public function setPriceIncludeRepairs($priceIncludeRepairs)
    {
        $this->price_include_repairs = $priceIncludeRepairs;

        return $this;
    }

    /**
     * Get priceIncludeRepairs
     *
     * @return integer
     */
    public function getPriceIncludeRepairs()
    {
        return $this->price_include_repairs;
    }

    /**
     * Set priceIncludePhone
     *
     * @param integer $priceIncludePhone
     *
     * @return CommercialProperty
     */
    public function setPriceIncludePhone($priceIncludePhone)
    {
        $this->priceIncludePhone = $priceIncludePhone;

        return $this;
    }

    /**
     * Get priceIncludePhone
     *
     * @return integer
     */
    public function getPriceIncludePhone()
    {
        return $this->priceIncludePhone;
    }

    /**
     * Set priceIncludeElectricity
     *
     * @param integer $priceIncludeElectricity
     *
     * @return CommercialProperty
     */
    public function setPriceIncludeElectricity($priceIncludeElectricity)
    {
        $this->priceIncludeElectricity = $priceIncludeElectricity;

        return $this;
    }

    /**
     * Get priceIncludeElectricity
     *
     * @return integer
     */
    public function getPriceIncludeElectricity()
    {
        return $this->priceIncludeElectricity;
    }

    /**
     * Set hasAutotransfer
     *
     * @param integer $hasAutotransfer
     *
     * @return CommercialProperty
     */
    public function setHasAutotransfer($hasAutotransfer)
    {
        $this->hasAutotransfer = $hasAutotransfer;

        return $this;
    }

    /**
     * Get hasAutotransfer
     *
     * @return integer
     */
    public function getHasAutotransfer()
    {
        return $this->hasAutotransfer;
    }

    /**
     * Set hasWaterSupply
     *
     * @param integer $hasWaterSupply
     *
     * @return CommercialProperty
     */
    public function setHasWaterSupply($hasWaterSupply)
    {
        $this->hasWaterSupply = $hasWaterSupply;

        return $this;
    }

    /**
     * Get hasWaterSupply
     *
     * @return integer
     */
    public function getHasWaterSupply()
    {
        return $this->hasWaterSupply;
    }

    /**
     * Set hasGasSupply
     *
     * @param integer $hasGasSupply
     *
     * @return CommercialProperty
     */
    public function setHasGasSupply($hasGasSupply)
    {
        $this->hasGasSupply = $hasGasSupply;

        return $this;
    }

    /**
     * Get hasGasSupply
     *
     * @return integer
     */
    public function getHasGasSupply()
    {
        return $this->hasGasSupply;
    }

    /**
     * Set hasRailway
     *
     * @param integer $hasRailway
     *
     * @return CommercialProperty
     */
    public function setHasRailway($hasRailway)
    {
        $this->hasRailway = $hasRailway;

        return $this;
    }

    /**
     * Get hasRailway
     *
     * @return integer
     */
    public function getHasRailway()
    {
        return $this->hasRailway;
    }

    /**
     * Set hasCanalisation
     *
     * @param integer $hasCanalisation
     *
     * @return CommercialProperty
     */
    public function setHasCanalisation($hasCanalisation)
    {
        $this->hasCanalisation = $hasCanalisation;

        return $this;
    }

    /**
     * Get hasCanalisation
     *
     * @return integer
     */
    public function getHasCanalisation()
    {
        return $this->hasCanalisation;
    }

    /**
     * Set hasPhone
     *
     * @param integer $hasPhone
     *
     * @return CommercialProperty
     */
    public function setHasPhone($hasPhone)
    {
        $this->hasPhone = $hasPhone;

        return $this;
    }

    /**
     * Get hasPhone
     *
     * @return integer
     */
    public function getHasPhone()
    {
        return $this->hasPhone;
    }

    /**
     * Set has220v
     *
     * @param integer $has220v
     *
     * @return CommercialProperty
     */
    public function setHas220v($has220v)
    {
        $this->has220v = $has220v;

        return $this;
    }

    /**
     * Get has220v
     *
     * @return integer
     */
    public function getHas220v()
    {
        return $this->has220v;
    }

    /**
     * Set has380v
     *
     * @param integer $has380v
     *
     * @return CommercialProperty
     */
    public function setHas380v($has380v)
    {
        $this->has380v = $has380v;

        return $this;
    }

    /**
     * Get has380v
     *
     * @return integer
     */
    public function getHas380v()
    {
        return $this->has380v;
    }

    /**
     * Set hasOpticalFiber
     *
     * @param integer $hasOpticalFiber
     *
     * @return CommercialProperty
     */
    public function setHasOpticalFiber($hasOpticalFiber)
    {
        $this->hasOpticalFiber = $hasOpticalFiber;

        return $this;
    }

    /**
     * Get hasOpticalFiber
     *
     * @return integer
     */
    public function getHasOpticalFiber()
    {
        return $this->hasOpticalFiber;
    }

    /**
     * Set hasLiftTruck
     *
     * @param integer $hasLiftTruck
     *
     * @return CommercialProperty
     */
    public function setHasLiftTruck($hasLiftTruck)
    {
        $this->hasLiftTruck = $hasLiftTruck;

        return $this;
    }

    /**
     * Get hasLiftTruck
     *
     * @return integer
     */
    public function getHasLiftTruck()
    {
        return $this->hasLiftTruck;
    }

    /**
     * Set hasTowerCrane
     *
     * @param integer $hasTowerCrane
     *
     * @return CommercialProperty
     */
    public function setHasTowerCrane($hasTowerCrane)
    {
        $this->hasTowerCrane = $hasTowerCrane;

        return $this;
    }

    /**
     * Get hasTowerCrane
     *
     * @return integer
     */
    public function getHasTowerCrane()
    {
        return $this->hasTowerCrane;
    }

    /**
     * Set hasGantryCrane
     *
     * @param integer $hasGantryCrane
     *
     * @return CommercialProperty
     */
    public function setHasGantryCrane($hasGantryCrane)
    {
        $this->hasGantryCrane = $hasGantryCrane;

        return $this;
    }

    /**
     * Get hasGantryCrane
     *
     * @return integer
     */
    public function getHasGantryCrane()
    {
        return $this->hasGantryCrane;
    }

    /**
     * Set hasCraneBeam
     *
     * @param integer $hasCraneBeam
     *
     * @return CommercialProperty
     */
    public function setHasCraneBeam($hasCraneBeam)
    {
        $this->hasCraneBeam = $hasCraneBeam;

        return $this;
    }

    /**
     * Get hasCraneBeam
     *
     * @return integer
     */
    public function getHasCraneBeam()
    {
        return $this->hasCraneBeam;
    }

    /**
     * Set hasLyada
     *
     * @param integer $hasLyada
     *
     * @return CommercialProperty
     */
    public function setHasLyada($hasLyada)
    {
        $this->hasLyada = $hasLyada;

        return $this;
    }

    /**
     * Get hasLyada
     *
     * @return integer
     */
    public function getHasLyada()
    {
        return $this->hasLyada;
    }

    /**
     * Set hasOverheadCrane
     *
     * @param integer $hasOverheadCrane
     *
     * @return CommercialProperty
     */
    public function setHasOverheadCrane($hasOverheadCrane)
    {
        $this->hasOverheadCrane = $hasOverheadCrane;

        return $this;
    }

    /**
     * Get hasOverheadCrane
     *
     * @return integer
     */
    public function getHasOverheadCrane()
    {
        return $this->hasOverheadCrane;
    }

    /**
     * Set hasElevator
     *
     * @param integer $hasElevator
     *
     * @return CommercialProperty
     */
    public function setHasElevator($hasElevator)
    {
        $this->hasElevator = $hasElevator;

        return $this;
    }

    /**
     * Get hasElevator
     *
     * @return integer
     */
    public function getHasElevator()
    {
        return $this->hasElevator;
    }

    /**
     * Set hasTelpher
     *
     * @param integer $hasTelpher
     *
     * @return CommercialProperty
     */
    public function setHasTelpher($hasTelpher)
    {
        $this->hasTelpher = $hasTelpher;

        return $this;
    }

    /**
     * Get hasTelpher
     *
     * @return integer
     */
    public function getHasTelpher()
    {
        return $this->hasTelpher;
    }

    /**
     * Set passengerLift
     *
     * @param integer $passengerLift
     *
     * @return CommercialProperty
     */
    public function setPassengerLift($passengerLift)
    {
        $this->passengerLift = $passengerLift;

        return $this;
    }

    /**
     * Get passengerLift
     *
     * @return integer
     */
    public function getPassengerLift()
    {
        return $this->passengerLift;
    }

    /**
     * Set serviceLift
     *
     * @param integer $serviceLift
     *
     * @return CommercialProperty
     */
    public function setServiceLift($serviceLift)
    {
        $this->serviceLift = $serviceLift;

        return $this;
    }

    /**
     * Get serviceLift
     *
     * @return integer
     */
    public function getServiceLift()
    {
        return $this->serviceLift;
    }

    /**
     * Set hasCafes
     *
     * @param integer $hasCafes
     *
     * @return CommercialProperty
     */
    public function setHasCafes($hasCafes)
    {
        $this->hasCafes = $hasCafes;

        return $this;
    }

    /**
     * Get hasCafes
     *
     * @return integer
     */
    public function getHasCafes()
    {
        return $this->hasCafes;
    }

    /**
     * Set hasCinemas
     *
     * @param integer $hasCinemas
     *
     * @return CommercialProperty
     */
    public function setHasCinemas($hasCinemas)
    {
        $this->hasCinemas = $hasCinemas;

        return $this;
    }

    /**
     * Get hasCinemas
     *
     * @return integer
     */
    public function getHasCinemas()
    {
        return $this->hasCinemas;
    }

    /**
     * Set hasHospitals
     *
     * @param integer $hasHospitals
     *
     * @return CommercialProperty
     */
    public function setHasHospitals($hasHospitals)
    {
        $this->hasHospitals = $hasHospitals;

        return $this;
    }

    /**
     * Get hasHospitals
     *
     * @return integer
     */
    public function getHasHospitals()
    {
        return $this->hasHospitals;
    }

    /**
     * Set hasEducational
     *
     * @param integer $hasEducational
     *
     * @return CommercialProperty
     */
    public function setHasEducational($hasEducational)
    {
        $this->hasEducational = $hasEducational;

        return $this;
    }

    /**
     * Get hasEducational
     *
     * @return integer
     */
    public function getHasEducational()
    {
        return $this->hasEducational;
    }

    /**
     * Set woodenWindows
     *
     * @param integer $woodenWindows
     *
     * @return CommercialProperty
     */
    public function setWoodenWindows($woodenWindows)
    {
        $this->woodenWindows = $woodenWindows;

        return $this;
    }

    /**
     * Get woodenWindows
     *
     * @return integer
     */
    public function getWoodenWindows()
    {
        return $this->woodenWindows;
    }

    /**
     * Set metalPlastWindows
     *
     * @param integer $metalPlastWindows
     *
     * @return CommercialProperty
     */
    public function setMetalPlastWindows($metalPlastWindows)
    {
        $this->metalPlastWindows = $metalPlastWindows;

        return $this;
    }

    /**
     * Get metalPlastWindows
     *
     * @return integer
     */
    public function getMetalPlastWindows()
    {
        return $this->metalPlastWindows;
    }

    /**
     * Set woodenWindowsEur
     *
     * @param integer $woodenWindowsEur
     *
     * @return CommercialProperty
     */
    public function setWoodenWindowsEur($woodenWindowsEur)
    {
        $this->woodenWindowsEur = $woodenWindowsEur;

        return $this;
    }

    /**
     * Get woodenWindowsEur
     *
     * @return integer
     */
    public function getWoodenWindowsEur()
    {
        return $this->woodenWindowsEur;
    }

    /**
     * Set cityFiasId
     *
     * @param string $cityFiasId
     *
     * @return CommercialProperty
     */
    public function setCityFiasId($cityFiasId)
    {
        $this->cityFiasId = $cityFiasId;

        return $this;
    }

    /**
     * Get cityFiasId
     *
     * @return string
     */
    public function getCityFiasId()
    {
        return $this->cityFiasId;
    }

    /**
     * Set district
     *
     * @param int $district
     *
     * @return CommercialProperty
     */
    public function setDistrict($district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return int
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set streetFiasId
     *
     * @param string $streetFiasId
     *
     * @return CommercialProperty
     */
    public function setStreetFiasId($streetFiasId)
    {
        $this->streetFiasId = $streetFiasId;

        return $this;
    }

    /**
     * Get streetFiasId
     *
     * @return string
     */
    public function getStreetFiasId()
    {
        return $this->streetFiasId;
    }

    /**
     * Set houseFiasId
     *
     * @param string $houseFiasId
     *
     * @return CommercialProperty
     */
    public function setHouseFiasId($houseFiasId)
    {
        $this->houseFiasId = $houseFiasId;

        return $this;
    }

    /**
     * Get houseFiasId
     *
     * @return string
     */
    public function getHouseFiasId()
    {
        return $this->houseFiasId;
    }

    /**
     * Set block
     *
     * @param string $block
     *
     * @return CommercialProperty
     */
    public function setBlock($block)
    {
        $this->block = $block;

        return $this;
    }

    /**
     * Get block
     *
     * @return string
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * Set exclusive
     *
     * @param integer $exclusive
     *
     * @return CommercialProperty
     */
    public function setExclusive($exclusive)
    {
        $this->exclusive = $exclusive;

        return $this;
    }

    /**
     * Get exclusive
     *
     * @return integer
     */
    public function getExclusive()
    {
        return $this->exclusive;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return CommercialProperty
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return CommercialProperty
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set location
     *
     * @param integer $location
     *
     * @return CommercialProperty
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return integer
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set moderationStatus
     *
     * @param integer $moderationStatus
     *
     * @return CommercialProperty
     */
    public function setModerationStatus($moderationStatus)
    {
        $this->moderationStatus = $moderationStatus;

        return $this;
    }

    /**
     * Get moderationStatus
     *
     * @return integer
     */
    public function getModerationStatus()
    {
        return $this->moderationStatus;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return CommercialProperty
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set textOnTheSite
     *
     * @param string $textOnTheSite
     *
     * @return CommercialProperty
     */
    public function setTextOnTheSite($textOnTheSite)
    {
        $this->textOnTheSite = $textOnTheSite;

        return $this;
    }

    /**
     * Get textOnTheSite
     *
     * @return string
     */
    public function getTextOnTheSite()
    {
        return $this->textOnTheSite;
    }

    /**
     * Set openSale
     *
     * @param integer $openSale
     *
     * @return CommercialProperty
     */
    public function setOpenSale($openSale)
    {
        $this->openSale = $openSale;

        return $this;
    }

    /**
     * Get openSale
     *
     * @return integer
     */
    public function getOpenSale()
    {
        return $this->openSale;
    }

    /**
     * Set netSale
     *
     * @param integer $netSale
     *
     * @return CommercialProperty
     */
    public function setNetSale($netSale)
    {
        $this->netSale = $netSale;

        return $this;
    }

    /**
     * Get netSale
     *
     * @return integer
     */
    public function getNetSale()
    {
        return $this->netSale;
    }

    /**
     * Set moderationAt
     *
     * @param \DateTime $moderationAt
     *
     * @return CommercialProperty
     */
    public function setModerationAt($moderationAt)
    {
        $this->moderationAt = $moderationAt;

        return $this;
    }

    /**
     * Get moderationAt
     *
     * @return \DateTime
     */
    public function getModerationAt()
    {
        return $this->moderationAt;
    }

    /**
     * Set lastСalled
     *
     * @param \DateTime $lastСalled
     *
     * @return CommercialProperty
     */
    public function setLastСalled($lastСalled)
    {
        $this->lastСalled = $lastСalled;

        return $this;
    }

    /**
     * Get lastСalled
     *
     * @return \DateTime
     */
    public function getLastСalled()
    {
        return $this->lastСalled;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CommercialProperty
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return CommercialProperty
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set fromFirm
     *
     * @param integer $fromFirm
     *
     * @return CommercialProperty
     */
    public function setFromFirm($fromFirm)
    {
        $this->fromFirm = $fromFirm;

        return $this;
    }

    /**
     * Get fromFirm
     *
     * @return integer
     */
    public function getFromFirm()
    {
        return $this->fromFirm;
    }

    /**
     * Set source
     *
     * @param \AppBundle\Entity\Source $source
     *
     * @return CommercialProperty
     */
    public function setSource(\AppBundle\Entity\Source $source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return \AppBundle\Entity\Source
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set agent
     *
     * @param \AppBundle\Entity\User $agent
     *
     * @return CommercialProperty
     */
    public function setAgent(\AppBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \AppBundle\Entity\User
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Add contact
     *
     * @param \AppBundle\Entity\Client $contact
     *
     * @return CommercialProperty
     */
    public function addContact(\AppBundle\Entity\Client $contact)
    {
        $this->contacts[] = $contact;

        return $this;
    }

    /**
     * Remove contact
     *
     * @param \AppBundle\Entity\Client $contact
     */
    public function removeContact(\AppBundle\Entity\Client $contact)
    {
        $this->contacts->removeElement($contact);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Return source id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("source")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getSourceId()
    {
        if ($this->source) {
            return array('id' => $this->source->getId());
        }

        return null;
    }

    /**
     * Return agent id
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("agent")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array|null
     */
    public function getAgentId()
    {
        if ($this->agent) {
            return array('id' => $this->agent->getId());
        }

        return null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("contacts")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getContactsId()
    {
        $arr = array();

        if ($this->contacts) {
            foreach ($this->contacts as $contact) {
                $arr[] = array('id' => $contact->getId());
            }
        }

        return $arr;
    }

    /**
     * Get contract exclusive Ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("contracts_exclusive")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getContractsExclusiveId()
    {
        $arr = array();

        if ($this->contractsExclusive) {
            foreach ($this->contractsExclusive as $contractExclusive) {
                $arr[] = array('id' => $contractExclusive->getId());
            }
        }

        return $arr;
    }

    /**
     * Get contract exclusive Ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("contracts_view")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getContractsViewId()
    {
        $arr = array();

        if ($this->contractsView) {
            foreach ($this->contractsView as $contractView) {
                $arr[] = array('id' => $contractView->getId());
            }
        }

        return $arr;
    }

    /**
     * Get contract exclusive Ids
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("contracts_cooperation")
     * @JMS\Groups({"List", "Selected"})
     *
     * @return array
     */
    public function getContractsCooperationId()
    {
        $arr = array();

        if ($this->contractsCooperation) {
            foreach ($this->contractsCooperation as $contractCooperation) {
                $arr[] = array('id' => $contractCooperation->getId());
            }
        }

        return $arr;
    }
}
