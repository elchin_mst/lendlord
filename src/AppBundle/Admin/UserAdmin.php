<?php
// src/AppBundle/Admin/UserAdmin.php

namespace AppBundle\Admin;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class UserAdmin extends AbstractAdmin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('lastName')->add('firstName')->add('middleName')->add(
                'birthday'
            )->add('internalPhone')->add('mobilePhone')->add('homePhone')->add(
                'address'
            )->add('isUnitManager')->add('isMentor')->add('isHeadOffice')->add(
                'isHeadDepartment'
            )->add('username')->add('email')->add('password')->add('isActive')
            ->add('isFired')
            ->add('rolesCollection', EntityType::class, [
                'required' => false,
                'empty_data' => null,
                "multiple" => true,
                'placeholder' => 'Отсутсвует',
                'class' => 'AppBundle:Role',
                'choice_label' => 'role',

            ])
            ->add('unit')->add('offices')
            ->add('department')->add('position')->add('category')->add('unitManages')
            ->add('officeDepartmentManages')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('lastName');
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('lastName')->addIdentifier('firstName');
    }

    // Fields to be shown on show action
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('firstName')->add('lastName')->add('middleName');
    }
}